#!/bin/bash

#MINERVA_INSTANCE=http://localhost:8080/minerva/api
MINERVA_INSTANCE=https://minerva-dev.lcsb.uni.lu/minerva/api/

minerva_token=`curl -X POST -c - --data "login=anonymous&password=" ${MINERVA_INSTANCE}/doLogin | grep MINERVA_AUTH_TOKEN | awk '{print $7}'`


#rm -r ../out/*

for f in `find ../data/ -mindepth 2 -name "*.sbgn" `
do


    out_path_sbml=${f/data/sbml}.sbml
    out_dir_sbml=${out_path_sbml%/*}

    out_path_svg=${f/data/svg}.svg
    out_dir_svg=${out_path_svg%/*}


    if [ ! -d "$out_dir_sbml" ]; then
        mkdir -p "$out_dir_sbml"
    fi

    if [ ! -d "$out_dir_svg" ]; then
        mkdir -p "$out_dir_svg"
    fi

	curl -X POST --cookie "MINERVA_AUTH_TOKEN=${minerva_token}" --data-binary @${f} -H "Content-Type: text/plain;charset=UTF-8" ${MINERVA_INSTANCE}/convert/SBGN-ML:SBML > $out_path_sbml
	curl -X POST --cookie "MINERVA_AUTH_TOKEN=${minerva_token}" --data-binary @${f} -H "Content-Type: text/plain;charset=UTF-8" ${MINERVA_INSTANCE}/convert/image/SBGN-ML:svg > $out_path_svg
		
done    
