#!/usr/bin/env bash

TEST_ROOT=..
TALENT_UTILS=../../../talent/utils
PYTHON=python3

check_exit_code(){
    if [ $? -ne 0 ]; then
        tput setaf 1; echo "The last command failed. Can't continue..."
        exit
    fi
}

mkdir cache
for dir in `ls -d ${TEST_ROOT}/sbml/*`; do
    target=${dir##*/}
    target_path=${dir}/${target}.sbgn.sbml
    echo "target " $target

    out_dir=${TEST_ROOT}/pred/ins/${target}/
    echo "out_dir " $out_dir
     if [ ! -d "$out_dir" ]; then
        mkdir -p "$out_dir"
    fi
    rm  "$out_dir"/*

    for template_path in `ls -1 ${dir}/${target}_*`; do
        template=${template_path##*/}
        template=${template/.sbml/}
        echo "Laying out target " $target "against template " $template
        #echo ${PYTHON} ${TALENT_UTILS}/transfer.py -tgt ${target_path} -tmp ${template_path} -o ${out_dir}${target}---${template}--- -ddup-tmp false -ddup-tgt false -s settings_insert.json -cache cache
        # ${PYTHON} ${TALENT_UTILS}/transfer.py -tgt ${target_path} -tmp ${template_path} -o ${out_dir}/${target}---${template}--- -ddup-tmp false -ddup-tgt false -s settings_insert.json -cache cache
        echo "${PYTHON} ${TALENT_UTILS}/transfer.py -tgt ${target_path} -tmp ${template_path} -o ${out_dir} -ddup-tmp true -ddup-tgt true -s settings_insert.json -cache cache"
        ${PYTHON} ${TALENT_UTILS}/transfer.py -tgt ${target_path} -tmp ${template_path} -o ${out_dir} -ddup-tmp true -ddup-tgt true -s settings_insert.json -cache cache
        check_exit_code

    done

    for pkl in `ls -1 ${out_dir}/*.pkl`; do
        echo "Exporting " $pkl
        echo "${PYTHON} ${TALENT_UTILS}/export.py -i $pkl -f svg > ${pkl}.svg"       
        
        ${PYTHON} ${TALENT_UTILS}/export.py -i $pkl -f svg > ${pkl}.svg
        check_exit_code
    done

    echo "Reporting " $out_dir
    echo "python3 report_ins.py -i ${out_dir}"
    python3 report_ins.py -i ${out_dir}
    check_exit_code
done

python3 report_ins_merge.py -i ${TEST_ROOT}/pred/ins/
