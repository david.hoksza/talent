import argparse
import glob
import os
import re

def main(dir_name: str):

    with open("{}/report.html".format(dir_name), "w") as f:
        f.write("<html><head><style>"                
                ".item {width: 500px; float: left; padding-right: 100px} "
                ".group {clear: left}"
                "</style></head><body>")

        print('dir_name', dir_name)
        for report_path in glob.glob("{}/**/report.html".format(dir_name)):
            path = re.match(r'(.*)[/\\].*', report_path.replace(dir_name, "")).group(1)
            with open(report_path, "r") as report_file:
                report = report_file.read()
                content = re.match(r'.*<body>(.*)</body>.*', report).group(1)
                content = content.replace('src="', 'src="{}/'.format(path))
                f.write(content)

        f.write("</body></html>")
        f.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()


    parser.add_argument("-i", "--input",
                        required=True,
                        help="Path to directory with results")
    args = parser.parse_args()

    main(args.input)