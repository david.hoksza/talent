#!/usr/bin/env bash

TEST_ROOT=..
TALENT_UTILS=../../../talent/utils
PYTHON=python3

check_exit_code(){
    if [ $? -ne 0 ]; then
        tput setaf 1; echo "The last command failed. Can't continue..."
        exit
    fi
}

mkdir cache
for dir in `ls -d ${TEST_ROOT}/sbml/*`; do
    template=${dir##*/}
    template_path=${dir}/${template}.sbgn.sbml

    out_dir=${TEST_ROOT}/pred/del/${template}/
    if [ ! -d "$out_dir" ]; then
        mkdir -p "$out_dir"
    fi
    rm  "$out_dir"/*
    

    for target_path in `ls -1 ${dir}/${template}_*`; do
        target=${target_path##*/}
        echo "Laying out target " $target "against template " $template
        echo "${PYTHON} ${TALENT_UTILS}/transfer.py -tgt ${target_path} -tmp ${template_path} -o ${out_dir} -ddup-tmp true -ddup-tgt true -s settings.json -cache cache"
        ${PYTHON} ${TALENT_UTILS}/transfer.py -tgt ${target_path} -tmp ${template_path} -o ${out_dir} -ddup-tmp true -ddup-tgt true -s settings.json -cache cache
        check_exit_code
    done

    for pkl in `ls -1 ${out_dir}/*.pkl`; do
        echo "Exporting " $pkl
        echo "${PYTHON} ${TALENT_UTILS}/export.py -i $pkl -f svg > ${pkl}.svg"
        ${PYTHON} ${TALENT_UTILS}/export.py -i $pkl -f svg > ${pkl}.svg
        check_exit_code
    done
done
