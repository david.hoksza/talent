import argparse
import glob
import os
import re

def main(dir_name: str):
    tgt_name = re.match(r'.*[\\/](.*)[\\/].*', dir_name).group(1)
    tmptgt_path = "../../../svg/{}".format(tgt_name)
    tgt_orig_path = "{}/{}.sbgn.svg".format(tmptgt_path, tgt_name)

    with open("{}/report.html".format(dir_name), "w") as f:
        f.write("<html><head><style>"                
                ".item {width: 500px; float: left; padding-right: 100px} "
                ".group {clear: left}"
                "</style></head><body>")

        for f_path in glob.glob("{}/*.svg".format(dir_name)):
            f_name = os.path.basename(f_path)

            tmp_name = re.match(r'.*--from--(.*)-tgt.*', f_name).group(1)
            tmp_path = "{}/{}.svg".format(tmptgt_path, tmp_name)
            f.write('<div class="group">'
                        '<div class="group-header">{}</div>'                    
                        '<div class="item tgt-pred"><img src="{}"></div>'
                        '<div class="item tgt-orig"><img src="{}"></div>'
                        '<div class="item tmp"><img src="{}"></div>'
                    '</div>'.format(tmp_name, f_name, tgt_orig_path, tmp_path))

        f.write("</body></html>")
        f.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()


    parser.add_argument("-i", "--input",
                        required=True,
                        help="Path to directory with results")
    args = parser.parse_args()

    main(args.input)