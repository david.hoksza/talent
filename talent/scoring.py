import networkx as nx
import numpy as np
from collections import Counter
import logging

from . import graph_utils as gu

from sged.cost import Costs

# def node_dist(n1, n2):
#     #nodes are reactions so it should take into account
#     #type of reaction (especially association or dissociation might be generally different from state transition?
#     if n1 and not n2:
#         return 4
#     if not n1 and n2:
#         return 4
#     else:
#         return 0
#
# def edge_dist(e1, e2):
#     return 4


def extract_neighbors(g, g_orig):
    ns = {}
    for n in g.nodes():
        ns[n] = list(nx.all_neighbors(g_orig, n))
    return ns

def dist_neighbors(n1, nbs1, g1, n2, nbs2, g2):

    def neighborhood_desc(g, node, nbs):
        ids = []
        for nb in nbs:
            sbo_term = str(gu.get_species(g, nb).get_type())
            for role in g[node][nb]:
                ids.append(role.name+sbo_term)
        return ids

    c1 = Counter(neighborhood_desc(g1, n1, nbs1))
    c2 = Counter(neighborhood_desc(g2, n2, nbs2))

    # c1 = Counter([g1[n1][n]["role"] + str(g1.nodes[n]['data'].getSboTerm()) for n in nbs1])
    # c2 = Counter([g2[n2][n]["role"] + str(g2.nodes[n]['data'].getSboTerm()) for n in nbs2])
    # c1 = Counter([str(g1.nodes[n]["sboTerm"]) for n in nbs1])
    # c2 = Counter([str(g2.nodes[n]["sboTerm"]) for n in nbs2])

    c1.subtract(c2)

    # d = sum([(abs(x)) for x in c1.values()])
    # sbo_diff = sum([(max(x, 0)) for x in c1.values()]) #if n1 is subset of n2 then distance = 0
    # d = 1 - 1 / (sbo_diff + 0.5) if sbo_diff > 0 else 0
    sbo_diff = sum([abs(x) for x in c1.values()])  # if n1 is subset of n2 then distance = 0
    d = 1 - 1 / (sbo_diff + 0.5) if sbo_diff > 0 else 0 #sbo_diff is integeter and thus second smallest value is 1 => 1/x < 1



    # cmps = []
    # for nbs in [nbs1, nbs2]:
    #     cmps.append(set([n['compartmentId'] for n in nbs if 'compartmentId' in n]))
    #
    # cmp_diff = abs(len(cmps[0])-len(cmps[1]))%
    # d += 1 - 1 / (cmp_diff + 0.5) if cmp_diff > 0 else 0
    #
    # d /= 2

    return d
    # return 0 if sbo_diff == 0 else 1


def get_sim_matrix(g1, g1_orig, g2, g2_orig) -> np.ndarray:
    neighbors = [extract_neighbors(g1, g1_orig), extract_neighbors(g2, g2_orig)]

    nodes1 = list(g1.nodes())
    nodes2 = list(g2.nodes())

    matrix = np.empty([len(nodes1), len(nodes2)])
    for i1 in range(len(nodes1)):
        ns1 = list(neighbors[0][nodes1[i1]])
        for i2 in range(len(nodes2)):
            ns2 = list(neighbors[1][nodes2[i2]])
            matrix[i1,i2] = dist_neighbors(nodes1[i1], ns1, g1_orig, nodes2[i2], ns2, g2_orig)

    return matrix


class LayoutNodeCosts(Costs):

    # def __init__(self, g1, g1_orig, g2, g2_orig):
    def __init__(self, g1, g2):
        self.node_dict = [{}, {}]
        for [g, node_dict] in [[g1.get_reaction_graph(), self.node_dict[0]], [g2.get_reaction_graph(), self.node_dict[1]]]:
            i = 0
            for n in g.nodes():
                node_dict[n] = i
                i += 1

        self.sim_matrix = get_sim_matrix(g1.get_reaction_graph(), g1.get_original(),
                                         g2.get_reaction_graph(), g2.get_original())

        self.mean_update_cost = np.mean(self.sim_matrix)

    def update(self, operation):
        return self.sim_matrix[self.node_dict[0][operation[0]], self.node_dict[1][operation[1]]]

    def insert(self, operation):
        return 5 * self.mean_update_cost
        # return 3

    def delete(self, operation):
        return 5 * self.mean_update_cost
        # return 3

class LayoutEdgeCosts(Costs):

    # def __init__(self, g1, g1_orig, g2, g2_orig):
    def __init__(self, g1, g2):

        self.sim_matrix = get_sim_matrix(g1.get_reaction_graph(), g1.get_original(),
                                         g2.get_reaction_graph(), g2.get_original())

        self.mean_update_cost = np.mean(self.sim_matrix)

    def insert(self, operation):
        # return self.mean_update_cost
        return 1

    def update(self, operation):
        return 1

    def delete(self, operation):
        # return self.mean_update_cost
        return 1
