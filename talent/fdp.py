import networkx as nx
import math
from statistics import median
import copy
import numpy as np
from scipy import stats
from random import random
from typing import Dict, List, NamedTuple
from sortedcontainers import SortedList
from enum import Enum


from talent import graph_utils as gu
from talent import bioentity as be
from talent import graphics as gr
from talent import settings

class Configuration:
    def __init__(self, bbs:Dict[str, gr.Rectangle]=[], fixed: List[str]=[]):
        self.bbs: Dict[str, gr.Rectangle] = bbs
        self.fixed: List[str] = fixed

    def __copy__(self):
        return Configuration(bbs=copy.deepcopy(self.bbs), fixed=self.fixed)

    def generate_new(self, shift_radius: float):
        conf_new = copy.copy(self)
        for rect in [v for k,v in conf_new.bbs.items() if k not in self.fixed]:
            shift = [(random() * 2 - 1) * shift_radius, (random() * 2 - 1) * shift_radius]
            rect.shift(shift)
        return conf_new


class EnergyEvaluator:

    def __init__(self, g:nx.MultiGraph, r_id: str, conf:Configuration):

        # the energy function should have the following components:
        # - repulsive component - forcing the layout
        # - edge length component - forcing optimal lengths
        # - crossing component - minimize the number of crossings
        # - orthogonal component - forcing horizontal and vertical positions for main reactant and products if these are ODS

        self.components = {
            "repulsive": {
                "weight": 1,
                "func": self.__repulsive_energy
            },
            "edge_length": {
                "weight": 1,
                "func": self.__edge_length_energy
            }
        }

        self.rs: gu.ReactionSpecies = gu.get_reaction_species(g, r_id)
        rl: be.ReactionLayout = gu.get_predicted_layout(g, r_id)
        self.mr_id = rl.get_main_reactant_id()
        self.mp_id = rl.get_main_product_id()
        self.mr_pos = rl.get_main_reactant_pos()
        self.mp_pos = rl.get_main_product_pos()
        self.r_cp = rl.get_reactants_connection_pos()
        self.p_cp = rl.get_products_connection_pos()
        self.r_c = rl.get_center()

        r_size = gr.utils.size(self.mr_pos - self.mp_pos)
        self.max_radius = max(r_size, settings.render.optimal_species_reaction_dist*2)

        for name, comp in self.components.items():
            energies = [comp["func"](conf.generate_new(self.max_radius)) for i in range(100)]
            med = median(energies)
            comp["weight"] = 1 / med / len(self.components) #each component has such a weight that the overall median energy equals one

        self.e_median = 1


    def evaluate_energy(self, conf: Configuration) -> float:

        energy = 0

        if len(conf.bbs) <= 1:
            return energy

        es = [c["func"](conf) for c in self.components.values()]
        print(es)
        return sum(es)

    def __repulsive_energy(self, conf:Configuration):
        # TODO: Maybe only consider distances between reactants, products and modifiers
        rects = list(conf.bbs.values())
        energy_repulsive = 0
        for i1 in range(len(rects)):
            r1 = rects[i1]
            for i2 in range(i1 + 1, len(rects)):
                r2 = rects[i2]
                energy_repulsive += r1.distance_rect(r2)
        return self.components["repulsive"]["weight"] / energy_repulsive

    def __edge_length_energy(self, conf:Configuration):
        # Edge lengths component
        energy_edge_lengths = 0
        for s_ids, pos in (self.rs.reactantIds, self.mr_pos), (self.rs.productIds, self.mp_pos), (self.rs.modifierIds, self.r_c):
            for s_id in s_ids:
                dist = gr.utils.dist(conf.bbs[s_id].get_center(), pos)
                energy_edge_lengths += dist * dist * dist
        return self.components["edge_length"]["weight"] * energy_edge_lengths


def davidson_harel(g: nx.MultiGraph, r_id: str) -> Configuration:

    #TODO: optimize structures - used tuple instead of rectangle and dict.copy instead of deep copy
    cnt_iterations = 500

    s_ids = list(g[r_id])
    ods_ids = [id for id in g[r_id] if len(g[id]) == 1]

    if len(ods_ids) <= 1:
        assert False

    bbs = {id: gu.get_predicted_layout(g, id).get_bb().get() for id in s_ids}
    rl = gu.get_predicted_layout(g, r_id)
    fixed = set(s_ids) - set(ods_ids)
    fixed.add(rl.get_main_reactant_id())
    fixed.add(rl.get_main_product_id())
    conf = Configuration(bbs=bbs, fixed=list(fixed))

    ee = EnergyEvaluator(g, r_id, conf)

    temperature = ee.e_median
    temperature_step = temperature / cnt_iterations
    factor = 1 / 2 * math.exp(1) # at the beginning we will have 50% to accept solutions which are median of the sampled solutions

    max_radius = ee.max_radius
    radius_step = max_radius / cnt_iterations
    radius = max_radius

    e = ee.evaluate_energy(conf)
    solutions = SortedList(key=lambda x: x["energy"])
    solutions.add({"energy": e, "solution": conf})

    i = 0
    while temperature > 0:
        i +=1
        conf_new = conf.generate_new(radius)
        # radius -= radius_step
        e_new = ee.evaluate_energy(conf=conf_new)
        solutions.add({"energy": e, "solution": conf})

        r = random()
        # print(r, math.exp((e-e_new)/temperature), e, e_new)
        if e_new < e or math.exp((e-e_new)/temperature) > r:
            if (e_new < e):
                print("better solution in step", i)
            else:
                print("accepted worse")
            e = e_new
            conf = conf_new
            solutions.add({"energy": e, "solution": conf})

        temperature -= temperature_step

    print(solutions[0]["energy"])
    return solutions[0]["solution"]


class ROLE(Enum):
    REACTANT = 0
    MODIFIER = 1
    PRODUCT = 2


class Force:

    def __init__(self, g: nx.MultiGraph, r_id: str, s_ids: List[str], bbs: Dict[str, gr.Rectangle]):
        self.s_ids = s_ids
        self.bbs = bbs
        self.rs: gu.ReactionSpecies = gu.get_reaction_species(g, r_id)
        rl: be.ReactionLayout = gu.get_predicted_layout(g, r_id)
        self.mr_id = rl.get_main_reactant_id()
        self.mp_id = rl.get_main_product_id()
        self.mr_pos = rl.get_main_reactant_pos()
        self.mp_pos = rl.get_main_product_pos()
        self.r_cp = rl.get_reactants_connection_pos()
        self.p_cp = rl.get_products_connection_pos()
        self.r_c = rl.get_center()


        self.attractors = {
            ROLE.REACTANT: [self.r_cp, self.mr_pos],
            ROLE.MODIFIER: [self.r_c],
            ROLE.PRODUCT: [self.p_cp, self.mp_pos]
        }

        self.repulsors = {
            ROLE.REACTANT: [self.r_cp],
            ROLE.MODIFIER: [],
            ROLE.PRODUCT: [self.p_cp]
        }

        self.roles:Dict[str, ROLE] = {}
        for ids, role in (self.rs.reactantIds, ROLE.REACTANT), (self.rs.modifierIds, ROLE.MODIFIER), (self.rs.productIds, ROLE.PRODUCT):
            for id in ids:
                self.roles[id] = role

    def repulsive(self) -> Dict[str, np.ndarray]:

        displacement:Dict[str, np.ndarray] = {s_id:np.array([0.0,0.0]) for s_id in self.bbs}

        dist_min = 20

        # we iterate overl all s_ids, because we need to account for repulsion for nodes which are fixed,
        # however, these nodes are not shifted, so we do not note displacement for them
        all_ids = sorted(list(self.bbs))
        for i1, s_id1 in enumerate(all_ids):
            bb1 = self.bbs[s_id1]
            for i2 in range(i1+1, len(all_ids)):
                s_id2 = all_ids[i2]
                bb2 = self.bbs[s_id2]
                delta = bb2.distance_rect(bb1, two_d=True)
                if delta[0] == 0 and delta[1] == 0:
                    delta = np.array([random()*0.1, random()*0.1])
                delta_size = gr.utils.size(delta)
                # if s_id2 == self.mr_id or s_id2 == self.mp_id:
                #     delta_size /=4
                disp = (dist_min / delta_size) * gr.utils.normalize(delta)
                if s_id1 in self.s_ids:
                    displacement[s_id1] += disp
                if s_id2 in self.s_ids:
                    displacement[s_id2] -= disp

            for pos_attractor in self.repulsors[self.roles[s_id1]]:
                delta = self.bbs[s_id1].distance_point(pos_attractor, two_d=True)
                if delta[0] == 0 and delta[1] == 0:
                    delta = np.array([random()*0.1, random()*0.1])
                delta_size = gr.utils.size(delta)
                displacement[s_id1] -= (dist_min / delta_size) * gr.utils.normalize(delta)

        return displacement


    def attractive(self) -> Dict[str, np.ndarray]:

        dist_opt = settings.render.optimal_species_reaction_dist / 2

        displacement: Dict[str, np.ndarray] = {s_id: np.array([0.0, 0.0]) for s_id in self.bbs}

        for s_id in self.s_ids:
            for pos_attractor in self.attractors[self.roles[s_id]]:
                delta = self.bbs[s_id].distance_point(pos_attractor, two_d=True)
                displacement[s_id] += gr.utils.normalize(delta) * (gr.utils.size(delta) - dist_opt) / dist_opt # we deduct dist_opt because we don't wont the nodes to get to the attractors
        return displacement


def springs(g: nx.MultiGraph, r_id: str) -> Dict[str, np.ndarray]:

    #TODO: optimize structures - used tuple instead of rectangle and dict.copy instead of deep copy
    cnt_iterations = 500

    s_ids = list(g[r_id])
    ods_ids = [id for id in g[r_id] if len(g[id]) == 1]

    if len(ods_ids) == 0:
        return {}

    bbs = {id: gu.get_predicted_layout(g, id).get_bb().get() for id in s_ids}
    rl = gu.get_predicted_layout(g, r_id)
    fixed_ids = set(s_ids) - set(ods_ids)
    fixed_ids.add(rl.get_main_reactant_id())
    fixed_ids.add(rl.get_main_product_id())

    position_ids = sorted(list(set(s_ids) - fixed_ids))

    rl: be.ReactionLayout = gu.get_predicted_layout(g, r_id)
    time = max(gr.utils.size(rl.get_main_reactant_pos() - rl.get_main_product_pos()), 2*settings.render.optimal_species_reaction_dist)

    time_step = time / cnt_iterations

    force = Force(g, r_id, position_ids, bbs)


    for i in range(cnt_iterations):

        disp: Dict[str, np.ndarray] = {}
        disp_repulsive = force.repulsive()
        disp_attractive = force.attractive()
        for s_id in position_ids:
            # print(time, s_id, disp_repulsive[s_id], disp_attractive[s_id])
            disp[s_id] = disp_attractive[s_id] + disp_repulsive[s_id]

        for s_id in position_ids:
            # shift = gr.utils.normalize(disp[s_id]) * min(gr.utils.size(disp[s_id]), time)
            shift = disp[s_id]
            bbs[s_id].shift(shift)

        time -= time_step

    return {id: bb.get_center() for id, bb in bbs.items()}













