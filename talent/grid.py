import numpy as np

from typing import List


class Grid:
    def __init__(self, x_coords: List, y_coords: List):
        self.xs = x_coords
        self.ys = y_coords
        self.x_spacing = x_coords[1] - x_coords[0] if len(x_coords) > 1 else 0
        self.y_spacing = y_coords[1] - y_coords[0] if len(y_coords) > 1 else 0

    def size(self) -> int:
        return len(self.xs) * len(self.ys)

    def get_coords(self, i: int, j: int) -> np.ndarray:
        assert i < len(self.xs) and j < len(self.ys)
        return np.array([self.xs[i], self.ys[j]])

    def get_closest_grid_lines(self, point: np.array):
        """
        Get indexes closest x and y grid lines
        :param point:
        :return:
        """

        # Start with 0th x and y line and then go from 1st line and check whether it is closer
        closest = [0, 0]

        closest_dist = abs(point[0]-self.xs[0])
        for i in range(1, len(self.xs)):
            dist = abs(point[0] - self.xs[i])
            if dist < closest_dist:
                closest_dist = dist
                closest[0] = i

        closest_dist = abs(point[1] - self.ys[0])
        for i in range(1, len(self.ys)):
            dist = abs(point[1] - self.ys[i])
            if dist < closest_dist:
                closest_dist = dist
                closest[1] = i

        return closest

    def get_closest_coords(self, point: np.array):

        closest = [self.xs[0], self.ys[0]]

        closest_dist = abs(point[0]-self.xs[0])
        for i in range(1, len(self.xs)):
            dist = abs(point[0] - self.xs[i])
            if dist < closest_dist:
                closest_dist = dist
                closest[0] = self.xs[i]

        closest_dist = abs(point[1] - self.ys[0])
        for i in range(1, len(self.ys)):
            dist = abs(point[1] - self.ys[i])
            if dist < closest_dist:
                closest_dist = dist
                closest[1] = self.ys[i]

        return closest

    def get_grid_lines_points_counts(self, points):
        lines_nodes_x = [0 for i in range(len(self.xs))]
        lines_nodes_y = [0 for i in range(len(self.xs))]
        for p in points:
            x = p[0]
            for i in range(len(self.xs)):
                if x - self.xs[i] == 0:
                    lines_nodes_x[i] += 1
                    break
            for i in range(len(self.ys)):
                if x - self.ys[i] == 0:
                    lines_nodes_y[i] += 1
                    break

        return [lines_nodes_x, lines_nodes_y]