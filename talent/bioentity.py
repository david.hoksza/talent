import numpy as np
import networkx as nx
from enum import Enum

import sys
import copy

from abc import abstractmethod

from typing import List, Dict, Tuple


class LAYOUT_TYPE(Enum):
    ORIGINAL = 0
    PREDICTED = 1


from . import layout as lt
from . import graphics as gr
from . import settings
from .routing import RoutingGraph

from . import graph_utils as gu


class TYPE(Enum):
    REACTION = 0
    SPECIES = 1


class SPECIES_ROLE(Enum):
    # Based on the SBO - https://www.ebi.ac.uk/sbo/main/display?sboId=SBO:0000003
    REACTANT = 0
    INTERACTOR = 1
    SUBSTRATE = 2
    SIDESUBSTRATE = 3


    MODIFIER = 4
    DUAL_ACTIVITY_MODIFIER = 5
    INHIBITOR = 6
    ACTIVATOR = 7
    MODIFIER_OF_UNKNOWN_ACTIVITY = 8
    STIMULATOR = 9

    PRODUCT = 10
    SIDEPRODUCT = 11

    @staticmethod
    def FROM_SBO_STRING(sbo:str)->'SPECIES_ROLE':
        return SPECIES_ROLE[sbo.upper().replace(' ', '_').replace('-', '_')]

    def is_reactant(self):
        return self.value <= 3

    def is_main_reactant(self):
        # TODO: not sure whether there can't be multiple reactants with reactant or substrater role thus not being able to determine which of the species is the main one
        return self == SPECIES_ROLE.MAIN_REACTANT()

    def is_modifier(self):
        return self.value >= 4 and self.value <= 9

    def is_product(self):
        return self.value >= 10

    def is_main_product(self):
        return self == SPECIES_ROLE.PRODUCT

    @staticmethod
    def MAIN_PRODUCT():
        return SPECIES_ROLE.PRODUCT

    @staticmethod
    def MAIN_REACTANT():
        return SPECIES_ROLE.SUBSTRATE

    @staticmethod
    def CONTAINS_REACTANT_ROLE(roles: List['SPECIES_ROLE'])->bool:
        for role in roles:
            if role.is_reactant():
                return True
        return False

    @staticmethod
    def CONTAINS_MAIN_REACTANT_ROLE(roles: List['SPECIES_ROLE']) -> bool:
        for role in roles:
            if role.is_main_reactant():
                return True
        return False

    @staticmethod
    def CONTAINS_PRODUCT_ROLE(roles: List['SPECIES_ROLE']) -> bool:
        for role in roles:
            if role.is_product():
                return True
        return False

    @staticmethod
    def CONTAINS_REACTANT_PRODUCT_ROLE(roles: List['SPECIES_ROLE']) -> bool:
        return SPECIES_ROLE.CONTAINS_REACTANT_ROLE(roles) or SPECIES_ROLE.CONTAINS_PRODUCT_ROLE(roles)

    @staticmethod
    def CONTAINS_MAIN_PRODUCT_ROLE(roles: List['SPECIES_ROLE']) -> bool:
        for role in roles:
            if role.is_main_product():
                return True
        return False


    @staticmethod
    def CONTAINS_MAIN_ROLE(roles: List['SPECIES_ROLE']) -> bool:
        return SPECIES_ROLE.CONTAINS_MAIN_REACTANT_ROLE(roles) or SPECIES_ROLE.CONTAINS_MAIN_PRODUCT_ROLE(roles)


    @staticmethod
    def CONTAINS_MODIFIER_ROLE(roles: List['SPECIES_ROLE']) -> bool:
        for role in roles:
            if role.is_modifier():
                return True
        return False


class Visual:
    def __init__(self, size=None, color=None):
        self._color = color
        self._size = size
        
    def get_color(self):
        return self._color

    def set_color(self, color):
        self._color = color
        
    def get_size(self):
        return self._size

    def set_size(self, size):
        self._size = size


def convert_to_np(point) -> np.ndarray:
    if point is not None and not isinstance(point, np.ndarray):
        return np.array(point)
    else:
        return point


class BioEntityLayout:
    def __init__(self, bioentity: 'BioEntity', center: np.ndarray or None, copied: bool or None):

        self._center: np.ndarray = convert_to_np(center)
        self._bioentity: BioEntity = bioentity
        self._visual: Visual = Visual()
        self._copied: bool = copied # Whether the layout was copied from target to template

    def __repr__(self):
        return "center: {}".format(repr(self._center))

    # def __deepcopy__(self, memo={}):
    #     cls = self.__class__
    #     result = cls.__new__(cls)
    #     memo[id(self)] = result
    #
    #     setattr(result, '_center', copy.deepcopy(self._center))
    #     setattr(result, '_visual', copy.deepcopy(self._visual))
    #     setattr(result, '_copied', copy.deepcopy(self._copied))
    #     setattr(result, '_bioentity', self._bioentity)
    #     return result

    # def __deepcopy__(self, memo):
    #     cls = self.__class__
    #     result = cls.__new__(cls)
    #     memo[id(self)] = result
    #     for k, v in self.__dict__.items():
    #         if k == '_bioentity':
    #             setattr(result, k, self.__dict__[k])
    #         else:
    #             setattr(result, k, copy.deepcopy(v, memo))
    #     return result

    def is_initialized(self):
        return self.get_center() is not None

    def get_center(self) -> np.ndarray:
        return self._center

    def set_center(self, center: np.ndarray):

        self._center = center

    def get_bioentity(self):
        return self._bioentity

    def set_bioentity(self, bioentity: 'BioEntity'):
        self._bioentity = bioentity

    def get_visual(self):
        return self._visual

    def set_visual(self, visual):
        self._visual = visual

    def get_layout_type(self) -> LAYOUT_TYPE:
        for layout_type in LAYOUT_TYPE:
            if self.get_bioentity().get_layout(layout_type) == self:
                return layout_type

        raise Exception('Unknown layout type')

    def is_copied(self) -> bool:
        return self._copied

    def set_copied(self, copied: bool):
        self._copied = copied

    @abstractmethod
    def get_min_max(self) -> List[np.ndarray]:
        pass

    @abstractmethod
    def shift(self, vect: np.ndarray):
        pass


class BoundingBox():

    def __init__(self, center: np.ndarray, width, height):
        self._bb: gr.Rectangle = None
        self._ports: List[np.ndarray] = []
        self.__width = width
        self.__height = height

        self.update(center)

    def __repr__(self):
        return repr(self._bb)

    def get(self) -> gr.Rectangle:
        return self._bb

    def update(self, center):

        w_half = self.__width / 2
        h_half = self.__height / 2
        self._bb = gr.Rectangle(center[0] - w_half, center[1] - h_half, center[0] + w_half, center[1] + h_half)

        self._ports = []
        self.get_ports()

    def get_ports(self) -> List[np.ndarray]:

        if len(self._ports) == 0:
            bb = self.get()
            for line in [bb.get_line_top(), bb.get_line_right(), bb.get_line_bottom(), bb.get_line_left()]:
                self._ports.append(line[0] + (line[1] - line[0]) / 2)

        return self._ports

    def shift(self, vect: np.ndarray):
        self._bb.shift(vect)
        for i in range(len(self._ports)):
            self._ports[i] = self._ports[i] + vect


class SpeciesLayout(BioEntityLayout):

    def __init__(self, bioentity: 'BioEntity', center: np.ndarray, bb_width: float, bb_height: float, copied: bool):

        self._center: np.ndarray = None
        self._bb: BoundingBox = None
        self.__width = settings.render.species_glyph_width #bb_width
        self.__height = settings.render.species_glyph_height #bb_height

        super().__init__(bioentity, center, copied)

        if center is not None:
            self.set_center(center)

    def __repr__(self):
        return "center: {}, bb: {}".format(repr(self._center), repr(self._bb))

    def shift(self, vect: np.ndarray):
        if self._center is not None:
            self._center = self._center + vect
        if self._bb is not None:
            self._bb.shift(vect)


    def get_min_max(self) -> List[np.ndarray]:
        bb:gr.Rectangle = self.get_bb().get()
        return [bb.get_lt(), bb.get_rb()]

    def set_center(self, center: np.ndarray, recompute_reaction_layout: bool = True ):

        self._center = np.array(center)
        if self._bb is not None:
            self._bb.update(center)
        else:
            self._bb = BoundingBox(center, width=self.__width, height=self.__height)

        if not recompute_reaction_layout:
            return

        # we need to update layout of all the reactions connected to the species with updated positoin
        e = self.get_bioentity()
        if e is not None:
            g = e.get_graph()

            for r_id in g[e.get_id()]:
                l:ReactionLayout = gu.get_node_data(g.nodes[r_id]).get_layout(self.get_layout_type())
                l.recompute_layout()

    def get_bb(self) -> BoundingBox:
        return self._bb

    def cnt_intersections(self, masked_r_ids: List[str] = []):
        be = self.get_bioentity()
        s_id = be.get_id()
        g = be.get_graph()

        cnt = 0
        for r_id in g[s_id]:
            rl: ReactionLayout = gu.get_layout(g, r_id, layout_type=self.get_layout_type())
            for role in gu.get_roles(g, r_id, s_id):
                srl:SpeciesRolePoints = rl.get_sr_points(s_id, role)
                cnt += srl.cnt_intersections(g, masked_r_ids=masked_r_ids)





class SpeciesRolePoints:
    """
    Stores information about a list of points connecting species to its connection point
    in the reaction (either center or reactant/products connection point) or points from
    rectants/products connection point to the center (then the species id and role is None).
    The list of points includes the species port point and the end reaction point.
    The list of points always goes always towards the center of the reaction.
    """
    def __init__(self, s_id: str or None, role: SPECIES_ROLE or None, points: List[np.ndarray]):
        self.s_id: str = s_id
        self.role = role
        self.points = points

    @property
    def s_id(self) -> str:
        return self.__s_id

    @s_id.setter
    def s_id(self, s_id: str):
        self.__s_id = s_id

    @property
    def role(self) -> SPECIES_ROLE:
        return self.__role

    @role.setter
    def role(self, role: SPECIES_ROLE):
        self.__role = role

    @property
    def points(self) -> List[np.ndarray]:
        return self.__points

    @points.setter
    def points(self, points: List[np.ndarray]):
        self.__points = copy.deepcopy(points)

    def shift(self, vector: np.ndarray):
        for i in range(len(self.points)):
            self.points[i] = self.points[i] + vector

    def update_species_point(self, point: np.ndarray):
        self.__points[0] = point

    def update_connection_point(self, point: np.ndarray):
        self.__points[len(self.__points)-1] = point

    def cnt_intersections(self, g: nx.MultiGraph, masked_r_ids: List[str] = []):
        for line in points_to_lines(self.points):
            return gu.count_intersections(g, line, masked_r_ids=masked_r_ids)


def points_to_lines(points: List[np.ndarray]) -> List[gr.Line]:
    lines: List[gr.Line] = []
    for i in range(1, len(points)):
        lines.append(gr.Line(points[i - 1], points[i], allow_zero_length=True))
    return lines


class ReactionLayout(BioEntityLayout):

    def __init__(self, bioentity: 'BioEntity', center: np.ndarray, copied: bool):
        super().__init__(bioentity, center, copied)

        self._reactants_connection_pos = np.array(self._center)
        self._products_connection_pos = np.array(self._center)

        self._main_reactant_pos = None
        self._main_product_pos = None

        self._main_reactant_id = None
        self._main_product_id = None
        self._side_reactants_ids = []
        self._side_products_ids = []
        self._modifiers_ids = []

        self._reactant_sr_points:List[SpeciesRolePoints] = []
        self._modifier_sr_points: List[SpeciesRolePoints] = []
        self._product_sr_points: List[SpeciesRolePoints] = []

        #TODO these are redundant with SpeciesRolePoints and center of reaction
        self._reactants_center_points: List[np.ndarray] = [] #curve connecting reactants connection point with the reaction center
        self._products_center_points: List[np.ndarray] = [] #curve connecting products connection point with the reaction center

    def set_center(self, center: np.ndarray):
        c = np.array(center)
        self._center = c
        if len(self._products_center_points) > 0:
            self._products_center_points[-1] = c
        if len(self._reactants_center_points) > 0:
            self._reactants_center_points[-1] = c

    def treat_overlapping_ends(self):
        """
        When exporting a graph to CellDesigner, there can't be reaciton lines which start and end in the same point, e.g.
        when a reactant and a product share position and the connection point.
        :return:
        """

        points = []
        for srp in self._reactant_sr_points + self._modifier_sr_points + self._product_sr_points:
            point = [srp.points[0][0], srp.points[0][1]]
            while point in points:
                point[0] += 1
            points.append(point)
            srp.points[0] = np.array(point)

    def shift(self, vector: np.ndarray):

        def update_with_copy(arr:List[np.ndarray]):
            for i in range(len(arr)):
                arr[i] = arr[i] + vector

        self._center = self._center + vector
        self._reactants_connection_pos = self._reactants_connection_pos + vector
        self._products_connection_pos = self._products_connection_pos + vector
        self._main_reactant_pos = self._main_reactant_pos + vector
        self._main_product_pos = self._main_product_pos + vector

        update_with_copy(self._reactants_center_points)
        update_with_copy(self._products_center_points)

        for srp in self._reactant_sr_points + self._modifier_sr_points + self._product_sr_points:
            srp.shift(vector)

    def get_min_max(self) -> List[np.ndarray]:
        min_x = sys.maxsize
        min_y = sys.maxsize
        max_x = -sys.maxsize
        max_y = -sys.maxsize

        for line in self.get_all_lines():
            for i in [0, 1]:
                p = line[i]
                if p[0] < min_x: min_x = p[0]
                if p[0] > max_x: max_x = p[0]
                if p[1] < min_y: min_y = p[1]
                if p[1] > max_y: max_y = p[1]
        return [np.array([min_x, min_y]), np.array([max_x, max_y])]

    def get_species_ids(self):
        return [self.get_main_reactant_id()] + [self.get_main_product_id()] \
               + self.get_side_products_ids() + self.get_side_reactants_ids() + self.get_modifiers_ids()

    def get_main_line(self) -> np.ndarray:
        return gr.utils.normalize(self.get_main_product_pos() - self.get_main_reactant_pos())

    def get_main_reactant_pos(self) -> np.ndarray:
        return self._main_reactant_pos

    def get_main_product_pos(self):
        return self._main_product_pos
    
    def set_reactants_connection_pos(self, point: np. ndarray) -> np.ndarray:
        self._reactants_connection_pos = np.array(point)
        if len(self._reactants_center_points) > 0:
            self._reactants_center_points[0] = np.array(point)

    def get_reactants_connection_pos(self) -> np.ndarray:
        return self._reactants_connection_pos
    
    def set_products_connection_pos(self, point: np. ndarray):
        self._products_connection_pos = np.array(point)
        if len(self._products_center_points) > 0:
            self._products_center_points[0] = np.array(point)

    def get_products_connection_pos(self) -> np.ndarray:
        return self._products_connection_pos

    def get_main_reactant_id(self):
        return self._main_reactant_id
    
    def set_main_reactant_id(self, main_reactant_id):
        self._main_reactant_id = main_reactant_id

    def get_main_product_id(self):
        return self._main_product_id
    
    def set_main_product_id(self, main_product_id):
        self._main_product_id = main_product_id

    def get_side_reactants_ids(self):
        return self._side_reactants_ids

    def get_reactants_ids(self) -> List[str]:
        return [self.get_main_reactant_id()] + self.get_side_reactants_ids()

    def get_products_ids(self) -> List[str]:
        return [self.get_main_product_id()] + self.get_side_products_ids()

    def get_side_products_ids(self):
        return self._side_products_ids

    def get_modifiers_ids(self):
        return self._modifiers_ids

    def replace_species_id(self, s_id_old: str, s_id_new: str):
        found = False # we use found instead of directly returning when the id is found because a species can be in multiple roles in a single reaction
        if s_id_old == self._main_reactant_id:
            self._main_reactant_id = s_id_new
            found = True
        if s_id_old == self._main_product_id:
            self._main_product_id = s_id_new
            found = True
        for l in [self._side_products_ids, self._side_reactants_ids, self._modifiers_ids]:
            if s_id_old in l:
                l[l.index(s_id_old)] = s_id_new
                found = True

        assert found
    #
    # def get_side_species_line(self, id_ss, role, layout_type=LAYOUT_TYPE.PREDICTED) -> gr.Line:
    #
    #
    #     return self.get_sr_points(id_ss, role)
    #
    #     # pos_ss = lt.get_node_pos(self.get_bioentity().get_graph().nodes[id_ss], layout_type)
    #     # if id_ss in self.get_side_reactants_ids():
    #     #     return gr.Line(self.get_reactants_connection_pos(), pos_ss)
    #     # elif id_ss in self.get_side_products_ids():
    #     #     return gr.Line(pos_ss, self.get_products_connection_pos())
    #     # else:
    #     #     return gr.Line(pos_ss, self.get_center())

    def get_species_connecting_point(self, id) -> np.ndarray:
        if id == self.get_main_reactant_id() or id in self.get_side_reactants_ids():
            return self.get_reactants_connection_pos()
        elif id == self.get_main_product_id() or id in self.get_side_products_ids():
            return self.get_products_connection_pos()
        else:
            return self.get_center()

    def get_species_port_point(self, s_id, layout_type=LAYOUT_TYPE.PREDICTED) -> np.ndarray:
        cp = self.get_species_connecting_point(s_id)
        g = self.get_bioentity().get_graph()
        l:SpeciesLayout = gu.get_node_data(g.nodes[s_id]).get_layout(layout_type)
        bb = l.get_bb().get()
        # We create a smaller bounding box so that when testing intersects with the lines
        # the intersections which only touch are not taken into account
        bb_mod = gr.Rectangle(bb.get_x1()+1, bb.get_y1()+1, bb.get_x2()-1, bb.get_y2()-1)
        ports = l.get_bb().get_ports()

        non_intersecting_ports = []
        for port in ports:
            if gr.utils.equal(cp, port):
                return port

            cpp = gr.Line(cp, port)
            if not cpp.intersects_rectangle(bb_mod):
                non_intersecting_ports.append(port)
        if len(non_intersecting_ports) == 0:
            non_intersecting_ports = ports

        if len(non_intersecting_ports) == 1:
            return non_intersecting_ports[0]
        else:
            return gr.utils.closest(cp, non_intersecting_ports)

    def intersection(self, rect: gr.Rectangle or List[gr.Rectangle]):
        lines = self.get_all_lines()
        if isinstance(rect, gr.Rectangle):
            return rect.intersects_lines(lines)
        else:
            assert isinstance(rect, list)
            for r in rect:
                if r.intersects_lines(lines):
                    return True
                else:
                    return False

    def set_main_chain_points(self, points: List[np.ndarray], adjust_species_end=False):
        """
        The points list contains list of points for a new main chain starting in main reactant and
        ending with main product. New reaction center is positioned in the centr of this line (either
        the middle point or a point in the middle of the middle line segment if there is an odd number of
        points or there are just 2 points). The connection points for reactants and products (if there are
        multiple points for products and reactants) 1/5 of the distance from the center to the main product/reactant.
        In the end, all species are updated to connect to the new connection points.

        :param points:
        :param adjust_species_end: If set to true, the points start and end in the center of the species and need
        to be moved to the border so that one can see the arrows in case of products.
        :return:
        """

        assert len(points) > 1


        if adjust_species_end:
            g = self.get_bioentity().get_graph()
            for ix_first, ix_second in [(0, 1), (len(points) - 1, len(points) - 2)]:
                l = gr.Line(points[ix_first], points[ix_second])
                s_id = self.get_main_reactant_id() if ix_first == 0 else self.get_main_product_id()
                bb:gr.Rectangle = gu.get_predicted_layout(g, s_id).get_bb().get()
                for edge in [bb.get_line_left(), bb.get_line_left(), bb.get_line_left(), bb.get_line_left()]:
                    p = gr.np_seg_intersect(edge, l)
                    if p is not None:
                        points[ix_first] = p
                        break


        # Locate center
        # The center of the reaction will be positioned in the middle of the curve connecting the reactant and the product
        cnt_points = len(points)

        # if cnt_points % 2 == 1:
        #     ix_center = int(cnt_points / 2)
        #     pos_center = points[ix_center]
        #     pos_center_left = points[:ix_center][::-1]
        #     pos_center_right = points[ix_center + 1:]
        # else:

        aux_lines = points_to_lines(points)
        length = sum([l.length() for l in aux_lines])
        aux_length = 0
        ix_center = 0
        while aux_length < length / 2:
            aux_length += aux_lines[ix_center].length()
            ix_center += 1
        ix_center -= 1
        #now ix_center is at the point which starts the line which contains point which is in the middle between the reactant and the product
        pos_center = (points[ix_center] + points[ix_center+1]) / 2
        pos_center_left = points[:ix_center + 1][::-1]
        pos_center_right = points[ix_center + 1:]

        # if cnt_points == 2:
        #     pos_center = (points[1] + points[0]) / 2
        # elif cnt_points % 2 == 1:
        #     ix_center = int(cnt_points / 2)
        #     pos_center = points[ix_center]
        #     pos_center_left = points[:ix_center][::-1]
        #     pos_center_right = points[ix_center + 1:]
        # else:
        #     ix_right = int(cnt_points / 2)
        #     ix_left = ix_right - 1
        #     pos_center = (points[ix_left] + points[ix_right]) / 2
        #     pos_center_left = points[:ix_left+1][::-1]
        #     pos_center_right = points[ix_right:]

        self.set_center(pos_center)

        if len(self._reactants_center_points) > 0:
            #If there is more then one reactant, we have reactant_connection_point-center curve. Currently we consider only a line
            self._reactants_center_points[-1] = np.array(self._center)

            # if cnt_points == 2:
            #     self.set_reactants_connection_pos(self._center - (points[1] - points[0]) / 10)
            # else:
            #     self.set_reactants_connection_pos(pos_center_left[0])
            v = (pos_center_left[0] - pos_center) / 5
            if gr.utils.size(v) < settings.render.min_connection_point_offset and cnt_points > 3:
                v = (pos_center_left[0] - pos_center)
            self.set_reactants_connection_pos(pos_center + v)
        else:
            self.set_reactants_connection_pos(pos_center)

        if len(self._products_center_points) > 0:
            v = (pos_center_right[0] - pos_center) / 5

            if gr.utils.size(v) < settings.render.min_connection_point_offset and cnt_points > 3:
                v = (pos_center_right[0] - pos_center)
            self.set_products_connection_pos(pos_center + v)
        else:
            self.set_products_connection_pos(pos_center)

        for srp in self._reactant_sr_points:
            srp.points[-1] = np.array(self.get_reactants_connection_pos())
        for srp in self._product_sr_points:
            srp.points[-1] = np.array(self.get_products_connection_pos())
        for srp in self._modifier_sr_points:
            srp.points[-1] = np.array(self.get_center())

        # TODO: update main path itself (srp)
        if cnt_points > 2:
            srp = self.get_sr_points(self.get_main_reactant_id(), SPECIES_ROLE.MAIN_REACTANT())
            srp.points = pos_center_left[::-1] + [pos_center]
            srp = self.get_sr_points(self.get_main_product_id(), SPECIES_ROLE.MAIN_PRODUCT())
            srp.points = pos_center_right[::-1] + [pos_center]

        # rcp = self._reactants_center_points
        # if len(rcp) > 0:
        #     rcp[0] = self.get_reactants_connection_pos()
        #     rcp[1] = self.get_center()
        # pcp = self._products_center_points
        # if len(rcp) > 0:
        #     pcp[0] = self.get_products_connection_pos()
        #     pcp[1] = self.get_center()

    def get_sr_points(self, s_id: str, role:SPECIES_ROLE) -> SpeciesRolePoints:
        for srp in self._reactant_sr_points + self._modifier_sr_points + self._product_sr_points:
            if srp.s_id == s_id and srp.role.value == role.value:
                return srp
        assert False

    def get_main_chain_points(self) -> List[np.ndarray]:
        """
        :return: Points on the main chain starting with the species role point and ending with the product
        """
        points = self.get_species_curve_points(self.get_main_reactant_id(), SPECIES_ROLE.MAIN_REACTANT()) + \
              self.get_reactants_center_curve_points() + self.get_products_center_curve_points()[::-1] + \
              self.get_species_curve_points(self.get_main_product_id(), SPECIES_ROLE.MAIN_PRODUCT())[::-1]

        for i in range(len(points)-2, -1, -1):
            if np.all(np.equal(points[i], points[i+1])):
                del points[i+1]

        return points


    def main_chain_intersection(self, rg:RoutingGraph):
        """
        Main chain consists of line going from main product to main reactant.
        """

        lines = points_to_lines(self.get_main_chain_points())
        for l in lines:
            if rg.lineIntersectsRects(l):
                return True

        return False

        # if isinstance(rect, gr.Rectangle):
        #     return rect.intersects_lines(lines, consider_border=False)
        # else:
        #     assert isinstance(rect, list)
        #     for r in rect:
        #         if r.intersects_lines(lines, consider_border=False):
        #             return True
        #     return False

    def side_chain_intersection(self, rg: RoutingGraph) -> List[Tuple[str, SPECIES_ROLE]]:
        """
        Main chain consists of line going from side product/reactants and modifiers to the
        reaction connection points.
        """

        s_id_role = []

        r_id: Reaction = self.get_bioentity().get_id()
        g: nx.MultiGraph = self.get_bioentity().get_graph()

        for s_id in g[r_id]:
            for role in gu.get_roles(g, r_id, s_id):
                if not role.is_main_product() and not role.is_main_reactant():
                    for l in self.get_species_curve_lines(s_id, role):
                        if rg.lineIntersectsRects(l):
                            s_id_role.append((s_id, role))

        return s_id_role

        # s_id_role = []
        # if isinstance(rect, gr.Rectangle):
        #     rect = [rect]
        #
        # r_id:Reaction = self.get_bioentity().get_id()
        # g:nx.MultiGraph = self.get_bioentity().get_graph()
        #
        # for s_id in g[r_id]:
        #     for role in gu.get_roles(g, r_id, s_id):
        #         if not role.is_main_product() and not role.is_main_reactant():
        #             for r in rect:
        #                 if r.intersects(self.get_species_curve_lines(s_id, role)):
        #                     s_id_role.append((s_id, role))
        #
        # return s_id_role


    def get_all_lines(self) -> List[gr.Line]:
        lines: List[gr.Line] = []

        return self.get_reactants_center_curve_lines() + self.get_products_center_curve_lines() + \
               self.get_reactants_curve_lines() + self.get_modifiers_curve_lines() + self.get_products_curve_lines()

    def get_all_lines_idealized(self) -> List[gr.Line]:
        """
        Gets all lines going from the center to each of the species center
        :return:
        """
        lines: List[gr.Line] = []

        center = self.get_center()
        g = self.get_bioentity().get_graph()
        layout_type = self.get_layout_type()

        for id in self.get_species_ids():
            lines.append(gr.Line(center, gu.get_layout(g, id, layout_type).get_center(), allow_zero_length=True))

        return lines

    
    def get_reactants_curve_lines(self) -> List[gr.Line]:
        lines:List[gr.Line] = []
        for srp in self._reactant_sr_points:
            lines += points_to_lines(srp.points)
        return lines
    
    def get_modifiers_curve_lines(self) -> List[gr.Line]:
        lines:List[gr.Line] = []
        for srp in self._modifier_sr_points:
            lines += points_to_lines(srp.points)
        return lines
    
    def get_products_curve_lines(self) -> List[gr.Line]:
        lines:List[gr.Line] = []
        for srp in self._product_sr_points:
            lines += points_to_lines(srp.points)
        return lines

    def set_species_curve_points(self, s_id: str, role: SPECIES_ROLE, points: List[np.ndarray], adjust_species_end = True):
        """

        :param s_id:
        :param role:
        :param points:
        :param adjust_species_end:  If set to true, the points start in the center of the species and need
        to be moved to the border so that one can see the arrows in case of products.
        :return:
        """

        if adjust_species_end:
            g = self.get_bioentity().get_graph()
            l = gr.Line(points[0], points[1])
            bb:gr.Rectangle = gu.get_predicted_layout(g, s_id).get_bb().get()
            for edge in [bb.get_line_left(), bb.get_line_left(), bb.get_line_left(), bb.get_line_left()]:
                p = gr.np_seg_intersect(edge, l)
                if p is not None:
                    points[0] = p
                    break

        for srp in self._reactant_sr_points + self._modifier_sr_points + self._product_sr_points:
            if srp.s_id == s_id and srp.role.value == role.value:
                srp.points = points

    def adjust_ends(self):
        """
        When rerouting, the starting and end positions are centers of species and not its boundary,
        therefore it might be necesarry to adjust the position of the lines ending and starting in the species
        to start and end only on the border of the corresponding species
        :return:
        """

        g = self.get_bioentity().get_graph()
        for srp in self._reactant_sr_points + self._modifier_sr_points + self._product_sr_points:
            if len(srp.points) > 1:
                l = gr.Line(srp.points[0], srp.points[1], allow_zero_length=True) #allow_zero_length because the species can intersect
                bb: gr.Rectangle = gu.get_predicted_layout(g, srp.s_id).get_bb().get()
                for edge in [bb.get_line_left(), bb.get_line_top(), bb.get_line_right(), bb.get_line_bottom()]:
                    p = gr.np_seg_intersect(edge, l)
                    if p is not None and p is not True: #true in case they are colinear
                        srp.points[0] = p
                        break



    def get_species_curve_points(self, s_id:str, role:SPECIES_ROLE) -> List[np.ndarray]:

        for srp in self._reactant_sr_points + self._modifier_sr_points + self._product_sr_points:
            if srp.s_id == s_id and srp.role.value == role.value:
                return srp.points

        assert False

    def get_species_curve_lines(self, s_id:str, role:SPECIES_ROLE) -> List[gr.Line]:
        return points_to_lines(self.get_species_curve_points(s_id, role))

    def get_reactants_center_curve_points(self) -> List[np.ndarray]:
        return self._reactants_center_points


    def get_reactants_center_curve_lines(self) -> List[gr.Line]:
        return points_to_lines(self.get_reactants_center_curve_points())

    def get_products_center_curve_points(self) -> List[np.ndarray]:
        return self._products_center_points

    def get_products_center_curve_lines(self) -> List[gr.Line]:
        return points_to_lines(self.get_products_center_curve_points())

    def recompute_layout(self):

        self.__init__(self.get_bioentity(), self.get_center(), self.is_copied())

        cnt_substartes = 0
        pos_substrate = None #This will be set to the position of main substrate or first product if no main substrate is available. Then it is used mainly to located the center of the reaction
        cnt_products = 0
        pos_product = None

        g = self.get_bioentity().get_graph()
        r_id = self.get_bioentity().get_id()

        for s_id in g[r_id]:
            roles = gu.get_roles(g, r_id, s_id)
            if SPECIES_ROLE.CONTAINS_REACTANT_ROLE(roles):
                cnt_substartes += 1
                if cnt_substartes == 1:
                    pos_substrate = lt.get_node_pos(g.nodes[s_id])

                if SPECIES_ROLE.REACTANT in roles or SPECIES_ROLE.SUBSTRATE in roles or cnt_substartes == 1:
                    # REACTANT and SUBSTRATE stand for main reactant (both can be encountered), the remaining are SIDESUBSTRATE
                    # See the product case for explanation of the cnt_substrates==1 condition
                    # assert self._main_reactant_pos is None
                    self._main_reactant_pos = lt.get_node_pos(g.nodes[s_id])
                    pos_substrate = self._main_reactant_pos
                    if self.get_main_reactant_id() is not None:
                        self._side_reactants_ids.append(self.get_main_reactant_id())
                    self.set_main_reactant_id(s_id)
                else:
                    self._side_reactants_ids.append(s_id)

            if SPECIES_ROLE.CONTAINS_PRODUCT_ROLE(roles):
                # PRODUCT stands for the main reactant (both can be encountered), the remaining are SIDEPRODUCT
                cnt_products += 1
                if cnt_products == 1:
                    pos_product = lt.get_node_pos(g.nodes[s_id])

                if SPECIES_ROLE.PRODUCT in roles or cnt_products == 1:
                    # The condition has cnt_products == 1 because it can happen that we remove a part of reaction
                    # because of compartment splitting and then only side products will remain. In such a case,
                    # we wouldn't have a main product id without this condition
                    # assert self._main_product_pos is None
                    self._main_product_pos = lt.get_node_pos(g.nodes[s_id])
                    pos_product = self._main_product_pos
                    if self.get_main_product_id() is not None:
                        self._side_products_ids.append(self.get_main_product_id())
                    self.set_main_product_id(s_id)
                else:
                    self._side_products_ids.append(s_id)

            if SPECIES_ROLE.CONTAINS_MODIFIER_ROLE(roles):
                self._modifiers_ids.append(s_id)

        assert cnt_substartes > 0 or cnt_products > 0

        for s_id in g[r_id]:
            if not gu.get_predicted_layout(g, s_id).is_initialized():
                # if not all neighbors of the reaction has yet been initialized, we do not recompute the layout
                return

        displacement = np.array([0, 0])
        if pos_substrate is not None and pos_product is not None:
            self._center = self.__get_center(pos_substrate, pos_product)
            displacement = (self._center - pos_substrate) / 5
        elif pos_substrate is not None:
            self._center = pos_substrate
        else:
            self._center = pos_product

        self._reactants_connection_pos = self._center
        self._products_connection_pos = self._center
        if cnt_substartes > 1:
            # wee need a line connecting connection point of reactants and the center of the reaction
            self._reactants_connection_pos = self._center - displacement

        if cnt_products > 1:
            # wee need a line connecting connection point of reactants and the center of the line
            self._products_connection_pos = self._center + displacement

        for s_id in g[r_id]:

            # s_pos = gu.get_predicted_layout(g, s_id).get_center()
            s_pos = self.get_species_port_point(s_id)
            assert s_pos is not None

            cnt_roles = len(g[r_id][s_id])
            cnt_mod = 0
            for role in gu.get_roles(g, r_id, s_id):

                if role.is_reactant():
                    r_pos = self._reactants_connection_pos
                elif role.is_product():
                    r_pos = self._products_connection_pos
                else:
                    r_pos = self._center

                if role.is_reactant():
                    self._reactant_sr_points.append(SpeciesRolePoints(s_id, role, [s_pos, r_pos]))
                elif role.is_product():
                    self._product_sr_points.append(SpeciesRolePoints(s_id, role, [s_pos, r_pos]))
                else:
                    if np.all(np.equal(s_pos, self._center)):
                        #can happen if, for example, reactant and product share position (after alignment to a grid of a too low granularity)
                        displacement = np.array([0,0])
                    else:
                        displacement = (self._center - s_pos) / np.linalg.norm(self._center - s_pos) * 10
                    if cnt_roles == 1:
                        self._modifier_sr_points.append(SpeciesRolePoints(s_id, role, [s_pos, r_pos-displacement]))
                    else:
                        # We have one species which is in roles and thus we need multiple lines which
                        # do not overlap. In such case, the modifier curve consists of two lines

                        cnt_mod += 1
                        vec = self._center - s_pos
                        vec_norm = gr.utils.normalize(vec)
                        vec_norm_perp = np.array(
                            [-vec_norm[1], vec_norm[0]])  # vector perpendicular to the species-reaction line

                        # the new coordinates is perpendicular to the the point in the middle of species-reaction line
                        # and the distance ot that middle point is width of a species multiplied by index of that modifier
                        # which would be relevant if we had multiple identical modifiers (not sure if that could happen)
                        pos_middle = s_pos + vec / 2 + vec_norm_perp * settings.render.species_glyph_width / 2 * cnt_mod

                        self._modifier_sr_points.append(SpeciesRolePoints(s_id, role, [s_pos, pos_middle, r_pos - displacement]))
                    
        if not np.all(np.equal(self._center, self._products_connection_pos)):
            self._products_center_points += [self._products_connection_pos, self._center]
        if not np.all(np.equal(self._center, self._reactants_connection_pos)):
            self._reactants_center_points += [self._reactants_connection_pos, self._center]

        if (self._main_reactant_id is None or self._main_product_id is None):
            asdf=1
        assert self._main_reactant_id is not None and self._main_product_id is not None


    def __get_center(self, pos_reactant: np.ndarray, pos_product: np.ndarray) -> np.ndarray:
        # We can have two species which are connected by multiple reactions (e.g. with different modifiers) and then
        # the reaction curves need to be set up in such a way that they are discernible.
        # First, overlapping reactions are identified. Then available positions for centers are identified. Finally,
        # each of the reactions is assigned a position in a way respecting position of other species, so that, for example,
        # if one of the reations has a modifier, it's center is closer to the modifier then the center of a reaction without
        # a modifier. Otherwise the reactions cruces would cross.
        center = (pos_reactant + pos_product) / 2

        r_ids_parallel = self.__get_parallel_reaction_ids()

        # This function is called for every reaction in a group and the reactions can have swapped positions of main
        # reactant and product. But to be consistent in positions calculations, we need to have always the same positions
        # of the two species.
        # TODO: more correct would be to compute the positions only once per group, but then we would need to handle the situations
        # with overlapping reactions as a post processing step
        if pos_product[0] < pos_reactant[0] or (pos_product[0] == pos_reactant[0] and pos_product[1] < pos_reactant[1]):
            p1 = pos_product
            p2 = pos_reactant
        else:
            p2 = pos_product
            p1 = pos_reactant

        if len(r_ids_parallel) == 1:
            return center

        p2_p1_perp = np.array([p1[1] - p2[1], p2[0] - p1[0]])
        p2_p1_perp_norm = gr.utils.normalize(p2_p1_perp)
        p2_p1_perp_sp = p2_p1_perp_norm * float(settings.render.overlapping_reactions_center_spacing)

        positions = []
        cnt = len(r_ids_parallel)
        even = cnt % 2 == 0
        for i in range(cnt):
            shift = p2_p1_perp_sp * (i - int(cnt / 2))
            if even:
                # if we have even number of readtions, the original center is not used
                shift += p2_p1_perp_sp / 2
            positions.append(center + shift)

        # with positions for reactions centers identified, we need to assign them to the reactions
        # for each reaction we get the center of mass based on the positions of the species and then
        # we compute the distances of the COMs to the original center with taking into account whether
        # they are left or right of r_p, sort the reactions and asssign the positions in this order

        g = self.get_bioentity().get_graph()
        r_dist = []
        for r_id in r_ids_parallel:
            com = gu.get_reaction(g, r_id).get_com()
            dist = gr.utils.dist(com, center)
            side = ((p2[0] - p1[0]) * (com[1] - p1[1]) - (p2[1] - p1[1]) * (com[0] - p1[0]))
            if side < 0:
                dist = -dist
            r_dist.append((dist, r_id))
        r_dist.sort()

        r_id = self.get_bioentity().get_id()
        for i in range(len(r_dist)):
            if r_dist[i][1] == r_id:
                return positions[i]

        assert False

    def __get_parallel_reaction_ids(self) -> List[str]:
        """
        Get all reactions which can have the same reaction line connecting main reactant and main product because
        these would overlap. We get main reactant id and main product id and find all reactions which connect them.
        For each of these reactions we check whether the two species are also in the main reactant and main product role.
        If so, such reaction will be returned.
        :return:
        """

        g = self.get_bioentity().get_graph()
        r_id = self.get_bioentity().get_id()


        mr_id = self.get_main_reactant_id()
        mp_id = self.get_main_product_id()

        pr_ids = set()
        for pr_id in gu.get_connecting_nodes(g, mr_id, mp_id):
            if pr_id == r_id:
                continue
            r_roles = gu.get_roles(g, pr_id, mr_id)
            p_roles = gu.get_roles(g, pr_id, mp_id)
            if (SPECIES_ROLE.REACTANT in r_roles or SPECIES_ROLE.SUBSTRATE in r_roles or SPECIES_ROLE.PRODUCT in r_roles) \
                and \
                (SPECIES_ROLE.REACTANT in p_roles or SPECIES_ROLE.SUBSTRATE in p_roles or SPECIES_ROLE.PRODUCT in p_roles):
                pr_ids.add(pr_id)

        return [r_id] + list(pr_ids)



class BioEntity:

    def __init__(self, g:nx.MultiGraph, id, name=None, type: int=None, element_id=None,
                 layouts:Dict[LAYOUT_TYPE,BioEntityLayout]=None):
        self._g = g
        self._id = id
        self.__id_history: List[str] = []
        self._name = name
        self._type:int = type #preferably an SBO id 290 (this is then used e.g. in is_simple_molecule)
        self._element_id = element_id
        if layouts is None:
            self._layouts = {
                LAYOUT_TYPE.ORIGINAL: BioEntityLayout(self, None, None),
                LAYOUT_TYPE.PREDICTED: BioEntityLayout(self, None, None)
            }
        else:
            for l_id in layouts:
                layouts[l_id].set_bioentity(self)
            self._layouts = layouts
        self._sbml_object = None

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            if k == '_g':
                setattr(result, k, self.__dict__[k])
            else:
                setattr(result, k, copy.deepcopy(v, memo))
        return result

    def get_id(self):
        return self._id

    def get_original_id(self):
        return self.__id_history[-1] if len(self.__id_history) > 0 else self.get_id()

    def set_id(self, id):
        if id is not None:
            self.__id_history.append(self._id)
        self._id = id

    def get_graph(self) -> nx.MultiGraph:
        return self._g

    def set_graph(self, g: nx.MultiGraph):
        self._g = g

    def get_name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def get_type(self):
        return self._type

    def set_type(self, type):
        self._type = type

    def get_element_id(self):
        return self._element_id

    def set_element_id(self, element_id):
        self._element_id = element_id

    def get_layout(self, layout_type: LAYOUT_TYPE)->BioEntityLayout:
        return self._layouts[layout_type]

    def get_predicted_layout(self)->BioEntityLayout:
        return self.get_layout(layout_type=LAYOUT_TYPE.PREDICTED)

    def set_layout(self, layout:BioEntityLayout, layout_type: LAYOUT_TYPE):
        layout.set_bioentity(self)
        self._layouts[layout_type] = layout

    def get_sbml_object(self):
        return self._sbml_object

    def set_sbml_object(self, sbml_object):
        self._sbml_object = sbml_object

    def is_species(self):
        return NotImplemented


class SpeciesFeature():
    """
    Class for handling the SBML's features available in the SBML Multi package.
    The class provides a way how to deal with the necesitty to show only certain
    feature types (via the __visual property).
    """

    def __init__(self, cnt: int, feature_type: str, feature_name: str, values: List[str]):
        self.__cnt = cnt
        self.__feature_type = feature_type
        self.__feature_name = feature_name
        self.__values = values
        self.__visual: str = "" # Holds serialized features which are visible in the interface such as phosphorilation (since currently there is not well-defined way how to encode them)

        self.__set_visual()



    @property
    def cnt(self) -> int:
        return self.__cnt

    @property
    def feature_type(self) -> str:
        return self.__feature_type

    @property
    def feature_name(self) -> str:
        return self.__feature_name

    @property
    def values(self) -> List[str]:
        return self.__values

    @property
    def visual(self) -> str:
        return self.__visual

    def serialize(self, filter_out: List[str]=[], with_type: bool=True, delimiter: str= "") -> str:
        rv = self.feature_type + "_" if with_type else ""
        for i in range(self.cnt):
            rv += delimiter.join(sorted(filter(lambda x: x not in filter_out, self.values)))
        return rv

    def is_visual(self) -> bool:
        return self.__visual != ""

    def __set_visual(self):

        # Based on https://git-r3lab.uni.lu/minerva/core/blob/master/model/src/main/java/lcsb/mapviewer/model/map/species/field/ModificationState.java (SHA b64bc6840990b90c6c81865c32048d555031c120)
        modifications = {
            "phosphorylated": "P",
            "acetylated": "Ac",
            "ubiquitinated": "Ub",
            "methylated": "Me",
            "hydroxylated": "OH",
            "myristoylated": "My",
            "sulfated": "S",
            "prenylated": "Pr",
            "glycosylated": "G",
            "palmytoylated": "Pa",
            "unknown": "?",
            # "empty": "",
            "protonated": "H",
            "don't care": "*",
        }
        if 'modification_type' in self.feature_type and self.feature_name in modifications.keys():

            self.__visual = modifications[self.feature_name]

        if 'structural_state' in self.feature_type:
            self.__visual = self.serialize(with_type=False, delimiter=",")


class Species(BioEntity):

    def __init__(self, g:nx.MultiGraph, id, name = None, type:SPECIES_ROLE = None, element_id=None,
                 compartment_id=None, compartment_name=None, layouts:Dict[LAYOUT_TYPE,SpeciesLayout]=None,
                 features: [SpeciesFeature]=[]):

        super().__init__(g=g, id=id, name=name, type=type, element_id=element_id, layouts=layouts)
        self.set_compartment_id(compartment_id)
        self.set_compartment_name(compartment_name)
        self.set_fetures(features)

    def get_compartment_id(self):
        return self._compartment_id

    def set_compartment_id(self, compartment_id):
        self._compartment_id = compartment_id
        
    def get_compartment_name(self) -> str:
        return self._compartment_name

    def set_compartment_name(self, compartment_name):
        self._compartment_name = compartment_name

    def get_fetures_serialized(self, feature_types: List[str]=None, filter_out: List[str]=[], with_type=True) -> str:
        rv = ""

        for f in sorted(self.get_fetures(), key=lambda x: x.feature_type):
            if feature_types is None or f.feature_type in feature_types:
                rv += f.serialize(filter_out=filter_out, with_type=with_type)

        return rv

    def get_visual_features_serialized(self) -> str:
        vs = [f.visual for f in sorted(self.get_fetures(), key=lambda x: x.feature_type) if f.is_visual()]
        return "".join(vs)

    def get_fetures(self) -> List[SpeciesFeature]:
        return self._features

    def set_fetures(self, fetures: List[SpeciesFeature]):
        self._features = [f for f in fetures if f.is_visual()]

    def is_species(self):
        return True

    def is_simple_molecule(self):
        return self.get_type() == 247



class Reaction(BioEntity):

    def __init__(self, g:nx.MultiGraph, id, name = None, type: SPECIES_ROLE = None, element_id=None,
                 layouts:Dict[LAYOUT_TYPE,ReactionLayout]=None, is_reversible=False):
        super().__init__(g=g, id=id, name=name, type=type, element_id=element_id, layouts=layouts)
        self._reversible = is_reversible

    def is_species(self) -> bool:
        return False

    def is_reversible(self) -> bool:
        return self._reversible

    def set_reversible(self, reversible: bool):
        self._reversible = reversible

    def is_valid(self):
        g = self.get_graph()
        has_reactant = False
        has_product = False
        r_id = self.get_id()
        for s_id in g[r_id]:
            roles = gu.get_roles(g, r_id, s_id)
            if SPECIES_ROLE.CONTAINS_MAIN_REACTANT_ROLE(roles):
                has_reactant = True
            if SPECIES_ROLE.CONTAINS_MAIN_PRODUCT_ROLE(roles):
                has_product = True

        return has_reactant and has_product

    def get_com(self) -> np.ndarray:
        """

        :return: Center of mass
        """
        g = self.get_graph()
        s_ids = list(g[self.get_id()])
        com = sum(lt.get_node_pos(g.nodes[s_id]) for s_id in s_ids)

        assert len(s_ids) > 0
        return com / len(s_ids)

