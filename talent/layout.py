import networkx as nx
import copy
import sys
import math
import logging
from typing import List, Any, Dict, Set, Tuple, Collection
from scipy.optimize import linear_sum_assignment
import numpy as np
from random import random

from . import beautification as bt
from . import reaction_graph as rg
from . import settings
from . import bioentity as be
from . import graph_utils as gu
from . import graphics as gr
from . import fdp

from itertools import combinations, product


class NodesMapping:
    """
    To store mapping between the ids of the nodes in the target graph and the result graph. I.e. each time a node
    is added to the result graph, a mapping is added.
    """
    def __init__(self):
        self.__sourceTarget: Dict[str, List[str]] = {}
        self.__targetSource: Dict[str, List[str]] = {}

    def addMapping(self, source, target):

        if source not in self.__sourceTarget:
            self.__sourceTarget[source] = []
        self.__sourceTarget[source].append(target)

        if target not in self.__targetSource:
            self.__targetSource[target] = []
        self.__targetSource[target].append(source)

    def removeMapping(self, source, target):

        self.__sourceTarget[source].remove(target)
        if len(self.__sourceTarget[source]) == 0:
            del self.__sourceTarget[source]

        self.__targetSource[target].remove(source)
        if len(self.__targetSource[target]) == 0:
            del self.__targetSource[target]


    def getTargetMapping(self, target)->List[Any]:
        return self.__targetSource[target]

    def getSourceMapping(self, source):
        return self.__sourceTarget[source]

    def getAllTargetIds(self) -> List[str]:
        return list(self.__targetSource.keys())

    def getAllSourceIds(self) -> List[str]:
        return list(self.__sourceTarget.keys())


class Stats:
    med_one_degree_species_distance = 0
    med_reactions_distance = 0
    width = 0
    height = 0


class MinMaxCoords:
    def __init__(self, mins: np.ndarray = None, maxs: np.ndarray = None):
        self.min: np.ndarray = mins if mins is not None else np.array([sys.maxsize, sys.maxsize]) #x and y minimums
        self.max: np.ndarray = maxs if maxs is not None else np.array([-sys.maxsize, -sys.maxsize]) # x and y maximums

    def get_dim(self) -> np.ndarray:
        return np.array([self.max[0] - self.min[0], self.max[1] - self.min[1]])

    def get_rect(self) -> 'gr.Rectangle':
        return gr.Rectangle(self.min[0], self.min[1], self.max[0], self.max[1])

    def has_point(self, p: np.ndarray) -> bool:
        return self.min[0] <=  p[0] <= self.max[0] and self.min[1] <=  p[1] <= self.max[1]

    def push_min(self, m):
        for i in range(len(m)):
            if m[i] < self.min[i]:
                self.min[i] = m[i]

    def push_max(self, m):
        for i in range(len(m)):
            if m[i] > self.max[i]:
                self.max[i] = m[i]

# class CCGraphNode(NamedTuple):
#     x: float
#     y: float
#     width: float
#     height: float
#     g: nx.MultiGraph


def get_mapping_dicts(m):
    dicts = [{}, {}] # The first dictionary contains mapping from target to template, while the second mapping from template to target
    for op in m:
        if op[0] is not None:
            dicts[0][op[0]] = op[1]
        if op[1] is not None:
            dicts[1][op[1]] = op[0]

    return dicts

def get_reaction_direction(g: nx.MultiGraph, available_ids: List[str] or Set[str], s_ids: List[str], r_center: np.ndarray):
    """
    Get the direction vector for a reaction. This vector goes from the reaction center to the center of provided
    species. Optimally, this would go from center to the connection point of reactants or products or modifiers (in case
    of multiple species in given role).
    :param available_ids: All available species of a given reaction
    :param s_ids: Species based on which positions the directions should be determined (typically reactans, products or modifiers)
    :param r_center: Center of the reaction for which the direction should be determiend
    :return:
    """
    known_ids = set(available_ids).intersection(s_ids)
    if len(known_ids) == 0:
        return None
    dir = np.array([0.0, 0.0])
    for id in known_ids:
        dir += gu.get_predicted_layout(g, id).get_center() - r_center
    dir /= len(known_ids)
    return gr.utils.normalize(dir)


def intersects_forbidden(pos: np.ndarray, forbidden: List[gr.Rectangle]) -> bool:

    for r in forbidden:
        if r.intersects_rectangle(gr.Rectangle.from_point(pos, settings.render.species_glyph_width, settings.render.species_glyph_height)):
            return True
    return False

def layout_on_line(s_ids: List[str], center: np.ndarray, dir: np.ndarray, forbidden: List[gr.Rectangle]=[]) -> Dict[str, np.ndarray]:
    """
    Lay out species on a line difend by a point and a direction vector. The positions are alternating between left
    and right of the center, skipping the positions which are inside the forbidden areas.
    :param s_ids:
    :param center:
    :param dir:
    :param forbidden:
    :return:
    """




    rv = {}

    assert dir[0] != 0 or dir[1] != 0

    if len(s_ids) == 0:
        return rv

    if len(s_ids) == 1 and not intersects_forbidden(center, forbidden):
        return {s_ids[0]: center}

    if dir[0] == 0 or dir[1] == 0:
        s_dist = gr.utils.size(settings.render.optimal_neighboring_species_dist * dir)
    else:
        s_dist = settings.render.optimal_neighboring_species_dist[0]

    pos = np.array(center)
    alt = -1
    odd = len(s_ids) % 2
    if not odd:
        pos -= dir * s_dist / 2

    i = 0
    # for i in range(len(s_ids)):
    poss = []
    while len(poss) < len(s_ids):
        alt *= -1
        j = i - 1 if odd else i
        i += 1
        shift = dir * (math.floor(j / 2) + 1) * alt * s_dist
        aux_pos = pos + shift

        if intersects_forbidden(aux_pos, forbidden):
            continue

        if shift[0] < 0 or shift[1] < 0:
            poss.insert(0, aux_pos)
        else:
            poss.append(aux_pos)

        rv[s_ids[len(rv)]] = aux_pos

    # for i in range(len(poss)):
    #     rv[s_ids[i]] = poss[i]

    return rv


def layout_on_perp_line(g, s_ids, r_center, dir, forbidden: List[gr.Rectangle], same_type_rects: List[gr.Rectangle] = [],
                        line_distance: float = settings.render.optimal_species_reaction_dist) -> Dict[str, np.ndarray]:
    """
    Get center of positions for the input species. The positions will be on a line perependicular
    to the line defined by center and direction vector (which is normalized). The purpose is to lay out
    reactants, product, or modifiers on a line in given distance from the reaction center and
    if list of forbidden areas is provided, output only positions where the bounding boxes
    of the species would not intersect with these areas.

    The species are processed in the order the are input. The placement starts at the center (if the size of input is odd)
     and the following positions alternate left and right side.

    :param s_ids:
    :param r_center:
    :param dir:
    :param forbidden: The positions need to be tested so that they
    :param same_type_rects: Bounding boxes of species of the same type
    :return:
    """

    assert (dir[0] != 0 or dir[1] != 0)

    if len(s_ids) == 0:
        return {}



    pos = r_center + dir * line_distance
    i = 2
    # While the position of the intended center does not intersect anything, try to increase the distance. The exeption
    # is when we meet the rectangle of the same type. The example when this is usefull is when we are laying out
    # side product of a reaction which has the main product alreay laid out
    while intersects_forbidden(pos, forbidden) and not intersects_forbidden(pos, same_type_rects):
        pos = r_center + dir * i * line_distance
        i+=1

    dir_ortho1 = gr.utils.orthogonal(dir)
    dir_ortho2 = - dir_ortho1

    # Connsider both possible directions of orthogonal line. This might be important if we are laying out
    # a connecting species between existing reaction and an inserted reaction while the connecting species is
    # inserted as well. In such a case we need the connecting species to be positioned between the two reaction.
    # However, it could happen that if we chose the wrong direction it would be positioned on the outside of the
    # space between the two reactions.
    #  S1 ------r1-----> S2
    #             \
    #              \
    #               X
    #              /
    #             /
    #  S3 ------r2-----> S4

    poss1 = layout_on_line(s_ids, pos, dir_ortho1, forbidden)
    poss2 = layout_on_line(s_ids, pos, dir_ortho2, forbidden)

    # For each species we consider all the distances of its tentative position to all the species which has been
    # copied from template and pick position with lowest sum of the distances.

    poss = {}
    taken_poss = []
    for s_id in s_ids:
        pos1 = poss1[s_id]
        pos2 = poss2[s_id]

        if gr.utils.equal(pos1, pos2):
            poss[s_id] = pos1
        elif (int(pos1[0]), int(pos1[1])) in taken_poss:
            poss[s_id] = pos2
        elif (int(pos2[0]), int(pos2[1])) in taken_poss:
            poss[s_id] = pos1
        else:
            pos_nbs = [gu.get_predicted_layout(g, id).get_center() for id in gu.get_neibhbors_in_distance(g, s_id, 2) if gu.get_predicted_layout(g, id).is_copied()]

            sum_dist1 = sum([gr.utils.dist(pos1, pos_nb) for pos_nb in pos_nbs])
            sum_dist2 = sum([gr.utils.dist(pos2, pos_nb) for pos_nb in pos_nbs])
            if len(pos_nbs) == 0 or sum_dist1 < sum_dist2:
                poss[s_id] = pos1
            else:
                poss[s_id] = pos2
        taken_poss.append((int(poss[s_id][0]), int(poss[s_id][1])))

    return poss


def impute_dirs(dir_reactants: np.ndarray, dir_products: np.ndarray, dir_modifiers: np.ndarray) -> List[np.ndarray]:

    # TODO: This preferes only one side. I.e. if only a modifier is known then there should be two possibilities of how to lay out reactants and products
    # In case of modifiers this is handled outside of this procedure when two also -dir_modifiers is considered, but not in case of two possible positions of reactants and products

    rv_reactants = np.array(dir_reactants) if dir_reactants is not None else None
    rv_products = np.array(dir_products) if dir_products is not None else None
    rv_modifiers = np.array(dir_modifiers) if dir_modifiers is not None else None

    if rv_reactants is None:
        if rv_products is not None:
            rv_reactants = -rv_products
        elif rv_modifiers is not None:
            rv_reactants = gr.utils.orthogonal(rv_modifiers)
        else:
            assert False

    if rv_products is None:
        if rv_reactants is not None:
            rv_products = -rv_reactants
        elif rv_modifiers is not None:
            rv_products = -gr.utils.orthogonal(rv_modifiers)
        else:
            assert False

    if rv_modifiers is None:
        if rv_reactants is not None:
            rv_modifiers = gr.utils.orthogonal(rv_reactants)
        elif rv_products is not None:
            rv_modifiers = gr.utils.orthogonal(rv_products)
        else:
            assert False

    return [rv_reactants, rv_products, rv_modifiers]


def reorder_s_ids_preferernce(g: nx.MultiGraph, r_id: str, s_ids: Collection[str]) -> List[str]:
    """
    Reorder the input list of reactions in such a way that first will be main reactants/products
    :param g:
    :param s_ids:
    :param r_id:
    :return:
    """

    if len(s_ids) == 0:
        return []

    s_r = [(s_id, be.SPECIES_ROLE.CONTAINS_MAIN_ROLE(gu.get_roles(g, s_id, r_id))) for s_id in s_ids]
    s_r_sorted = sorted(s_r, key=lambda x: (not x[1], x[0]))

    return [sr[0] for sr in s_r_sorted]


def get_dist_to_closest(g: nx.MultiGraph, pos: np.ndarray, s_ids: Collection[str]) -> float:
    dists = [gr.utils.dist(pos, gu.get_predicted_layout(g, s_id).get_center()) for s_id in s_ids ]
    return min(dists)


def get_pos_for_connecting_species(g: nx.MultiGraph, r_id: str, s_ids: Collection[str], r_known_connecting_s_ids: List[str]) -> Dict[str, List[np.ndarray]]:
    """
    Get list of dictionaries possible positions for the species. Since modifiers can be laid "left" or "right" of
    the main reaction line, this can lead to multiple sets of positions.

    Potentially treating the result not as a single list of positions, but list of possible positions opens door for
    testing multiple strategies of how to lay the nodes (not just "left" or "right" for modifiers).

    :param g:
    :param r_id: Reaction, center of which is correctly set
    :param s_id: Species, position of which we would like to know
    :return:
    """
    rv = {}

    if len(s_ids) == 0:
        return rv

    #TODO: Correct way would be to ensure that when positioinig a species and reactions within the strategy searching, they do not share position
    discern_reaction_species(g, [r_id])

    l: be.ReactionLayout = gu.get_predicted_layout(g, r_id)
    r_center = l.get_center()
    r_reactant_ids = l.get_reactants_ids()
    r_products_ids = l.get_products_ids()
    r_modifiers_ids = l.get_modifiers_ids()
    
    # Get direction vectors from center to products, center to reactants 
    # These vectors are based on averages of the known connecting species (could be based on multiple species)

    copied = [s_id for s_id in g[r_id] if gu.get_predicted_layout(g, s_id).is_copied()]
    known_cs_and_copied = set(r_known_connecting_s_ids + copied)

    # Get direction of reactants, products and modifiers of the current reaction based on already known species
    # dir_reactants = get_reaction_direction(g, known_cs_and_copied, r_reactant_ids, r_center)
    # dir_products = get_reaction_direction(g, known_cs_and_copied, r_products_ids, r_center)
    mr_id = l.get_main_reactant_id()
    mp_id = l.get_main_product_id()

    if mr_id not in known_cs_and_copied and mp_id not in known_cs_and_copied or mr_id==mp_id:
        # If there is no main reactant or product which could orient the reaction we use all reactants and products and average
        r_ids_for_dir = r_reactant_ids
        p_ids_for_dir = r_products_ids
    else:
        r_ids_for_dir = [mr_id]
        p_ids_for_dir = [mp_id]

    dir_reactants = get_reaction_direction(g, known_cs_and_copied, r_ids_for_dir, r_center)
    dir_products = get_reaction_direction(g, known_cs_and_copied, p_ids_for_dir, r_center)
    dir_modifiers = get_reaction_direction(g, known_cs_and_copied, r_modifiers_ids, r_center)

    dist_reactants = settings.render.optimal_species_reaction_dist if dir_reactants is None else gr.utils.dist(l.get_main_reactant_pos(), r_center)
    dist_products = settings.render.optimal_species_reaction_dist if dir_products is None else gr.utils.dist(l.get_main_product_pos(), r_center)
    dist_modifiers = settings.render.optimal_species_reaction_dist if dir_modifiers is None else get_dist_to_closest(g, r_center, known_cs_and_copied.intersection(r_modifiers_ids))

    dir_reactants, dir_products, dir_modifiers = impute_dirs(dir_reactants, dir_products, dir_modifiers)

    s_id_reactans = reorder_s_ids_preferernce(g, r_id, set(r_reactant_ids).intersection(s_ids))
    s_id_products = reorder_s_ids_preferernce(g, r_id, set(r_products_ids).intersection(s_ids))
    s_id_modifiers = reorder_s_ids_preferernce(g, r_id, set(r_modifiers_ids).intersection(s_ids))

    # Should a species be in multiple roles, the order of treatment of types of nodes determines what role will be decisive

    def add_dict(d):
        for id in d:
            if id not in rv:
                rv[id] = []
            rv[id].append(d[id])

    forbidden_reactants: List[gr.Rectangle] = [gu.get_predicted_layout(g, s_id).get_bb().get() for s_id in known_cs_and_copied if s_id in r_reactant_ids]
    forbidden_products: List[gr.Rectangle] = [gu.get_predicted_layout(g, s_id).get_bb().get() for s_id in known_cs_and_copied if s_id in r_products_ids]
    forbidden_modifiers: List[gr.Rectangle] = [gu.get_predicted_layout(g, s_id).get_bb().get() for s_id in known_cs_and_copied if s_id in r_modifiers_ids]
    forbidden = forbidden_reactants + forbidden_products + forbidden_modifiers

    add_dict(layout_on_perp_line(g, s_id_reactans, r_center, dir_reactants, forbidden, forbidden_reactants, dist_reactants))
    add_dict(layout_on_perp_line(g, s_id_products, r_center, dir_products, forbidden, forbidden_products, dist_products))
    add_dict(layout_on_perp_line(g, s_id_modifiers, r_center, dir_modifiers, forbidden, forbidden_modifiers, dist_modifiers))
    add_dict(layout_on_perp_line(g, s_id_modifiers, r_center, -dir_modifiers, forbidden, forbidden_modifiers, dist_modifiers))

    return rv


def lay_out_reaction_center(g: nx.MultiGraph, r_id: str, positioned_connecting_s_ids: List[str], ignored_r_ids: List[str]):

    def get_center(s_id, dir_norm):
        return s_id + dir_norm * settings.render.optimal_species_reaction_dist

    r_lt:be.ReactionLayout = gu.get_predicted_layout(g, r_id)
    mr_id = r_lt.get_main_reactant_id()
    mp_id = r_lt.get_main_product_id()
    assert mr_id is not None and mp_id is not None
    mr_copied = gu.get_predicted_layout(g, mr_id).is_copied()
    mp_copied = gu.get_predicted_layout(g, mp_id).is_copied()
    if mr_copied and mp_copied:
        #if both reactant and product were copied, the position of the reaction should be correct
        return

    if mr_id in positioned_connecting_s_ids:
        connecting_s_ids_to_use = [mr_id]
    elif mp_id in positioned_connecting_s_ids:
        connecting_s_ids_to_use = [mp_id]
    else:
        connecting_s_ids_to_use = positioned_connecting_s_ids

    poss = []
    connecting_s_ids_to_use_pos: Dict[str, np.ndarray] = {s_id:gu.get_predicted_layout(g, s_id).get_center() for s_id in connecting_s_ids_to_use}
    dir = [0,0]
    for s_id in connecting_s_ids_to_use:
        s_id_pos = connecting_s_ids_to_use_pos[s_id]
        for r_id1 in g[s_id]:
            if r_id1 != r_id and r_id1 not in ignored_r_ids:
                r_id1_pos = gu.get_predicted_layout(g, r_id1).get_center()
                dir = gr.utils.normalize(s_id_pos - r_id1_pos)
                pos = get_center(s_id_pos, dir)
                poss.append(pos)

    assert len(poss) > 0

    center = sum(poss) / len(poss)

    if settings.orthogonalize_inserted_reactions:
        # Try to orthogonalize the of the reaction
        if len(connecting_s_ids_to_use_pos) == 1:
            # If there is only one connecting species, align the reaction_center-species line with one of horizontal or vertical axis (whichever is closer)
            s_id_pos = connecting_s_ids_to_use_pos[connecting_s_ids_to_use[0]]
            current_dir = center - s_id_pos
            if current_dir[0] == 0 and current_dir[1] == 0:
                # It can happen that there are two reactions which are positioned against each other on a horizontal/vertical line with the connecting specis in their center.
                # The center of r_id reaction then is placed exactly at position of the connecting species
                # TODO: -ortho is also a possibility. This should probably be another strategy
                dir = gr.utils.orthogonal(dir)
            else:
                if gr.utils.is_more_horizontal(current_dir):
                    dir = np.array([1, 0]) if current_dir[0] > 0 else np.array([-1, 0])
                else:
                    dir = np.array([0, 1]) if current_dir[1] > 0 else np.array([0, -1])

            center = get_center(s_id_pos, dir)


    r_lt.set_center(center)

def get_positioned_connecting_species(g:nx.MultiGraph, rg: nx.MultiGraph, r_id: str, ignored_r_ids: List[str]) -> List[str]:
    css = set()
    for r_id1 in rg[r_id]:
        if r_id1 in ignored_r_ids:
            continue
        css = css.union(gu.get_connecting_nodes(g, r_id1, r_id))

    return list(css)


def aux_lay_out_reaction_center(g, rg, r_id, ignored_r_ids):
    """

    :param g:
    :param rg:
    :param r_id:
    :param ignored_r_ids: Reaction center is determined based on positions of connecting species with neighboring
    reactions. However, only reactions whose centers have already been determined need to be considered.
    :return:
    """
    known_connecting_s_ids = get_positioned_connecting_species(g, rg, r_id, ignored_r_ids)
    if len(known_connecting_s_ids) > 0:
        # We might not have any konwn connectin species. It can happen that we have only a single rection mapped
        # and then there is no copied connecting species based on which we could adjust layout of the current reaction
        lay_out_reaction_center(g, r_id, known_connecting_s_ids, ignored_r_ids)
    return known_connecting_s_ids

def get_ods_nbs_not_copied(g: nx.MultiGraph, r_id: str) -> Dict[str,str]:

    rv = {}
    ods = gu.get_ods_nbs(g, r_id)
    for s_id in ods:
        if not gu.get_predicted_layout(g, s_id).is_copied():
            rv[s_id] = ods[s_id]
    return rv

StrategyType = Dict[str, np.ndarray]


def get_ods_strategies(g: nx.MultiGraph, r_ids: List[str])-> List[StrategyType]:
    rv: List[StrategyType] = []  # stores strategies (positions) for every reaction
    ods_strategies = []

    for r_id in r_ids:
        ods_strategies.append(layout_ods_for_inserted_reaction(g, r_id))
    # now we combine the available strategies for every reaction into a list with all possible combinations for rection
    ods_strategies_combinations: List[Tuple[StrategyType]] = list(product(*ods_strategies))
    for c in ods_strategies_combinations:
        # a strategy combination is a list of strategies for reaction and it needs to be combined into a single dictionar
        strategy = {}
        for d in c:
            strategy = {**strategy, **d}
        rv.append(strategy)

    return rv


def get_ods_r_ids_for_layout(g, r_ids: Collection[str]):
    return sorted([r_id for r_id in r_ids if len(get_ods_nbs_not_copied(g, r_id)) > 0])


def join_strategies(*strategies) -> List[StrategyType]:
    """
    Combines strategies. If we have two strategies lists [[a:1, b:2],[a:3,b:4]] and [[c:2]] then we are interested
    in the product of the strategies, i.e.: [[a:1, b:2, c:2], [a:3, b:4, c:2]]

    :param strategies: each argument is a list of strategies, i.e. list different possible positions for a given
    set of species/reactions. I.e. each strategy in a given strategy list deals with the same list of entities.
    It is expected that each of the strategies represented by different parameters deal with non-intersecting
    sets of entities.
    :return: A list of strategies, with all the possible combinations of strategies.
    """
    s_non_empty = [s for s in strategies if len(s) > 0]
    ixs = [list(range(len(s))) for s in s_non_empty]
    if len(ixs) == 0:
        return []

    jss: List[StrategyType] = []
    for s_ix_comb in product(*ixs):
        js = {}
        for i, ss in enumerate(s_non_empty):
            s = ss[s_ix_comb[i]] # obtain i-th strategy in the given strategy list
            if len(set(js).intersection(s)) != 0:
                asdf = 1
            assert len(set(js).intersection(s)) == 0
            js.update(s)
        jss.append(js)
    return jss


def get_layout_cc_strategies(g_res: nx.MultiGraph, tgt: rg.ReactionGraph, cc: List[str], last_not_laid_boundary_r: List[str] = []) -> List[StrategyType]:
    """
    Gets possible positions for reactions and connecting species in the provided connected component (in the target reaction graph).
    This is meant as a recursive procedure where in each iteration we lay out centers of the reactions on the border
    of the CC which are not in the CC and connecting nodes on the border. The reason why we need to lay out also centers of
    already laid out reaction is that these centers might not have been laid out correctly as the positoins of some of their
    species were not known (the species connecting these reactions with the reactions in CC). Correct positioning of
    reaction centers is important since they are used to lay out the reactions in CC.
    border with not yet mapped reaction
    :param g_res:
    :param tgt:
    :param _cc:
    :param last_not_laid_boundary_r:
    :return:
    """

    tgt_rg: nx.MultiGraph = tgt.get_reaction_graph()

    strategies: List[StrategyType] = []
    strategy_r: StrategyType = {}

    poss_r_original: Dict[str, np.ndarray] = {}
    poss_s_original = {}

    eb = sorted(list(nx.edge_boundary(tgt_rg, cc)))
    nl_boundary_r = sorted(set([e[0] for e in eb]))  # not laid reaction, i.e. on the inside of the CC boundary
    pl_boundary_r = sorted(set([e[1] for e in eb]).difference(cc))  # partially laid reactions, i.e. on the "outside" of the CC boundary the tgt_rg contains also the reactions to be re-laid out so these need to be removed

    boundary_r = set(pl_boundary_r).union(nl_boundary_r)

    ss = layout_strategies_for_dead_end_reactions(
        g_res=g_res,
        tgt=tgt,
        cc=cc,
        last_boundary_r=last_not_laid_boundary_r,
        poss_r_original=poss_r_original,
        poss_s_original=poss_s_original)
    strategies = join_strategies(strategies, ss)

    if len(cc) == 0:
        return strategies
        # before finishing with the connecting component we need to lay out the center of the last set of reactions
        # positions of these centers are dependent on the just laid out species, i.e. current combination/strategy
        ss = get_layout_cc_strategies_last_layer(g=g_res, tgt_rg=tgt_rg, last_not_laid_boundary_r=last_not_laid_boundary_r)
        strategies = join_strategies(strategies, ss)
    else:

        cn_pos_alternatives: Dict[str,List[np.ndarray]] = get_connecting_species_pos_alternatives(g=g_res,
                                                                                                  r_ids=pl_boundary_r,
                                                                                                  poss_r_original=poss_r_original,
                                                                                                  strategy_r=strategy_r,
                                                                                                  target_r_ids=cc,
                                                                                                  not_laid_r_ids= boundary_r,
                                                                                                  tgt_rg=tgt_rg)
        assert len(cn_pos_alternatives) > 0



        ss = strategies_for_boundary_species(g_res=g_res,
                                             tgt=tgt,
                                             cc=cc,
                                             cn_pos_alternatives=get_pos_combinations(cn_pos_alternatives),
                                             poss_s_original=poss_s_original,
                                             pl_boundary_r=pl_boundary_r,
                                             nl_boundary_r=nl_boundary_r,
                                             do_recursion=True)
        strategies = join_strategies(strategies, ss, [strategy_r])

    # Change back positions of the species so that the graph is in the state as before trying given strategy
    for id in poss_s_original:
        gu.get_predicted_layout(g_res, id).set_center(poss_s_original[id], recompute_reaction_layout=False)

    # Change back positions of the species so that the graph is in the state as before trying given strategy
    for id in poss_r_original:
        gu.get_predicted_layout(g_res, id).set_center(poss_r_original[id])

    return strategies

def strategies_for_boundary_species(
        g_res: nx.MultiGraph,
        tgt: rg.ReactionGraph,
        cc: List[str],
        cn_pos_alternatives: List[Dict[str, np.ndarray]],
        poss_s_original: Dict[str, np.ndarray],
        pl_boundary_r: List[str],
        nl_boundary_r: List[str],
        do_recursion: bool):

    strategies:List[StrategyType] = []
    # With connecting species set up we can lay out ODS from the laid border reactions if these were inserted
    r_ids_for_ods_layout = sorted(get_ods_r_ids_for_layout(g_res, pl_boundary_r))

    new_cc = list(set(cc).difference(nl_boundary_r))

    for ix_alterantive, positions in enumerate(cn_pos_alternatives):
        # For each set of the possible positions of the connecting species on the border
        strategy_s: StrategyType = {}

        for id, pos in positions.items():
            strategy_s[id] = pos
            s_lt: be.SpeciesLayout = gu.get_predicted_layout(g_res, id)
            if ix_alterantive == 0:
                poss_s_original[id] = s_lt.get_center()
            s_lt.set_center(pos, recompute_reaction_layout=False)

        if len(r_ids_for_ods_layout) > 0:

            for r_id in r_ids_for_ods_layout:
                # When laying out connecting species, the reaction center might have changed and needs to be updated
                # as position of ODS depends on it. However, this does not apply when one of the ODS is the main reactant
                # or product because in that case we would compute the reaction center based on invalid coordinates
                r_lt:be.ReactionLayout = gu.get_predicted_layout(g_res, r_id)
                ods = gu.get_ods_nbs(g_res, r_id)
                if r_lt.get_main_product_id() not in ods and r_lt.get_main_reactant_id() not in ods:
                    gu.get_predicted_layout(g_res, r_id).recompute_layout()

            strategies_ods = get_ods_strategies(g_res, r_ids_for_ods_layout)

            for ix_ods_strategy, strategy_ods in enumerate(strategies_ods):

                # Now we apply a strategy and call recursion. This needs to be done because strategies of the following
                # reactions can depend on current ODS strategy as one of the ODSs can be a reactant and product
                for s_id in strategy_ods:
                    lt = gu.get_predicted_layout(g_res, s_id)
                    if ix_ods_strategy == 0:
                        # If this is the first strategy we are exploring, we need to remember the original position of the affected ODS
                        poss_s_original[s_id] = lt.get_center()
                    lt.set_center(strategy_ods[s_id])

                ss = get_layout_cc_strategies(g_res=g_res, tgt=tgt, cc=new_cc, last_not_laid_boundary_r=nl_boundary_r) if do_recursion else []
                strategies += join_strategies(ss, [strategy_s], [strategy_ods])

        else:
            ss = get_layout_cc_strategies(g_res=g_res, tgt=tgt, cc=new_cc, last_not_laid_boundary_r=nl_boundary_r) if do_recursion else []
            strategies += join_strategies(ss, [strategy_s])

    return strategies


def layout_strategies_for_dead_end_reactions(g_res: nx.MultiGraph,
                                             tgt: rg.ReactionGraph,
                                             cc: List[str],
                                             last_boundary_r: List[str],
                                             poss_r_original: Dict[str, np.ndarray],
                                             poss_s_original: Dict[str, np.ndarray]):

    # By removing reactions in the not-laid boundary form cc we expect that they will then be laid out in the
    # next iteration when they will be treated as the other side of the boundary and laid out. However, there
    # is a special situation when this is not true. Consider the following connected not-laid reactions situation:
    #  B - C - D - E and let B, C, D be connected to A which is laid. Then B and C are connected by a species which
    # would not be laid out as B and C will be removed as they are part of the not laid boundary, but there is no
    # connection to the rest of the CC. There is only the connection between D and E and thus D will be laid out in the
    # next iteration and with it the connecting species between D and E, but not the species which connect B-C and C-D.
    # To prevent this situation, we recursively call the layout strategy function with B, C being the last_not_laid_boundary_r.

    if len(last_boundary_r) == 0:
        return []

    strategy_r = {}

    if len(cc) == 0:
        dead_end_r = last_boundary_r
    else:
        dead_end_r = []

        cc_nb = set()
        for r_id in cc:
            cc_nb = cc_nb.union(g_res[r_id])

        for r_id in last_boundary_r:
            if len(cc_nb.intersection(g_res[r_id])) == 0:
                dead_end_r.append(r_id)

    cn_pos_alternatives: Dict[str, List[np.ndarray]] = get_connecting_species_pos_alternatives(
        g=g_res,
        r_ids=dead_end_r,
        poss_r_original=poss_r_original,
        strategy_r=strategy_r,
        target_r_ids=dead_end_r,
        not_laid_r_ids=list(set(cc).union(dead_end_r)),
        tgt_rg=tgt.get_reaction_graph())



    ss = strategies_for_boundary_species(g_res=g_res,
                                         tgt=tgt,
                                         cc=cc,
                                         cn_pos_alternatives=get_pos_combinations(cn_pos_alternatives),
                                         poss_s_original=poss_s_original,
                                         pl_boundary_r=dead_end_r,
                                         nl_boundary_r=[],
                                         do_recursion=False)
    return join_strategies(ss, [strategy_r])


def get_layout_cc_strategies_last_layer(g: nx.MultiGraph, tgt_rg, last_not_laid_boundary_r):
    strategies: List[StrategyType] = []
    strategy_r: StrategyType = {}
    poss_r_original: Dict[str, np.ndarray] = {}

    for r_id in last_not_laid_boundary_r:
        r_lt: be.ReactionLayout = gu.get_predicted_layout(g, r_id)
        poss_r_original[r_id] = r_lt.get_center()
        aux_lay_out_reaction_center(g, tgt_rg, r_id, [])
        strategy_r[r_id] = r_lt.get_center()

    ods_r_ids = get_ods_r_ids_for_layout(g, last_not_laid_boundary_r)
    if len(ods_r_ids) > 0:
        for strategy_ods in get_ods_strategies(g, ods_r_ids):
            strategies.append({**strategy_r, **strategy_ods})
    else:
        strategies.append(strategy_r)

    for id in poss_r_original:
        gu.get_predicted_layout(g, id).set_center(poss_r_original[id])

    return strategies


def get_connecting_species_pos_alternatives(g: nx.MultiGraph, r_ids: List[str], poss_r_original,
                                            strategy_r, target_r_ids, not_laid_r_ids, tgt_rg)->Dict[str, List[np.ndarray]]:
    """

    :param g:
    :param r_ids: Ids of reactions centers of which need to determined
    :param poss_r_original:
    :param strategy_r:
    :param target_r_ids: Inserted reactions ids which have not yet been laid out. This is used to position only species which
    connect the r_ids to these reactions, i.e. only what has not yet been laid out.
    :param not_laid_r_ids: Reactions which  need to be laid out. These include also mapped reactions, but
    which need to be reconsidered in the light of the need to lay out the inserted reactions. I.e. reactions on the edge boundary.
    :param tgt_rg:
    :return:
    """
    cn_poss: Dict[str, List[List[np.ndarray]]] = {}  # keeps for every connecting species a list of list of positions. For a species there can be n possible position for each of the m reactions

    cn_ids_new = set()
    for r_id in r_ids:

        r_lt: be.ReactionLayout = gu.get_predicted_layout(g, r_id)
        poss_r_original[r_id] = r_lt.get_center()
        known_connecting_s_ids = aux_lay_out_reaction_center(g, tgt_rg, r_id, ignored_r_ids=not_laid_r_ids)
        strategy_r[r_id] = r_lt.get_center()

        for not_laid_r_id in set(tgt_rg[r_id]).intersection(target_r_ids): # only for neighboring reactions which have not been laid out yet
            cn_ids_aux = set(gu.get_connecting_nodes(g, not_laid_r_id, r_id)).difference(known_connecting_s_ids)
            cn_ids = [] #ids of species position of which we are interested in
            for cn_id in cn_ids_aux:
                cn_lt = gu.get_predicted_layout(g, cn_id)
                if cn_lt.is_copied():
                    # If the node is copied from template it is the connecting species at the boundary of a reaction which
                    # was inserted and the position of the connecting species was transferred from template. In such case, we
                    # simply keep the position
                    cn_poss[cn_id] = [[cn_lt.get_center()]]
                else:
                    cn_ids.append(cn_id)
                    cn_ids_new.add(cn_id)
            for cn_id, cn_pos in get_pos_for_connecting_species(g, r_id, cn_ids, known_connecting_s_ids).items():
                if cn_id not in cn_poss:
                    cn_poss[cn_id] = []
                cn_poss[cn_id].append(cn_pos)

            # cn_poss = {**cn_poss, **get_pos_for_connecting_species(g, r_id, cn_ids, known_connecting_s_ids)}

    # Get the average position for each connecting specis. Since for modifiers we consider multiple positions,
    # for a species, we can have multiple average positions (that is the reason for havein dictionary of lists
    cn_average_poss:Dict[str,List[np.ndarray]] = {}
    for id in cn_poss:
        cn_average_poss[id] = []
        pos_combitnations = list(product(*cn_poss[id]))
        for comb in pos_combitnations:
            cn_average_poss[id].append(sum(comb)/len(comb))

    return cn_average_poss


def get_pos_combinations(positions: Dict[str,List[np.ndarray]]) -> List[Dict[str,np.ndarray]]:

    rv: List[Dict[str, List[np.ndarray]]] = []
    cn_average_poss_keys = list(positions)  # ids of the connecting species
    cn_average_poss_values = list(positions.values())  # contains list of possible positions for every connecting species

    for combination in product(*cn_average_poss_values):
        rv.append(dict(zip(cn_average_poss_keys, combination)))

    return rv




def inter_layer_connecting_species_strategies(g: nx.MultiGraph, r_ids: List[str], poss_r_original,
                                  strategy_r, cc, r_ids_to_lay_out, tgt_rg) -> Dict[str,List[np.ndarray]]:
    NotImplemented

def dead_end_reactions_strategies(g: nx.MultiGraph, r_ids: List[str], poss_r_original,
                                  strategy_r, cc, r_ids_to_lay_out, tgt_rg) -> Dict[str,List[np.ndarray]]:
    # to remember positions of the reactions and also species, both ODS and connecting (connecting r_ids)
    NotImplemented


def pick_best_strategy(g_res, strategies: List[StrategyType]) -> int:

    if len(strategies) == 0:
        assert False

    if len(strategies) == 1:
        return 0

    def get_lines(g: nx.MultiGraph, r_ids: List[str]) -> List[gr.Line]:
        rv: List[gr.Line] = []
        ll = [gu.get_predicted_layout(g, id).get_all_lines_idealized() for id in r_ids]
        for l in ll:
            rv += l
        return rv


    s_id_g = gu.get_all_species_ids(g_res)
    r_id_g = gu.get_all_reaction_ids(g_res)
    s_id_used = list(set(s_id_g).intersection(strategies[0]))
    s_id_not_used = list(set(s_id_g).difference(strategies[0]))
    r_id_used = list(set(r_id_g).intersection(strategies[0]))
    r_id_not_used = list(set(r_id_g).difference(strategies[0]))
    rects_g = [gu.get_predicted_layout(g_res, id).get_bb().get() for id in s_id_not_used]
    lines_g = get_lines(g_res, r_id_not_used)

    min_val = sys.maxsize
    min_line_length = sys.maxsize
    min_ix = -1

    ix = -1
    cs = remember_current_positions_for_strategy(g_res, strategies[0].keys())
    for s in strategies:
        ix += 1
        cnt = 0
        line_length = 0

        apply_strategy(g_res, s)

        rects: List[gr.Rectangle] = []
        for s_id in s_id_used:
            rects.append(gr.Rectangle.from_point(s[s_id], settings.render.species_glyph_width, settings.render.species_glyph_height))
        for r1 in rects:
            for r2 in rects_g:
                if r1.intersects_rectangle(r2):
                    cnt +=1
            for l in lines_g:
                if r1.intersects_line(l):
                    cnt += 1

        lines: List[gr.Line] = get_lines(g_res, r_id_used)
        for l1 in lines:
            line_length += l1.length()
            for l2 in lines_g:
                if l1.intersects_line(l2):
                    cnt +=1
            for r in rects_g:
                if l1.intersection_rectangle(r) is not None:
                    cnt +=1

        if cnt < min_val or cnt == min_val and line_length < min_line_length:
            min_val = cnt
            min_line_length = line_length
            min_ix = ix

    apply_strategy(g_res, cs)

    return min_ix


# def enrich_layout_strategy_with_ods(g: nx.MultiGraph, tgt: rg.ReactionGraph, s:StrategyType, cc:List[str]) -> List[StrategyType]:
#
#
#     r_ids_ods = [r_id for r_id in cc if len(gu.get_ods_nbs(g, r_id)) > 0]
#     if len(r_ids_ods) == 0:
#         return [s]
#
#     rv: List[StrategyType] = []
#
#     for id in s:
#         gu.get_predicted_layout(g, id).set_center(s[id])
#
#     # The positions need to be laid out in the order in which the reactions were being added because it can happen
#     # that one of the ODS is reactant or product and then position of ODS of the next reaction depends on these positions
#     r_id_ods_ordered = []
#     ccc = list(cc)
#     tgt_rg = tgt.get_reaction_graph()
#     while len(ccc) > 0:
#         eb = nx.edge_boundary(tgt_rg, ccc)
#         ccc_boundary = [e[0] for e in eb]
#         r_id_ods_ordered += ccc_boundary
#         for b in ccc_boundary:
#             ccc.remove(b)
#
#     tgt.get_reaction_graph()
#
#     ods_strategies: List[StrategyType] = []
#     for r_id in r_id_ods_ordered:
#         ods_strategies.append(layout_ods_for_inserted_reaction(g, r_id))
#         # TODO: since layout of ODS of following reactions might depend on this one in case one of the ODS is reactant or product,
#         # we need to store the new ODS positions into the graph. We can use any strategy since the reactant and product have only one possible position
#     combinations:List[Tuple[StrategyType]] = list(product(*ods_strategies))
#     # Each combination contains one dictionary for every reaction => List of Tuples
#
#     for c in combinations:
#         for d in c:
#             rv.append({**d, **s})
#
#     return rv


# def enrich_layout_strategies_with_ods(g: nx.MultiGraph, tgt: rg.ReactionGraph, strategies: List[StrategyType], cc:List[str]) -> List[StrategyType]:
#
#     if len(strategies) == 0:
#         return strategies
#
#     rv: List[StrategyType] = []
#
#     # Remember the original positions
#     original_poss = {}
#     for id in strategies[0]:
#         original_poss[id] = gu.get_predicted_layout(g, id).get_center()
#
#     # Enrich each of the strategies
#     for s in strategies:
#         rv += enrich_layout_strategy_with_ods(g, tgt, s, cc)
#
#     # Put the original positions back
#     for id in original_poss:
#         gu.get_predicted_layout(g, id).set_center(original_poss[id])
#
#     return rv

def discern_reaction_species(g: nx.MultiGraph, r_ids: Set[str]):
    for r_id in r_ids:
        r_pos = gu.get_predicted_layout(g, r_id).get_center()
        for s_id in g[r_id]:
            s_pos = gu.get_predicted_layout(g, s_id).get_center()
            if gr.utils.equal(r_pos, s_pos):
                shift = [random() - 0.5, random() - 0.5]
                if gr.utils.equal(r_pos, [0,0]):
                    shift[0] = 0.1
                s_pos += shift #s_pos is np.array so it is an object, by changing

def layout_ins_cc(g_res: nx.MultiGraph, tgt: rg.ReactionGraph, cc: List[str]):
    # When determining positions of inserted reactions, we compare positions of reactions with position of their species
    # to obtain the direction vector (get_reaction_direction function). However, it is necessary for the reaction and
    # its species to share the position. Therfore, if this happens for any of the reaction, we need to shift the positions
    # slightly so that the direction vector is not zero.
    cc_neighborhood = [e[1] for e in nx.edge_boundary(tgt.get_reaction_graph(), cc)]
    discern_reaction_species(g_res, set(cc).union(cc_neighborhood))

    #TODO: limit the number of possible explored strategies
    strategies: List[StrategyType] = get_layout_cc_strategies(g_res, tgt, cc)

    # As soon as all the reactions have their centers and connecting species set up, we can lay out one degree species
    # strategies = enrich_layout_strategies_with_ods(g_res, tgt, strategies, cc)

    if len(strategies) == 0:
        # This is possible if the contains only one reaction whose all species have been laid out, i.e. the target adds reaction
        # but all its species are already in tempalte
        assert len(cc) == 1
        return


    ix_best = pick_best_strategy(g_res, strategies)
    strategy = strategies[ix_best]

    #Check if all the reactions were positioned
    # for r_id in cc:
    #     if r_id not in strategy:
    #         assert False

    apply_strategy(g_res, strategy)


def remember_current_positions_for_strategy(g_res: nx.MultiGraph, s_ids: List[str]) -> StrategyType:
    cp: StrategyType = {}
    for id in s_ids:
        cp[id] = gu.get_predicted_layout(g_res, id).get_center()
    return cp

def apply_strategy(g_res: nx.MultiGraph, strategy:StrategyType):
    for id in strategy:
        layout = gu.get_predicted_layout(g_res, id)
        if isinstance(layout, be.SpeciesLayout):
            layout.set_center(strategy[id], recompute_reaction_layout=False)
        else:
            layout.set_center(strategy[id])

def reposition_inserted_reactions(g_res: nx.multigraph, tgt: rg.ReactionGraph, stats:Stats):

    tgt_rg = tgt.get_reaction_graph()

    ins_r_ids = []
    ins_s_ids = []
    for id in g_res:
        if not gu.get_predicted_layout(g_res, id).is_copied():
            if gu.is_species(g_res, id):
                ins_s_ids.append(id)
            else:
                ins_r_ids.append(id)

    if len(ins_r_ids) == 0:
        return
    # create graph of not yet mapped reactions
    ins_r_ids = sorted(ins_r_ids)
    ins_rg = nx.subgraph(tgt_rg, ins_r_ids)
    # extract connected components from that graph, i.e. sets of reactions which need to be laid out together
    ccs = [list(cc) for cc in nx.algorithms.components.connected_components(ins_rg)]
    for cc_ins_g in sorted(ccs):
        # the content of CC needs to be sorted so that the outcome is deterministic
        layout_ins_cc(g_res, tgt, sorted(cc_ins_g))

    for r_id in gu.get_all_reaction_ids(g_res):
        gu.get_node_data(g_res.nodes[r_id]).get_layout(be.LAYOUT_TYPE.PREDICTED).recompute_layout()

    return


    # FDP SOLUTION

    n_ids = list(g.nodes())
    poss = {}
    copied = []
    for n_id in n_ids:
        l = gu.get_predicted_layout(g, n_id)
        poss[n_id] = l.get_center()
        if l.is_copied():
            copied.append(n_id)
    fixed = list(n_ids)
    for r_id in gu.get_all_reaction_ids(g):
        if r_id not in copied:
            fixed.remove(r_id)
            for s_id in gu.get_reaction_species(g, r_id).get_ids():
                fixed.remove(s_id)


    g_temp = nx.Graph()
    g_temp.add_nodes_from(n_ids)
    edges = combinations(n_ids, 2)
    g_temp.add_edges_from(edges)

    shortest_paths = nx.floyd_warshall(g)
    for u,v in g_temp.edges:
        g_temp[u][v]['weight'] = 1/shortest_paths[u][v]*0.1

    new_pos = nx.spring_layout(g_temp, k=80, pos=poss, fixed=fixed, seed=42, iterations=50000)
    for n_id in new_pos:
        pos = new_pos[n_id]
        gu.get_predicted_layout(g, n_id).set_center(np.array(pos))


def single_species_layout(tgt: nx.MultiGraph) -> nx.MultiGraph:
    assert len(tgt) == 1

    g_res = nx.MultiGraph()
    s_id = list(tgt.nodes())[0]
    gu.copy_node(tgt, g_res, s_id)
    set_predicted_position(g_res.nodes[s_id], pos=np.array([0, 0]))

    return g_res


def transfer_layout(tgt: rg.ReactionGraph, tmp: rg.ReactionGraph or None, mapping, do_beautify=False) -> nx.MultiGraph:

    if tmp is None:
        return single_species_layout(tgt.get_original())


    # tgt_rg = tgt.get_reaction_graph()
    tgt_orig = tgt.get_original()
    # tmp_rg = tmp.get_reaction_graph()
    tmp_orig = tmp.get_original()

    tmp_stats = get_graph_stats(tmp_orig)

    md = get_mapping_dicts(mapping)

    # mapping between nodes in target and result
    nodes_mapping = NodesMapping()

    g_res = nx.MultiGraph()  # a resulting graph which is being gradually built (positions of mapped nodes copied, the rest computed)

    ###################################################
    ############# add reactions to target #############
    ##################################################

    unmapped_reactions = tl_mapped_reactions(g_res, tgt, tmp, mapping, nodes_mapping)
    tl_unmapped_reactions(g_res, tgt, tmp, unmapped_reactions, tmp_stats, nodes_mapping)

    ##################################################
    #############  add species to target #############
    ##################################################

    laid_out_rs_tgt, laid_out_rs_tmp = tl_mapped_connecting_species(g_res, tmp, tgt, md, unmapped_reactions, nodes_mapping)
    tl_not_mapped_connecting_species(g_res, tmp, tgt, md, laid_out_rs_tgt, laid_out_rs_tmp, tmp_stats, nodes_mapping)

    tl_one_degree_species(g_res, tmp, tgt, md, laid_out_rs_tgt, laid_out_rs_tmp, tmp_stats, nodes_mapping)

    for r_id in gu.get_all_reaction_ids(g_res):
        gu.get_node_data(g_res.nodes[r_id]).get_layout(be.LAYOUT_TYPE.PREDICTED).recompute_layout()

    reposition_inserted_reactions(g_res, tgt, tmp_stats)

    for r_id in gu.get_all_reaction_ids(g_res):
        fix_ods_intersections(g_res, r_id)


    # for r_id in gu.get_all_reaction_ids(g_res):
    #     gu.get_node_data(g_res.nodes[r_id]).get_layout(be.LAYOUT_TYPE.PREDICTED).recompute_layout()

    if do_beautify:
        cnt = gu.count_intersections(g_res)
        logging.info("Overlaps before beautification: {}".format(cnt))

        bc = bt.BeautificationChain(g_res)
        bc.add([
            # BeautificationStep(BEAUTY_OPERATION_TYPE.PUSH_AWAY_SIDE_SPECIES),

            # bt.BeautificationOp(bt.BEAUTY_OPERATION_TYPE.ALIGN_TO_GRID),
            # bt.BeautificationOp(bt.BEAUTY_OPERATION_TYPE.REMOVE_EMPTY_BANDS),
            # bt.BeautificationOp(bt.BEAUTY_OPERATION_TYPE.HANDLE_REACTION_SPECIES_OVERLAPS)

            bt.BeautificationOp(bt.BEAUTY_OPERATION_TYPE.DEDUPLICATE),
        ])
        bc.run()
        g_res = bc[-1].g


        # beautify(g_res, tmp_stats)

        # beautify(g_res)

        cnt = gu.count_intersections(g_res)
        logging.info("Overlaps after beautification: {}".format(cnt))

    validate_layout(g_res, tgt_orig)

    return g_res


def layout_ods_for_inserted_reaction(g: nx.MultiGraph, r_id) -> List[Dict[str, np.ndarray]]:

    # Layout of ODS for an inserted reaction.
    # Lay out reactants and products on the oposite sides on a line perpendicular  to the main reaction lines,
    # which are the lines connecting center of reaction to the COM of reactants/products which are either
    # connecting other reactions or were copied from the template

    # TODO: Correct way would be to ensure that when positioinig a species and reactions within the strategy searching, they do not share position
    discern_reaction_species(g, [r_id])

    r_s_ids = gu.get_reaction_species(g, r_id)
    # The center needs already to be set up
    r_lt = gu.get_predicted_layout(g, r_id)
    r_center = r_lt.get_center()

    laid_out_s_ids = []
    for s_id in gu.get_all_species_ids(g):
    # for s_id in g[r_id]:
        l = gu.get_predicted_layout(g, s_id)
        if l.is_copied() or len(g[s_id]) > 1:
            laid_out_s_ids.append(s_id)

    mr_id = r_lt.get_main_reactant_id()
    mp_id = r_lt.get_main_product_id()

    if mr_id not in laid_out_s_ids and mp_id not in laid_out_s_ids or mr_id == mp_id:
        # If there is no main reactant or product which could orient the reaction we use all reactants and products and average
        r_ids_for_dir = r_s_ids.reactantIds
        p_ids_for_dir = r_s_ids.productIds
    else:
        r_ids_for_dir = [mr_id]
        p_ids_for_dir = [mp_id]

    # dir_reactants = get_reaction_direction(g, laid_out_s_ids, r_s_ids.reactantIds, r_center)
    # dir_products = get_reaction_direction(g, laid_out_s_ids, r_s_ids.productIds, r_center)
    dir_reactants = get_reaction_direction(g, laid_out_s_ids, r_ids_for_dir, r_center)
    dir_products = get_reaction_direction(g, laid_out_s_ids, p_ids_for_dir, r_center)
    dir_modifiers = get_reaction_direction(g, laid_out_s_ids, r_s_ids.modifierIds, r_center)

    dist_reactants = settings.render.optimal_species_reaction_dist if dir_reactants is None else gr.utils.dist(gu.get_predicted_layout(g, mr_id).get_center(), r_center)
    dist_products = settings.render.optimal_species_reaction_dist if dir_products is None else gr.utils.dist(gu.get_predicted_layout(g, mp_id).get_center(), r_center)
    dist_modifiers = settings.render.optimal_species_reaction_dist if dir_modifiers is None else get_dist_to_closest(g,
                                                                                                                     r_center,
                                                                                                                     set(laid_out_s_ids).intersection(
                                                                                                                         r_s_ids.modifierIds))
    # if dir_reactants is not None:
    #     dir_modifiers = gr.utils.orthogonal(dir_reactants)
    # elif dir_products is not None:
    #     dir_modifiers = gr.utils.orthogonal(dir_products)

    dir_reactants, dir_products, dir_modifiers = impute_dirs(dir_reactants, dir_products, dir_modifiers)

    not_laid_reactant_ids = reorder_s_ids_preferernce(g, r_id, set(r_s_ids.reactantIds).difference(laid_out_s_ids))
    not_laid_products_ids = reorder_s_ids_preferernce(g, r_id, set(r_s_ids.productIds).difference(laid_out_s_ids))
    not_laid_modifiers_ids = reorder_s_ids_preferernce(g, r_id, set(r_s_ids.modifierIds).difference(laid_out_s_ids))

    taken_reactants: List[gr.Rectangle] = [gu.get_predicted_layout(g, s_id).get_bb().get() for s_id in laid_out_s_ids if s_id in r_s_ids.reactantIds]
    taken_products: List[gr.Rectangle] = [gu.get_predicted_layout(g, s_id).get_bb().get() for s_id in laid_out_s_ids if s_id in r_s_ids.productIds]
    taken_modifiers: List[gr.Rectangle] = [gu.get_predicted_layout(g, s_id).get_bb().get() for s_id in laid_out_s_ids if s_id in r_s_ids.modifierIds]
    taken = taken_reactants + taken_products + taken_modifiers

    #TODO when a group of species is laid out the taken_bb should be extended to encompass also the bbs of the new species
    s_id_pos = {**layout_on_perp_line(g, not_laid_reactant_ids, r_center, dir_reactants, taken, taken_reactants, line_distance=dist_reactants)}
    s_id_pos = {**s_id_pos, **layout_on_perp_line(g, not_laid_products_ids, r_center, dir_products, taken, taken_products, line_distance=dist_products)}
    if len(not_laid_modifiers_ids) > 0:
        s_id_pos_mod1 = {**s_id_pos, **layout_on_perp_line(g, not_laid_modifiers_ids, r_center, dir_modifiers, taken, taken_modifiers, line_distance=dist_modifiers)}
        s_id_pos_mod2 = {**s_id_pos, **layout_on_perp_line(g, not_laid_modifiers_ids, r_center, -dir_modifiers, taken, taken_modifiers, line_distance=dist_modifiers)}
        return [s_id_pos_mod1, s_id_pos_mod2]
    else:
        return [s_id_pos]


def layout_ods_de_novo_mapped(g_res: nx.MultiGraph, r_id, ods_ids, stats) -> Dict[str, np.ndarray]:
    """
    Layout not yet mapped ODS of a reaction which was mapped to a template reaction.
    """
    rv = {}

    if len(ods_ids) > 0:

        existing_poss = [get_node_pos(g_res.nodes[s_id]) for s_id in g_res[r_id]]
        poss = position_on_circle(len(ods_ids), gu.get_predicted_layout(g_res, r_id).get_center(),
                                  stats.med_one_degree_species_distance,
                                  existing_poss)
        for i in range(len(poss)):
            rv[ods_ids[i]] = poss[i]

    return rv


def get_ods_mapping(tgt_ods_ids: List[str], tgt_r_id: str, tgt_g: nx.MultiGraph, tmp_ods_ids: List[str], tmp_r_id: str, tmp_g: nx.MultiGraph) -> List[List[int]]:
    dm = get_node_distance_matrix(tgt_ods_ids, tgt_r_id, tgt_g, tmp_ods_ids, tmp_r_id, tmp_g)
    return list(linear_sum_assignment(dm))


def tl_one_degree_species(g_res: nx.MultiGraph, tmp:rg.ReactionGraph, tgt:rg.ReactionGraph, md,
                          laid_out_rs_tgt, laid_out_rs_tmp, tmp_stats, nodes_mapping:NodesMapping):
    tgt_rg = tgt.get_reaction_graph()

    tgt_rg_nodes = tgt_rg.nodes()

    tgt_orig = tgt.get_original()
    tmp_orig = tmp.get_original()

    tgt_orig_nodes = tgt_orig.nodes()
    tmp_orig_nodes = tmp_orig.nodes()

    for r_id_tgt in sorted(tgt_rg_nodes):

        ods_nbs_tgt = {**gu.get_ods_nbs(tgt_orig, r_id_tgt), **get_connecting_ods(r_id_tgt, g_res, tgt_orig, nodes_mapping)}
        ods_nbs_tgt_keys = sorted(list(ods_nbs_tgt.keys()))
        # If the procedure for laying out ODS is called multiple times, only the newly added species will be laid out
        for s_id in ods_nbs_tgt_keys:
            if rs_used(laid_out_rs_tgt, r_id_tgt, s_id):
                del ods_nbs_tgt[s_id]

        if len(ods_nbs_tgt) == 0:
            continue

        ods_nbs_tgt_keys = sorted(list(ods_nbs_tgt.keys()))

        r_id_tmp = md[0][r_id_tgt]
        if r_id_tmp:

            # FIRST LAY OUT THE MAPPED ODS

            # if given reaction was mapped to template, let's use munkers algorithm to find optimal mapping
            # between ods in target and template, lay out those and add the not mapped ods
            ods_nbs_tmp = sorted([s for s in gu.get_ods_nbs(tmp_orig, r_id_tmp) if not rs_used(laid_out_rs_tmp, r_id_tmp, s)])

            lsa = get_ods_mapping(ods_nbs_tgt_keys, r_id_tgt, tgt_orig, ods_nbs_tmp, r_id_tmp, tmp_orig)

            #lay out mapped ods
            tgt_nb_to_remove = [] #to keep track of ods which will still need to be mapped after application of the mapping
            for i in range(len(lsa[0])):
                ix_nbs_tgt = lsa[0][i]
                ix_nbs_tmp = lsa[1][i]
                tmp_nb = ods_nbs_tmp[ix_nbs_tmp]
                tgt_nb = ods_nbs_tgt_keys[ix_nbs_tgt]

                tgt_roles = gu.get_roles(tgt_orig, r_id_tgt, tgt_nb)
                tmp_roles = gu.get_roles(tmp_orig, r_id_tmp, tmp_nb)

                if not gu.roles_intersect(tgt_roles, tmp_roles):
                    continue

                tgt_nb_to_remove.append(tgt_nb)

                new_node = tgt_orig_nodes[tgt_nb]
                copy_predicted_layout(tmp_orig_nodes[tmp_nb], new_node)

                new_node_id = '{}_{}'.format(r_id_tgt, tgt_nb)
                # g_res.add_node(new_node_id, **new_node)
                add_node(g_res, nodes_mapping, new_node_id, tgt_nb, **new_node)
                for role in ods_nbs_tgt[tgt_nb]:
                    g_res.add_edge(r_id_tgt, new_node_id, role)
                mark_rs_used(laid_out_rs_tmp, r_id_tmp, tmp_nb)
                mark_rs_used(laid_out_rs_tgt, r_id_tgt, tgt_nb)

            for n in tgt_nb_to_remove:
                del ods_nbs_tgt[n]

            ods_nbs_tgt_keys = sorted(list(ods_nbs_tgt.keys()))

            # LAY OUT THE NOT YET MAPPED ODS
            poss = layout_ods_de_novo_mapped(g_res, r_id_tgt, ods_nbs_tgt_keys, tmp_stats)
        else:
            # IF THE REACTION WAS INSTERTED LAY OUT ALL THE ODS
            poss = layout_ods_de_novo_mapped(g_res, r_id_tgt, ods_nbs_tgt_keys, tmp_stats)
            # poss = layout_ods_de_novo_not_mapped(g_res, tgt_orig, r_id_tgt, ods_nbs_tgt)

        for s_id in ods_nbs_tgt_keys:
            new_node_id = '{}_{}'.format(r_id_tgt, s_id)
            new_node = copy.deepcopy(tgt_orig_nodes[s_id])
            set_predicted_position(new_node, poss[s_id])
            add_node(g_res, nodes_mapping, new_node_id, None, **new_node)

            for role in ods_nbs_tgt[s_id]:
                g_res.add_edge(r_id_tgt, new_node_id, role)

        
        


def fix_ods_intersections(g: nx.MultiGraph, r_id: str):
    """
    When finding positions for one degree species, the Munkers algorithm in combination with positioning on the circle
    does not considered possible
    line intersections which can be caused by laying some of the reactants closer to the products
    connection point in such a way that line intersection of reaction and products ods appear.

    The solution is to sort positions of all ods based on angle distance from the main_product-reaction_center
    line and then reassign the positions in the way that first, products, then modifiers and finally reactants
    are assigned positions from the sorted list.

    However, this procedure can not be carried out before the the ReactionLayout has been initalized which is
    only after all the species in the target graph have a position.

    #TODO similar fix might be needed also for connecting species, however that case is more complicated since one needs to take into account also the rest of the graph.

    :param g:
    :param r_id:
    :return:
    """
    #

    ods_nbs_tgt = set(gu.get_ods_nbs(g, r_id))
    # ods_nbs_tgt = [s_id for s_id in gu.get_ods_nbs(g, r_id) if gu.get_predicted_layout(g, s_id).is_copied()]

    if len(ods_nbs_tgt) <= 1:
        return

    ods_nbs_tgt_pos: Dict[str, np.ndarray] = {}
    for s_id in ods_nbs_tgt:
        ods_nbs_tgt_pos[s_id] = get_node_pos(g.nodes[s_id])


    rl: be.ReactionLayout = gu.get_node_data(gu.get_node(g, r_id)).get_layout(be.LAYOUT_TYPE.PREDICTED)
    poss_sorted = sort_ods_poss_by_angle_dist_from_center_cp(ods_nbs_tgt_pos.values(), rl)
    rs = gu.get_reaction_species(g, r_id)

    # The positions are sorted in the increasing angle distance from the center-reactant_connection_point
    # If there is only product_connection_point (=1 reactant) it is sorted in decreasing distance from center-product_connection_point

    i = 0

    ods_reactants_ids = ods_nbs_tgt.intersection(rs.reactantIds)
    ods_products_ids = ods_nbs_tgt.intersection(rs.productIds).difference(ods_reactants_ids)
    ods_modifiers_ids = ods_nbs_tgt.intersection(rs.modifierIds).difference(ods_reactants_ids).difference(ods_products_ids)

    poss_reactants = poss_sorted[:len(ods_reactants_ids)]
    poss_modifiers = poss_sorted[len(ods_reactants_ids):len(poss_sorted)-len(ods_products_ids)]
    poss_products = poss_sorted[len(poss_sorted)-len(ods_products_ids):]

    def assign_positions(g: nx.MultiGraph, s_ids: Collection[str], previous_pos: Dict[str, np.ndarray], new_poss: List[np.ndarray]):
        """
        Possitions are assigned so that if a species is positioned somewhere and that position is in the list of available positions
        we do not touch it. I.e. we try to touch the layout as least as possible.
        """
        aux_new_poss = [list(x) for x in new_poss] #convert to list so that we can easilly search in it
        to_reposition_s_ids = []
        for s_id in s_ids:
            pos = list(previous_pos[s_id])
            if pos in aux_new_poss:
                aux_new_poss.remove(pos)
            else:
                to_reposition_s_ids.append(s_id)


        i = 0
        for s_id in to_reposition_s_ids:
            set_node_pos(g.nodes[s_id], np.array(aux_new_poss[i]))
            i += 1

    assign_positions(g, ods_reactants_ids, ods_nbs_tgt_pos, poss_reactants)
    assign_positions(g, ods_modifiers_ids, ods_nbs_tgt_pos, poss_modifiers)
    assign_positions(g, ods_products_ids, ods_nbs_tgt_pos, poss_products)


def sort_ods_poss_by_angle_dist_from_center_cp(poss: List[np.ndarray], rl: 'be.ReactionLayout') -> List[np.ndarray]:
    pos_c = rl.get_center()
    # pos_mr = rl.get_main_reactant_pos()
    cnt_r = len(rl.get_reactants_ids())
    cnt_p = len(rl.get_products_ids())
    pos_pcp = rl.get_products_connection_pos()

    assert cnt_r > 0 or cnt_p > 0

    # v_c_mr = gr.utils.normalize(pos_mr - pos_c)
    if cnt_r > 1:
        v_c_cp = gr.utils.normalize(rl.get_reactants_connection_pos() - pos_c)
    elif cnt_p > 1:
        v_c_cp = gr.utils.normalize(rl.get_products_connection_pos() - pos_c)
    else:
        v_c_cp = gr.utils.normalize(rl.get_main_reactant_pos() - pos_c)

    pos_angle = []
    for pos in poss:
        v_c_p = gr.utils.normalize(pos - pos_c)
        angle = np.arccos(np.clip(np.dot(v_c_cp, v_c_p), -1.0, 1.0))
        if angle <= math.pi:
            pos_angle.append((angle, pos))
        else:
            pos_angle.append((2*math.pi-angle, pos))

    pos_sorted = [pa[1] for pa in sorted(pos_angle, key=lambda x:(x[0],x[1][0], x[1][1]))]
    if cnt_r == 1 and cnt_p > 1:
        # The positions are sorted based on angle distance from the product
        pos_sorted.reverse()

    return pos_sorted


def position_on_circle(n_points, center, radius, existing_points = []) -> np.ndarray:
    """
    Calculates positions of points on a circle centered in the center with given radius. If
    there are existing points, the new points are inserted into the biggest free sector. I.e.,
    the nodes are currently not distributed across multiple sectors.

    :param n_points:
    :param center:
    :param radius:
    :param existing_points: Positions of neighboring species
    :return:
    """
    # https://stackoverflow.com/questions/5300938/calculating-the-position-of-points-in-a-circle



    coords = []
    c = np.array(center)

    ep_rad = []
    for p in existing_points:
        v = gr.utils.normalize(p-c)
        angle_rad = np.arccos(v[0])
        if v[1] < 0:
            angle_rad = 2 * math.pi - angle_rad
        ep_rad.append(angle_rad)
    ep_rad.sort()

    if len(ep_rad) == 0:
        min_rad = 0
        rad_max_range = 2*math.pi
    elif len(ep_rad) == 1:
        min_rad = ep_rad[0]
        rad_max_range = 2 * math.pi
    else:
        rad_max_range = ep_rad[0] + (2 * math.pi - ep_rad[len(ep_rad)-1])
        min_rad = ep_rad[len(ep_rad)-1]
        for i in range(1, len(ep_rad)):
            if ep_rad[i] - ep_rad[i-1] > rad_max_range:
                min_rad = ep_rad[i-1]
                rad_max_range = ep_rad[i] - ep_rad[i-1]

    rad_int = rad_max_range / (n_points + 1) #plus two so that edge nodes do share radius with existing nodes
    for i in range(1, n_points+1):
        angle_rad = min_rad + i*rad_int
        coords.append([radius * math.cos(angle_rad) + center[0],
                       radius * math.sin(angle_rad) + center[1]])

    return np.array(coords)

def tl_mapped_reactions(g_res: nx.MultiGraph, tgt, tmp, mapping, nodes_mapping)->List[str]:
    # copy coordinates for mapped reactions

    tgt_rg_nodes = tgt.get_reaction_graph().nodes()
    tmp_rg_nodes = tmp.get_reaction_graph().nodes()

    unmapped_reactions = []
    for m in mapping:
        if m[0] and m[1]:
            new_node = copy.deepcopy(tgt_rg_nodes[m[0]])
            copy_predicted_layout(tmp_rg_nodes[m[1]], new_node)
            # add the reaction
            add_node(g_res, nodes_mapping, m[0], m[0], **new_node)
        elif not m[1] and m[0]:
            unmapped_reactions.append(m[0])

    return sorted(unmapped_reactions)

def tl_unmapped_reactions(g_res: nx.MultiGraph, tgt, tmp, unmapped_reactions:List[str], tmp_stats, nodes_mapping):
    """
    Reactions not mapped to anything - new coordinates need to be decided + the neighborhood
    should be updated to accomodate this insertion. If there is only one reaction inserted in between mapped
    reactions, then the new reaction can be mapped in the center, however, there can be more not mapped reactions
    which, moreover can have complex internal structure in terms of edges connecting them.
    Therefore, it makes sense to treat all not mapped reactions
    which create a connected component as a unit, which should be laid out between nodes determined by their edge
    boundary.

    One can think of multiple strategies how to lay out a cluster of not mapped reactions:

        1. Consider edge boundary consisting of edges where one reaction is already laid out while the second is not.
        Then we can come up with a set of rules how to get position of the second reactio. E.g. if they share a
        species which is the main product in the first reaction and main reactant in the second, this might tell us
        where the reaction should be positioned. Another case would be if they share a species which modifies the
        first reaction (then the position of the modulator is different and so is the position of second reaction).
        Another strategy needs to be devised when the not laid out reaction connects to multiple already laid out reactions.

        The advantage of this strategy is that we have control of the positioning, however one needs to take into account
        all the possible cases.

        2. Lay out all reactions in the cluster of not yet laid out reactions via force directed placement or similar
        approach with positions of already laid out notes to serve as anchors whose position can't change. The

        To get a reasonable positions of reactions, we need to consider positions of species as well. However,
        laying out species takes place only after we lay out reactions. A solution is to take connecting species positions of
        the mapped reactions from the template layout. The ratio is that position of these species won't change much in
        the target once they are laid out.

    :param tgt:
    :param tmp:
    :param unmapped_reactions:
    :return:
    """

    tmp_orig = tmp.get_original()
    tmp_orig_nodes = tmp_orig.nodes()
    tgt_orig = tgt.get_original()
    tgt_rg = tgt.get_reaction_graph()
    tgt_rg_nodes = tgt_rg.nodes()

    # create graph of not yet mapped reactions
    unmapped_reactions = sorted(unmapped_reactions)
    tgt_rg_unmapped = nx.subgraph(tgt_rg, unmapped_reactions)
    # extract connected components from that graph, i.e. sets of reactions which need to be laid out together
    # distortion = False

    for cc_rg_unmapped in list(nx.algorithms.components.connected_components(tgt_rg_unmapped)):
        eb = sorted(list(nx.edge_boundary(tgt_rg, cc_rg_unmapped)))
        assert(len(eb) > 0)
        # remember reactions which neighbor directly with laid out reactions
        laid_boundary_r = set([e[1] for e in eb])
        # not_laid_boundary_r = sorted(set([e[0] for e in eb]))
        for r in cc_rg_unmapped:
            new_node = copy.deepcopy(tgt_rg_nodes[r])
            add_node(g_res, nodes_mapping, r, r, **new_node)
            # g_res.add_node(r, **new_node)

        # Position the new reactions
        g_res_nodes = g_res.nodes()
        nodes = [g_res_nodes[n] for n in laid_boundary_r]

        com = get_center_of_mass(nodes)

        for r in cc_rg_unmapped:
            set_predicted_position(g_res_nodes[r], com, copied=False)
            set_predicted_position(g_res_nodes[r], np.array([0,0]), copied=False)


def tl_mapped_connecting_species(g_res: nx.MultiGraph, tmp, tgt, md, unmapped_reactions:List, nodes_mapping:NodesMapping):

    tgt_rg = tgt.get_reaction_graph()
    tmp_rg = tmp.get_reaction_graph()

    tmp_rg_ddup = tmp.get_reaction_graph()
    tmp_rg_dup = tmp.get_reaction_graph(False)

    tgt_orig = tgt.get_original()
    tmp_orig = tmp.get_original()

    tgt_orig_nodes = tgt_orig.nodes()
    tmp_orig_nodes = tmp_orig.nodes()

    laid_out_rs_tmp = list() #reaction_tmp, species_tmp
    laid_out_rs_tgt = list()

    cs_mappging: Dict[(str, str), str] = {} # connecting species mapping. Stores mapping between target reaction id, and target species id and the result graph species id

    def go_through_reactions(dup):
        """

        :param dup: Whether we lay out duplicated connecting species. That means species connected in target, but
        disconnected in template (due to duplication).
        The non-duplicated connecting species in template are the real connecting species because they
        connect two reactions not only in the deduplicated graph, but really in the layout graph.
        :return:
        """
        # take r1,r2 from which connect neighbors in duplicated tmp and lay out those first
        for (r1_tmp, r2_tmp) in sorted(tmp_rg_ddup.edges()):

            if r1_tmp not in md[1] or r2_tmp not in md[1]:
                continue

            r1_tgt = md[1][r1_tmp]
            r2_tgt = md[1][r2_tmp]

            if r1_tgt not in md[0] or r2_tgt not in md[0]:
                continue

            cn_tgt = gu.common_neighbors(tgt_orig, False, r1_tgt, r2_tgt)
            # nbs_tgt = list(set(cn_tgt['non-dup']))
            # nbs_tgt = [x for x in nbs_tgt if not already_used_triplet(laid_out_rs_tgt, r1_tgt, r2_tgt, x)]

            #for tgt, we are interested only for non-duplicated nodes, becaue if there is a duplication present, we use when layouting
            nbs_tgt = filter_common_species(laid_out_rs_tgt, r1_tgt, r2_tgt, cn_tgt['non-dup'])

            if len(nbs_tgt) == 0:
                continue

            nbs_tgt.sort()

            cn_tmp = gu.common_neighbors(tmp_orig, True, r1_tmp, r2_tmp)
            key = 'dup' if dup else 'non-dup'
           # filter out species which has already been laid out (each species is considered as many times as there are roles in which it is involved in the reactions)
            nbs_tmp = filter_common_species(laid_out_rs_tmp, r1_tmp, r2_tmp, cn_tmp[key])
            if len(nbs_tmp) == 0:
                continue
            nbs_tmp.sort()

            if dup:
                # We need to match the deduplicated set of nodes in tgt with set of duplicated nodes in tmp.
                # In order to do the mapping, we match the species in target with species
                # in one duplicated connecting species with one of the reactions and based on this mapping
                # we then duplicate the species in target.
                # Therefore we separate tmp neighbors into two sets based on which reaction they belong to and
                # use the smaller set as the one for matching. We use the smaller set because it might matter in situations
                # where a species is present multiple times in one of the reactions.

                nbs_tmp_dup = [gu.duplicate_attrs(tmp_orig, n) for n in nbs_tmp]
                r1_nbs_tmp = []
                for n in nx.all_neighbors(tmp_orig, r1_tmp):
                    if gu.duplicate_attrs(tmp_orig, n) in nbs_tmp_dup:
                        r1_nbs_tmp.append(n)
                r2_nbs_tmp = []
                for n in nx.all_neighbors(tmp_orig, r2_tmp):
                    if gu.duplicate_attrs(tmp_orig, n) in nbs_tmp_dup:
                        r2_nbs_tmp.append(n)

                r1_used = len(r1_nbs_tmp) <= len(r2_nbs_tmp)
                nbs_tmp = r1_nbs_tmp if r1_used else r2_nbs_tmp


            # TODO possibly a problem with using dupplicated vs deduplicated graphs, and thus different number of neighbors in the node distance function
            dm = get_node_distance_matrix(nbs_tgt, r1_tgt, tgt_orig, nbs_tmp, r1_tmp, tmp_orig)
            lsa = list(linear_sum_assignment(dm))

            # When we find mapping between a template node and target node we still
            # need to obtain the duplicated node in the template (i.e. the corresponding
            # node for the template node in the second reaction). It can happen that
            # there are multiple candidates (e.g. if there are two "identical" nodes
            # duplicated between two reactions). The following dictionary holds information
            # about how many times given species was duplicated between the two reactions
            # and used to go trhough all the species duplicates in the duplication process.
            cnt_used_tmp_nodes = {}

            for i in range(len(lsa[0])):
                ix_nbs_tgt = lsa[0][i]
                ix_nbs_tmp = lsa[1][i]
                tgt_nb = nbs_tgt[ix_nbs_tgt]
                tmp_nb = nbs_tmp[ix_nbs_tmp]


                if tmp_nb in cnt_used_tmp_nodes:
                    cnt_used_tmp_nodes[tmp_nb] += 1
                else:
                    cnt_used_tmp_nodes[tmp_nb] = 0

                if dup:
                    # Find corresponding duplicate to tmp_nb, where tmp_nb are deduplicated connecting
                    # species either from r1 or r2 (r1_used stores the information whether r1 was used or r2)
                    tmp_nb_dup = gu.get_duplicates(tmp_orig, tmp_nb, r2_nbs_tmp if r1_used else r1_nbs_tmp)[cnt_used_tmp_nodes[tmp_nb]]
                    if r1_used:
                        tmp_nb1 = tmp_nb
                        tmp_nb2 = tmp_nb_dup
                    else:
                        tmp_nb2 = tmp_nb
                        tmp_nb1 = tmp_nb_dup

                    # Create r1 and r2 neighbhours
                    for r_tmp, s_tmp in [(r1_tmp, tmp_nb1), (r2_tmp, tmp_nb2)]:

                        new_node_id = '{}__{}'.format(tgt_nb, s_tmp)

                        if new_node_id not in g_res:
                            new_node = copy.deepcopy(tgt_orig_nodes[tgt_nb])
                            copy_predicted_layout(tmp_orig_nodes[s_tmp], new_node)
                            add_node(g_res, nodes_mapping, new_node_id, tgt_nb, **new_node)
                            # g_res.add_node(new_node_id, **new_node)


                            #Connect the newly created species nodes with the corresponding reactions
                            r_tgt = md[1][r_tmp]
                            # add_edges(g_res, new_node_id, r_tgt, tmp_orig, s_tmp, r_tmp)

                            cnt_added_edges = add_edges(g_res, new_node_id, r_tgt, tgt_orig, tgt_nb, r_tgt, tmp_orig, s_tmp, r_tmp)
                            cs_mappging[(r_tgt, tgt_nb)] = new_node_id

                            mark_rs_used(laid_out_rs_tmp, r_tmp, s_tmp, cnt_added_edges)
                            mark_rs_used(laid_out_rs_tgt, r_tgt, tgt_nb, cnt_added_edges)
                else:

                    new_node_id = '{}__{}'.format(tgt_nb, tmp_nb)

                    if new_node_id not in g_res:

                        new_node = copy.deepcopy(tgt_orig_nodes[tgt_nb])
                        copy_predicted_layout(tmp_orig_nodes[tmp_nb], new_node)
                        # g_res.add_node(new_node_id, **new_node)
                        add_node(g_res, nodes_mapping, new_node_id, tgt_nb, **new_node)

                    # add edges between the new node and all reactions which connect to it in the duplicated template which transfer to a reaction

                    for r_tmp_nb in tmp_orig[tmp_nb]:
                        # for every reaction which is a neighbor of tmp_nb (the connecting species being inserted) in template
                        r_tgt_nb = md[1][r_tmp_nb] if r_tmp_nb in md[1] else None
                        if r_tgt_nb:
                            # if the reaction in template is mapped on a reaction in target, connect that reaction to the newly created node
                            # this will include at least reactions r1_tgt and r2_tgt
                            # add_edges(g_res, new_node_id, r_tgt_nb, tmp_orig, tmp_nb, r_tmp_nb)
                            cnt_added_edges = add_edges(g_res, new_node_id, r_tgt_nb, tgt_orig, tgt_nb, r_tgt_nb, tmp_orig, tmp_nb, r_tmp_nb)
                            cs_mappging[(r_tgt_nb, tgt_nb)] = new_node_id

                            mark_rs_used(laid_out_rs_tgt, r_tgt_nb, tgt_nb, cnt_added_edges)
                            mark_rs_used(laid_out_rs_tmp, r_tmp_nb, tmp_nb, cnt_added_edges)

    def connecting_species_of_inserted_reactions():

        for (r1_tgt, r2_tgt) in sorted(tgt_rg.edges()):
            if r1_tgt in unmapped_reactions and r2_tgt in unmapped_reactions:
                # If both reactions are inserted, then their connecting species could not have been laid out
                continue

            # One of the reactions is unmapped and one is mapped
            r_tgt_unmapped = None
            r_tgt_mapped = None
            if r1_tgt in unmapped_reactions:
                r_tgt_unmapped = r1_tgt
                r_tgt_mapped = r2_tgt
            if r2_tgt in unmapped_reactions:
                r_tgt_mapped = r1_tgt
                r_tgt_unmapped = r2_tgt

            if r_tgt_mapped:
                not_mapped_cn = []
                for s in gu.get_connecting_nodes(tgt_orig, r_tgt_mapped, r_tgt_unmapped):
                    # TODO : rs_used(laid_out_rs_tgt, r_tgt_unmapped, s) ensures that when s was duplicated,
                    # it will be used onlly once for given reaction. However, we now simply connect it to the
                    # first version of s which we encounter which might not be the most suitable one (maybe
                    # it should be the version of s should be based on some distance (however, r_tgt_unmapped
                    # has not yet been mapped)
                    if rs_used(laid_out_rs_tgt, r_tgt_mapped, s) and not rs_used(laid_out_rs_tgt, r_tgt_unmapped, s):
                        for role in gu.get_roles(tgt_orig, r_tgt_unmapped, s):
                            assert (r_tgt_mapped, s) in cs_mappging
                            # we need to find species name to which the species was mapped in the result and connect to that
                            g_res.add_edge(r_tgt_unmapped, cs_mappging[(r_tgt_mapped, s)], role)
                            mark_rs_used(laid_out_rs_tgt, r_tgt_unmapped, s)
                    elif not rs_used(laid_out_rs_tgt, r_tgt_mapped, s) and not rs_used(laid_out_rs_tgt, r_tgt_unmapped, s):
                        not_mapped_cn.append(s)
                not_mapped_cn.sort(reverse=True)

                # Try to use existing ODS of TMP reaction for the connecting species if there is a free one. This is usefull
                # for example in situation when only a single reaction (without species) has been removed from target. Then
                # a connecting species can become an ODS
                # 1a. Find ODS of the mapped reaction
                # 1b. Find ODS of the tempalte reaction of the mapped reaction
                # 2. Carry out ODS mapping between target and template ODSs
                # 3. Take not mapped ODS
                # 4. Use the ODS which are closest to the not mapped reaction and use them as connecting species

                if len(not_mapped_cn) == 0:
                    continue

                r_tmp_mapped = md[0][r_tgt_mapped]
                ods_tmp = list(gu.get_ods_nbs(tmp_orig, r_tmp_mapped).keys())
                ods_tmp = [s_id for s_id in ods_tmp if not s_used(laid_out_rs_tmp, s_id)]

                if len(ods_tmp) == 0:
                    continue

                ods_tgt = [id for id in gu.get_ods_nbs(tgt_orig, r_tgt_mapped).keys() if not rs_used(laid_out_rs_tgt, r_tmp_mapped, id) ]#make sure that this ODS has not been used for other reaction as a connecting species
                if len(ods_tgt) > 0:
                    # free_tmp_ods_ids = ods_tmp

                    # TODO: Not sure whether the following should be commented out or not. It would ensure that we only use for connecting species those species which would otherwise not be used at all. On the other hand, we migh miss relevant positions for the connecting species
                    lsa = get_ods_mapping(ods_tgt, r_tgt_mapped, tgt_orig, ods_tmp, r_tmp_mapped, tmp_orig)
                    ods_tmp_not_mapped_ixs = list(lsa[1]) # species from r_tmp_mapped which will be mapped
                    ods_tmp_not_mapped_ixs = set(range(len(ods_tmp))).difference(ods_tmp_not_mapped_ixs) # Keep in the not mapped on those
                    free_tmp_ods_ids = [ods_tmp[ix] for ix in ods_tmp_not_mapped_ixs]
                else:
                    free_tmp_ods_ids = ods_tmp

                pos_r_tgt = gu.get_predicted_layout(g_res, r_tgt_mapped).get_center()
                free_tmp_ods_ids_sorted = sorted(free_tmp_ods_ids, key=lambda s_id: gr.utils.dist(pos_r_tgt, gu.get_layout(tmp_orig, s_id, layout_type=be.LAYOUT_TYPE.ORIGINAL).get_center()))
                ix_not_mapped_cn, ix_free_tmp_ods_ids = get_ods_mapping(not_mapped_cn, r_tgt_mapped, tgt_orig, free_tmp_ods_ids_sorted, r_tmp_mapped, tmp_orig)
                for i in range(len(ix_not_mapped_cn)):
                    tgt_cn_id = not_mapped_cn[ix_not_mapped_cn[i]]
                    tmp_ods_id = free_tmp_ods_ids_sorted[ix_free_tmp_ods_ids[i]]
                # for i in range(min(len(not_mapped_cn), len(free_tmp_ods_ids_sorted))):
                #     tgt_cn_id = not_mapped_cn[i]
                #     tmp_ods_id = free_tmp_ods_ids_sorted[i]

                    new_node_id = '{}__{}'.format(tgt_cn_id, tmp_ods_id)
                    assert new_node_id not in g_res

                    new_node = copy.deepcopy(tgt_orig_nodes[tgt_cn_id])
                    copy_predicted_layout(tmp_orig_nodes[tmp_ods_id], new_node)
                    add_node(g_res, nodes_mapping, new_node_id, tgt_cn_id, **new_node)

                    for r_tgt in [r_tgt_mapped, r_tgt_unmapped]:
                        cnt_added_edges = add_edges(g_res, new_node_id, r_tgt, tgt_orig, tgt_cn_id, r_tgt, tmp_orig, tmp_ods_id, r_tmp_mapped)
                        cs_mappging[(r_tgt, tgt_cn_id)] = new_node_id
                        mark_rs_used(laid_out_rs_tgt, r_tgt, tgt_cn_id, cnt_added_edges)
                    mark_rs_used(laid_out_rs_tmp, r_tmp_mapped, tmp_ods_id)
                    # there is no "not mapped" template reaction since one of the target reaction was not mapped


    # First, we lay out the "true" connecting species, i.e. those which connect reactions in the template layout.
    go_through_reactions(False)

    # Then we take species which connect reactions in the deduplicated template. By mapping these species between
    # target and template we find out how species should be duplicated in the target and instead of creating one
    # deduplicated connecting species, we create multiple duplicated species.
    go_through_reactions(True)

    # Finally, we connect inserted reactions to existing mapped species where possible. E.g., if there
    # are reactions r1, r2, and r3 which are connected via a species A, but r3 was not mapped to
    # existing reations and r1 - A- r2 is laid out then we use A, to connect r3. If we didn't do this
    # A would be duplicated (probably) unnecessarily
    connecting_species_of_inserted_reactions()

    return laid_out_rs_tgt, laid_out_rs_tmp

def add_edges(g_res:nx.MultiGraph, s_id_res, r_id_res, g_tgt:nx.MultiGraph, s_id_tgt, r_id_tgt, g_tmp:nx.MultiGraph=None, s_id_tmp=None, r_id_tmp=None, simulate=False) -> int:
    """
    Copies edges between two nodes in source to target. Since we are working with multigraphs, it can indeed
    happen that two nodes (reaction and species) are connected via multiple edges with
    different labels (roles in our case).
    The template gives information about how many roles should be added and the preferred role type. I.e. if we
    have tmp with C->r1->A->r2->D structure and tgt with r1<->A<->r2 the preferred role for A in r1 to use
    when mapping r1-r2 is product.
    :param g_res:
    :param s_id_res:
    :param r_id_res:
    :param g_tgt:
    :param s_id_tgt:
    :param r_id_tgt:
    :param g_tmp:
    :param s_id_tmp:
    :param r_id_tmp:
    :return: Number of added edges (roles)
    """

    # Find role which is not yet taken (i.e. it is between n1 and n2 in source but not in target) and add the
    # corresponding nodes between n1 and n2 in target

    # Duplicates need to be taken into account. E.g. when we have situation
    # when there is species which is double connected (substrate, product) to two different reactions
    # and in both cases these reactions do not have mapping in the template. Then
    # not taking duplicates into account would cause one of the reactions having two pairs of
    # (substrate, product) connections to the same species because it is the same species in the target.
    s_res_dups = gu.get_duplicates(g_res, s_id_res, gu.get_all_species_ids(g_res))
    roles_res = set()
    for s_id in s_res_dups:
        if s_id in g_res[r_id_res]:
            roles_res = roles_res.union(gu.get_roles(g_res, s_id, r_id_res))

    # roles_res = gu.get_roles(g_res, s_id_res, r_id_res)
    roles_tgt = gu.get_roles(g_tgt, s_id_tgt, r_id_tgt)
    roles = set(roles_tgt).difference(roles_res)

    if g_tmp:
        roles_tmp = set(gu.get_roles(g_tmp, s_id_tmp, r_id_tmp))
        if roles.issuperset(roles_tmp):
            roles = roles.intersection(roles_tmp)

    if not simulate:
        for role_tgt in roles:
            g_res.add_edge(s_id_res, r_id_res, key=role_tgt)

    return len(roles)



    # for s_role in g_source[n1_source][n2_source]:
    #     if not(g_target.has_edge(n1_target, n2_target, s_role)):
    #         g_target.add_edge(n1_target, n2_target, s_role, **g_source[n1_source][n2_source][s_role])

    # if n2_source in g_source[n1_source] and (n1_target, n2_target) not in g_target.edges():
    #     g_target.add_edges(n1_target, n2_target, **g_source[n1_source][n2_source])
    # if n1_source in g_source[n2_source] and (n2_target, n1_target) not in g_target.edges():
    #     g_target.add_edges(n2_target, n1_target, **g_source[n2_source][n1_source])



def filter_common_species(laid_out_rs, r1, r2, nbs):
    """
    Checks for two reactions and a connecting species whether there is a connection
    which has not been laid out. This is because a species can be used multiple times
    for a pair of reactions, e.g. as a
    :param r1:
    :param r2:
    :param nbs:
    :return:
    """
    res = []
    for s in set(nbs):
        #how many times do we see s in nbs
        cnt_s = sum([s == nb for nb in nbs])

        # how many times (i.e. in how many roles) was the species laid out with r1 and r2
        cnt_r1s = rs_used(laid_out_rs, r1, s)
        cnt_r2s = rs_used(laid_out_rs, r2, s)

        # assert cnt_s >= cnt_r1s and cnt_s >= cnt_r2s
        # cnt_used = triplet_used(laid_out_rs, r1, r2, s)

        #TODO: !!!!!!!!!! using "max" here might not be correct - needs to be inspected !!!!!!!!!!!!
        for i in range(max(cnt_s - cnt_r1s, cnt_s - cnt_r2s)):
            res.append(s)

    return res


def get_node_pos(node, layout_type=be.LAYOUT_TYPE.PREDICTED) -> np.ndarray or None:
    layout = gu.get_node_data(node).get_layout(layout_type)
    if not layout.is_initialized():
        return None
    return layout.get_center()



# def get_node_bb(node, predicted_layout=True) -> gr.Rectangle:
#     center = get_node_pos(node, predicted_layout)
#     return gr.Rectangle(center[0] - settings.render.species_glyph_width / 2,
#                         center[1] - settings.render.species_glyph_height / 2,
#                         center[0] + settings.render.species_glyph_width / 2,
#                         center[1] + settings.render.species_glyph_height / 2)



def set_node_pos(node, pos: np.ndarray, layout_type=be.LAYOUT_TYPE.PREDICTED, copied: bool or None = None, recompute_reaction_layout: bool = True):

    node_data = gu.get_node_data(node)
    layout = node_data.get_layout(layout_type)

    if not layout:
        assert False #This should not happen any more and this assert is here just as an insurance policy
        node_data.set_layout(be.BioEntityLayout(pos))
    else:
        if isinstance(layout, be.SpeciesLayout):
            layout.set_center(pos, recompute_reaction_layout=recompute_reaction_layout)
        else:
            layout.set_center(pos)
        if copied is not None:
            layout.set_copied(copied)


def set_predicted_position(n, pos: np.ndarray, copied:bool = False, recompute_reaction_layout: bool = False):
    set_node_pos(n, pos, copied=copied, recompute_reaction_layout=recompute_reaction_layout)


def copy_predicted_layout(n_from, n_to):
    n_to_data: be.BioEntity = gu.get_node_data(n_to)
    n_to_data.set_layout(copy.deepcopy(gu.get_node_data(n_from).get_layout(be.LAYOUT_TYPE.ORIGINAL)), be.LAYOUT_TYPE.PREDICTED)
    n_to_data.get_layout(be.LAYOUT_TYPE.PREDICTED).set_copied(True)


def shift_node_coords(node, shift, layout_type=be.LAYOUT_TYPE.PREDICTED):
    set_node_pos(node, get_node_pos(node, layout_type) + shift, layout_type)


def get_center_of_mass(nodes, masses = None, layout_type=be.LAYOUT_TYPE.PREDICTED) -> np.ndarray:
    com = np.array([0.0, 0.0])
    mass = 0
    for i in range(len(nodes)):
        n = nodes[i]
        m = 1.0 if masses is None else masses[i]
        coords = get_node_pos(n, layout_type=layout_type)
        com += m * coords
        mass += m
    com /= mass

    return com


def get_graph_stats(g, layout_key=None) -> Stats:

    stats = Stats()

    ods = gu.get_one_degree_species(g)
    dists = []
    nodes = g.nodes()
    if len(ods) > 0:
        for s in ods:
            coords_r = get_node_pos(nodes[list(nx.all_neighbors(g, s))[0]], layout_type=be.LAYOUT_TYPE.ORIGINAL)
            coords_s = get_node_pos(nodes[s], layout_type=be.LAYOUT_TYPE.ORIGINAL)
            dists.append(gr.utils.dist(coords_r, coords_s))

        stats.med_one_degree_species_distance = np.median(dists)
    else:
        stats.med_one_degree_species_distance = settings.render.species_glyph_width

    reactions = gu.get_all_reaction_ids(g)
    dists.clear()
    if len(reactions) > 0:
        for i1 in range(len(reactions)):
            for i2 in range(i1+1, len(reactions)):
                if len(set(nx.all_neighbors(g, reactions[i1])).intersection(nx.all_neighbors(g, reactions[i2]))) > 0:
                    c1 = get_node_pos(nodes[reactions[i1]], layout_type=be.LAYOUT_TYPE.ORIGINAL)
                    c2 = get_node_pos(nodes[reactions[i2]], layout_type=be.LAYOUT_TYPE.ORIGINAL)
                    dists.append(gr.utils.dist(c1, c2))
        if len(dists) > 0:
            stats.med_reactions_distance = np.median(dists)

    if layout_key:
        min_p = [sys.maxsize, sys.maxsize]
        max_p = [-sys.maxsize, -sys.maxsize]
        for n in nodes:
            p = get_node_pos(nodes[n], layout_key)
            min_p[0] = min(min_p[0], p[0])
            min_p[1] = min(min_p[1], p[1])
            max_p[0] = max(max_p[0], p[0])
            max_p[1] = max(max_p[1], p[1])

        stats.width = max_p[0] - min_p[0]
        stats.height = max_p[1] - min_p[1]

    return stats


def l_not_mapped_connecting_species(g_res, tgt_orig, tmp_orig, to_layout, nodes_mapping, md, tmp_stats):

    for r1_tgt, r2_tgt, s_ids_to_add, new_ids, cn_existing in to_layout:

        if len(cn_existing) > 0:

            # If there already exist any connecting nodes which have been laid out, we use them to position the remaining,
            # not yet mapped connecting nodes, relative to them.
            # The not mapped connecting nodes will be distributed on a line perpendicular (or horizontal/vertical?) to the connecting line going through the main connecting species

            def get_average_pos(ids: List[str]) -> np.ndarray or None:
                if len(ids) == 0:
                    return None
                poss = [gu.get_predicted_layout(g_res, id).get_center() for id in ids]
                poss_filtered = [x for x in poss if x is not None]
                return sum(poss_filtered) / len(ids) if len(poss_filtered) > 0 else None

            # 1. Find out whether to use horizontal or vertical vector
            # We use two strategies:

            r1_l:be.ReactionLayout = gu.get_predicted_layout(g_res, r1_tgt)
            r2_l:be.ReactionLayout = gu.get_predicted_layout(g_res, r2_tgt)
            r1_center_tgt = r1_l.get_center() if r1_l.is_copied() else get_average_pos(list(g_res[r1_tgt]))
            r2_center_tgt = r2_l.get_center() if r2_l.is_copied() else get_average_pos(list(g_res[r2_tgt]))

            if r1_center_tgt[0] == r2_center_tgt[0] and r1_center_tgt[1] == r2_center_tgt[1] and len(s_ids_to_add):
                # This is a situation when two species are connected with two reactions which is a special situations and needs to be handled separately
                # We need to find the orientation of the reactions which is based on existing connecting species and their already laid out neighbors
                aux_poss = []
                cn_existing_tgt = [nodes_mapping.getSourceMapping(id)[0] for id in cn_existing]
                for cn_id in cn_existing_tgt:
                    nbs_centers = [gu.get_predicted_layout(g_res, id).get_center() for id in gu.get_neibhbors_in_distance(g_res, cn_id, 2)]
                    nbs_centers_existing = [pos for pos in nbs_centers if pos is not None]
                    if len(nbs_centers_existing) > 0:
                        #it can happen that none of the neighbors has been laid out, yet
                        aux_poss.append(sum(nbs_centers_existing)/len(nbs_centers_existing))
                aux_pos = sum(aux_poss)/len(aux_poss) if len(aux_poss) > 0 else np.array([0,0])
                cn_pos = sum([gu.get_predicted_layout(g_res, id).get_center() for id in cn_existing_tgt])/len(cn_existing_tgt)
                dir = cn_pos - aux_pos
                if gr.utils.is_more_horizontal(dir):
                    dir = np.array([1, 0]) if dir[0] > 0 else np.array([-1, 0])
                else:
                    dir = np.array([0, 1]) if dir[1] > 0 else np.array([0, -1])


                # s_id = s_ids_to_add[0]
                # poss[s_id] = np.array(cn_pos + dir * settings.render.optimal_species_reaction_dist * 2)
                poss = {}
                for s_id in s_ids_to_add:
                    #TODO!!!!!!!! - This assigns each connecting node the same position resulting in overlaps
                    poss[s_id] = np.array(cn_pos + dir * settings.render.optimal_species_reaction_dist*2)
            else:

                # If possible we will use position of the reaction from template to find out how to lay out the connecting species
                # The reason for using template and not the nascent result target is that it can be missing some of the nodes (such as ODS) which
                # might be important for deciding the orientation of the line on which the connecting species should be laid out
                r1_center_tmp_tgt = gu.get_original_layout(tmp_orig, md[0][r1_tgt]).get_center() if r1_tgt in md[0] and md[0][r1_tgt] is not None else r1_center_tgt
                r2_center_tmp_tgt = gu.get_original_layout(tmp_orig, md[0][r2_tgt]).get_center() if r2_tgt in md[0] and md[0][r2_tgt] is not None else r2_center_tgt

                r_vect = r1_center_tmp_tgt - r2_center_tmp_tgt
                # TODO: consider the role of the added species (we can place reactants and and products in such a way that the lines will cross unnecessarilly)
                if gr.utils.is_more_horizontal(r_vect):
                    dir = np.array([0, 1])
                else:
                    dir = np.array([1, 0])

                # Check if there are more reactants then products in the connecting species to lay out.
                # If so, change the direction of the direction vector
                r1_roles_score = sum([gu.get_roles_score(gu.get_roles(tgt_orig, r1_tgt, s_id)) for s_id in s_ids_to_add])
                r2_roles_score = sum([gu.get_roles_score(gu.get_roles(tgt_orig, r1_tgt, s_id)) for s_id in s_ids_to_add])
                if r1_roles_score + r2_roles_score < 0:
                    dir = -dir

                # 2. Find the center
                cn_existing_lts: List[be.SpeciesLayout] = [
                    gu.get_predicted_layout(g_res, nodes_mapping.getSourceMapping(cn_id)[0]) for cn_id in cn_existing]
                center = sum([lt.get_center() for lt in cn_existing_lts]) / len(cn_existing)
                # if len(cn_existing) == 1:
                #     # If there is only one existing species, the center lies in that species which would then surely overlap
                #     # while laying on the perpendicular line so we can immediately shift it
                #     cd_p = center + dir * settings.render.optimal_neighboring_species_dist
                #     cd_n = center - dir * settings.render.optimal_neighboring_species_dist
                #
                #     if gr.utils.dist(r1_center_tmp_tgt, cd_p) + gr.utils.dist(r2_center_tmp_tgt, cd_p) < gr.utils.dist(
                #             r1_center_tmp_tgt, cd_n) + gr.utils.dist(r2_center_tmp_tgt, cd_n):
                #         center = cd_p
                #     else:
                #         center = cd_n

                cd_p = center + dir * settings.render.optimal_neighboring_species_dist
                cd_n = center - dir * settings.render.optimal_neighboring_species_dist

                if gr.utils.dist(r1_center_tmp_tgt, cd_p) + gr.utils.dist(r2_center_tmp_tgt, cd_p) < gr.utils.dist(
                        r1_center_tmp_tgt, cd_n) + gr.utils.dist(r2_center_tmp_tgt, cd_n):
                    dir = -dir

                # 3. Get bounding boxes of existing species (forbidden regions)
                forbidden = [lt.get_bb().get() for lt in cn_existing_lts]

                # 4. Get positions on the line around the center
                # We should optimally start at the side which is closer to the tentative "side" of the not mapped species.
                # This means that, for example, if there exists a connecting species s1 which is a reactant of r1 and r2 and
                # r1 goes horizontally from left to right and the not mapped species s2 is in the product role in r1 and r2 then
                # s2 needs to be laid out right of s1. If these are the only connecting species, length of poss will be 1 and
                # the position needs to be right of s1. Therefore, the dir vector needs to be in the correct direction.
                s_id_roles_scores: Dict[str, int] = {}

                for s_id in s_ids_to_add:
                    roles1: List[be.SPECIES_ROLE] = gu.get_roles(tgt_orig, r1_tgt, s_id)
                    roles2: List[be.SPECIES_ROLE] = gu.get_roles(tgt_orig, r2_tgt, s_id)
                    s_id_roles_scores[s_id] = gu.get_roles_score(roles1) + gu.get_roles_score(roles2)

                sum_roles_score: int = sum(list(s_id_roles_scores.values()))
                if sum_roles_score < 0:
                    # Majority of the new connecting species are in the reactant role and since the dir vector
                    # points toward product roles we need to switch the direction so that first will be laid out reactants
                    # (if there is space)
                    dir = -dir

                # Sort the species from reactants to products since the positions will be assigned in this order
                # and they will go from "left" to "right" with respect to the center and direction vector
                aux_s_ids_to_add = [x[0] for x in sorted(s_id_roles_scores.items(), key=lambda x: x[1])]
                poss = layout_on_line(s_ids=aux_s_ids_to_add, center=center, dir=dir, forbidden=forbidden)

                # 5. Swap positions to minimize number of line overlaps
                # TODO
        else:
            p_r1 = get_node_pos(g_res.nodes()[r1_tgt])
            p_r2 = get_node_pos(g_res.nodes()[r2_tgt])

            p_r1_r2 = p_r2 - p_r1
            p_r1_r2_c = p_r1 + p_r1_r2 / 2
            p_r1_r2_perp = np.array([p_r1[1] - p_r2[1], p_r2[0] - p_r1[0]])
            p_r1_r2_perp_n = gr.utils.normalize(p_r1_r2_perp)

            shift = tmp_stats.med_one_degree_species_distance

            p_species = p_r1_r2_c - p_r1_r2_perp_n * shift * int(len(s_ids_to_add) / 2)
            if len(s_ids_to_add) % 2 == 0:
                p_species += p_r1_r2_perp_n * shift / 2

            poss = {}
            for s_id in s_ids_to_add:
                poss[s_id] = np.array(p_species)
                p_species += p_r1_r2_perp_n * shift / 2

        # 6. Assign positions to nodes
        for s_id in s_ids_to_add:
            set_predicted_position(gu.get_node(g_res, new_ids[s_id]), poss[s_id], copied=False,
                                   recompute_reaction_layout=False)


def tl_not_mapped_connecting_species(g_res: nx.MultiGraph, tmp, tgt, md, laid_out_rs_tgt, laid_out_rs_tmp, tmp_stats, nodes_mapping:NodesMapping):

    """
    It might have happened that we have a connecting species between two mapped reactions,
    which was not mapped because there were more connecting species in target, let's say 4,
    but the corresponding pair of reactions in template had only 2 connecting species. Then the remaining
    2 species need to be inserted into the layout.
    Another case is when one of the reactions in target was inserted, then all of the connecting species need
    to be inserted.
    :param g_res:
    :param tmp:
    :param tgt:
    :param md:
    :param laid_out_rs_tgt:
    :return:
    """

    tgt_rg = tgt.get_reaction_graph()
    tgt_orig = tgt.get_original()
    tgt_orig_nodes = tgt_orig.nodes()

    to_layout = []

    for (r1_tgt, r2_tgt) in sorted(tgt_rg.edges()):  # TODO do this in the order of decreasing amount of common

        # check for each common species how many times each of them  is present in the target (r1 and r2 can share multiple species)
        # check how many times this species is connected to r1 and r2 in the resulting layout
        # if there is some connection missing, create it

        # Now we need to find which connecting species have not been laid out yet

        cn = gu.common_neighbors(tgt_orig, False, r1_tgt, r2_tgt)['non-dup']

        # We need to keep track of how many times a node has been inserted/laid out and how many
        # times it thus needs to be inserted into the layout. This is relevant
        # if a pair of reaction shares a species multiple times (e.g. in a loop).
        cnt_tgt_nodes_to_use = {}
        for n in set(cn):
            # cnt_r1_n_r2 = max(rs_used(laid_out_rs_tgt, r1_tgt, n), rs_used(laid_out_rs_tgt, r2_tgt, n))
            cnt_r1_n_r2 = max(rs_used(laid_out_rs_tgt, r1_tgt, n), rs_used(laid_out_rs_tgt, r2_tgt, n))
            cnt_tgt_nodes_to_use[n] = sum([x == n for x in cn]) - cnt_r1_n_r2

        s_ids_to_add = []
        for s_id_tgt in set(cn):
            # if the number of times the triplet was used before and now is < number of edges between r1 and r2 using nb_tgt we need to add a node into the layout
            if cnt_tgt_nodes_to_use[s_id_tgt] > 0:
                s_ids_to_add.append(s_id_tgt)
                cnt_tgt_nodes_to_use[s_id_tgt] -= 1

        s_ids_to_add = sorted(s_ids_to_add)

        new_ids = {}
        for s_id in list(s_ids_to_add):
            new_node = copy.deepcopy(tgt_orig_nodes[s_id])
            new_node_id = '{}__{}__{}'.format(s_id, r1_tgt, r2_tgt) #make sure that distinct nodes don't share id
            new_ids[s_id] = new_node_id
            add_node(g_res, nodes_mapping, new_node_id, s_id, **new_node)
            cnt1 = add_edges(g_res, new_node_id, r1_tgt, tgt_orig, s_id, nodes_mapping.getSourceMapping(r1_tgt)[0])
            cnt2 = add_edges(g_res, new_node_id, r2_tgt, tgt_orig, s_id, nodes_mapping.getSourceMapping(r2_tgt)[0])
            mark_triplet_used(laid_out_rs_tgt, r1_tgt, r2_tgt, s_id, cnt1, cnt2)
            # if cnt1 == 0 and cnt2 == 0:
            #     g_res.remove_node(new_node_id)
            #     nodes_mapping.removeMapping(source=s_id, target=new_node_id)
            #     s_ids_to_add.remove(s_id)
            # else:
            #     new_ids[s_id] = new_node_id
            #     mark_triplet_used(laid_out_rs_tgt, r1_tgt, r2_tgt, s_id)

        # In cases when any of the connecting nodes is not new we need to connect the reactions using the already available mapping nodes
        # This is the case when the connecting nodes connect more than 2 reactions
        cn_existing = set(cn).difference(s_ids_to_add)
        for n_id in cn_existing:
            # In the following we can safely take the first index only, since for every node in result we have one node in the source (this does not has to be true the other way around)
            mapped_id = nodes_mapping.getSourceMapping(n_id)[0]

            cnt1 = add_edges(g_res, mapped_id, r1_tgt, tgt_orig, n_id, nodes_mapping.getSourceMapping(r1_tgt)[0])
            cnt2 = add_edges(g_res, mapped_id, r2_tgt, tgt_orig, n_id, nodes_mapping.getSourceMapping(r2_tgt)[0])
            mark_triplet_used(laid_out_rs_tgt, r1_tgt, r2_tgt, n_id, cnt1, cnt2)

        if len(s_ids_to_add) > 0:
            # Layout of the connecting nodes happens only after all reactions are checked because existing connecting species
            # which are not yet connected (nodes exist but edges not) might be important for laying the connecting species
            to_layout.append((r1_tgt, r2_tgt, s_ids_to_add, new_ids, cn_existing))


    l_not_mapped_connecting_species(g_res, tgt_orig, tmp.get_reaction_graph(), to_layout, nodes_mapping, md, tmp_stats)



def get_connecting_ods(r_id, g_res:nx.MultiGraph, g_tgt:nx.MultiGraph, nodes_mapping:NodesMapping)->Dict[str,str]:
    """
    After laying out connecting species, it can happen that a connecting species
    which was not laid out remains without a position and should be laid out as a one-degree species. Example of such situation
    is when one species is connected multiple times to one reaction and one of the roles is not mapped,
    because the species is mapped against another species which already does not have free roles to use. Then
    such a species should either be connecting to an existing species or should be treated as a duplication.

    Example:
    tmp: A-1-A-2-A-3-B
    tgt: A-1-A-5-A-A-4-B    -> leading to deduplicated graph A double connected to 1, A double connected to 5, A and B single connected to 4
    mapping (tmp:tgt): 1:1,2:4, 3:5
    situation: after laying out connecting species, the product role of reaction 5 becomes not laid out and when
    checking re1-re5, all roles of re1 are already taken and thus the product role of re5 (s    pecies A) needs to be laid out
    as a one-degree species (which is ok, since re5 is mapped onto re3 and A should be laid out where B is positioned in template)

    """

    ods = {}
    for s_id in g_tgt[r_id]:
        # consider only connecting species
        if len(g_tgt[s_id]) > 1:
            # report the species for roles which are not yet covered

            already_mapped_roles = []
            r_mapping = nodes_mapping.getSourceMapping(r_id)[0] #reactions are not duplicated so the mapping is unique
            for s_mapping in nodes_mapping.getSourceMapping(s_id):
                # the s_id might have been duplicated and we are interested in only those mappings which are connected to the reaction of interest, i.e. r_id
                if s_mapping in g_res[r_mapping]:
                    already_mapped_roles += g_res[r_mapping][s_mapping]

            for role in g_tgt[r_id][s_id]:
                if role not in already_mapped_roles:
                    if s_id not in ods:
                        ods[s_id] = []
                    ods[s_id].append(role)

    return ods

def triplet_used(laid_out_rs, r1, r2, s):
    rs_used(laid_out_rs,r1, s)
    return min(rs_used(laid_out_rs, r1, s), rs_used(laid_out_rs, r2, s))


def mark_triplet_used(rs_pairs, r1, r2, s, r1_cnt: int=1, r2_cnt: int =2):
    for i in range(r1_cnt):
        rs_pairs.append((r1, s))
    for i in range(r2_cnt):
        rs_pairs.append((r2, s))

def rs_used(rs_pairs, r_id, s_id):
    # return (r,s) in rs_pairs
    return sum([(r_id,s_id) == x for x in rs_pairs])

def s_used(rs_pairs, s_id:str) -> bool:
    for p in rs_pairs:
        if p[1] == s_id:
            return True
    return False


def mark_rs_used(rs_pairs, r, s, cnt=1):
    """
    Mark the reaction-species pair as processed
    :param rs_pairs:
    :param r:
    :param s:
    :return:
    """
    for i in range(cnt):
        rs_pairs.append((r, s))


def get_node_distance_matrix(nodes1: List[str], r1_id: str, g1: nx.MultiGraph, nodes2: List[str], r2_id: str, g2: nx.MultiGraph, tmp_n_tgt_n_map = None):
    """

    :param nodes1: target
    :param center1:
    :param g1:
    :param nodes2: template
    :param center2:
    :param g2:
    :param tmp_n_tgt_n_map: tmp_n_tgt_n_map is a dictionary which stores information about all target nodes for which
    given template node was used as template so that we don't transfer a target node on already used template node,
    unless the already mapped nodes share a name with the considered target node(is possible duplicate)
    :return:
    """
    # computes distance matrix between nodes1 and nodes2 where nodes1 are neighbors of center1 in g1 and likewise for g2
    #

    m = []
    for n1 in nodes1:
        row = []
        for n2 in nodes2:
            if tmp_n_tgt_n_map is not None and n2 in tmp_n_tgt_n_map and gu.duplicate_attrs(g1, n1) != gu.duplicate_attrs(g1, tmp_n_tgt_n_map[n2][0]):
                dist = sys.maxsize
            else:
                dist = node_distance(n1, r1_id, g1, n2, r2_id, g2)
            row.append(dist)
        m.append(row)
    return m


def node_distance(n1, r1_id, g1, n2, r2_id, g2):

    dist = 4

    roles1 = gu.get_roles(g1, n1, r1_id)
    roles2 = gu.get_roles(g2, n2, r2_id)

    if set(roles1).intersection(roles2):
        dist -= 1

    if gu.get_node_data(g1.nodes[n1]).get_name() == gu.get_node_data(g2.nodes[n2]).get_name():
        dist -= 1

    if gu.get_node_data(g1.nodes[n1]).get_type() == gu.get_node_data(g2.nodes[n2]).get_type():
        dist -= 1

    nbs_diff = abs(len(list(nx.all_neighbors(g1, n1))) - len(list(nx.all_neighbors(g2, n2))))
    dist -= 1 - (1 / (nbs_diff) if nbs_diff > 0 else 0) #TODO make this right

    # area1 = get_area(n1[layout_key])
    # area2 = get_area(n1[layout_key])
    # sim += 1 - (area1-area2) / max(area1, area2)

    return dist

def add_node(g: nx.MultiGraph, nodes_mapping:NodesMapping, new_id, source_id, **node_info):
    """

    :param g:
    :param nodes_mapping:
    :param new_id:  ID in the result graph
    :param source_id: ID in the original target graph
    :param node_info:
    :return:
    """
    entity = gu.get_node_data(node_info)
    entity.set_graph(g)
    entity.set_id(new_id)
    g.add_node(new_id, **node_info)
    nodes_mapping.addMapping(source_id, new_id)


def zoom(g, center, d, nodes = [], layout_type=be.LAYOUT_TYPE.PREDICTED):

    for n_id, attrs in g.nodes(data=True):
        if len(nodes) > 0 and n_id not in nodes:
            continue
        coords = get_node_pos(attrs, layout_type=layout_type)
        if coords is None:
            continue
        dd = coords - center
        set_node_pos(attrs, center + dd + np.sign(dd) * d, layout_type=layout_type)


def min_max_coords(g: nx.MultiGraph, layout_type=be.LAYOUT_TYPE.PREDICTED)->MinMaxCoords:
    mmc = MinMaxCoords(np.array([sys.maxsize, sys.maxsize]), np.array([-sys.maxsize, -sys.maxsize]))

    for n_id, attrs in g.nodes(data=True):
        # coords = get_node_pos(attrs, layout_type=layout_type)
        minmax = gu.get_node_data(attrs).get_layout(layout_type=layout_type).get_min_max()
        mmc.push_min(minmax[0])
        mmc.push_max(minmax[1])

    return mmc

def pan(g, shift, nodes = [], layout_type=be.LAYOUT_TYPE.PREDICTED):

    for n_id, attrs in g.nodes(data=True):
        if len(nodes) > 0 and n_id not in nodes:
            continue
        gu.get_node_data(attrs).get_layout(layout_type=layout_type).shift(shift)
        # coords = get_node_pos(attrs, layout_type=layout_type)
        # set_node_pos(attrs, coords + shift, layout_type=layout_type)

def pan_to_origin(g, padding=np.array([0,0]), layout_type=be.LAYOUT_TYPE.PREDICTED):

    shift = -min_max_coords(g, layout_type=layout_type).min
    pan(g, shift+padding, layout_type=layout_type)

def pan_to_positive(g, padding=np.array([0,0]), layout_type=be.LAYOUT_TYPE.PREDICTED):

    min_coords = min_max_coords(g, layout_type=layout_type).min

    if min_coords[0] != 0 or min_coords[1] != 0:

        pan(g, np.array([0,0]) + np.array(padding) - min_coords)


def get_graph_for_fdp(g, reactions):
    """
    Creates and induced subgraph with reactions as nddes connected with edges if they share a species in
    the original graph. Edges are weighted by the number of common neighbors.
    :param g:
    :param reactions:
    :return:
    """
    g_res = nx.Graph()

    g_res.add_nodes_from(reactions)
    for i in range(len(reactions)):
        r1 = reactions[i]
        for j in range(i+1, len(reactions)):
            r2 = reactions[j]
            n = len(set(nx.all_neighbors(g, r1)).intersection(nx.all_neighbors(g, r2)))
            if n > 0:
                g_res.add_edge(r1, r2, weight=n)

    return g_res

def duplicate(g, s, r, dist):

    g_nodes = g.nodes()

    new_node = copy.deepcopy(g.nodes()[s])

    p_r = get_node_pos(g_nodes[r])
    p_s = get_node_pos(g_nodes[s])
    set_predicted_position(new_node, p_r + gr.utils.normalize(p_s - p_r) * dist)

    i = 0
    while 1:
        new_id = '{}_{}'.format(s, i)
        i+=1
        if new_id not in g:
            break

    g.add_node(new_id, **new_node)

    add_edges(g, r, new_id, g, r, s)

    g.remove_edge(r, s)
    g.remove_node(s)


def validate_layout(res: nx.MultiGraph, tgt: nx.MultiGraph):

    non_laid_out_nodes = []
    for node_id, attrs in res.nodes(data=True):
        if not gu.get_node_data(attrs).get_layout(be.LAYOUT_TYPE.PREDICTED):
            non_laid_out_nodes.append(node_id)

    if len(non_laid_out_nodes) > 0:
        raise Exception('The following nodes were not laid out {}'.format(', '.join(non_laid_out_nodes)))


    rg_res = gu.get_reaction_graph(res)
    rg_tgt = gu.get_reaction_graph(tgt)
    missing_reactions = set(rg_tgt.nodes()).difference(rg_res.nodes())
    added_reactions = set(rg_res.nodes()).difference(rg_tgt.nodes())

    if len(missing_reactions) > 0:
        raise Exception('The following reactions are missing in the result: {}'.format(', '.join(missing_reactions)))
    if len(added_reactions) > 0:
        raise Exception('The following reactions are abundant in the result: {}'.format(', '.join(added_reactions)))

    #We should also see the same number of edges with correct labels/roles
    for r in gu.get_all_reaction_ids(tgt):
        tgt_roles = []
        for s in tgt[r]:
            tgt_roles += [r.name for r in gu.get_roles(tgt, r, s)]
        res_roles = []
        for s in res[r]:
            res_roles += [r.name for r in gu.get_roles(res, r, s)]

        tgt_roles.sort()
        res_roles.sort()
        mismatches = []
        if "".join(tgt_roles) != "".join(res_roles):
            mismatches.append([r, tgt_roles, res_roles])
        for m in mismatches:
            print('Mismatch of reaction neibhours for reaction {}: the roles in the original graph were {} while the result graph has {}'.format(m[0], m[1], m[2]))
        if len(mismatches) > 0:
            raise Exception('Mismatchs in reactions')





