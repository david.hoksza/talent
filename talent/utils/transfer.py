import argparse
import logging
import networkx as nx
import os
import hashlib
import pickle
import re

from sged import sged_graph as ed

from typing import List, Dict, NamedTuple, Tuple, Set

import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__))+"/../..")

from talent import common, settings
from talent import graph_utils as gu
from talent import layout as lt
from talent import layout_merge as ltm
from talent import scoring
from talent.utils import export
from talent.reaction_graph import ReactionGraph
from talent.beautification import BeautificationChain
from talent.beautification import BeautificationOp
from talent.beautification import BEAUTY_OPERATION_TYPE


class MapTransferResult(NamedTuple):
    bc: BeautificationChain
    tmp: ReactionGraph or None #none in case of laying out single species graphs


MappingList = List[Tuple[str or None, str or None]] #List of (template;target) reaction id pairs
class MappingResult:
    def __init__(self, score: float, mapping: MappingList):
        self.score = score
        self.mapping = mapping


def get_rg_restricted_g(rg, g):
    res = gu.deepcopy_graph(g)

    # remove reaction which do not appear in the rg
    for n in gu.get_all_reaction_ids(res):
        if n not in rg.nodes():
            res.remove_node(n)

    # some species might become disconnected, so let's remove them
    for n in gu.get_all_species_ids(res):
        if len(list(nx.all_neighbors(res, n))) == 0:
            res.remove_node(n)

    return res



def add_single_species_components(g: nx.MultiGraph, tgt_ccs: List[ReactionGraph], tgt_name, cmp_name):

    ccs_ids: List[str] = []
    for cc in tgt_ccs:
        ccs_ids += list(cc.get_original().nodes())

    all_ids = set(gu.get_all_species_ids(g))
    in_complex_s_ids = gu.get_in_complex_s_ids(g)

    for n_id in all_ids.difference(ccs_ids).difference(in_complex_s_ids):
        g_cc = nx.MultiGraph()
        gu.copy_node(g, g_cc, n_id)

        g_cc_rg = nx.MultiGraph()
        tgt_ccs.append(
            ReactionGraph(g=g_cc,  name='{}-cmp_{}-cc_{}-n_id'.format(tgt_name, cmp_name, n_id), rg=g_cc_rg, rg_ddup=g_cc_rg)
        )


def process_tgt(tgt_name, tgt_fname, tgt_fmt, ddup, split_by_cmprtmnt, extract_full_reactions=False) -> Tuple[nx.MultiGraph, List[ReactionGraph]]:

    logging.info("Processing target")

    g = gu.load_graph(tgt_fname, tgt_fmt)
    if ddup:
        g = gu.get_ddup_graph(g)

    tgt_ccs: List[ReactionGraph] = []
    if split_by_cmprtmnt:
        for cmp_name in gu.get_list_of_compartments(g):
            gc = gu.extract_comparment(gu.removeOneComponents(g), cmp_name, extract_full_reactions)
            gc_rg = gu.get_reaction_graph(gc)
            gc_rg_ddup = gu.get_ddup_reaction_graph(gc_rg, gc) if ddup else gc_rg
            i = 0
            for cc_rg in sorted(gu.get_connected_components(gc_rg_ddup), key=lambda x:(len(x.nodes()), gu.serialize_graph(x)), reverse=True):
                tgt_ccs.append(ReactionGraph(get_rg_restricted_g(cc_rg, gc), rg=cc_rg, rg_ddup=cc_rg, ddup=ddup, name='{}-cmp_{}-cc_{}'.format(tgt_name, cmp_name, i)))
                i += 1

    else:
        g_rg = gu.get_reaction_graph(g)
        g_rg_ddup = gu.get_ddup_reaction_graph(g_rg, g) if ddup else g_rg
        i = 0
        for cc_rg in sorted(gu.get_connected_components(g_rg_ddup), key=lambda x: (len(x.nodes()), gu.serialize_graph(x)), reverse=True):
            tgt_ccs.append(ReactionGraph(get_rg_restricted_g(cc_rg, g), rg=cc_rg, rg_ddup=cc_rg, ddup=ddup, name='{}-cc_{}'.format(tgt_name, i)))
            i += 1
        cmp_name = ""

    add_single_species_components(g=g, tgt_ccs=tgt_ccs, tgt_name=tgt_name, cmp_name=cmp_name)

    logging.info("Obtained {} target components".format(len(tgt_ccs)))
    return g, tgt_ccs


def process_db(tmps_path, fmt, split_by_cmprtmnt, ddup, cc_before_ddup) -> List[ReactionGraph]:
    """
    DB is a directory with json files of the compartments to be used as templates
    :param dir_tmps:
    :param fmt:
    :param split_by_cmprtmnt:
    :param ddup:
    :param cc_before_ddup: Whether to split network by compartments before doing deduplication
    if ddup is set to true. This can have impact on the resulting connected components if there
    is a specie which is duplicated across multiple connected components which would become disconnected
    after deduplication, but are not disconnected before.
    :return:
    """

    logging.info("Processing database of templates")

    def process(g, g_rg, ddup, cc_before_ddup, name):
        tmps = []
        if cc_before_ddup:
            i = 0
            for cc_rg in sorted(gu.get_connected_components(g_rg),
                                key=lambda x: (len(g.nodes()), gu.serialize_graph(x))):
                cc_rg_ddup = gu.get_ddup_reaction_graph(cc_rg, g) if ddup else g_rg
                tmps.append(ReactionGraph(get_rg_restricted_g(cc_rg, g), rg=cc_rg, rg_ddup=cc_rg_ddup, ddup=ddup,
                                          name='{}-cc{}'.format(name, i)))
                i += 1
        else:
            gc_rg_ddup = gu.get_ddup_reaction_graph(g_rg, g) if ddup else g_rg
            i = 0
            for cc_rg in sorted(gu.get_connected_components(gc_rg_ddup),
                                key=lambda x: (len(g.nodes()), gu.serialize_graph(x))):
                # rg_ddup=cc_rg because connected component of a deduplicated graph is again a deduplicated graph
                tmps.append(ReactionGraph(get_rg_restricted_g(cc_rg, g), rg=cc_rg, rg_ddup=cc_rg, ddup=ddup,
                                          name='{}-cc{}'.format(name, i)))
                i += 1

        return tmps

    tmps = []
    cnt_cmprtments = 0

    tmps_is_dir = os.path.isdir(tmps_path)

    tmp_files = os.listdir(tmps_path) if tmps_is_dir else [tmps_path]

    for fn in tmp_files:
        # if fn.endswith(fmt):
        tmp_path = "{}/{}".format(tmps_path, fn) if tmps_is_dir else fn
        logging.info("Reading in {}".format(tmp_path))
        g = gu.load_graph(tmp_path, fmt)

        if split_by_cmprtmnt:
            for cmprtmentName in gu.get_list_of_compartments(g):
                cnt_cmprtments += 1
                gc = gu.extract_comparment(gu.removeOneComponents(g), cmprtmentName)
                gc_rg = gu.get_reaction_graph(gc)
                name = "{}--{}".format(common.extract_target_from_fn(fn), cmprtmentName)
                tmps += process(gc, gc_rg, ddup, cc_before_ddup, name)
        else:
            cnt_cmprtments += 1
            g_rg = gu.get_reaction_graph(gu.removeOneComponents(g))
            name = common.extract_target_from_fn(fn)
            tmps += process(g, g_rg, ddup, cc_before_ddup, name)

    logging.info("DB contains {} compartment()s and {} connected component(s)".format(cnt_cmprtments, len(tmps)))
    return tmps

def get_node_edge_costs(tgt, tmps) -> List:
    logging.info("Computing node and edge costs between the target and all {} templates".format(len(tmps)))

    # mapp_name = get_tgt_tmp_name(tgt, tmp)
    # if load_map:
    #     res = load_mapping(mapp_name, map_params)
    #
    # if store_map:
    #     save_mapping(res, mapp_name, map_params)
    return [{'node': scoring.LayoutNodeCosts(tgt, tmp),
             'edge':scoring.LayoutEdgeCosts(tgt, tmp)} for tmp in tmps]

def serialize_mapping(mapping):
    res = ""
    for [i1, i2] in mapping:
        res += "{}:{}->".format(i1, i2)
    return res


def merge_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z


def get_mapping_file_name(name, params):
    keys = sorted(params.keys())
    fn = '{}_{}.transfer'.format(name, '_'.join('{}-{}'.format(k, params[k]) for k in keys))
    # return fn
    return hashlib.sha512(str.encode(fn)).hexdigest()


def get_params(ged_params):
    return {**ged_params, **settings.arguments}


def save_mapping(ged_res, name, ged_params, cache_path):
    pickle.dump(ged_res, open(cache_path + '//' + get_mapping_file_name(name, get_params(ged_params)), "wb"))


def load_mapping(name, ged_params, cache_path):
    file_name = cache_path + '//' + get_mapping_file_name(name, get_params(ged_params))

    return pickle.load(open(file_name, "rb")) if os.path.isfile(file_name) else None


def get_tgt_tmp_name(tgt: ReactionGraph or str, tmp: ReactionGraph or str):
    tgt_name = tgt.get_name() if isinstance(tgt, ReactionGraph) else tgt
    tmp_name = tmp.get_name() if isinstance(tmp, ReactionGraph) else tmp
    return "{}--from--{}".format(tgt_name, tmp_name)


def convert_ed_result(res) -> List[MappingResult]:
    return [MappingResult(score=r["distance"], mapping=r["mapping"]) for r in res]


def obtain_ed(tgt, tmp, costs, cache_path, ed_settings={}, load_map=True, store_map=True, min_mapped_ratio=0) -> List[MappingResult]:
    """

    :param tgt:
    :param tmp:
    :param costs:
    :param cache_path:
    :param ed_settings:
    :param load_map:
    :param store_map:
    :param min_mapped_ratio:
    :return: List of mappings. Each mappings is a list of pairs of (target, template) reaction ids
    """

    if (len(tmp.get_reaction_graph())/len(tgt.get_reaction_graph()) < min_mapped_ratio):
        return []

    mapp_name = get_tgt_tmp_name(tgt, tmp)

    logging.info("Obtaining mapping for target {} (size {}) and template {} (size: {})".format(
        tgt.get_name(), len(tgt.get_reaction_graph()), tmp.get_name(), len(tmp.get_reaction_graph())
    ))

    map_params = merge_dicts(ed_settings, {'tgt_ddup': tgt.is_ddup(), 'tmp_ddup': tgt.is_ddup()})

    res = None
    if cache_path and load_map:
        res = load_mapping(mapp_name, map_params, cache_path)

    if res is None:
        res =convert_ed_result(
            ed(tgt.get_reaction_graph(), tmp.get_reaction_graph(),
                 node_costs=costs['node'], edge_costs=costs['edge'], user_settings=ed_settings)
            )
        logging.info("Computed mapping for {} -> distance: {} -> mapping: {}".format(mapp_name, res[0].score
                if len(res) > 0 else 'None', res[0].mapping if len(res) > 0 else 'None'))
    else:
        logging.info("Using stored mapping for {} -> distance: {} -> mapping: {}".format(mapp_name, res[0].score
                if len(res) > 0 else 'None', res[0].mapping if len(res) > 0 else 'None'))

    if cache_path and store_map:
        save_mapping(res, mapp_name, map_params, cache_path)

    cnt_mapped = 0
    if len(res) > 0:
        for m in res[0].mapping:
            if m[0] is not None and m[1] is not None:
                cnt_mapped += 1
    mapped_ratio = cnt_mapped / len(tgt.get_reaction_graph())
    logging.info("Mapped ratio: {}".format(mapped_ratio))

    return res if mapped_ratio >= min_mapped_ratio else []
    # return res

def approximate_distances(tgt, tmps, node_edge_costs, cache_path)->List[List[MappingResult]]:

    logging.info("Approximating distances")

    approx_res = []

    for i in range(len(tmps)):
    # for tmp in tmps:

        ed_settings = settings.ed_for_approximation

        res = obtain_ed(tgt, tmps[i], node_edge_costs[i], cache_path, ed_settings)
        # res = ed(tgt_rg, tmp_rg, node_costs=node_edge_costs['node'], edge_costs=['edge'], user_settings=ed_settings)
        approx_res.append(res)

    return approx_res

def convert_provided_mapping(mapping:str, tgt: nx.MultiGraph, tmp: nx.MultiGraph) -> MappingList:
    """
    :param tgt: The mapping will be provided with ids how the appear in the input tgt graph. However, this will
    undergo conversion to SBML through minerva conversion API which will modify the identifiers and therefore we need
    to find mapping between these and the original ids.
    :param tmp:
    :param mapping: Mapping information in the format tgt_reaction_1:tmp_reaction_1;tgt_reaction_2:tmp_reaction_2;...
    :return:
    """
    tgt_id_mapping = dict([(gu.get_node_data(id, tgt).get_element_id(), id) for id in tgt])
    tmp_id_mapping = dict([(gu.get_node_data(id, tmp).get_element_id(), id) for id in tmp])

    ml: MappingList = []
    r_id_tmp = []
    r_id_tgt = []
    for s in mapping.split(";"):
        pair = s.split(":")
        id_tgt = tgt_id_mapping[pair[0]]
        id_tmp = tmp_id_mapping[pair[0]]
        ml.append((id_tgt, id_tmp))
        r_id_tgt.append(id_tgt)
        r_id_tmp.append(id_tmp)

    for r_id in gu.get_all_reaction_ids(tgt):
        if r_id not in r_id_tgt:
            ml.append((r_id, None))

    for r_id in gu.get_all_reaction_ids(tmp):
        if r_id not in r_id_tmp:
            ml.append((None, r_id))

    return ml



def map_transfer(tgt: ReactionGraph, tmps: List[ReactionGraph], cache_path: str, mapping:str) -> List[MapTransferResult]:

    rv: List[MapTransferResult] = []

    if len(tgt.get_original()) == 1:
        logging.info("Laying single species graph")
        tl_res:nx.MultiGraph = lt.transfer_layout(tgt, None, None)
        bc = BeautificationChain(tl_res)
        rv.append(MapTransferResult(bc=bc, tmp=None))
        return rv

    assert (len(tmps) > 0)

    if mapping is not None:

        if len(tmps) > 1:
            # TODO: This can be resolved using infomration about which template contains the mapped target-template reactions
            raise Exception("Providing mapping for situation when multiple templates or more compartments in a single template area available. ")

        tmp = tmps[0]
        c_mapping = convert_provided_mapping(mapping, tgt.get_original(), tmp.get_original())

        tl_res: nx.MultiGraph = lt.transfer_layout(tgt, tmp, c_mapping)  # TODO result can contain multiple mappings with the same minimum distance
        bc = BeautificationChain(tl_res)
        rv.append(MapTransferResult(bc=bc, tmp=tmp))


    else:

        ed_settings = settings.ed

        # get costs (match, indel) for the edit distance for each tgt-tmp pair based on node types
        costs = get_node_edge_costs(tgt, tmps)

        if len(tmps) > 1:
            approx_res:List[List[MappingResult]] = approximate_distances(tgt, tmps, costs, cache_path)
            approx_res_ix_sort:List[List[MappingResult]] = sorted(range(len(approx_res)), key=lambda k: approx_res[k][0].score if len(
                approx_res[k]) > 0 else sys.float_info.max)
        else:
            logging.info("Skipping approximation")

        for i in range(min(settings.cnt_approx_to_try, len(tmps))):

            if len(tmps) > 1:
                ix_tmp = approx_res_ix_sort[i]
            else:
                ix_tmp = 0

            tmp = tmps[ix_tmp]
            logging.info("Picking {} as the template ({}. closest template)".format(tmp.get_name(), i))

            res: List[MappingResult] = obtain_ed(tgt, tmp, costs[ix_tmp], cache_path, ed_settings)
            if len(res) == 0:
                res = approx_res[ix_tmp]
                logging.info("Used approximate mapping")
                if len(res) == 0:
                    logging.info("No mapping available. Skipping...")
                    continue
            logging.info("Used mapping: distance {} - {}".format(res[0].score, res[0].mapping))

            logging.info("Transferring layout from {} to {}".format(tmp.get_name(), tgt.get_name()))
            tl_res: nx.MultiGraph = lt.transfer_layout(tgt, tmp, res[0].mapping)  # TODO result can contain multiple mappings with the same minimum distance
            bc = BeautificationChain(tl_res)
            rv.append(MapTransferResult(bc=bc, tmp=tmp))

    return rv

def map_transfer_store(tgt:ReactionGraph, tmps: List[ReactionGraph], cache_path: str, output_path: str, mapping:str) -> List[str]:

    assert (len(tmps) > 0)

    rv_fnames: List[str] = []

    mtrs: List[MapTransferResult] = map_transfer(tgt, tmps, cache_path, mapping=mapping)

    i = 0
    for mtr in mtrs:
        tmp = mtr.tmp # Will be None if the target contains just a single species (no template is then used)
        bc = mtr.bc
        fname = "{}{}-tmp_order_{}-tgt-ddup_{}_tmp-ddup_{}.pkl".format(output_path, common.sanitize_string(get_tgt_tmp_name(tgt, tmp if tmp else '')), i,
                                                                        tgt.is_ddup(), tmp.is_ddup() if tmp else '')
        i += 1
        rv_fnames.append(fname)
        logging.info("Storing transferred layout into {}".format(fname))
        bc.save(fname)

    return rv_fnames


def group_gs_by_compartment(gs: List[nx.MultiGraph]) -> Dict[str, List[nx.MultiGraph]]:
    rv: Dict[str, List[nx.MultiGraph]] = {}

    for g in gs:
        assert(len(g) > 0)
        s_id = gu.get_all_species_ids(g)[0]
        cmp_name = gu.get_node_data(s_id, g).get_compartment_name()
        if cmp_name not in rv:
            rv[cmp_name] = []
        rv[cmp_name].append(g)

    return rv


def get_rg_name_without_cmp(gs: List[ReactionGraph]) -> List[str]:
    names = set()
    for g in gs:
        if g is None:
            #can happen when no template was used (one degree compartment)
            continue
        name = g.get_name()
        name = re.sub(r'--.*', '', name)
        names.add(name)
    return list(names)


def transfer(tgt_name, tgt_fname, tgt_fmt, split_by_cmprtmnt, tmps_path, tmp_fmt,
             output_path, separate_cmprtmnt,
             ddup_tgt=False,
             ddup_tmp=False, cache_path=None, tgt_sbml_source=None, tmp_cc_before_ddup=True,
             mapping:str=None
             ):
    """

    :param tgt_name:
    :param tgt_fname:
    :param tgt_fmt:
    :param split_by_cmprtmnt:
    :param dir_tmps:
    :param tmp_fmt:
    :param ddup_tgt:
    :param ddup_tmp:
    :param use_cache:
    :param tgt_sbml_source: The target does not need to be provided as SBML, but can be preprocessed and
    passed as json file. However, if export to SBML is needed, the original SBML file with the network structure
    is taken and layout added on top of that.
    :param tmp_cc_before_ddup:
    :return:
    """

    settings.arguments = {
        'ddup_tgt': ddup_tgt,
        'ddup_tmp': ddup_tmp
    }


    if separate_cmprtmnt:
        g_tgt_complete, tgt_ccs = process_tgt(tgt_name, tgt_fname, tgt_fmt, ddup_tgt, True)
        tmps: List[ReactionGraph] = process_db(tmps_path, tmp_fmt, True, ddup_tmp, tmp_cc_before_ddup)

        i = 0
        mtrss: List[List[MapTransferResult]] = []
        for tgt_cc in tgt_ccs:
            logging.info("Mapping connected component {} ({} reactions)".format(i, len(tgt_cc.get_reaction_graph())))
            mtrss.append(map_transfer(tgt_cc, tmps, cache_path, mapping=mapping))
            i += 1

        # Extract best mapping from each transfer result list
        g_ress = [mtrs[0].bc.g for mtrs in mtrss]
        tmps_ress = [mtrs[0].tmp for mtrs in mtrss]

        # Each graph is shifted to the beginning of the coordinate frame because we then need to reposition them relative to each other
        for g_res in g_ress:
            lt.pan_to_origin(g_res)

        # Group results by compartment
        cmp_gs: Dict[str, List[nx.MultiGraph]] = group_gs_by_compartment(gs=g_ress)
        # Create CC
        # g_tgt_complete: nx.MultiGraph = gu.load_graph(tgt_fname, tgt_fmt)
        g_res = ltm.merge_compartments(cmp_gs=cmp_gs, g_tgt=g_tgt_complete)
        bc = BeautificationChain(g_res)

        # tmp_name = "".join([tmp_res.get_name()  for tmp_res in tmps_ress if tmp_res is not None]) #single-species components do not have a template
        tmp_names = get_rg_name_without_cmp(tmps_ress)
        if len(tmp_names) == 1:
            tmp_name = tmp_names[0]
        else:
            tmp_name = "multiple"
        fname = "{}{}-tgt-ddup_{}_tmp-ddup_{}.pkl".format(output_path, common.sanitize_string(get_tgt_tmp_name(tgt_name, tmp_name)), ddup_tgt, ddup_tmp)
        bc.save(fname)
        export.export(input_path=fname, format="svg", minerva_instance=settings.minerva_instance, output_path=fname + ".svg")
        export.export(input_path=fname, format="celldesigner", minerva_instance=settings.minerva_instance, output_path=fname + ".xml")
        export.export(input_path=fname, format="sbml", minerva_instance=settings.minerva_instance, output_path=fname + ".sbml.xml")
        export.export(input_path=fname, format="sbgn", minerva_instance=settings.minerva_instance, output_path=fname + ".sbgn")



    else:
        g_tgt_complete, tgt_ccs = process_tgt(tgt_name, tgt_fname, tgt_fmt, ddup_tgt, split_by_cmprtmnt)
        tmps: List[ReactionGraph] = process_db(tmps_path, tmp_fmt, split_by_cmprtmnt, ddup_tmp, tmp_cc_before_ddup)

        i = 0
        for tgt in tgt_ccs:
            logging.info("Mapping connected component {} (size {})".format(i, len(tgt.get_reaction_graph())))
            i += 1
            # fnames = map_transfer_store(tgt, tmps, cache_path, output_path, i)
            fnames = map_transfer_store(tgt, tmps, cache_path, output_path, mapping=mapping)
            # TODO: remove in distribution
            for fname in fnames:
                export.export(input_path=fname, format="svg", minerva_instance=settings.minerva_instance, output_path=fname + ".svg")
                export.export(input_path=fname, format="celldesigner", minerva_instance=settings.minerva_instance, output_path=fname + ".xml")
                export.export(input_path=fname, format="sbml", minerva_instance=settings.minerva_instance, output_path=fname + ".sbml.xml")
                export.export(input_path=fname, format="sbgn", minerva_instance=settings.minerva_instance, output_path=fname + ".sbgn")


def validate_arguments(args):
    if args.mapping is not None and os.path.isdir(args.template_path):
        raise Exception("Mapping information can be provided only when using a single template, not a database of templates")


if __name__ == '__main__':

    common.init_logging()


    # tgt_dir = ' ../sbml/F020-pyrimidine-SBGNv02/'
    # tgt_name = 'F020-pyrimidine-SBGNv02.sbgn'
    # tmp_dir = '../sbml/F020-pyrimidine-SBGNv02/F020-pyrimidine-SBGNv02_p1_1.sbgn.sbml'
    # out_dir = '../pred/ins/F006-ACLY-SBGNv02/'
    # cache_dir = 'cache'
    # transfer(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.sbml".format(tgt_dir, tgt_name), tgt_fmt="sbml",
    #          tmps_path=tmp_dir, tmp_fmt="sbml",
    #          output_path=out_dir,
    #          split_by_cmprtmnt=False,
    #          ddup_tgt=False, ddup_tmp=False, tmp_cc_before_ddup=False, cache_path=cache_dir)
    # exit(0)

    parser = argparse.ArgumentParser()

    parser.add_argument("-tgt", "--target",
                        required=True,
                        help="Target path")
    parser.add_argument("-tgt-fmt", "--target-format",
                        required=False,
                        default="sbml",
                        help="Target file format (sbml|celldesigner|sbgn|pkl)")
    # parser.add_argument("-tgt-sbml-source", "--target-sbml-source-file",
    #                     required=False,
    #                     help="SBML file with the target network without layout to which layout section with the predicted layout will be added.")
    parser.add_argument("-tmp", "--template-path",
                        required=True,
                        help="Path containing either a template file or a directory with templates")
    parser.add_argument("-tmp-fmt", "--template-format",
                        required=False,
                        default="sbml",
                        help="Template file format (sbml|celldesigner|sbgn|pkl)")
    parser.add_argument("-o", "--output",
                        required=True,
                        help="The path including prefix of the output mapping files (there will be one mapping file per compartment and chosen template)")
    parser.add_argument("-ddup-tgt",
                        required=False,
                        type=common.str2bool,
                        default=False)
    parser.add_argument("-ddup-tmp",
                        required=False,
                        type=common.str2bool,
                        default=True)
    parser.add_argument("-sep-cmp", "--separate-compartments",
                        required=False,
                        type=common.str2bool,
                        help="The target is split by compartments into multiple connected components, each of which is laid out separately, possibly using different templates, and the result is then assembled into a single resulting layout",
                        default = True)
    parser.add_argument("-split-cmp", "--split-by-compartments",
                        required=False,
                        type=common.str2bool,
                        help="Splits target into compartments and each of them having a separate layout (this option is not considered if --sep-cmp=True)",
                        default=False)
    parser.add_argument("-tmp-split-by-cc-before-ddup", "--tmp-split-by-cc-before-ddup",
                        required=False,
                        type=common.str2bool,
                        default=False)
    parser.add_argument("-cache",
                        required=False,
                        help="Directory where the cache files can be stored. When the argument is nto present, the cache won't be used",
                        default=False)
    parser.add_argument("-s", "--settings",
                        required=False,
                        help="File with edit distance settings")
    parser.add_argument("-m", "--mapping",
                        required=False,
                        help='Mapping of the reaction between template and target graph in the format "r_tmp1-r_tgt1;r_tmp2-r_tgt2". This is valid only if a single template is used.')

    args = parser.parse_args()

    validate_arguments(args)

    name = common.extract_target_from_fn(args.target)

    if args.settings:
        settings.parse_settings_file(args.settings)



    transfer(
        tgt_name=name, tgt_fname=args.target, tgt_fmt=args.target_format, ddup_tgt=args.ddup_tgt,
        tmps_path=args.template_path, tmp_fmt=args.template_format, ddup_tmp=args.ddup_tmp,
        output_path=args.output,
        separate_cmprtmnt=args.separate_compartments,
        split_by_cmprtmnt=args.split_by_compartments,
        tmp_cc_before_ddup=args.tmp_split_by_cc_before_ddup,
        cache_path=args.cache, mapping=args.mapping)