import argparse

import os
import sys
sys.path.append(".")
sys.path.append("../../")

from talent import sbml
from talent import common
from talent import graph_utils as gu



def main(input):

    g = gu.load_graph(input, 'sbml')

    if len(g) == 0:
        print("Empty graph. Exiting...")
        exit()

    target = common.extract_target_from_fn(input)

    # gu.draw([gu.removeOneComponents(g), gu.removeOneDegreeSpecies(gu.removeOneComponents(g))], with_labels=False, node_size=5)
    # return

    # print('---------------------------------------')
    # print('Original graph stats')
    # print('---------------------------------------')
    # gu.printStatistics(g)
    #
    # print('---------------------------------------')
    # print('Graph with 1-components removed stats')
    # print('---------------------------------------')
    # gu.printStatistics(gu.removeOneComponents(g))

    # cmprtmentName = 'Neuroinflammation'
    # cmprtmentName = 'ROS Metabolism'
    # cmprtmentName = 'Microtubules'
    # cmprtmentName = 'ER stress signaling'


    g_stripped = gu.removeOneDegreeSpecies(g)
    g_rg = gu.get_reaction_graph(g_stripped)
    g_rg_ddup = gu.get_ddup_reaction_graph(g_rg, g)
    gu.draw([g, g_stripped, g_rg, g_rg_ddup], outputFile='out/{}.pdf'.format(target))

    for cmprtmentName in gu.get_list_of_compartments(g):
    # for cmprtmentName in ['Neuroinflammation']:
        print ('--------------------------------------')
        print('{} stats'.format(cmprtmentName))
        g_compartment = gu.extract_comparment(gu.removeOneComponents(g), cmprtmentName)
        if len(g_compartment) == 0:
            continue
        gu.printStatistics(g_compartment)
        print('---------------------------------------')


        # gu.printStatistics(g_compartment)
        g_compartment_stripped = gu.removeOneDegreeSpecies(g_compartment)
        g_compartment_rg = gu.get_reaction_graph(g_compartment)
        g_compartment_rg_ddup = gu.get_ddup_reaction_graph(g_compartment_rg, g_compartment)

        # gu.draw([g_compartment, gu.perturb_species(g_compartment, 'remove', ratio=0.5)])
        gu.draw([g_compartment, g_compartment_stripped, g_compartment_rg, g_compartment_rg_ddup], outputFile='out/{}-{}.pdf'.format(target, gu.format_cmprtmnt_name(cmprtmentName)))
        # gu.draw([g_compartment, g_compartment_stripped])
        # gu.draw([g_compartment,  g_compartment_stripped, g_compartment_rg])

        # gu.save_graph(g_compartment, 'out/{}-{}.edgelist'.format(target, gu.format_cmprtmnt_name(cmprtmentName)), 'edgelist')



if __name__ == '__main__':
    parser = argparse.ArgumentParser()


    # main('../../data/test/pd_autophagy1.xml')
    # exit()

    parser.add_argument("-i", "--input",
                        required=True,
                        help="Input SBML file to analyze")
    args = parser.parse_args()

    # g = gu.get_layout_graph(gu.load_sbml(args.target))
    # gu.saveGraph(g, "out/reconmap.json", "json")
    # exit()

    main(args.input)