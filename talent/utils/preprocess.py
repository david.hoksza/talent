import argparse

import os
import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__))+"/../..")

from talent import common
from talent import graph_utils as gu

def main(args):

    target = common.extract_target_from_fn(args.input)

    out_format = args.out_format if args.out_format else "pkl"

    g = gu.load_graph(args.input, 'sbml')
    gu.save_graph(g, "{}/{}.{}".format(args.out_dir, target, out_format), out_format)

    for cmprtmentName in gu.get_list_of_compartments(g):
    # for cmprtmentName in ['microglia']:
    # for cmprtmentName in ['Ubiquitin Proteasome System']:

        print("Processing {}".format(cmprtmentName))

        gc = gu.extract_comparment(gu.removeOneComponents(g), cmprtmentName)
        rg_suffix = ''
        if args.reaction_graph:
            gc = gu.get_reaction_graph(gc)
            rg_suffix = '-rg'

        gu.save_graph(gc, "{}/{}-{}{}.{}".format(args.out_dir, target, gu.format_cmprtmnt_name(cmprtmentName), rg_suffix, out_format), out_format)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input",
                        required=True,
                        help="Input file in the sbml format")
    parser.add_argument("-od", "--out-dir",
                        required=True,
                        help="Output directory.")
    parser.add_argument("-of", "--out-format",
                        required=False,
                        help="Format of the output file. Allowed values are [pkl, edgelist] (default=pkl).")
    parser.add_argument("-rg", "--reaction-graph",
                        action='store_true',
                        help="Whether the file should be converted to a reaction graph first.")

    args = parser.parse_args()

    main(args)