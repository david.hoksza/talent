import argparse
import logging
import os
import pickle
import json

import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__))+"/../..")


from talent import common
from talent import beautification as bt
from talent import settings
from talent.utils import export


def beautify(bc: bt.BeautificationChain, definition: str):
    d = json.loads(definition)
    # the definiciton should contain a list of operations where each operation should be json having type and params for that operation
    ops = []
    for op in d:
        op_type = bt.BEAUTY_OPERATION_TYPE[op['type']]
        op_params = op['params'] if 'params' in op else None
        ops.append(bt.BeautificationOp(type=op_type, params=op_params))
    cnt_ops_before = len(bc)
    bc.add(ops)
    bc.run(ix_starting=cnt_ops_before)


def process(input_path, output_path: str=None, definition: str=None, list_chain: bool = False):

    if not os.path.isfile(input_path):
        logging.error("The {} could not be found".format(input_path))
        exit(1)

    bc:bt.BeautificationChain = pickle.load(open(input_path, "rb"))

    if list_chain:
        print(bc)

    if definition is not None:
        beautify(bc=bc, definition=definition)
        if output_path is None:
            output_path = input_path
        pickle.dump(bc, open(output_path, "wb"))
        export.export(input_path=output_path, format="svg", minerva_instance=settings.minerva_instance,
                      output_path=output_path + ".svg")


if __name__ == '__main__':

    common.init_logging()

    # process(input_path="out/pred/BIOMD0000000002_url_sbo-cc_0--from--R-MMU-8956320-Nucleobase_biosynthesis-Mus_musculus_fixed-cc1-tmp_order_0-tgt-ddup_False_tmp-ddup_False.pkl",
    #         list_chain=True)
    # process(input_path="out/pred/test1-1.pkl", list_chain=True)
    # process(input_path="out/pred/test1.pkl", output_path="out/pred/test1-1.pkl", definition='[{"type": "ALIGN_TO_GRID", "params": null}]')
    #
    # exit(0)

    parser = argparse.ArgumentParser()
    #
    parser.add_argument("-i", "--input",
                        required=True,
                        help="Input network with layout generated by tbl_transfer or by tbl_beautify")
    parser.add_argument("-o", "--output",
                        required=False,
                        help="Path to store the result. When not specified, the input path will be used.")
    parser.add_argument("-d", "--definition",
                        required=False,
                        help="Definition of the chain of beautification transformation in JSON. The chain will be applied on any existing transformation in the input file")
    parser.add_argument("-l", "--list",
                        required=False,
                        action='store_true',
                        default=True,
                        help="Lists all beautification transformation which are present in the input file")


    args = parser.parse_args()

    process(input_path=args.input, output_path=args.output, definition=args.definition, list_chain=args.list)