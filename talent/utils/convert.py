import argparse
import requests
import os
import logging

import sys
sys.path.append(os.path.dirname(os.path.abspath(__file__))+"/../..")
from talent import common
from talent import settings


def minerva_convert(in_string: str, in_format: str, out_format: str, minerva_instance: str):

    in_format = in_format.lower()
    out_format = out_format.lower()

    # We carry out the conversion even when from and to format are the same because minerva adds default layout which we later rely on
    # if in_format == out_format:
    #     result = in_string
    # else:

    if in_format == "sbml":
        minerva_in_format = "SBML"
    elif in_format == "celldesigner":
        minerva_in_format = "CellDesigner_SBML"
    elif in_format == "sbgn":
        minerva_in_format = "SBGN-ML"
    else:
        raise Exception("{} - Unknown MINERVA format".format(in_format))

    image_format = False
    if out_format == "sbml":
        minerva_out_format = "SBML"
    elif out_format == "celldesigner":
        minerva_out_format = "CellDesigner_SBML"
    elif out_format == "sbgn":
        minerva_out_format = "SBGN-ML"
    elif out_format in ["svg", "pdf", "png"]:
        minerva_out_format = out_format
        image_format = True
    else:
        raise Exception("{} - Unknown MINERVA format".format(in_format))

    # token = requests.post("{}/api/doLogin".format(minerva_instance), data={'login': 'anonymous', 'password': ''})
    # token = token.cookies['MINERVA_AUTH_TOKEN']

    api_address_format_string = "{}/api/convert/image/{}:{}" if image_format else "{}/api/convert/{}:{}"
    api_address = api_address_format_string.format(minerva_instance, minerva_in_format, minerva_out_format)

    # response = requests.post(api_address, cookies=dict(MINERVA_AUTH_TOKEN=token), data=in_string)
    headers = {'Content-Type': 'text/xml; charset=utf-8', }

    response = requests.post(api_address, data=in_string.encode('utf-8'), headers=headers)
    if response.status_code == 500:
        # TODO: this is not the optimal way. THere is surely a better way how to pass the information about the encoding to the server
        response = requests.post(api_address, data=in_string, headers=headers)

    result = response.content if out_format == "pdf" or out_format == "png" else response.text

    return result


def sbml_convert(sbml_string: str, format: str, minerva_instance: str):
    return minerva_convert(in_string=sbml_string, in_format="sbml", out_format=format, minerva_instance=minerva_instance)


def convert(in_path:str, in_format: str, out_path:str, out_format: str, minerva_instance: str):

    if not os.path.isfile(in_path):
        logging.error("The {} could not be found".format(in_path))
        exit(1)

    with common.open_file(in_path) as f_in:
        out_data = minerva_convert(in_string=f_in.read(), in_format=in_format, out_format=out_format, minerva_instance=minerva_instance)
        mode = "wb" if out_format.lower() == "pdf" or out_format.lower() == "png" else "w"
        with open(out_path, mode) as f_out:
            f_out.write(out_data)


if __name__ == '__main__':

    common.init_logging()

    parser = argparse.ArgumentParser()
    #
    parser.add_argument("-i", "--input",
                        required=True,
                        help="Input file name")
    parser.add_argument("-o", "--output",
                        required=True,
                        help="Output file name")
    parser.add_argument("-if", "--in-format",
                        required=True,
                        help="The format of the output file [pdf, png, svg, sbgn, sbml, celldesigner]")
    parser.add_argument("-of", "--out-format",
                        required=True,
                        help="The format of the output file [pdf, png, svg, sbgn, sbml, celldesigner]")
    parser.add_argument("-m", "--minerva",
                        required=False,
                        default=settings.minerva_instance,
                        help="The minerva instance conversion module of which will be used for the conversion")

    args = parser.parse_args()

    convert(in_path=args.input, in_format=args.in_format,
            out_path=args.output, out_format=args.out_format,
            minerva_instance=args.minerva)
