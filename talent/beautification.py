import numpy as np
import networkx as nx
import sys
import copy
import math
import logging
import json
import pickle

from typing import List
from typing import Tuple

from . import bioentity as be
from . import graphics as gr
from . import layout as lt
from . import graph_utils as gu
from . import sbml
from . import settings
from .grid import Grid
from .routing import RoutingGraph
from .routing import OrthogonalLines
from . import fdp

from typing import Dict

from enum import Enum

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    # this is here just so that type checking works
    import bioentity as be

CompartmentDimensions = Dict[str, 'lt.MinMaxCoords']


class BEAUTY_OPERATION_TYPE(Enum):
    COMPACT_TRAILING_PATHS = 0
    INFLATE = 1
    REMOVE_EMPTY_BANDS = 2
    ALIGN_TO_GRID = 3
    HANDLE_REACTION_SPECIES_OVERLAPS = 4
    DUPLICATE = 5
    DEDUPLICATE = 6
    ONE_DEGREE_SPECIES = 7
    # PUSH_AWAY_SIDE_SPECIES = 5

    def get_params(self):
        if self == BEAUTY_OPERATION_TYPE.COMPACT_TRAILING_PATHS:
            return CompactTrailingPathsParams()
        if self == BEAUTY_OPERATION_TYPE.INFLATE:
            return InflateParams()
        if self == BEAUTY_OPERATION_TYPE.REMOVE_EMPTY_BANDS:
            return RemoveEmptyBandsParams()
        if self == BEAUTY_OPERATION_TYPE.ALIGN_TO_GRID:
            return AlignToGridParams()
        if self == BEAUTY_OPERATION_TYPE.HANDLE_REACTION_SPECIES_OVERLAPS:
            return HandleReactionSpeciesOverlapsParams()
        if self == BEAUTY_OPERATION_TYPE.DUPLICATE:
            return DuplicateParams()
        if self == BEAUTY_OPERATION_TYPE.DEDUPLICATE:
            return DeduplicateParams()
        if self == BEAUTY_OPERATION_TYPE.ONE_DEGREE_SPECIES:
            return OneDegreeSpeciesParams()


class CompactTrailingPathsParams:
    def __init__(self, params=None):
        if params is None:
            self.iteration_count = 4
        elif isinstance(params, CompactTrailingPathsParams):
            self.iteration_count = params.iteration_count
        else:
            self.iteration_count = params["iteration_count"]


class OneDegreeSpeciesParams:
    def __init__(self, params=None):
        if params is None:
            self.iteration_count = 100
        elif isinstance(params, OneDegreeSpeciesParams):
            self.iteration_count = params.iteration_count
        else:
            self.iteration_count = params["iteration_count"]


class InflateParams:
    def __init__(self, params=None):
        if params is None:
            self.species_padding_ratio = settings.min_species_padding_pct / 100.0
            self.reactant_product_distance = 2 * settings.beautification.inflate_reactant_product_distance
        elif isinstance(params, InflateParams):
            self.species_padding_ratio = params.species_padding_ratio
            self.reactant_product_distance = params.reactant_product_distance
        else:
            self.species_padding_ratio = params["padding_ratio"]
            self.species_padding_ratio = params["reactant_product_distance"]


class RemoveEmptyBandsParams:
    def __init__(self, params=None):
        if params is None:
            self.target_space: List = [settings.render.species_glyph_width * 1.5, settings.render.species_glyph_height * 1.5]
        elif isinstance(params, RemoveEmptyBandsParams):
            self.target_space = copy.deepcopy(params.target_space)
        else:
            self.target_space = params["target_space"]


class AlignToGridParams:
    def __init__(self, params=None):
        if params is None:
            self.spacing: List = [settings.render.species_glyph_width, settings.render.species_glyph_height]
        elif isinstance(params, AlignToGridParams):
            self.spacing = copy.deepcopy(params.spacing)
        else:
            self.spacing = params["spacing"]


class HandleReactionSpeciesOverlapsParams:
    def __init__(self, params=None):
        None


class DuplicateParams:
    def __init__(self, params=None):
        if params is None:
            self.distance_relative: List = settings.beautification.duplication_distance_relative
            self.distance_absolute: List = settings.beautification.duplication_distance_absolute
            self.node_degree: int = settings.beautification.duplication_node_degree
            self.type_simple_molecules: bool = settings.beautification.duplication_type_simple_molecules
            self.type_macromolecules: bool = settings.beautification.duplication_type_macromolecules
        elif isinstance(params, DuplicateParams):
            self.distance_relative = copy.deepcopy(params.distance_relative)
            self.distance_absolute = copy.deepcopy(params.distance_absolute)
            self.node_degree = copy.deepcopy(params.node_degree)
            self.type_simple_molecules = copy.deepcopy(params.type_simple_molecules)
            self.type_macromolecules = copy.deepcopy(params.type_macromolecules)
        else:
            self.distance_relative = params["distance_relative"]
            self.distance_absolute = params["distance_absolute"]
            self.node_degree = params["node_degree"]
            self.type_simple_molecules = params["type_simple_molecules"]
            self.type_macromolecules = params["type_macromolecules"]


class DeduplicateParams:
    def __init__(self, params=None):
        if params is None:
            self.distance_relative: List = settings.beautification.deduplication_distance_relative
            self.distance_absolute: List = settings.beautification.deduplication_distance_absolute
            self.simple_molecules: bool = settings.beautification.deduplication_simple_molecules
            self.large_molecules: bool = settings.beautification.deduplication_large_molecules
            self.cnt: int = settings.beautification.deduplication_cnt
        elif isinstance(params, DeduplicateParams):
            self.distance_relative = copy.deepcopy(params.distance_relative)
            self.distance_absolute = copy.deepcopy(params.distance_absolute)
            self.simple_molecules = copy.deepcopy(params.simple_molecules)
            self.large_molecules = copy.deepcopy(params.large_molecules)
            self.cnt = copy.deepcopy(params.cnt)
        else:
            self.distance_relative = params["distance_relative"]
            self.distance_absolute = params["distance_absolute"]
            self.simple_molecules = params["simple_molecules"]
            self.large_molecules = params["large_molecules"]
            self.cnt = params["cnt"]


class BeautificationOp:

    def __init__(self, type: BEAUTY_OPERATION_TYPE, params: Dict = None):
        self.__type: BEAUTY_OPERATION_TYPE = type
        if params is None:
            self.__params = type.get_params()
        else:
            self.__params = params
        self.__g:nx.MultiGraph = None

    @property
    def type(self):
        return self.__type

    @property
    def params(self):
        return self.__params

    @property
    def g(self):
        return self.__g

    def run(self, g:nx.MultiGraph):
        self.__g = gu.deepcopy_graph(g)

        if self.type == BEAUTY_OPERATION_TYPE.COMPACT_TRAILING_PATHS:
             compact_trailing_paths(self.__g, CompactTrailingPathsParams(self.__params))
        elif self.type == BEAUTY_OPERATION_TYPE.INFLATE:
            inflate(self.g, InflateParams(self.__params))
        elif self.type == BEAUTY_OPERATION_TYPE.REMOVE_EMPTY_BANDS:
            remove_empty_bands(self.__g, RemoveEmptyBandsParams(self.__params))
        elif self.type == BEAUTY_OPERATION_TYPE.ALIGN_TO_GRID:
            align_to_grid(self.g, AlignToGridParams(self.__params))
        # elif self.type == BEAUTY_OPERATION_TYPE.PUSH_AWAY_SIDE_SPECIES:
        #     push_away_side_species(self.__g)
        elif self.type == BEAUTY_OPERATION_TYPE.HANDLE_REACTION_SPECIES_OVERLAPS:
            handle_reaction_species_overlaps(self.__g)
        elif self.type == BEAUTY_OPERATION_TYPE.DUPLICATE:
            duplicate(self.__g, DuplicateParams(self.__params))
        elif self.type == BEAUTY_OPERATION_TYPE.DEDUPLICATE:
            deduplicate(self.__g, DeduplicateParams(self.__params))
        elif self.type == BEAUTY_OPERATION_TYPE.ONE_DEGREE_SPECIES:
            one_degreee_species(self.__g, OneDegreeSpeciesParams(self.__params))
        else:
            raise Exception('Unsupported beautification operation: {}'.format(self.type))


def json_serialize(obj):

    if isinstance(obj, BeautificationOp):
        return {
            'type': obj.type.name,
            'params': obj.params
        }
    # if isinstance(obj, BEAUTY_OPERATION_TYPE):
    #     return obj.name

    return obj.__dict__


class BeautificationChain:
    def __init__(self, g: nx.MultiGraph):
        self.__g = g
        self.__steps:List[BeautificationOp] = []

    def __getitem__(self, item: int) -> BeautificationOp:
        return self.__steps[item]

    @property
    def g(self):
        return self.__g

    def __len__(self):
        return len(self.__steps)

    def add(self, op: BeautificationOp or List[BeautificationOp], at=None):
        """

        :param op:
        :param at: Index at which the op should be added. If this is different from the position of the
        last operation, the operations from that position forward will be removed
        :return:
        """

        if at is not None:
            assert at < len(self)
            del self.__steps[at:]

        if isinstance(op, list):
            for o in op:
                assert isinstance(o, BeautificationOp)
            self.__steps += op
        else:
            assert isinstance(op, BeautificationOp)
            self.__steps.append(op)

    def __str__(self):
        return json.dumps(self.__steps, default=json_serialize)

    def save(self, fname):
        pickle.dump(self, open(fname, "wb"))

    def run(self, ix_starting=0):
        assert len(self.__steps) > ix_starting
        ix = 0
        while ix < ix_starting:
            assert self.__steps[ix].g is not None
            ix += 1
        g = self.__g if ix_starting == 0 else self.__steps[ix-1].g
        for ix1 in range(ix, len(self.__steps)):
            logging.info("Starting beautification step {}".format(ix1))
            self.__steps[ix1].run(g)
            g = self.__steps[ix1].g
            logging.info("Beautification step {} finished".format(ix1))


class OverlapInfoSpeciesLine:
    def __init__(self, s_id, line:gr.Line):
        self.__s_id = s_id
        self.__line = line

    @property
    def s_id(self):
        return self.__s_id

    @property
    def line(self):
        return self.__line


def beautify(g: nx.MultiGraph):
    # return;




    # beautify_duplicate(g, tmp_stats)
    # inflate(g)
    # for i in range(2): compact(g)


    # compact_free_space(g)

    #

    # compact_trailing_paths(g)
    # compact_trailing_paths(g)
    # inflate(g)
    # remove_empty_grid_lines(g)
    # align_to_grid(g)
    # push_away_modifiers(g)
    # align_to_grid(g)
    # inflate(g)
    # for i in range(5):compact_trailing_paths(g)
    # compact_trailing_paths(g)

    for i in range(5): compact_trailing_paths(g)

    gsw = settings.render.species_glyph_width
    gsh = settings.render.species_glyph_height

    # push_away_side_species(g)
    align_to_grid(g, spacing=[gsw, gsh])
    inflate(g)
    remove_empty_bands(g, spacing=[gsh, gsh], target_space=[2*gsh, 2*gsh])
    handle_reaction_species_overlaps(g)



    # handle_overlapping_segements(g)
    # handle_reaction_species_overlaps(g, 0.5*gsh, 0.5*gsh, 1)

def one_degreee_species(g: nx.MultiGraph, params: OneDegreeSpeciesParams=OneDegreeSpeciesParams()):
    logging.info("begin FDP")
    for r_id in gu.get_all_reaction_ids(g):
        for s_id, pos in fdp.springs(g, r_id).items():
            lt.set_predicted_position(g.nodes[s_id], pos)
    logging.info("end FDP")

    for r_id in gu.get_all_reaction_ids(g):
        gu.get_predicted_layout(g, r_id).recompute_layout()


def duplicate_condition(dist: np.ndarray, node_degree: int, params: DuplicateParams, dim: np.ndarray) -> bool:
    relative_dist = [dist[0] / dim[0], dist[1] / dim[1]]
    return (
            (dist[0] > params.distance_absolute[0] or dist[1] > params.distance_absolute[1]) and
            (relative_dist[0] > params.distance_relative[0] or relative_dist[1] > params.distance_relative[1])
           )
           # or node_degree > params.node_degree


def get_layout_dimensions(g: nx.MultiGraph) -> np.ndarray:
    mm = lt.min_max_coords(g)
    return np.array([mm.max[0] - mm.min[0], mm.max[1] - mm.min[1]])


def get_compartments_dimensions(g: nx.MultiGraph) -> CompartmentDimensions:
    """
    Get dimension for every compartment in the graph. The dimensions are determined based on the bouding boxes of
    the species.
    :param g:
    :return:
    """

    mmc: Dict[str, lt.MinMaxCoords] = {}

    for s_id in gu.get_all_species_ids(g):
        s = gu.get_species(g, s_id)
        minmax = s.get_predicted_layout().get_min_max()
        cmp_id = s.get_compartment_id()
        if cmp_id not in mmc:
            mmc[cmp_id] = lt.MinMaxCoords()
        mm = mmc[cmp_id]
        mm.push_min(minmax[0])
        mm.push_max(minmax[1])

    # dims:Dict[str, np.ndarray] = {}
    #
    # for k, v in mmc.items():
    #     dims[k] = np.array([v.max[0] - v.min[0], v.max[1] - v.min[1]])
    #
    #
    # return  np.array([mm.max[0] - mm.min[0], mm.max[1] - mm.min[1]])
    return mmc


def duplicate(g: nx.MultiGraph, params: DuplicateParams=DuplicateParams()):
    dims = get_compartments_dimensions(g)
    duplicate_modifiers(g=g, params=params, dims=dims)
    duplicate_non_modifiers(g=g, params=params, dims=dims)


def duplicate_modifiers(g: nx.MultiGraph, params: DuplicateParams, dims: CompartmentDimensions):
    logging.info("Starting duplication of modifiers")
    for s_id in gu.get_all_species_ids(g):
        if len(g[s_id]) > 1:
            s_pos = gu.get_predicted_layout(g, s_id).get_center()
            dim: lt.MinMaxCoords = dims[gu.get_species(g, s_id).get_compartment_id()]
            for r_id in list(g[s_id]):
                affected = False
                roles = gu.get_roles(g, r_id, s_id)
                if be.SPECIES_ROLE.CONTAINS_MODIFIER_ROLE(roles):
                    r_center = gu.get_predicted_layout(g, r_id).get_center()
                    if not (dim.min[0] <= r_center[0] <= dim.max[0] and dim.min[1] <= r_center[1] <= dim.max[1]):
                        # If the reaction center is in different compartment we do not duplicate as the duplicate
                        # could be placed into different compartment and thus increase the compartment dimension
                        # leading to compartments overlap
                        continue

                    dist: np.ndarray = abs(r_center - s_pos)
                    node_degree = len(g[s_id])
                    if duplicate_condition(dist=dist, node_degree=node_degree, params=params, dim=dim.get_dim()):
                        affected = True
                        duplicate_modifier(g=g, s_id=s_id, s_pos=s_pos, r_id=r_id, r_pos=r_center, roles=roles)
                if affected:
                    gu.get_predicted_layout(g, r_id).recompute_layout()

            if len(g[s_id]) == 0:
                # the species was only in modifier roles and all the edges were duplicated, so the original node became disconnected
                g.remove_node(s_id)

    logging.info("Finished duplication of modifiers")


def duplicate_modifier(g: nx.MultiGraph, s_id: str, s_pos: np.ndarray,  r_id: str, r_pos: np.ndarray, roles: List['be.SPECIES_ROLE']):
    new_node = copy.deepcopy(g.nodes[s_id])
    s: be.Species = gu.get_node_data(new_node)
    new_id = s.get_id()+'_dup_'+r_id
    s.set_id(new_id)

    new_pos = r_pos + gr.utils.normalize(s_pos - r_pos) * settings.beautification.duplication_optimal_modifier_distance
    lt.set_predicted_position(new_node, new_pos)

    g.add_node(new_id, **new_node)
    for role in roles:
        g.remove_edge(s_id, r_id, role)
        g.add_edge(r_id, new_id, role)

    #replace the link to the old s_id in the ReactionLayout
    gu.get_predicted_layout(g, r_id).replace_species_id(s_id_old=s_id, s_id_new=new_id)


def duplicate_non_modifier(g: nx.MultiGraph, s_id: str, s_pos: np.ndarray, s_id_tgt: str, s_pos_tgt: np.ndarray, r_id: str):
    new_node = copy.deepcopy(g.nodes[s_id])
    s: be.Species = gu.get_node_data(new_node)
    new_id = s.get_id() + '_dup_' + r_id
    s.set_id(new_id)

    new_pos = s_pos_tgt + gr.utils.normalize(s_pos - s_pos_tgt) * settings.beautification.duplication_optimal_non_modifier_distance
    lt.set_predicted_position(new_node, new_pos)

    g.add_node(new_id, **new_node)
    for role in gu.get_roles(g, r_id, s_id):
        g.remove_edge(s_id, r_id, role)
        g.add_edge(r_id, new_id, role)

    # replace the link to the old s_id in the ReactionLayout
    gu.get_predicted_layout(g, r_id).replace_species_id(s_id_old=s_id, s_id_new=new_id)


def duplicate_non_modifiers(g: nx.MultiGraph, params: DuplicateParams, dims: CompartmentDimensions):

    logging.info("Starting duplication of non-modifiers")

    for r_id in gu.get_all_reaction_ids(g):

        affected = False

        # retrieve all species with a degree > =1, i.e. potential candidates for duplication
        s_ids_connecting = [s_id for s_id in g[r_id] if len(g[s_id]) > 1 and be.SPECIES_ROLE.CONTAINS_REACTANT_PRODUCT_ROLE(gu.get_roles(g, r_id, s_id))]
        if len(s_ids_connecting) < 1:
            continue

        # get the species with minimum degree
        s_id_min = sorted(s_ids_connecting, key=lambda s_id: (len(g[s_id]), s_id))[0]
        pos1 = gu.get_predicted_layout(g, s_id_min).get_center()
        cmp1 = gu.get_species(g, s_id_min).get_compartment_id()
        dim = dims[cmp1].get_dim()

        # for each (candidate?) species of given reaction check whether it should be duplicated and if so, duplicate it in
        # the direction of the min degree species
        # for s_id in list(g[r_id]):
        for s_id in s_ids_connecting:
            cmp2 = gu.get_species(g, s_id).get_compartment_id()
            if s_id == s_id_min or cmp1 != cmp2:
                continue
            pos2 = gu.get_predicted_layout(g, s_id).get_center()
            dist: np.ndarray = abs(pos1 - pos2)
            node_degree = len(g[s_id])
            if duplicate_condition(dist=dist, node_degree=node_degree, params=params, dim=dim):
                # We need to duplicate in the direction of the species with lower connectivity
                if s_id in s_ids_connecting:
                    s_id_tgt = s_id_min
                    pos_tgt = pos1
                    s_id_src = s_id
                    pos_src = pos2
                else:
                    s_id_tgt = s_id
                    pos_tgt = pos2
                    s_id_src = s_id_min
                    pos_src = pos1
                duplicate_non_modifier(g=g, s_id=s_id_src, s_pos=pos_src, s_id_tgt=s_id_tgt, s_pos_tgt=pos_tgt, r_id=r_id)

                affected = True

        if affected:
            gu.get_predicted_layout(g, r_id).recompute_layout()

    logging.info("Finished duplication of non-modifiers")


def deduplicate_condition(dist: np.ndarray, params: DeduplicateParams, dim: np.ndarray) -> bool:
    relative_dist = [dist[0] / dim[0], dist[1] / dim[1]]
    return (
            (dist[0] < params.distance_absolute[0] and dist[1] < params.distance_absolute[1]) and
            (relative_dist[0] < params.distance_relative[0] and relative_dist[1] < params.distance_relative[1])
           )


def deduplicate(g: nx.MultiGraph, params: DeduplicateParams=DeduplicateParams()):
    """
    group duplicated species
    filter out species type which won't be duplicated (such as simple molecules)
    for each group
    STEP X: create all-to-all pairs and remove those which do not pass the deduplication requirements
    if there is no pair left, BREAK
    pick the closest pair of species
    remove the node which is less connected and reroute reactions which are using it to use the non-removed species
    GOTO X

    :param g:
    :param params:
    :return:
    """

    centrality = gu.get_centrality(g)
    dim: np.ndarray = get_layout_dimensions(g)

    duplicates:Dict[Tuple, List[str]] = get_duplication_groups(g, params)
    affected_r_ids: List[str] = []
    for s_ids in duplicates.values():

        dist_pairs: List[(float, str, str)] = []
        for i in range(len(s_ids)):
            s_id1 = s_ids[i]
            pos1 = gu.get_predicted_layout(g, s_id1).get_center()

            for j in range(i+1, len(s_ids)):
                s_id2 = s_ids[j]
                pos2 = gu.get_predicted_layout(g, s_id2).get_center()
                dist = abs(pos2 - pos1)
                if deduplicate_condition(dist=dist, params=params, dim=dim):
                    dist_pairs.append({'dist': gr.utils.size(dist), 's_id1': s_id1, 's_id2': s_id2})

        dp_sorted = sorted(dist_pairs, key=lambda x: x['dist'])

        for i in range(min(params.cnt, len(dp_sorted))):
            s_id1 = dp_sorted[0]['s_id1']
            s_id2 = dp_sorted[0]['s_id2']

            if centrality[s_id1] > centrality[s_id2]:
                s_id_from = s_id2
                s_id_to = s_id1
            else:
                s_id_from = s_id1
                s_id_to = s_id2

            deduplicate_pair(g, s_id_from=s_id_from, s_id_to=s_id_to, affected_r_ids=affected_r_ids)
            # When a node is merged, all instances in the pair list which contain that node needs to be removed
            for i_del in range(len(dp_sorted)-1, -1, -1):
                if dp_sorted[i_del]['s_id1'] == s_id_from or dp_sorted[i_del]['s_id2'] == s_id_from:
                    del dp_sorted[i_del]

            if len(dp_sorted) == 0:
                break

            centrality = gu.get_centrality(g)

    # Since we removed some of the species, we invalidated ReactionLayout of the impacted reactions which keep lists
    # of species
    for r_id in affected_r_ids:
        gu.get_predicted_layout(g, r_id).recompute_layout()


def deduplicate_pair(g: nx.MultiGraph, s_id_from: str, s_id_to: str, affected_r_ids:List[str]):
    for r_id in g[s_id_from]:
        if r_id not in affected_r_ids:
            affected_r_ids.append(r_id)
        for role in g[s_id_from][r_id]:
            g.add_edge(s_id_to, r_id, role, **g.edges[s_id_from, r_id, role])
    g.remove_node(s_id_from)


def get_duplication_groups(g: nx.MultiGraph, params: DeduplicateParams):
    duplicates: Dict[Tuple, List[str]] = {}

    # Retrieve species which are of type required by the parameters
    s_ids = []
    for s_id in gu.get_all_species_ids(g):
        species: be.Species = gu.get_species(g, s_id)
        is_simple_molecule = species.is_simple_molecule()
        if (not params.simple_molecules and is_simple_molecule) or (not params.large_molecules and not is_simple_molecule):
            continue
        s_ids.append(s_id)


    while len(s_ids) > 0:
        # get all duplicates of the first element in list
        aux_dups = gu.get_duplicates(g, s_ids[0], s_ids)
        # get duplication attributes
        da = gu.duplicate_attrs(g, s_ids[0])
        # store the group
        duplicates[da] = aux_dups
        # remove the group elements from the list of species
        for s_id in aux_dups:
            s_ids.remove(s_id)

    return duplicates


def handle_reaction_species_overlaps(g: nx.MultiGraph):

    logging.info("Fix species-reactions overlaps")

    rects = [gu.get_predicted_layout(g, s_id).get_bb().get() for s_id in gu.get_all_species_ids(g)]
    rg: RoutingGraph = RoutingGraph(rects)

    logging.info("Routing graph created")

    points: List[Tuple] = []
    s_ids: List[str] = []
    for s_id in gu.get_all_species_ids(g):
        bb: be.BoundingBox = gu.get_predicted_layout(g, s_id).get_bb()

        center = gu.get_predicted_layout(g, s_id).get_center()
        rg.add_node(center, center=True, s_id=s_id)
        for port in bb.get_ports():
            p_shift = port + gr.utils.normalize(port - center) * settings.render.routing_offset
            points.append(p_shift)
            s_ids.append(s_id)

    rg.add_nodes_and_connect(points, s_ids)
    logging.info("Nodes added to the routing graph")

    # Ones the routing graph is ready, we can fix overlaps for every reaction, i.e. see if any of the
    # lines in the reaction intersect any species bounding box and if so, find an alternative route
    ol:OrthogonalLines = OrthogonalLines()
    fix_main_chains_species_overlap(g, rg, ol)
    fix_side_chains_species_overlaps(g, rg, ol)


def fix_main_chains_species_overlap(g: nx.MultiGraph, rg: RoutingGraph, ol: OrthogonalLines):
    for r_id in gu.get_all_reaction_ids(g):
        rl:be.ReactionLayout = gu.get_predicted_layout(g, r_id)
        if fix_main_chain_species_overlap(rl, rg, ol):
            rl.adjust_ends()

def fix_main_chain_species_overlap(rl: "be.ReactionLayout", rg: RoutingGraph, ol: OrthogonalLines) -> bool:

    if rl.main_chain_intersection(rg):
        points = rl.get_main_chain_points()

        # The path from main reactant to main product intersect a bounding box and thus needs to be removed
        rg.remove_path(points)

        pos_src = rl.get_main_reactant_pos()
        pos_tgt = rl.get_main_product_pos()

        path = rg.find_path(pos_src, pos_tgt, ol)

        logging.info("Adding new path to the routing graph")

        if len(path) > 1:
            #TODO: correctly handle situation with path of length 1
            rg.add_path(path)

            logging.info("Updating main chain path")

            rl.set_main_chain_points(path)
            ol.add_points(path)

            logging.info("Updating main chain path finished")
            return True
        else:
            # If we did not find a path, return the lines form the original path to the list of lines
            rg.add_path(points)
            return False


def fix_side_chains_species_overlaps(g: nx.MultiGraph, rg: RoutingGraph, ol: OrthogonalLines):

    # The path between a side species and its connecting point go from a port of that species to the reaction connecting
    # point. To find the best path, we need to find a path from the centre of that species to the connecting point of a reaction.
    # However this connecting point is not in the routing graph and thus needs to be added and later removed for each of the
    # reaction. Since adding a point is quite expensive operation (line to every other possible point needs to be considered)
    # we do this in advance for all the reactions (with side species whose connecting line intersect a species) in one go
    # which is much faster due to the employment of the line sweep algorithm.
    # The downside is that a path for a reaction now could, theoretically lead through an other reaction's connecting point.
    # TODO: in path finding only consider points with set species

    logging.info("Fixing side chains species overlap")

    r_id_overlaps = {}
    connecting_points = set()
    for r_id in gu.get_all_reaction_ids(g):
        rl: be.ReactionLayout = gu.get_predicted_layout(g, r_id)
        sci = rl.side_chain_intersection(rg)
        if len(sci) > 0:
            r_id_overlaps[r_id] = sci
            for s_id, role in sci:
                p = tuple(rl.get_species_connecting_point(s_id))
                if not rg.point_in_graph(p):
                    connecting_points.add(p)

    logging.info("Adding connecting points")
    rg.add_nodes_and_connect(points=list(connecting_points), s_ids=[None] * len(connecting_points), interconnect=False)

    for r_id, sci in r_id_overlaps.items():
        rl: be.ReactionLayout = gu.get_predicted_layout(g, r_id)
        adjust_ends = False
        for s_id, role in sci:
            if fix_side_chain_species_overlap(rl, s_id, role, rg, ol):
                adjust_ends = True
        if adjust_ends:
            rl.adjust_ends()

    for p in connecting_points:
        rg.remove_node(p)


    logging.info("Finished fixing side chains species overlap")


def fix_side_chain_species_overlaps(rl: "be.ReactionLayout", rg: RoutingGraph) -> bool:

    overlap = False
    for s_id, role in rl.side_chain_intersection(rg):
        if fix_side_chain_species_overlap(rl, s_id, role, rg):
            overlap = True
    logging.info("Finished fixing side chains species overlap")
    return overlap


def adjust_collinear_path(path: List[np.ndarray], rl: 'be.ReactionLayout'):
    """
    Since we are aiming for orthogonal paths, it can happen that, for example, a modifier will be connected
    with the reaction center with a line whose first line basically coincides with the main reaction line. This
    might look extremely strange if there is a symbol for the connection which is then crossed by the main
    reaction line. This procedure checks the angle of the main reaction line and the first line in the path and
    if the angle is too small, the second line is shortened so that the angle gets reasonable.
    :param path:
    :return:
    """
    assert len(path) > 1
    if len(path) == 2:
        return

    r_center = rl.get_center()
    r_dir = gr.utils.normalize(rl.get_main_product_pos() - r_center)
    p_dir = gr.utils.normalize(path[-2] - path[-1])

    a = a1 = gr.utils.get_angle(r_dir, p_dir)
    a2 = gr.utils.get_angle(-r_dir, p_dir)

    if a1 > a2:
        direction = 1
        a = a2
        r_dir = -r_dir

    rad_angle = 0.18 #about 10%
    if a < rad_angle:
        p = r_center + r_dir
        x1 = path[-2]
        x2 = path[-3]

        direction = -1 if x1[0] < x2[0] or x1[1] < x2[1] else 1 #if the path goes south or west we will "add" the angle
        p_rot = gr.utils.rotate(r_center, p, direction*rad_angle)
        p_isect = gr.Line(r_center, p_rot).intersection(gr.Line(x1, x2))
        path[-2] = p_isect


def fix_side_chain_species_overlap(rl: "be.ReactionLayout", s_id: str, role: "be.SPECIES_ROLE", rg: RoutingGraph, ol: OrthogonalLines) -> bool:

    points = rl.get_sr_points(s_id, role).points

    g = rl.get_bioentity().get_graph()
    pos_src = gu.get_predicted_layout(g, s_id).get_center()
    pos_tgt = rl.get_species_connecting_point(s_id)

    # if rg.point_in_graph(tuple(pos_tgt)):
    #     added = False
    # else:
    #     rg.add_nodes_and_connect(points=[pos_tgt], s_ids=[None])
    #     added = True
    # logging.info("c")
    path = rg.find_path(pos_src, pos_tgt, ol)
    # logging.info("d")
    # if added:
    #     rg.remove_node(pos_tgt)

    if len(path) > 1:
        adjust_collinear_path(path, rl)
        rl.set_species_curve_points(s_id, role, path)
        rg.add_path(path)
        ol.add_points(path)
        return True
    else:
        rg.add_path(points)
        return False




def create_grid(g: nx.MultiGraph,
                spacing: List[int] = [settings.render.species_glyph_width, settings.render.species_glyph_height],
                cnt_padding_lines=0) -> Grid:

    minmax = lt.min_max_coords(g)

    x_grid_coord = []
    for pos in range(math.floor(minmax.min[0]), math.floor(minmax.max[0]+1), spacing[0]):
        x_grid_coord.append(pos)

    y_grid_coord = []
    for pos in range(math.floor(minmax.min[1]), math.floor(minmax.max[1]+1), spacing[1]):
        y_grid_coord.append(pos)

    assert len(y_grid_coord) > 0 and len(x_grid_coord) > 0

    for i in range(cnt_padding_lines):
        x_grid_coord = [x_grid_coord[0] - spacing[0]] + x_grid_coord
        x_grid_coord.append(x_grid_coord[-1] + spacing[1])
        y_grid_coord = [y_grid_coord[0] - spacing[0]] + y_grid_coord
        y_grid_coord.append(y_grid_coord[-1] + spacing[1])

    return Grid(x_grid_coord, y_grid_coord)


def align_to_grid(g: nx.MultiGraph, params: AlignToGridParams=AlignToGridParams()):

    grid = create_grid(g, params.spacing)

    for n_id in gu.get_all_species_ids(g):
        node = g.nodes[n_id]
        nc = lt.get_node_pos(node)
        cgl = grid.get_closest_grid_lines(nc)
        lt.set_node_pos(node, [grid.xs[cgl[0]], grid.ys[cgl[1]]])


def remove_empty_axis(g: nx.MultiGraph, is_x, grid_spacing, nl_assignment, cnt_lines, target_empty_lines: int):

    nodes = g.nodes()

    ix_axis = 0 if is_x else 1

    last_non_empty = 0
    for i in range(1, len(cnt_lines)):

        if cnt_lines[i] > 0:

            if i - last_non_empty - 1 > target_empty_lines :
                shift_size = -(grid_spacing * (i - (last_non_empty + target_empty_lines + 1)))
                if is_x:
                    shift = np.array([shift_size, 0])
                else:
                    shift = np.array([0, shift_size])

                for n_id in nl_assignment:
                    ix_line = nl_assignment[n_id][ix_axis]
                    if ix_line >= i:
                        lt.shift_node_coords(nodes[n_id], shift)

            last_non_empty = i


def remove_empty_bands(g: nx.MultiGraph,
                       params:RemoveEmptyBandsParams = RemoveEmptyBandsParams()):


    def handle_axis(coords, ts, axis):

        ts_axis = ts[axis]
        active = {coords[0][1]}
        for i in range(1, len(coords)):
            s_id = coords[i][1]
            if coords[i][2] == 1:
                active.remove(s_id)
            else:
                dist = math.fabs(coords[i][0] - coords[i-1][0])
                if len(active) == 0 and dist > ts_axis:
                    shift = np.array([-(dist-ts_axis), 0]) if axis == 0 else np.array([0, -(dist-ts_axis)])
                    for j in range(i, len(coords)):
                        if coords[j][2] == 0:
                            lt.shift_node_coords(g.nodes[coords[j][1]], shift)
                active.add(s_id)

    xs = []
    ys = []
    for s_id in gu.get_all_species_ids(g):
        bb:gr.Rectangle = gu.get_predicted_layout(g, s_id).get_bb().get()
        #for each record we have inforamtion about it's position, species id, and whether is the first or second coordinate of the boundind box
        xs.append((bb.get_x1(), s_id, 0))
        xs.append((bb.get_x2(), s_id, 1))
        ys.append((bb.get_y1(), s_id, 0))
        ys.append((bb.get_y2(), s_id, 1))

    xs = sorted(xs)
    ys = sorted(ys)

    handle_axis(xs, params.target_space, 0)
    handle_axis(ys, params.target_space, 1)



    # nl_assignment = {} # for each node stores its closest axis assignment, i.e. ixs of closest x and y grid lines
    # cnt_x_lines = [0 for i in range(len(grid.xs))]
    # cnt_y_lines = [0 for i in range(len(grid.ys))]
    # for n_id in gu.get_all_species_ids(g):
    #
    #     p = lt.get_node_pos(nodes[n_id])
    #     cgl = grid.get_closest_grid_lines(p) #indexes of closest x and y grid lines
    #
    #     nl_assignment[n_id] = cgl
    #     cnt_x_lines[cgl[0]] += 1
    #     cnt_y_lines[cgl[1]] += 1
    #
    # target_empty_lines = [math.ceil(target_space[0] / spacing[0]), math.ceil(target_space[1] / spacing[1])]
    # remove_empty_axis(g, True, grid.x_spacing, nl_assignment, cnt_x_lines, target_empty_lines[0])
    # remove_empty_axis(g, False, grid.y_spacing, nl_assignment, cnt_y_lines, target_empty_lines[1])



def get_trailing_pairs(g: nx.MultiGraph) -> List[Tuple[str, str]]:
    """
    Get id pairs of  where the second id corresponds to a one-degree node. This can be both species or reaction as
    in the process we are removing the trailing species from the auxiliary graph.
    :param g:
    :return:
    """

    pairs = []
    for n in g:
        if len(g[n]) == 1:
            pairs.append((list(g[n])[0], n ))
    return pairs


def get_compacted(g: nx.MultiGraph, s_id, compacted: List[str]) -> List[str]:
    """
    Identify nodes which were already processed (i.e. they are in the chain "behind" the current node) and will
    have to be shifted as the current node is being shifted.
    :param g:
    :param s_id:
    :param shift:
    :param compacted:
    :return:
    """

    processed = []
    shifted = []
    n_ids = [s_id]

    while True:
        nb_ids = set()
        for n_id in n_ids:
            nb_ids = nb_ids.union(g[n_id])

        # We want to shift only nodes which were already compacted and were not yet processed
        nb_ids = list(nb_ids.intersection(compacted).difference(processed))

        if len(nb_ids) == 0:
            break

        if gu.is_species(g, nb_ids[0]): #all the nodes are either species or reactions (bipartite graph)
            for nb_id in nb_ids:
                nb = g.nodes()[nb_id]
                shifted.append(nb)

        processed += nb_ids

        n_ids = nb_ids

    return shifted


def shift_nodes_if_improves(g, nodes_to_shift, r_pos, s_pos, shift, cnt_overlaps_before):

    shifted = False

    for n in nodes_to_shift:
        lt.set_node_pos(n, lt.get_node_pos(n) + shift)

    cnt_overlaps_after = gu.count_intersections(g, gr.Line(r_pos, s_pos))

    if cnt_overlaps_after > cnt_overlaps_before:
        for n in nodes_to_shift:
            lt.set_node_pos(n, lt.get_node_pos(n) - shift)
    else:
        shifted = True

    return shifted


def compact_pair(g: nx.MultiGraph, r_id: str, s_id: str, compacted: List[str], dims: CompartmentDimensions):
    """

    :param g:
    :param r_id:
    :param s_id:
    :param compacted: List of species and reacion ids which have already been compacted
    :return:
    """

    r_pos = gu.get_predicted_layout(g, r_id).get_center()
    s_pos = gu.get_predicted_layout(g, s_id).get_center()
    sr = r_pos - s_pos

    if gr.utils.size(sr) < settings.beautification.compact_optimal_dist:
        return

    #get nodes that have to be shifted together with s_id, i.e. are "behind" s and have already beeen compacted
    nodes_to_shift = get_compacted(g, s_id, compacted)
    r_nb_ids = list(g[r_id])
    if len(r_nb_ids) == 2:
        # If it is a reaction connecting just two species, the shift vector will be based on the two species
        s_nb_id = r_nb_ids[0] if r_nb_ids[1] == s_id else r_nb_ids[1]
        s_nb_pos = gu.get_predicted_layout(g, s_nb_id).get_center()
        ssnb = s_nb_pos - s_pos
        shift = gr.utils.normalize(ssnb) * (gr.utils.size(ssnb) - settings.beautification.compact_optimal_dist)
    else:
        # If we have reactions with multiple species, we will shift the species towards the reaction center
        shift = gr.utils.normalize(sr) * (gr.utils.size(sr) - settings.beautification.compact_optimal_dist / 2)

    # cnt_overlaps_before = gu.count_intersections(g, gr.Line(r_pos, s_pos))

    s_cmp_dim = dims[gu.get_species(g, s_id).get_compartment_id()]
    if not s_cmp_dim.has_point(s_pos + shift):
        # Modify shift so that it goes to the compartment border
        r = s_cmp_dim.get_rect()
        s_s_shift = gr.Line(s_pos, s_pos + shift)
        isect = s_s_shift.intersection(r)
        shift = isect - s_pos
        shift -= gr.utils.normalize(shift) * 80 #we position the species close to the border not on the border


    if gr.utils.size(shift) > settings.render.species_glyph_width:
        for n in nodes_to_shift + [gu.get_node(g, s_id)]:
            lt.set_node_pos(n, lt.get_node_pos(n) + shift)

    # while gr.utils.size(shift) > settings.render.species_glyph_width:
        # if shift_nodes_if_improves(g, nodes_to_shift + [gu.get_node(g, s_id)], r_pos, s_pos, shift, cnt_overlaps_before):
        #     break
        # else:
        #     shift = shift / 2


def recenter_reactions(g: nx.MultiGraph):
    """
    Some beautification operations only work over species coordinates and then reaction coordinates are off.
    That might be an issue in some procedure which take reaction positions into account. Although, when laying
    out the network in SBML, the positions reactions points (center, connection points of reactants and product)
     are computed only from species positions.
    """

    nodes = g.nodes()
    for r_id in gu.get_all_reaction_ids(g):
        rc = sbml.ReactionSbmlCoords(g, r_id)
        lt.set_node_pos(nodes[r_id], rc.center)


def compact_trailing_paths(g: nx.MultiGraph, params: CompactTrailingPathsParams = CompactTrailingPathsParams()):

    dims = get_compartments_dimensions(g)

    #as we currently move the species to their optimal distance, we do not actually need the iteration_count
    # for i in range(params.iteration_count):
    for i in [1]:
        g_aux = gu.deepcopy_graph(g) # we need a deepcopy since it is a temporary structure which will need to be modified

        compacted = [] # We need to store information about which species has already been processed,
                        # because when shifing a node we also need to shift with it all the nodes which are down the chain

        while True:

            tp = get_trailing_pairs(g_aux)
            if len(tp) == 0:
                break
            for p in tp:
                if gu.is_reaction(g_aux, p[0]):
                    # only species are shifted since reaction positions are in the end computed from the positions of the species
                    compact_pair(g, p[0], p[1], compacted, dims)

                compacted.append(p[1]) #this includes both species and reaction

            g_aux.remove_nodes_from([p[1] for p in tp])

        # recenter_reactions(g)



def shift_all_species_away_from(g, n_id, center: np.ndarray, shift:np.ndarray):

    # When a species is repositioned, all reactions which are connected to it are repositioned as well

    for node_id in gu.get_all_species_ids(g):

        if node_id == n_id:
            continue

        node = g.nodes[node_id]

        coords = lt.get_node_pos(node)
        sc = coords - center #stores the direction in which the node should me shifted

        direction = np.sign(sc)

        if sc[0] == 0 and sc[1] == 0:
            # If the node sits exactly on top of the node from which it should be pushed, we choose
            # the direction based on the weighted average of positions of neighboring species
            nn_ids = gu.get_neibhbors_in_distance(g, node_id, 1) + gu.get_neibhbors_in_distance(g, node_id, 2) + gu.get_neibhbors_in_distance(g, node_id, 3)
            if len(nn_ids) == 0:
                # arbitrary
                direction = np.array([1,1])
            else:
                aux = sum([lt.get_node_pos(g.nodes[nn_id]) for nn_id in nn_ids]) / len(nn_ids) - center
                if aux[0] != 0 or aux[1] != 0:
                    direction = np.sign(aux)
                else:
                    # The average position of all the neighboring species is exactly 0
                    # eg if the two nodes are B, D then the following will lead to such situation A----BD----C.
                    # In such situation let's pick the first neighboring species and move in that direction
                    direction = np.sign(lt.get_node_pos(g.nodes[nn_ids[0]]) - center)

        new_coords = gr.shift_cartesian(coords, shift * direction)
        lt.set_node_pos(node, new_coords)


def inflate_species_overlap(g: nx.MultiGraph, params: InflateParams):

    gw = settings.render.species_glyph_width
    gh = settings.render.species_glyph_height

    padding = np.array([gw, gh]) * params.species_padding_ratio

    s_ids = gu.get_all_species_ids(g)

    cnt = 0
    for i1 in range(len(s_ids)):
        cnt += 1

        # center1 = lt.get_node_pos(g.nodes[s_ids[i1]])
        r1: gr.Rectangle = gu.get_predicted_layout(g, s_ids[i1]).get_bb().get()
        i_closest = -1
        dist_closest = sys.maxsize

        for i2 in range(i1 + 1, len(s_ids)):

            r2: gr.Rectangle = gu.get_predicted_layout(g, s_ids[i2]).get_bb().get()

            intersection = r1.intersects_rectangle(r2, padding)
            if intersection:
                dist = np.linalg.norm(r2.get_center() - r1.get_center())
                if dist < dist_closest:
                    dist_closest = dist
                    i_closest = i2
                    if dist_closest == 0:
                        break

        if i_closest >= 0:
            # TODO make the shift based on the padding
            dist = abs(lt.get_node_pos(g.nodes[s_ids[i1]]) - lt.get_node_pos(g.nodes[s_ids[i_closest]]))
            shift = np.array([gw - dist[0] + padding[0], gh - dist[1] + padding[1]])
            shift_all_species_away_from(g, s_ids[i1], r1.get_center(), shift)


def inflate_reactions(g: nx.MultiGraph, params: InflateParams):
    min_dist = params.reactant_product_distance

    for r_id in gu.get_all_reaction_ids(g):
        rl = gu.get_predicted_layout(g, r_id)
        pos_mr = rl.get_main_reactant_pos()
        pos_mp = rl.get_main_product_pos()
        dist_mr_mp = gr.utils.dist(pos_mr, pos_mp)


        if dist_mr_mp < min_dist:
            # push the species from each other
            if dist_mr_mp == 0:
                mr_id = rl.get_main_reactant_id()
                mp_id = rl.get_main_product_id()
                r_species = set(g[r_id])
                mr_neighbors = set(gu.get_neibhbors_in_distance(g, mr_id, 2)) - r_species
                mp_neighbors = set(gu.get_neibhbors_in_distance(g, mp_id, 2)) - r_species
                pos_mr_n = sum([gu.get_predicted_layout(g, n_id).get_center() for n_id in mr_neighbors])/len(mr_neighbors) if len(mr_neighbors) > 0 else pos_mr
                pos_mp_n = sum([gu.get_predicted_layout(g, n_id).get_center() for n_id in mp_neighbors]) / len(mp_neighbors) if len(mp_neighbors) > 0 else pos_mp
                if gr.utils.equal(pos_mp_n, pos_mr_n):
                    shift = np.array([min_dist, 0])
                else:
                    aux = pos_mp_n - pos_mr_n
                    if gr.utils.is_more_horizontal(aux):
                        shift = np.array([min_dist, 0]) if pos_mp_n[0] > pos_mr_n[0] else np.array([-min_dist, 0])
                    else:
                        shift = np.array([0, min_dist]) if pos_mp_n[1] > pos_mr_n[1] else np.array([0, -min_dist])
            else:
                pos_opt = pos_mr + gr.utils.normalize(pos_mp - pos_mr) * min_dist
                shift = abs(pos_opt - pos_mp)

            shift_all_species_away_from(g, n_id=rl.get_main_reactant_pos(), center=pos_mr, shift=shift)
            # update of the neibhoring reactions happens inside the procedure which updates position of a species



def inflate(g: nx.MultiGraph, params: InflateParams=InflateParams()):
    inflate_reactions(g, params)
    inflate_species_overlap(g, params)




# def push_away_side_species(g: nx.MultiGraph):
#     """
#     This method checks for modifiers and sidesubstrate and sideproducts which intersect lines connecting main reactants and products and
#     shifts them so that they do not intesect it any more. This is especially needed if the centers
#     of a modifier, reactant and product have the same x or y coordinate (which can easily happen after
#     aligning to a grid) in which case the all the modifier connecting line coincide with the main reaction line
#     :param g:
#     :return:
#     """
#     # TODO this will have to be modified as soon as complex lines become implemented
#
#     def get_new_coords(rp_point, ortho):
#         return rp_point + ortho * settings.render.modifiers_optimal_dist
#
#     for r_id in gu.get_all_reaction_ids(g):
#
#         rs_ids = gu.get_reaction_species(g, r_id)
#         rsl:be.ReactionLayout = gu.get_node_data(g.nodes[r_id]).get_layout(layout_type=be.LAYOUT_TYPE.PREDICTED) #sbml.ReactionSbmlCoords(g, r_id)
#
#         bb_mr = gu.get_predicted_layout(g, rsl.get_main_reactant_id()).get_bb().get()
#         bb_mp = gu.get_predicted_layout(g, rsl.get_main_product_id()).get_bb().get()
#
#         coord_r = rsl.get_main_reactant_pos()
#         coord_p = rsl.get_main_product_pos()
#
#         # https://answers.unity.com/questions/535822/how-to-find-the-vector-that-passes-through-a-point.html
#         d = coord_p - coord_r
#
#         side_species_ids = set(rs_ids.modifierIds+ rsl.get_side_reactants_ids() + rsl.get_side_products_ids())
#
#         #TODO handle side species lying on line with the reaction
#         for id_ss in side_species_ids:
#             bb_ss = gu.get_predicted_layout(g, id_ss).get_bb().get()
#
#             ssl = rsl.get_side_species_line(id_ss)
#
#             if rsl.intersection(bb_ss) or ssl.intersects_rectangle(bb_mr) or ssl.intersects_rectangle(bb_mp):
#                 coord_ss = lt.get_node_pos(g.nodes[id_ss])
#                 o = coord_ss - coord_r
#
#                 # pv = coord_ss - coord_r - o.dot(d)/d.dot(d)*d.p #vector perpendicular to the coord_r--coord_p segment passing trhough the coord_ss
#                 pv = coord_ss - coord_r - gr.utils.dot(o, d) / gr.utils.dot(d, d) * d  # vector perpendicular to the coord_r--coord_p segment passing trhough the coord_ss
#                 rp_point = coord_ss - pv
#
#                 on_line = np.all(rp_point == coord_ss)
#
#                 if on_line:
#                     #if the side species lies on line with the reaction, then we simply use orthogonal vector to the reaction line
#                     ortho = -gr.utils.normalize(gr.utils.orthogonal(coord_p - coord_r))
#                 else:
#                     ortho = gr.utils.normalize(pv)
#
#                 new_ss = get_new_coords(rp_point, ortho)
#
#                 if on_line:
#                     # when the side species is on line with the main reaction line,
#                     # we have two possibilities of how to orient the orthogonal vector
#                     scp = rsl.get_species_connecting_point(id_ss)
#                     new_ss_ortho = get_new_coords(rp_point, -ortho)
#                     if gu.count_intersections(g, gr.Line(scp, new_ss_ortho)) < gu.count_intersections(g, gr.Line(scp, new_ss)):
#                         new_ss = new_ss_ortho
#
#                 lt.set_node_pos(g.nodes[id_ss], new_ss)

# class OverlapInfo:
#
#     def __init__(self, r1_id, r2_id):
#         self.__r1_id = r1_id
#         self.__r2_id = r2_id
#         self.__r1_species_covered_by_r2:List[OverlapInfoSpeciesLine] = []
#         self.__r2_species_covered_by_r1:List[OverlapInfoSpeciesLine] = []
#
#     @property
#     def r1_id(self):
#         return self.__r1_id
#
#     @property
#     def r2_id(self):
#         return self.__r2_id
#
#     @property
#     def r1_species_covered_by_r2(self) -> List[OverlapInfoSpeciesLine]:
#         return self.__r1_species_covered_by_r2
#
#     @property
#     def r2_species_covered_by_r1(self) -> List[OverlapInfoSpeciesLine]:
#         return self.__r2_species_covered_by_r1
#
#     def __eq__(self, other: 'OverlapInfo'):
#         return (self.r1_id == other.r1_id and self.r2_id == other.r2_id) or (self.r2_id == other.r1_id and self.r1_id == other.r2_id)
#
#     def is_overlapping(self) -> bool:
#         return len(self.r1_species_covered_by_r2) > 0 or len(self.r2_species_covered_by_r1) > 0
#
#     def is_partially_overlapping(self):
#         # We use >=1 because it can happen that a center of a reaction overlaps with a species bounding box
#         # and in such case we have one species overlaped by two lines
#         return self.is_overlapping() and (len(self.r1_species_covered_by_r2) >= 1 or len(self.r2_species_covered_by_r1) >= 1)
#
#     def is_fully_overlapping(self):
#         return self.is_overlapping() and not self.is_partially_overlapping()
#
#     def get_overlapped_r_ids(self):
#         ids = []
#         if len(self.r2_species_covered_by_r1) > 0:
#             ids.append(self.r2_id)
#         if len(self.r1_species_covered_by_r2) > 0:
#             ids.append(self.r1_id)
#         return ids
#
#     def get_species_overlapped_by(self, r_id) -> List[str]:
#         return [sl.s_id for sl in self.get_species_line_overlapped_by(r_id)]
#
#     def get_species_line_overlapped_by(self, r_id) -> List[OverlapInfoSpeciesLine]:
#         if self.r1_id == r_id:
#             return self.r2_species_covered_by_r1
#         elif self.r2_id == r_id:
#             return self.r1_species_covered_by_r2
#         else:
#             return []
#
#     def get_overlapping_r_ids(self):
#         ids = []
#         if len(self.r2_species_covered_by_r1) > 0:
#             ids.append(self.r1_id)
#         if len(self.r1_species_covered_by_r2) > 0:
#             ids.append(self.r2_id)
#         return ids


# def resolve_partially_overlapping(g, ois: List[OverlapInfo]):
#     NotImplemented
#
# def resolve_fully_overlapping(g, ois: List[OverlapInfo]):
#
#     fo_ois =  [oi for oi in ois if oi.is_fully_overlapping()]
#
#     for oi in fo_ois:
#         r_id = oi.get_overlapping_r_ids()[0] #there should be only one since its fully overlapping
#
#         #identify all OI species which are intersected by this reaction
#         intersected_ids = []
#         for oi2 in ois:
#             if oi2 == oi:
#                 continue
#             intersected_ids += oi2.get_species_overlapped_by(r_id)
#
#         # we pick one line of the reaction which overlaps a species
#         # !!!!! there is an assumption that all the segments which are overlapping with other reaction lines
#         # are lying on one the same line and that the center of the reaction is on this line as well
#         line = oi.get_species_line_overlapped_by(r_id)[0].line
#
#         # move the center of the lines perpendicularly to the line so that it does not intersect the species any more
#
#
#
# def get_overlap_info(g, r1_id, r2_id) -> OverlapInfo:
#     oi = OverlapInfo(r1_id, r2_id)
#
#     lt1:be.ReactionLayout = gu.get_predicted_layout(g, r1_id)
#     lt2:be.ReactionLayout = gu.get_predicted_layout(g, r2_id)
#
#     species1 = set(lt1.get_species_ids())
#     species2 = set(lt2.get_species_ids())
#     species_common = species1 & species2
#
#     #do not consider shared species
#     species1 = list(species1 - species_common)
#     species2 = list(species2 - species_common)
#
#
#     bbs1: List[gr.Rectangle] = [gu.get_predicted_layout(g, s_id).get_bb().get() for s_id in species1]
#     bbs2: List[gr.Rectangle] = [gu.get_predicted_layout(g, s_id).get_bb().get() for s_id in species2]
#
#     l1_lines = lt1.get_all_lines()
#     l2_lines = lt2.get_all_lines()
#
#     l1_co_lines: List[gr.Line] = []
#     l2_co_lines: List[gr.Line] = []
#     for l1 in l1_lines:
#         for l2 in l2_lines:
#             if gr.orientation(l1[0], l1[1], l2[0]) == 0 and gr.orientation(l1[0], l1[1], l2[1]) == 0:
#                 if l1 not in l1_co_lines:
#                     l1_co_lines.append(l1)
#                 if l2 not in l2_co_lines:
#                     l2_co_lines.append(l2)
#
#     for l1 in l1_co_lines:
#         for i in range(len(species2)):
#             if l1.intersects_rectangle(bbs2[i]):
#                 oi.r2_species_covered_by_r1.append(OverlapInfoSpeciesLine(species2[i], l1))
#
#     for l2 in l2_co_lines:
#         for i in range(len(species2)):
#             if l2.intersects_rectangle(bbs1[i]):
#                 oi.r1_species_covered_by_r2.append(OverlapInfoSpeciesLine(species1[i], l2))
#
#     return oi
#
#
# def get_overlapping_reactions(g: nx.MultiGraph) -> List[OverlapInfo]:
#
#     ois:List[OverlapInfo] = []
#
#     r_ids = gu.get_all_reaction_ids(g)
#     for i1 in range(len(r_ids)):
#         for i2 in range(i1+1, len(r_ids)):
#             oi = get_overlap_info(g, r1_id=r_ids[i1], r2_id=r_ids[i2])
#             if oi.is_overlapping():
#                 ois.append(oi)
#
#
#     return ois
#
# def handle_overlapping_segements(g: nx.MultiGraph):
#
#     o_r = get_overlapping_reactions(g)
#     # resolve_partially_overlapping(g, o_r)
#     resolve_fully_overlapping(g, o_r)
#
#
#
# def compact(g: nx.MultiGraph):
#     i = 1
#     compact_trailing_paths(g)
#
#


# def remove_empty_grid_lines(g: nx.MultiGraph, grid:Grid=None,
#                             spacing: List[int]= [settings.render.species_glyph_width, settings.render.species_glyph_height],
#                             target_space: List = [settings.render.species_glyph_width, settings.render.species_glyph_height]):
#     if grid is None:
#         grid = create_grid(g, spacing)
#
#     nodes = g.nodes()
#
#     nl_assignment = {} # for each node stores its closest axis assignment, i.e. ixs of closest x and y grid lines
#     cnt_x_lines = [0 for i in range(len(grid.xs))]
#     cnt_y_lines = [0 for i in range(len(grid.ys))]
#     for n_id in gu.get_all_species_ids(g):
#
#         p = lt.get_node_pos(nodes[n_id])
#         cgl = grid.get_closest_grid_lines(p) #indexes of closest x and y grid lines
#
#         nl_assignment[n_id] = cgl
#         cnt_x_lines[cgl[0]] += 1
#         cnt_y_lines[cgl[1]] += 1
#
#     target_empty_lines = [math.ceil(target_space[0] / spacing[0]), math.ceil(target_space[1] / spacing[1])]
#     remove_empty_axis(g, True, grid.x_spacing, nl_assignment, cnt_x_lines, target_empty_lines[0])
#     remove_empty_axis(g, False, grid.y_spacing, nl_assignment, cnt_y_lines, target_empty_lines[1])
