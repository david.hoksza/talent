import networkx as nx
from networkx.readwrite import json_graph
import copy
import json
# import jsonpickle
import pickle
import logging

from typing import List
from typing import Dict

import warnings

import numpy as np
import matplotlib.pyplot as plt
# import matplotlib.pyplot
# matplotlib.use('agg')
# import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import seaborn as sns
import random

from . import graphics as gr
from . import bioentity as be
from .utils import convert
from . import common
from . import settings


class ReactionSpecies:
    def __init__(self):
        self.reactantIds = []
        self.modifierIds = []
        self.productIds = []

    def get_ids(self):
        return self.reactantIds + self.modifierIds + self.productIds


def remove_invalid_reactions(g:nx.MultiGraph):
    """
    Remove reactions which are not valid. Currently, these include reactions which do not have
    main reactant or product which can happen, e.g., when splitting a network into subnetworks (e.g. based
    on compartment membership).
    :param g:
    :return:
    """
    to_remove = []
    for r_id in get_all_reaction_ids(g):
        if not get_node_data(g.nodes[r_id]).is_valid():
            to_remove.append(r_id)
            logging.warning("Reaction {} is missing either the main reactant or product. Removing...".format(r_id))
    g.remove_nodes_from(to_remove)
    return g


def load_graph(f_name, format) -> nx.MultiGraph:
    # if format == 'json':
    #     return remove_invalid_reactions(load_json(f_name))
    if format == 'pkl':
        return remove_invalid_reactions(load_pkl(f_name))
    # elif format == 'sbml':
    #     g, sbml_helper = load_sbml(f_name, source_is_file=True)
    #     return remove_invalid_reactions(g)
    else:
        with common.open_file(f_name) as f:
            # Even when converting from SBML without a layout, the conversion procedure enriches the file with layout
            sbml_string = convert.minerva_convert(in_string=f.read(), in_format=format, out_format="sbml",
                                minerva_instance=settings.minerva_instance)
            g, sbml_helper = load_sbml(source=sbml_string, source_is_file=False)
            return remove_invalid_reactions(g)


def load_sbml(source, source_is_file):
    from . import sbml

    g = nx.MultiGraph()
    sbml_helper = sbml.SbmlHelper(source, source_is_file)
    for s in sbml_helper.getSpecies():
        g.add_node(s.getId(), type='SPECIES', data=s)
    for r in sbml_helper.getReactions():
        g.add_node(r.getId(), type='REACTION', data=r)
        for id in r.getReactants():
            g.add_edge(id, r.getId(), sbml.SPECIES_ROLE.REACTANT)
        for id in r.getProducts():
            g.add_edge(r.getId(), id, sbml.SPECIES_ROLE.PRODUCT)
        for id in r.getModifiers():
            g.add_edge(id, r.getId(), sbml.SPECIES_ROLE.MODIFIER)

    return get_layout_graph(g), sbml_helper


def create_sbml():
    '''
    Creates SBML structure (without layout or render information) from a graph
    :param g:
    :return:
    '''
    from . import sbml
    return sbml.SbmlHelper()


def graph_to_sbml(g:nx.MultiGraph) -> str:


    sbml = create_sbml()
    sbml.addGraph(g)

    sbml.removeExistingLayouts()
    sbml.addLayout(g)

    return sbml.get_sbml_string()


def format_cmprtmnt_name(cmprtmentName):
    return cmprtmentName.replace('/', '_').replace('\n', '__')


def load_json(f_name):
    with open(f_name, 'rb') as f:
    # with common.open_file(f_name, 'r') as f:
        # data = json.load(f)
        # data = jsonpickle.deco%de(f.read())
        data = pickle.load(f)
        #TODO validate that the loaded graph is indeed a valid layout graph
        return json_graph.node_link_graph(data)


def load_pkl(f_name):
    with open(f_name, 'rb') as f:
        g = pickle.load(f)
        return g


def serialize_graph(g):
    nodes = '--'.join(sorted(g.nodes()))
    edges = '--'.join(['{}_{}'.format(e[0], e[1]) for e in sorted(g.edges())])
    return nodes + edges
    # return pickle.dumps(json_graph.node_link_data(g))



def save_graph(g, path, format):
    if format == 'pkl':
        # data = json_graph.node_link_data(g)
        # with common.open_file(path, 'w') as f:
        with open(path, 'wb') as f:
            # f.write(json.dumps(data, indent=2))
            # f.write(jsonpickle.encode(data))
            # pickle.dump(data, f)
            pickle.dump(g, f)
    # elif format=='json':
    # json is not supported anymore since the strucutre contain circular references and Enums which are difficult to serialize
    #     data = json_graph.node_link_data(g)
    #     with common.open_file(path, 'w') as f:
    #         f.write(json.dumps(data, indent=2))
    elif format == 'edgelist':
        nx.write_edgelist(g, path, data=False, delimiter="\t")
    else:
        logging.error("{} - unsupported output format".format(format))
        exit(1)


def getNeighborsCount(g, filters):
    # g_undirected = g.to_undirected()
    # return [len(list(g_undirected.neighbors(node))) for node in get_filtered_nodes(g, filters)]
    return [len(list(g.neighbors(node))) for node in get_filtered_nodes(g, filters)]


def get_connected_components(g, copy=True, to_undirected=False):
    g_und = g.to_undirected() if to_undirected else g

    if copy:
        return [get_normalized_copy(get_subgraph(g, c)) for c in list(nx.algorithms.components.connected_components(g_und))]
    else:
        return [get_subgraph(g, c) for c in list(nx.algorithms.components.connected_components(g_und))]


def getConnectedComponentsSizes(g, filters = {}):
    counts = []
    for c in get_connected_components(g):
        counts.append(len(get_filtered_nodes(c, filters)))
    return counts


def printStatistics(g):

    print()
    print('-------------------- STATISTICS --------------------')
    sns.set(color_codes=True)

    print('{:50}{}'.format('Number of nodes', g.order()))
    print('{:50}{}'.format('Number of edges', g.size()))
    print('{:50}{}'.format('Number of species', len(get_filtered_nodes(g, {'type': be.TYPE.SPECIES}))))
    print('{:50}{}'.format('Number of reactions', len(get_filtered_nodes(g, {'type': be.TYPE.REACTION}))))
    print('{:50}{}'.format('Number of connected components', nx.algorithms.components.number_connected_components(g.to_undirected())))

    print('{:50}{}'.format('Sizes of connected components (species)',getConnectedComponentsSizes(g, {'type': 'SPECIES'})))

    # print('{:50}{}'.format('Cycle basis', [len(c) for c in  nx.algorithms.cycles.cycle_basis(g.to_undirected())] ))

    print('{:50}{}'.format('Species neighbors counts', getHistogram(getNeighborsCount(g, {'type': be.TYPE.SPECIES}))))
    print('{:50}{}'.format('Reactions neighbors counts', getHistogram(getNeighborsCount(g, {'type': be.TYPE.REACTION}))))

    # sns.distplot(getNeighborsCount({'type': 'REACTION'}), hist_kws={'density': False})
    # plt.show()

    print('----------------------------------------------------')
    print()


def getHistogram(arr):
    if len(arr) == 0:
        warnings.warn('Creating histogram from an empty array')
        return []
    hist, edges = np.histogram(arr, np.linspace(0, max(arr), max(arr) + 1))
    return hist


def get_filtered_nodes(g, filters, filterFunctions=[]) -> List[str]:
    """

    :param g:
    :param filters:
    :param dataFilterFunctions: list of functions to be applied on attrs['data']
    :return:
    """
    return sorted([node_id for node_id, attrs in g.nodes(data=True) if attributesMatch(attrs, filters, filterFunctions)])


def attributesMatch(attrs, filters, filterFunctions):
    for k, v in filters.items():
        if k not in attrs or attrs[k] != v:
            return False

    for fun in filterFunctions:
        if not fun(get_node_data(attrs)):
            return False

    return True


def draw(gs, layout_keys=None, txts=None, with_labels=True, node_size=50, outputFile=None):

    if type(gs) != type([]):
        gs = [gs]

    sns.set(color_codes=True)
    # ncols = (2 if len(gs) > 0 else 1)
    ncols = 1
    nrows = int(np.ceil(len(gs) / ncols))
    ix_plt = 0

    # plt.figure(1)

    for ix_gs in range(len(gs)):

        g = gs[ix_gs]

        if len(g) == 0:
            logging.warning("Trying to draw an empty graph!")
            continue

        layout_key = be.LAYOUT_TYPE.ORIGINAL if layout_keys is None else layout_keys[ix_gs]
        minX = float('inf')
        minY = float('inf')
        maxX = float('-inf')
        maxY = float('-inf')
        pos = {}
        labels = {}
        node_colors = []
        node_sizes = []
        for node_id, attrs in g.nodes(data=True):

            entity = get_node_data(attrs)

            layout = entity.get_layout(layout_key)

            node_colors.append((1,0,0) if entity.is_species() else (0,0,1))
            node_sizes.append(node_size)



            color = layout.get_visual().get_color()
            if color:
                node_colors[len(node_colors)-1] = color
            size = layout.get_visual().get_size()
            if size:
                node_sizes[len(node_sizes) - 1] = size

            name = entity.get_name()
            labels[node_id] = name if name else ''

            if not layout or layout.get_center() is None:
                pos[node_id] = None
            else:
                [x, y] = layout.get_center()
                y = -y
                pos[node_id] = [x, y]
                minX = min(x, minX)
                minY = min(y, minY)
                maxX = max(x, maxX)
                maxY = max(y, maxY)


        ix_plt += 1
        width = maxX - minX + 1
        height = maxY - minY + 1
        px_width = width/100
        plt.subplot(nrows, ncols, ix_plt) if not outputFile else plt.figure(figsize=(px_width, px_width*height/width))
        if txts:
            plt.title(txts[ix_plt-1])
        nx.draw(g, pos=pos, with_labels=with_labels, node_size=node_sizes, alpha=0.6, labels=labels, font_size=10, node_color=node_colors)



    plt.show() if not outputFile else multipage(outputFile)
    plt.close('all')


def multipage(filename, figs=None, dpi=200):
    pp = PdfPages(filename)
    if figs is None:
        figs = [plt.figure(n) for n in plt.get_fignums()]
    for fig in figs:
        fig.savefig(pp, format='pdf', dpi=dpi)
    pp.close()


def removeOneComponents(g):
    g_mod = deepcopy_graph(g)
    to_remove = []

    for c in nx.algorithms.components.connected_components(g_mod):
        if len(c) == 1:
            to_remove.append(list(c)[0])

    g_mod.remove_nodes_from(to_remove)
    return g_mod


def get_one_degree_species(g: nx.MultiGraph) -> List[str]:
    return [s_id for s_id in get_all_species_ids(g) if len(g[s_id]) > 0]


def removeOneDegreeSpecies(g):
    g_mod = deepcopy_graph(g)
    g_mod.remove_nodes_from(get_one_degree_species(g))
    return g_mod


# def imputeReactionsPositions(g):
#     g_mod = g.copy()
#     for node_id, attrs in g_mod.nodes(data=True):
#         if attrs['type'] == 'REACTION':
#             x = y = cnt = 0
#             for n in nx.all_neighbors(g_mod, node_id):
#                 cnt += 1
#                 x += g_mod.nodes[n]['bbox']['x']
#                 y += g_mod.nodes[n]['bbox']['y']
#             x /= cnt
#             y /= cnt
#             g_mod.nodes[node_id]['bbox'] = {
#                 'x': x,
#                 'y': y,
#                 'width': 0,
#                 'height': 0
#             }
#     return g_mod

def get_layout_graph(g) -> nx.MultiGraph:
    """

    #The layout graph contains for each layout node, i.e. for each glyph. That is different from the original
    # SBML-based graph, where one species can correspond to multiple glyphs in the layout (the layout attribute
    # has one set of information for every layout id)

    :param g:
    :return: Graph with nodes having data attribute being either bioentity.Species or bioentity.Reaction
    and edges labeled by bioentity.SPECIES_ROLE
    """



    g_mod = nx.MultiGraph()
    g_mod_attrs = {} #attributes for new nodes will be stored here and in the end added to a graph in batch mode

    for node_id, attrs in g.nodes(data=True):
        # Each node should contain a layout (intially obtained from MINERVA API when converting the input file (even when it already is SBML) to an SBML with layout)
        if attrs['data'].__class__.__name__ == 'Species' and attrs['data'].getLayout():
            #each species can be used in diffrent reactions within a compartment
            for l_id in attrs['data'].getLayout():
                #layout comes from sbmllib's getSpeciesLayoutData

                new_attrs = {'type': be.TYPE.SPECIES, 'data': attrs['data'].convert_to_bioentity(g_mod, l_id)}
                new_attrs['data'].set_id(l_id)
                new_attrs['data'].set_element_id(node_id)
                # Storing the original SBML object allows to obtain the original ID of the node, i.e. not the
                # layout ID. One can thus use it later to find out which nodes are duplicates (same object ID but
                # different layout id)
                new_attrs['data'].set_sbml_object(attrs['data'])

                # new_attrs['data'].setLayout(copy.deepcopy(attrs['data'].getLayout()[l_id]))
                #
                # new_attrs['data'].setElementId(node_id) #the deduplicated name of the species which is being used in the reactions' reactants and products lists as reference
                g_mod.add_node(l_id)

                g_mod_attrs[l_id] = new_attrs

    for node_id, attrs in g.nodes(data=True):
        if attrs['data'].__class__.__name__ == 'Reaction' and attrs['data'].getLayout():
            for l_id in attrs['data'].getLayout():
                # layout comes from sbmllib's getReactionLayoutData
                reaction_layout = attrs['data'].getLayout()[l_id]

                # new_attrs = copy.deepcopy(attrs)
                # new_attrs['data'].setLayout(copy.deepcopy(reaction_layout))
                # new_attrs['data'].getLayout().setSpecies(None)
                new_attrs = {'type': be.TYPE.REACTION, 'data': attrs['data'].convert_to_bioentity(g_mod, l_id)}
                new_attrs['data'].set_id(l_id)
                new_attrs['data'].set_element_id(node_id)
                new_attrs['data'].set_sbml_object(attrs['data'])

                # new_attrs['data'].setElementId(node_id)
                g_mod.add_node(l_id)
                g_mod_attrs[l_id] = new_attrs
                for species in reaction_layout.getSpecies():
                    role = be.SPECIES_ROLE.FROM_SBO_STRING(species.getRole())
                    g_mod.add_edge(l_id, species.getInstanceId(), role)
                    # if 'substrate' in role or 'modifier' in role:
                    #     g_mod.add_edge(l_id, species.getInstanceId(), role)
                    # else:
                    #     g_mod.add_edge(species.getInstanceId(), l_id, role)

    nx.set_node_attributes(g_mod, g_mod_attrs)
    return g_mod


def perturb_species(g, perturbation_type, absolute=0, ratio=0):
    if perturbation_type not in ['add', 'remove']:
        raise ValueError("Unknown perturbation type")
    if (absolute != 0 and ratio != 0):
        raise ValueError("Only either absolute or relative number")

    return perturb(g, perturbation_type, species_abs=absolute, species_ratio=ratio)


def perturb_reactions(g, perturbation_type, absolute=0, ratio=0):
    if perturbation_type not in ['add', 'remove']:
        raise ValueError("Unknown perturbation type")
    if (absolute != 0 and ratio != 0):
        raise ValueError("Only either absolute or relative number")

    return perturb(g, perturbation_type, reaction_abs=absolute, reaction_ratio=ratio)

def perturb_edges(g, perturbation_type, absolute=0, ratio=0):
    if perturbation_type not in ['add', 'remove']:
        raise ValueError("Unknown perturbation type")
    if (absolute != 0 and ratio != 0):
        raise ValueError("Only either absolute or relative number")

    return perturb(g, perturbation_type, edge_abs=absolute, edge_ratio=ratio)

def remove_incomplete_reactions(g_dir:nx.MultiGraph):
    """
    Removes reaction which are missing either a reactant or a product
    :param g_dir:
    :return:
    """
    g = g_dir.to_undirected()
    # g_nodes = g.nodes()
    g_edges = g.edges()

    to_remove = []
    for n_r in get_all_reaction_ids(g):

        reactant = False
        product = False
        for n_s in g[n_r]:
            roles = get_roles(g, n_r, n_s)
            if be.SPECIES_ROLE.CONTAINS_REACTANT_ROLE(roles):
            # if common.substr_in_list('substrate', roles):
                reactant = True
            if be.SPECIES_ROLE.CONTAINS_PRODCUT_ROLE(roles):
            # if common.substr_in_list('product', roles):
                product = True

        if not reactant or not product:
            to_remove.append(n_r)

    g_dir.remove_nodes_from(to_remove)


def remove_invalid_nodes(g:nx.MultiGraph):
    """
    Remove reactions which do not have any reactant or product, because such reactions are SBML-invalid
    :param g:
    :return:
    """


    to_remove = []
    for n_r in get_all_reaction_ids(g):

        valid = False
        reaction_species = []
        for n_s in g[n_r]:
            reaction_species.append(n_s)
            roles = get_roles(g, n_r, n_s)
            if be.SPECIES_ROLE.CONTAINS_REACTANT_ROLE(roles) or be.SPECIES_ROLE.CONTAINS_PRODCUT_ROLE(roles):
            # if common.substr_in_list('substrate', roles) or common.substr_in_list('product', roles):
                valid = True
                break


        if not valid:
            to_remove.append(n_r)
            # remove reaction's modifiers if they are not connected to something else
            for rs in reaction_species:
                if len(set(g[rs])) == 1:
                    # if the list of neighbors of the species contains only one reaction, then it is not connected
                    # to anything else than the reaction which is being removed
                    to_remove.append(rs)

    g.remove_nodes_from(to_remove)

# def remove_missing_species_from_reactions(g):
#     """
#     When one removes species, the list of products, reactants and modifiers the reactions connecting those species
#     will still keep references to them in their prod/react/mod lists. This procedure removes them
#     :param g:
#     :return:
#     """
#
#     species_ids = set([get_species(g, s).getId() for s in get_all_species_ids(g)])
#     for r_id in get_all_reaction_ids(g):
#         r = get_reaction(g, r_id)
#         r.setReactants(species_ids.intersection(r.getReactants()))
#         r.setModifiers(species_ids.intersection(r.getModifiers()))
#         r.setProducts(species_ids.intersection(r.getProducts()))




def perturb(g, perturbation_type, species_abs=0, species_ratio=0, reaction_abs=0, reaction_ratio=0, edge_abs=0, edge_ratio=0):

    if perturbation_type not in ['add', 'remove']:
        raise ValueError("Unknown perturbation type")
    if (species_abs != 0 and species_ratio != 0) or (reaction_abs != 0 and reaction_ratio != 0) or (edge_abs != 0 and edge_ratio != 0):
        raise ValueError("Only either absolute or relative number of species, reactions or edges can be specified at once")

    g_mod = deepcopy_graph(g)

    species_nodes = list(get_all_species_ids(g_mod))
    reaction_nodes = list(get_all_reaction_ids(g_mod))
    cnt_species_to_process = species_abs if species_abs > 0 else int(len(species_nodes) * species_ratio)
    cnt_reaction_to_process = reaction_abs if reaction_abs > 0 else int(len(reaction_nodes) * reaction_ratio)
    cnt_edges_to_process = edge_abs if edge_abs > 0 else int(g_mod.size() * edge_ratio)

    if perturbation_type == 'add':
        NotImplemented
    elif perturbation_type == 'remove':

        def remove_nodes(g, nodes, cnt):
            for i in range(cnt):
                ix_rand = random.randint(0, len(nodes)-1)
                g.remove_node(nodes[ix_rand])
                del nodes[ix_rand]

        remove_nodes(g_mod, species_nodes, cnt_species_to_process)
        remove_nodes(g_mod, reaction_nodes, cnt_reaction_to_process)

        edges = list(g_mod.to_undirected().edges(data=True))
        for i in range(cnt_edges_to_process):
            ix_rand = random.randint(0, len(edges)-1)
            g_mod.remove_edge(*edges[ix_rand])
            del edges[ix_rand]

    remove_invalid_nodes(g_mod)
    remove_incomplete_reactions(g_mod)
    # remove_missing_species_from_reactions(g_mod)

    return g_mod


def extract_comparment(g:nx.MultiGraph, name: str, full_reaction: bool=True):
    """

    :param g:
    :param name:
    :param full_reaction: If False, also reactions with species outside of the compartment are exported. However,
    at least one reactant and product of such reaction needs to be inside the compartment so that we end up with
    valid reactions.
    :return:
    """

    cmp_species = get_all_species_ids(g, [lambda data: data.get_compartment_name() == name])
    cmp_ids = []
    r_ids = []
    for s_id in cmp_species:
        r_ids += list(g[s_id])
    for r_id in set(r_ids):
        r_species = set(g[r_id])
        r_species_not_cmp = r_species - set(cmp_species)
        r_species_cmp = r_species - r_species_not_cmp
        include = False
        if full_reaction and len(r_species_not_cmp) == 0:
            include = True
        elif not full_reaction:
            # Even when not all the species of the reaction are in the compartment, the reactant and product
            # need to be present otherwise we would have incomplete reaction which we would not be able to layout
            rs = get_reaction_species(g, r_id)
            if len(r_species_cmp.intersection(rs.reactantIds)) > 0 and len(r_species_cmp.intersection(rs.productIds)) > 0:
                include = True
        if include:
            cmp_ids.append(r_id)
            cmp_ids += r_species_cmp

    return get_normalized_copy(get_subgraph(g, set(cmp_ids)))


def get_list_of_compartments(g, with_id=False):

    if with_id:
        l = []
        ids = set()
        for s_id in get_all_species_ids(g):
            data = get_node_data(g.nodes[s_id])
            id = data.get_compartment_id()
            name = data.get_compartment_name()
            if id not in ids:
                ids.add(id)
                l.append({'id': id, 'name': name})
        l = sorted(l, key=lambda x: x['id'])

    else:
        l = sorted(list(set([get_node_data(g.nodes[s_id]).get_compartment_name() for s_id in get_all_species_ids(g)])))

    return l


def get_normalized_copy(g):
    """
    Returns a copy of the NetworkX graph with fixed order of the nodes and edges in the inner structures.
    For example, when one creates the same subgraphs twice, the order of nodes in its internal structures
    is different. That can be an issue for methods which then somehow work on pseudo-random subsets of the graph.
    :param g:
    :return:
    """

    # if type(g) is nx.Graph:
    new_g = nx.MultiDiGraph() if g.is_directed() else nx.MultiGraph() #TODO: other graph types
    new_g.add_nodes_from(sorted(list(g.nodes(data=True))))
    # new_g.add_edges_from(sorted(list(g.edges(data=True))))
    new_g.add_edges_from(g.edges) # edges can't be sorted in multigraphs, because they are dicts
    return new_g


def get_all_reaction_ids(g) -> List[str]:
    return get_filtered_nodes(g, {'type': be.TYPE.REACTION})


def get_all_species_ids(g, filterFunctions = []) -> List[str]:
    return get_filtered_nodes(g, {'type': be.TYPE.SPECIES}, filterFunctions)


def get_first_species(g: nx.MultiGraph) -> 'be.Species':
    for id in g.nodes():
        entity = get_node_data(id, g)
        if entity.is_species():
            return entity
    assert False


def get_neibhbors_in_distance(g, n_id, dist):
    processed = [n_id]
    n_ids = [n_id]

    for i in range(dist):
        nn_ids = []
        for aux_n_id in n_ids:
            nn_ids += [x for x in g[aux_n_id] if x not in processed]
        processed += nn_ids
        n_ids = nn_ids
        if len(n_ids) == 0:
            break

    return list(set(n_ids))


# def get_species(g, id) -> sbml.Species:
#     return g.nodes()[id]['data']


def is_reaction(g, id) -> bool:
    return g.nodes()[id]['type'] == be.TYPE.REACTION


def is_species(g, id) -> bool:
    return g.nodes()[id]['type'] == be.TYPE.SPECIES


def get_node(g, node_id):
    return g.nodes[node_id]


def get_reaction(g, node_id) -> 'be.Reaction':
    return get_node_data(get_node(g, node_id))

def get_species(g, node_id) -> 'be.Species':
    return get_node_data(get_node(g, node_id))


def get_node_data(node: Dict or str, g:nx.MultiGraph = None) -> 'be.Species' or 'be.Reaction':
    if g:
        return get_node(g, node)['data']
    else:
        return node['data']


def set_node_data(g: nx.MultiGraph, n_id: str, data):
    get_node(g, n_id)['data'] = data

def get_layout(g, id, layout_type: 'be.LAYOUT_TYPE') -> 'be.SpeciesLayout' or 'be.ReactionLayout':
    return get_node_data(g.nodes[id]).get_layout(layout_type=layout_type)

def get_predicted_layout(g, id) -> 'be.SpeciesLayout' or 'be.ReactionLayout':
    return get_layout(g=g, id=id, layout_type=be.LAYOUT_TYPE.PREDICTED)

def get_original_layout(g, id) -> 'be.SpeciesLayout' or 'be.ReactionLayout':
    return get_node_data(g.nodes[id]).get_layout(layout_type=be.LAYOUT_TYPE.ORIGINAL)

# roles are ids of edges in the multigraph
# def get_edge_role(edge) -> str:
#     return edge['role']


def get_roles(g:nx.MultiGraph, n1_id, n2_id) -> List['be.SPECIES_ROLE']:
    """
    Get names of edges (=roles) between the input nodes
    :param g:
    :param n1:
    :param n2:
    :return:
    """

    return [] if n2_id not in g[n1_id] else list(g[n1_id][n2_id])

def roles_intersect(roles1: List['be.SPECIES_ROLE'], roles2: List['be.SPECIES_ROLE']) -> bool:
    return (be.SPECIES_ROLE.CONTAINS_REACTANT_ROLE(roles1) and be.SPECIES_ROLE.CONTAINS_REACTANT_ROLE(roles2)) or \
        (be.SPECIES_ROLE.CONTAINS_PRODUCT_ROLE(roles1) and be.SPECIES_ROLE.CONTAINS_PRODUCT_ROLE(roles2)) or \
        (be.SPECIES_ROLE.CONTAINS_MODIFIER_ROLE(roles1) and be.SPECIES_ROLE.CONTAINS_MODIFIER_ROLE(roles2))



def get_roles_score(roles: List['be.SPECIES_ROLE']) -> int:
    score = 0
    if be.SPECIES_ROLE.CONTAINS_PRODUCT_ROLE(roles):
        score += 1
    if be.SPECIES_ROLE.CONTAINS_REACTANT_ROLE(roles):
        score -= 1
    return score


# def is_role_reactant(role):
#     return 'substrate' in role
#
#
# def is_role_product(role):
#     return 'substrate' in role
#
#
# def is_role_modifier(role):
#     return role in ['activator', 'inhibitor', 'modifier']


def get_reaction_species(g:nx.MultiGraph, r_id: str) -> ReactionSpecies:

    rs = ReactionSpecies()
    for s_id in g[r_id]:
        # roles = get_roles(g, r_id, s_id)
        # we do not have if/elif/else because a species can be both substrate and modifier (e.g. in case of competitive binding)
        reactant_role_exists = False
        product_role_exists = False
        modifier_role_exists = False
        for role in get_roles(g, r_id, s_id):
            if role.is_reactant():
                reactant_role_exists = True
            if role.is_modifier():
                modifier_role_exists = True
            if role.is_product():
                product_role_exists = True

        if reactant_role_exists:
            rs.reactantIds.append(s_id)
        if product_role_exists:
            rs.productIds.append(s_id)
        if modifier_role_exists:
            rs.modifierIds.append(s_id)
    return rs

def get_ids_avg_pos(g: nx.MultiGraph, ids: List[str], layout_type: be.LAYOUT_TYPE):
    if len(ids) == 0:
        return None

    return sum([get_layout(g, id, layout_type=layout_type).get_center() for id in ids]) / len(ids)


def get_connecting_nodes(g:nx.Graph, n1_id, n2_id)->List[str]:
    return sorted(set(g[n1_id]).intersection(g[n2_id]))


def get_reaction_graph(g) -> nx.MultiGraph:

    if len(g) == 1:
        # component with a single species
        return nx.MultiGraph()

    # g_undirected = g.to_undirected()
    # rg = get_normalized_copy(g_undirected.subgraph(get_all_reaction_ids(g_undirected)))
    rg = get_normalized_copy(get_subgraph(g, get_all_reaction_ids(g)))
    # rg = g_undirected.subgraph(get_filtered_nodes(g_undirected, {'type': 'REACTION'})).copy()

    nodes = list(rg.nodes())

    neighbors = {}
    for node in nodes:
        neighbors[node] = set(nx.all_neighbors(g, node))


    for ix1 in range(len(nodes)):
        node_id1 = nodes[ix1]
        for ix2 in range(ix1+1, len(nodes)):
            node_id2 = nodes[ix2]
            # if len(list(nx.common_neighbors(g_undirected, node_id1, node_id2))) > 0:
            if neighbors[node_id1].intersection(neighbors[node_id2]):
                rg.add_edge(node_id1, node_id2)

    return rg


def duplicate_attrs(g, n):
    data = get_node_data(g.nodes()[n])
    return data.get_name(), data.get_compartment_name(), data.get_type(), data.get_fetures_serialized()


def get_ddup_reaction_graph(rg, g_orig):

    drg = deepcopy_graph(rg)
    # g_orig_und = g_orig.to_undirected() #TODO - should be undericted or not?

    nodes = list(drg.nodes())
    nbs = {}
    for n in nodes:
        nbs[n] = set()
        for nb in g_orig[n]:
            nbs[n].add(duplicate_attrs(g_orig, nb))
        # for nb in g_orig_und[n]:
        #     nbs[n].add(duplicate_attrs(g_orig_und, nb))
    for i1 in range(len(nodes)):
        n1 = nodes[i1]
        for i2 in range(i1 + 1, len(drg.nodes())):
            n2 = nodes[i2]
            if len(nbs[n1].intersection(nbs[n2])) > 0:
                if n1 not in drg[n2] and n2 not in drg[n1]:
                    drg.add_edge(n1, n2)

    return drg


def get_ddup_graph(g:nx.MultiGraph) -> nx.MultiGraph:
    g_new = deepcopy_graph(g)

    species_nodes = get_all_species_ids(g_new)
    # edges_to_remove = []
    nodes_to_remove = []
    edges_to_add = []

    for i1 in range(len(species_nodes)):
        n1 = species_nodes[i1]
        for i2 in range(i1+1, len(species_nodes)):
            n2 = species_nodes[i2]
            if duplicate_attrs(g_new, species_nodes[i1]) == duplicate_attrs(g_new, species_nodes[i2]):
                nodes_to_remove.append(n2)
                for nb2 in g_new[n2]:
                    #for all reactions connected to the duplicated species
                    # edges_to_remove.append((nb2, n2))
                    for edge_data in g_new[n2][nb2]:
                        edges_to_add.append((nb2, n1, edge_data))

    g_new.add_edges_from(edges_to_add)
    # g_new.remove_edges_from(edges_to_remove)
    g_new.remove_nodes_from(nodes_to_remove)

    return g_new


def get_ods_nbs(g: nx.MultiGraph, r_id: str)->Dict[str,str]:
    """
    One-degree species of reaction a reaction with its roles
    :param r_id:
    :param g:
    :return: Dictionary of species ids with its roles in the reaction
    """

    ods = {}
    for s_id in g[r_id]:
        if len(g[s_id]) == 1:
            ods[s_id] = g[r_id][s_id]
    return ods


def count_intersections(g: nx.MultiGraph, line:gr.Line = None, masked_r_ids:List[str] = [], layout_type=be.LAYOUT_TYPE.PREDICTED):
    """
    Counts the number of intersection of all lines in the graph with other lines and with species.
    :param g:
    :param line: If set, only intersections with given line are counted.
    :masked_r_ids: List of reaction ids which won't be considered
    :param layout_type:
    :return:
    """

    lines = []
    for r_id in get_all_reaction_ids(g):
        if r_id not in masked_r_ids:
            lines += get_node_data(g.nodes[r_id]).get_layout(layout_type).get_all_lines()

    cnt = 0

    # intersections with all the lines

    if line:
        for l in lines:
            if l != line and line.intersects_line(l):
                cnt += 1

    for i1 in range(len(lines)):
        for i2 in range(i1+1, len(lines)):
            l1 = lines[i1]
            l2 = lines[i2]
            if l1.intersects_line(l2):
                cnt += 1

    # intersections with all the species
    for s_id in get_all_species_ids(g):
        r_ids = list(g[s_id])
        if set(r_ids).intersection(masked_r_ids):
            continue
        bb = get_node_data(g.nodes[s_id]).get_layout(layout_type).get_bb().get()

        if line:
            if line.intersects_rectangle(bb):
                cnt += 1
        else:
            for l in lines:
                if l.intersects_rectangle(bb):
                    cnt += 1

    return cnt


def common_neighbors(g, is_tmp, r1, r2):
    """

    :param g:
    :param is_tmp: True if the graph is template graph.
    For template graph, when checking for common non-duplicated nodes we are interested in the
    real number of nodes in the layout, because these will be used as template positions. However, in target
    graph, which is duplicated, we need to count the number of edges which tell us how many times that node
    might possibly be duplicated between given two reactions. This is relevant only if two nodes/reactions
    share multiple species.
    :param r1:
    :param r2:
    :return: Dictionary containing true common nodes in of r1 and r2 in g, but also nodes which
    do not connect r1 and r2 in g, but are the same, i.e. duplicates.
    """

    nb1 = [n for n in nx.all_neighbors(g, r1)]
    nb2 = [n for n in nx.all_neighbors(g, r2)]

    cn = {
        'non-dup': []
        , 'dup': []
    }
    for n1 in nb1:
        for n2 in nb2:
            if n1 == n2:
                if is_tmp:
                    cn['non-dup'].append(n1)
                else:
                    # add the neighbours as many times  as there are supporting edges/roles
                    # r1 <-> A <-> r2   => 2
                    # r1 -> A <-> r2   => 1
                    cnt_roles1 = len(g[r1][n1])
                    cnt_roles2 = len(g[r2][n1])
                    for i in range(min(cnt_roles1, cnt_roles2)):
                        cn['non-dup'].append(n1)
            elif duplicate_attrs(g, n1) == duplicate_attrs(g, n2):
                for i in range(len(g[r1][n1])):
                    cn['dup'].append(n1)
                for i in range(len(g[r2][n2])):
                    cn['dup'].append(n2)

    cn['non-dup'] = list(cn['non-dup'])
    cn['dup'] = list(cn['dup'])

    return cn


def get_centrality(g:nx.MultiGraph) -> Dict[str, float]:
    return nx.betweenness_centrality(g, endpoints=True)



def get_duplicates(g: nx.MultiGraph, node_id: str, node_ids: List[str]) -> List[str]:
    """
    Finds (first) duplicate of a node in a set of nodes
    :param n:
    :param node_ids:
    :return:
    """
    dups = []
    attrs = duplicate_attrs(g, node_id)
    for n in node_ids:
        if duplicate_attrs(g, n) == attrs:
            dups.append(n)
    return sorted(dups)

def fix_bioentity_graph_links(g:nx.Graph):
    # TODO: This is really hacky. The problem is that node contains pointer to the graph which holds it and when
    # the subgraph is created it keeps reference to the original graph which causes some issues for example when
    # one wants to store the data
    for n_id in g.nodes():
        get_node_data(g.nodes[n_id]).set_graph(g)


def get_subgraph(g:nx.Graph, c):
    s = g.subgraph(c).copy()
    fix_bioentity_graph_links(s)
    return s

def deepcopy_graph(g:nx.Graph):
    g_new = copy.deepcopy(g)
    # g_new = g.copy()
    # for n_id in g_new.nodes:
    #     g_new.nodes[n_id]['data'] = copy.deepcopy(g.nodes[n_id]['data'] )
    fix_bioentity_graph_links(g_new)
    return g_new


def copy_node(g_src: nx.MultiGraph, g_tgt: nx.MultiGraph, n_id: str):
    g_tgt.add_node(n_id, **copy.deepcopy(g_src.nodes[n_id]))
    entity = get_node_data(n_id, g_tgt)
    entity.set_graph(g_tgt)
    entity.set_id(n_id)


def get_in_complex_s_ids(g: nx.MultiGraph) -> List[str]:

    rv: List[str] = []

    s_ids = get_all_species_ids(g)
    bbs: List[gr.Rectangle] = [get_original_sbml_bb(g, id) for id in get_all_species_ids(g)]

    for i in range(len(bbs)):
        s_id = s_ids[i]
        for j in range(len(bbs)):
            if i == j:
                # obviously center of a species is inside its bounding box
                continue
            if bbs[i].inside(bbs[j]):
                rv.append(s_id)
                break

    return rv


def get_original_sbml_bb(g, id) -> gr.Rectangle:
    sbml_bb = get_node_data(id, g).get_sbml_object().layout[id].bbox

    return gr.Rectangle(sbml_bb['x'], sbml_bb['y'], sbml_bb['x'] + sbml_bb['width'], sbml_bb['y'] + sbml_bb['height'])

def get_original_sbml_center(g, id) -> np.ndarray:
    return np.array(get_node_data(id, g).get_sbml_object().layout[id].center)
