import libsbml as sbml
import math
import sys
import numpy as np
import requests
import networkx as nx
import copy

from typing import List
from typing import Dict
from typing import NamedTuple

from . import common
from . import graph_utils as gu
from . import layout as lt
from . import settings
from . import bioentity as be
from . import graphics as gr

from enum import Enum


class MultiSpeciesFeatureType(NamedTuple):
    name: str
    value: Dict[str, str]


class MultiSpeciesType(NamedTuple):
    name: str
    value: Dict[str, MultiSpeciesFeatureType]

class IdGenerator:
    idCompartment = 0
    idSpecies = 0

    @staticmethod
    def getIdCompartment():
        IdGenerator.idCompartment += 1
        return 'c{}'.format(IdGenerator.idCompartment)

    @staticmethod
    def getIdSpecies():
        IdGenerator.idSpecies += 1
        return 's{}'.format(IdGenerator.idSpecies)

class SPECIES_ROLE(Enum):
    REACTANT = 0
    MODIFIER = 1
    PRODUCT = 2

class LayoutVisual:

    def __init__(self, size=None, color=None):
        self.size = size
        self.color = color

    def getSize(self):
        return self.size

    def setSize(self, size):
        self.size = size

    def getColor(self):
        return self.color

    def setColor(self, color):
        self.color = color

class SpeciesGlyph:
    def __init__(self, bbox=None, center=None, visual=LayoutVisual()):
        self.bbox = bbox
        self.center = center
        self.visual = visual

    def getBBox(self):
        return self.bbox

    def setBBox(self, bbox):
        self.bbox = bbox

    # def getBBoxRectange(self)->Rectangle:
    #     bb = self.getBBox()
    #     # return Rectangle(bb.x)


    def getCenter(self):
        return self.center

    def setCenter(self, center):
        self.center = center

    def getVisual(self):
        return self.visual

    def setVisual(self, visual):
        self.visual = visual

class ReactionGlyphSpecies:
    def __init__(self, instanceId, role):
        self.instanceId = instanceId
        self.role = role

    def getInstanceId(self):
        return self.instanceId

    def setInstanceId(self, instanceId):
        self.instanceId = instanceId

    def getRole(self):
        return self.role

    def setRole(self, role):
        self.role = role

class ReactionGlyph(SpeciesGlyph):
    def __init__(self, bbox=None, center=None, species=[]):
        SpeciesGlyph.__init__(self, bbox=bbox, center=center)
        self.species = species

    def getSpecies(self):
        return self.species

    def setSpecies(self, species: ReactionGlyphSpecies):
        self.species = species

class Species:

    def __init__(self, id = None, name = None, metaId = None, compartmentId = None, compartmentName = None,
                 sboTerm: int=None, layout=None, layoutPredicted = None, elementId = None, features=[]):
        self.id = id
        self.name = name
        self.metaId = metaId
        self.compartmentId = compartmentId
        self.compartmentName = compartmentName
        self.sboTerm: int = sboTerm
        self.layout = layout
        self.layoutPredicted = layoutPredicted
        self.elementId = elementId
        self.features = features

    def getId(self):
        return self.id

    def setId(self, id):
        self.id = id

    def getName(self):
        return self.name

    def setName(self, name):
        self.name = name

    def getMetaId(self):
        return self.metaId

    def setMetaId(self, metaId):
        self.metaId = metaId

    def getCompartmentId(self):
        return self.compartmentId

    def setCompartmentId(self, compartmentId):
        self.compartmentId = compartmentId

    def getCompartmentName(self):
        return self.compartmentName

    def setCompartmentName(self, compartmentName):
        self.compartmentName = compartmentName

    def getSboTerm(self):
        return self.sboTerm

    def setSboTerm(self, sboTerm):
        self.sboTerm = sboTerm

    def getLayout(self):
        return self.layout

    def setLayout(self, layout):
        self.layout = layout

    def getLayoutPredicted(self):
        return self.layoutPredicted

    def setLayoutPredicted(self, layoutPredicted):
        self.layoutPredicted = layoutPredicted

    def getElementId(self):
        return self.elementId

    def setElementId(self, elementId):
        self.elementId = elementId

    def getFeatures(self):
        return self.features

    def setFeatures(self, features):
        self.features = features

    # def getBoundingBox(self, predictedLayout=True)->gr.Rectangle:
    #     layout = self.getLayoutPredicted() if predictedLayout else self.getLayout()
    #     center = layout.getCenter()
    #     return gr.Rectangle(center[0] - settings.render.species_glyph_width/2,
    #                         center[1] - settings.render.species_glyph_height/2,
    #                         center[0] + settings.render.species_glyph_width / 2,
    #                         center[1] + settings.render.species_glyph_height / 2)

    def convert_to_bioentity(self, g:nx.MultiGraph, layout_id)->'be.Species':
        """

        :param layout_id: The SBML species's layout contains list of layout glyphs. If layout_id specivied,
        only selected layout will be copied into the species. Otherwise, it stays NULL
        :return:
        """

        center = None
        bb_width = None
        bb_height = None
        if self.getLayout() and layout_id:
            l = self.getLayout()[layout_id]
            center = np.array(l.center)
            bb_width = l.bbox["width"]
            bb_height = l.bbox["height"]
        slo = be.SpeciesLayout(None, np.array(center), bb_width=bb_width, bb_height=bb_height, copied=False)
        if self.getLayoutPredicted() and layout_id:
            l = self.getLayoutPredicted()[layout_id]
            center = np.array(l.center)
            bb_width = l.bbox["width"]
            bb_height = l.bbox["height"]
        slp = be.SpeciesLayout(None, np.array(center), bb_width=bb_width, bb_height=bb_height, copied=False)

        return be.Species(
            g, self.getId(), element_id=self.getElementId(), name=self.getName(), type=self.getSboTerm(),
            layouts={be.LAYOUT_TYPE.ORIGINAL: slo, be.LAYOUT_TYPE.PREDICTED: slp},
            compartment_id=self.getCompartmentId(), compartment_name=self.getCompartmentName(),
            features=self.getFeatures()
        )



class Reaction:

    def __init__(self, id = None, name = None, metaId = None, reactants = None, products = None, modifiers = None,
                 sboTerm: int = None, layout = None, layoutPredicted=None, elementId=None, reversible=False):
        self.id = id
        self.name = name
        self.metaId = metaId
        self.reactants = reactants
        self.products = products
        self.modifiers = modifiers
        self.sboTerm = sboTerm
        self.layout = layout
        self.layoutPredicted = layoutPredicted
        self.elementId = elementId
        self.reversible: bool = reversible

    def getId(self):
        return self.id

    def setId(self, id):
        self.id = id

    def getName(self):
        return self.name

    def setName(self, name):
        self.name = name

    def getMetaId(self):
        return self.metaId

    def setMetaId(self, metaId):
        self.metaId = metaId

    def getReactants(self):
        return self.reactants

    def setReactants(self, reactants):
        self.reactants = list(reactants)

    def getProducts(self):
        return self.products

    def setProducts(self, products):
        self.products = list(products)

    def getModifiers(self):
        return self.modifiers

    def setModifiers(self, modifiers):
        self.modifiers = list(modifiers)

    def getSboTerm(self):
        return self.sboTerm

    def setSboTerm(self, sboTerm):
        self.sboTerm = sboTerm

    def getLayout(self):
        return self.layout

    def setLayout(self, layout):
        self.layout = layout

    def getLayoutPredicted(self):
        return self.layoutPredicted

    def setLayoutPredicted(self, layoutPredicted):
        self.layoutPredicted = layoutPredicted

    def getElementId(self):
        return self.elementId

    def setElementId(self, elementId):
        self.elementId = elementId

    def isReversible(self) -> bool:
        return self.reversible

    def convert_to_bioentity(self, g:nx.MultiGraph, layout_id) -> 'be.Reaction':
        """

        :param layout_id: The SBML species's layout contains list of layout glyphs. If layout_id specivied,
        only selected layout will be copied into the reaction. Otherwise, it stays NULL.
        :return:
        """

        slo = be.ReactionLayout(None, np.array(self.getLayout()[layout_id].center) if self.getLayout() and layout_id else None, False)
        slp = be.ReactionLayout(None, np.array(self.getLayoutPredicted()[layout_id].center) if self.getLayoutPredicted() and layout_id else None, False)

        return be.Reaction(
            g, id, element_id=self.getElementId(), name=self.getName(), type=self.getSboTerm(),
            is_reversible= self.isReversible(),
            layouts={be.LAYOUT_TYPE.ORIGINAL: slo, be.LAYOUT_TYPE.PREDICTED: slp}
        )

class SbmlHelper:

    def __init__(self, source=None, source_is_file=True):

        self.layoutPlugin = None

        if source:
            self.read_sbml(source, source_is_file)
        else:
            self.create_sbml()

        self.set_properties()


    def set_properties(self):

        self.species = self.model.getListOfSpecies()
        self.compartments = self.model.getListOfCompartments()

        # print(self.species.get(0).getCompartment())
        # print(self.model.getListOfAllElements().getSize())
        # print("Line: {}".format(self.species.get(0).getLine()))
        # reactions = self.model.getListOfReactions()
        # print("Compartments:\n")
        # for c in self.compartments: print(c.getName());
        # if self.layoutPlugin != None:
        #     print("Number of layouts: {}".format(self.layoutPlugin.getNumLayouts()))

    def create_sbml(self):

        # sbmlns = sbml.SBMLNamespaces(3, 1, "layout", 1, "render", 1)
        sbmlns = sbml.SBMLNamespaces(3, 1, "layout", 1)
        # sbmlns = sbml.SBMLNamespaces(3, 1)

        self.document = sbml.SBMLDocument(sbmlns)

        # renderNsUri = "http://www.sbml.org/sbml/level3/version1/render/version1"
        # layoutNsUri = "http://www.sbml.org/sbml/level3/version1/layout/version1"

        # self.document.getSBMLNamespaces().addPackageNamespace("layout", 1)
        # self.document.getSBMLNamespaces().addPackageNamespace("render", 1)



        setp = self.document.setPkgRequired("layout", False)
        # print('setp', setp)
        # print(sbml.LIBSBML_PKG_UNKNOWN_VERSION)
        # self.document.setPkgRequired("render", True)



        # sbmlnsr = sbml.SBMLNamespaces(3, 1, "render", 1)
        # self.document = sbml.SBMLDocument(sbmlnsr)
        # self.document.setPkgRequired("render", True)

        self.model = self.document.createModel()

        self.model.setTimeUnits("second")
        self.model.setExtentUnits("mole")
        self.model.setSubstanceUnits('mole')

        per_second = self.model.createUnitDefinition()
        per_second.setId('per_second')
        unit = per_second.createUnit()
        unit.setKind(sbml.UNIT_KIND_SECOND)
        unit.setExponent(-1)
        unit.setScale(0)
        unit.setMultiplier(1)

    def __read_features(self) -> Dict[str, MultiSpeciesType]:
        features: Dict[MultiSpeciesType] = {}
        if self.multiPlugin:
            for st in self.multiPlugin.getListOfMultiSpeciesTypes():
                st_id = st.getId()
                st_name = st.getName()
                features[st_id] = MultiSpeciesType(st_name, {})
                for sft in st.getListOfSpeciesFeatureTypes():
                    # might also need sft.getName() in the future
                    sft_id = sft.getId()
                    sft_name = sft.getName()
                    features[st_id].value[sft_id] = MultiSpeciesFeatureType(sft_name, {})
                    for psfv in sft.getListOfPossibleSpeciesFeatureValues():
                        features[st_id].value[sft_id].value[psfv.getId()] = psfv.getName() # the name can be empty

        return features

    def __assign_features_to_species(self, features:Dict[str, MultiSpeciesType]) -> Dict:
        sid_features = {}
        if self.multiPlugin:
            for species in self.model.getListOfSpecies():
                s_id = species.getId()
                sid_features[s_id] = []
                multi = species.getPlugin('multi')
                st = multi.getSpeciesType()
                for sf in multi.getListOfSpeciesFeatures():
                    occ = sf.getOccur()
                    sft = sf.getSpeciesFeatureType()
                    fsft = features[st].value[sft]
                    vals = []
                    for val in sf.getListOfSpeciesFeatureValues():
                        valval = val.getValue()
                        assert (st in features)
                        assert (sft in features[st].value)
                        assert (valval in fsft.value)
                        vals.append(fsft.value[valval])
                        # for i in range(occ):
                        #     sid_features[s_id].append(features[st][sftZZ][valval])
                    sid_features[s_id].append(be.SpeciesFeature(cnt=occ, feature_type=sft, feature_name=fsft.name, values=vals))

        # TODO: possibly remove features which are shared across all species because those are irrelevant for discerning whether two glyps differ

        return sid_features

    def read_sbml(self, source, source_is_file):
        if source_is_file:
            self.document = sbml.SBMLReader().readSBMLFromFile(source)
        else:
            self.document = sbml.SBMLReader().readSBMLFromString(source)
        self.model = self.document.getModel()

        if self.model is None:
            raise Exception('Not able to load {}'.format(source))

        # if self.document.getNumErrors() > 0:
        #     print('!!!!!!!! ERRORS WHEN LOADING SBML !!!!!!!!!')
        #     self.document.printErrors()
        #     # raise Exception('Problem retrieving the SBML document')

        self.speciesGlyhps = {}
        self.reactionGlyhps = {}
        self.layoutPlugin = self.model.getPlugin('layout')
        self.multiPlugin = self.model.getPlugin('multi')

        if self.layoutPlugin is not None:

            layout = self.layoutPlugin.getLayout(0)

            if layout is not None:

                dimensions = layout.getDimensions()
                self.layoutDimensions = {
                    "width": dimensions.getWidth(),
                    "height": dimensions.getHeight()
                }


                for g in layout.getListOfSpeciesGlyphs():
                    species_id = g.getSpeciesId()
                    if species_id not in self.speciesGlyhps:
                        self.speciesGlyhps[species_id] = []
                    self.speciesGlyhps[g.getSpeciesId()].append(g)

                for g in layout.getListOfReactionGlyphs():
                    reaction_id = g.getReactionId()
                    if reaction_id not in self.reactionGlyhps:
                        self.reactionGlyhps[reaction_id] = []
                    self.reactionGlyhps[g.getReactionId()].append(g)

        return

    def get_sbml_string(self) -> str:
        return sbml.writeSBMLToString(self.document)

    def save(self, file_name):
        with common.open_file(file_name, "w") as f:
            f.write(self.get_sbml_string())

    def save_as_image(self, output_file_name, output_format = 'svg'):
        minerva_isntance = settings.minerva_instance

        # token = requests.get("{}/api/doLogin".format(minerva_isntance)).cookies['MINERVA_AUTH_TOKEN']
        if (output_format == 'pdf'):
            response = requests.post("{}/api/convert/image/SBML:pdf".format(minerva_isntance),
                                     # cookies=dict(MINERVA_AUTH_TOKEN=token),
                                     data=self.get_sbml_string())
        else:
            response = requests.post("{}/api/convert/image/SBML:svg".format(minerva_isntance),
                                     # cookies=dict(MINERVA_AUTH_TOKEN=token),
                                     data=self.get_sbml_string())

        with common.open_file(output_file_name, "w") as f:
            f.write(response.text)

    def getSpeciesById(self, id):
        return self.model.getListOfSpecies().getElementBySId(id)

    def getGlyphBBox(self, glyph):
        if glyph is None: return None

        bbox = glyph.getBoundingBox()
        pos = bbox.getPosition()
        return {
            'x': pos.getXOffset(),
            'y': pos.getYOffset(),
            'width': bbox.getWidth(),
            'height': bbox.getHeight(),
        }

    def getSpeciesBBox(self, id):

        glyph = self.speciesGlyhps[id]
        return self.getGlyphBBox(glyph)


    def getReactionBBox(self, id):

        glyph = self.reactionGlyhps[id]
        return self.getGlyphBBox(glyph)

    def getReactionGlyphSpeciesGlyphIds(self, glyph):
        return [ReactionGlyphSpecies(instanceId=srg.getSpeciesGlyphId(), role=srg.getRoleString()) for srg in glyph.getListOfSpeciesReferenceGlyphs()]

    def getLayoutDimensions(self):
        return self.layoutDimensions

    def getReactionGlyphCenter(self, glyph):

        dist_threshold = 30
        connect_points = []
        all_points = []

        points = []
        for srg in glyph.getListOfSpeciesReferenceGlyphs():
            for cs in srg.getCurve().getListOfCurveSegments():
                for point in [cs.getStart(),cs.getEnd()]:
                    points.append([point.getXOffset(), point.getYOffset()])

        for cs in glyph.getCurve().getListOfCurveSegments():
            for point in [cs.getStart(), cs.getEnd()]:
                points.append([point.getXOffset(), point.getYOffset()])

        for i1 in range(len(points)):
            for i2 in range(i1+1, len(points)):
                p1 = points[i1]
                p2 = points[i2]
                all_points = [p1,p2]
                if math.hypot(p2[0]-p1[0], p2[1]-p1[1]) < dist_threshold:
                    connect_points += [p1,p2]

        center = [0,0]



        if (len(connect_points) > 0):
            cnt_points = len(connect_points)
            for p in connect_points:
                center[0] += p[0]
                center[1] += p[1]
        else:
            cnt_points = len(all_points)
            for p in all_points:
                center[0] += p[0]
                center[1] += p[1]

        center[0] /= cnt_points
        center[1] /= cnt_points

        return center

    def getSpeciesLayoutData(self, id):
        glyphs = {}
        if id in self.speciesGlyhps:
            #if the input sbml is layout-less the speciesGlyphs will be empty
            for glyph in self.speciesGlyhps[id]:
                bbox = self.getGlyphBBox(glyph)
                center = [bbox['x'] + bbox['width'] / 2, bbox['y'] + bbox['height'] / 2]
                glyphs[glyph.getId()] = SpeciesGlyph(
                    bbox=bbox,
                    center=center
                )
        return glyphs

    def getReactionLayoutData(self, id):
        glyphs = {}
        if id in self.reactionGlyhps:
            # if the input sbml is layout-less the reactionGlyhps will be empty
            for glyph in self.reactionGlyhps[id]:
                bbox = self.getGlyphBBox(glyph)
                center = self.getReactionGlyphCenter(glyph)
                glyphs[glyph.getId()] = ReactionGlyph(
                    bbox=bbox,
                    center=center,
                    species=self.getReactionGlyphSpeciesGlyphIds(glyph)
                )
        return glyphs


    def getSpecies(self):

        compartments = {}
        for c in self.getCompartments():
            compartments[c.getId()] = c.getName()

        sid_features = self.__assign_features_to_species(self.__read_features())

        speciess = []
        for s in self.model.getListOfSpecies():
            # bbox = self.getSpeciesBBox(s.getId())
            s_id = s.getId()
            speciess.append(Species(
                id=s.getId(),
                name= s.getName(),
                metaId= s.getMetaId(),
                compartmentId=s.getCompartment() if s.getCompartment() else 'default',
                compartmentName=compartments[s.getCompartment()] if s.getCompartment() else 'default',
                sboTerm= s.getSBOTerm(),
                layout=self.getSpeciesLayoutData(s_id),
                features=sid_features[s_id] if s_id in sid_features else []
                # 'bbox': bbox,
                # 'center': [bbox['x']+ bbox['width'] / 2, bbox['y']+ bbox['height'] / 2]
            ))

        return speciess


    def getReactions(self):
        reactions = []
        for r in self.model.getListOfReactions():
            reactions.append(Reaction(
                id=r.getId(),
                metaId=r.getMetaId(),
                name=r.getName(),
                reactants=[s.getSpecies() for s in r.getListOfReactants()],
                products=[s.getSpecies() for s in r.getListOfProducts()],
                modifiers=[s.getSpecies() for s in r.getListOfModifiers()],
                sboTerm=r.getSBOTerm(),
                layout=self.getReactionLayoutData(r.getId()),
                reversible=r.isSetReversible() and r.getReversible()
                # 'bbox': self.getReactionBBox(r.getId()),
                # 'center': self.getReactionCenter(r.getId())
            ))

        return reactions


    def getCompartments(self):
        return self.model.getListOfCompartments()
        # compartments = []
        # for c in self.model.getListOfCompartments():
        #     print('{}:{}'.format(c.getId(), c.getName()))
        #     compartments.append({'id': c.getId(), 'name': c.getName()})
        # return compartments

    def getCompartmentElementsOfType(self, c, type):
        compartmentId = c.getId()

        compartmentElements = []
        if type == 'SPECIES':
            elements = self.model.getListOfSpecies()
        elif type == 'REACTION':
            elements = self.model.getListOfReactions()
        else:
            raise Exception('Unsupported element type')

        for i in range(len(elements)):
            s = elements.get(i)
            try:
                if s.getCompartment() == compartmentId:
                    compartmentElements.append(s)
            except:
                pass

        return compartmentElements


    def extractCompartment(self, c):

        new_model = self.document.createModel()

        try:
            new_model.addCompartment(c.clone())
        except:
            print('problem')

        species = self.getCompartmentElementsOfType(c, 'SPECIES')
        for s in species:
            new_model.addSpecies(s)

        return new_model.toSBML()



    def removeExistingLayouts(self):
        if self.layoutPlugin is not None:
            for i in range(self.layoutPlugin.getNumLayouts()-1,-1,-1):
                self.layoutPlugin.removeLayout(i)

    def addGraph(self, g):
        """
        Takes input graph and creates SBML network structure (without layout) from it.
        :param g: networkx graph
        :return:
        """

        cmps = gu.get_list_of_compartments(g, with_id = True)
        for cmp in cmps:

            comp = self.model.createCompartment()

            comp.setId(cmp['id'])
            comp.setName(cmp['name'])
            comp.setConstant(False)


        # ft_values = {}
        # for s_id in gu.get_all_species_ids(g):
        #     s: be.Species = gu.get_node_data(g.nodes[s_id])
        #     for f in s.get_fetures():
        #         ft = f.feature_type
        #         if ft not in ft_values:
        #             ft_values[ft] = set()
        #         ft_values[ft].add("".join(f.values))
        #
        # features_to_show = []
        # for ft in ft_values:
        #     if len(ft_values[ft]) > 1:
        #         features_to_show.append(ft)




        inserted_ids = set()
        for s_id in gu.get_all_species_ids(g):
            # The s_id is actually id of the glyph, but the species content is the same for different glyphs
            s: be.Species = gu.get_node_data(g.nodes[s_id])

            if s.get_element_id() in inserted_ids:
                continue
            else:
                inserted_ids.add(s.get_element_id())

            s_new = self.model.createSpecies()

            # fs = s.get_fetures_serialized(with_type=False, filter_out=["NULL", "false", "true"])
            fs = s.get_visual_features_serialized()
            if fs != "":
                fs = " ({})".format(fs)
            s_new.setId(s.get_element_id())
            s_new.setName(s.get_name() + fs)
            s_new.setCompartment(s.get_compartment_id())
            s_new.setSBOTerm(s.get_type())
            s_new.setInitialAmount(0)
            s_new.setHasOnlySubstanceUnits(True)
            s_new.setConstant(False)
            s_new.setBoundaryCondition(False)



        for r_id in gu.get_all_reaction_ids(g):
            r: be.Reaction = gu.get_node_data(g.nodes[r_id])

            r_new = self.model.createReaction()
            r_new.setId(r.get_element_id())
            r_new.setSBOTerm(r.get_type())
            r_new.setReversible(r.is_reversible())
            r_new.setFast(False)


            rss = gu.get_reaction_species(g, r_id)
            for s_id in rss.reactantIds:
                reaction_species = r_new.createReactant()
                reaction_species.setSpecies(gu.get_node_data(g.nodes[s_id]).get_element_id())
                reaction_species.setConstant(False)

            for s_id in rss.productIds:
                reaction_species = r_new.createProduct()
                reaction_species.setSpecies(gu.get_node_data(g.nodes[s_id]).get_element_id())
                reaction_species.setConstant(False)

            for s_id in rss.modifierIds:
                reaction_species = r_new.createModifier()
                reaction_species.setSpecies(gu.get_node_data(g.nodes[s_id]).get_element_id())

            # # TODO: The following cycles should be changed to using the graph and get_element_id function instead of using the SBML object which is probably not necessarry
            # for reactant_id in r.get_sbml_object().getReactants():
            #     reactant = r_new.createReactant()
            #     reactant.setSpecies(reactant_id)
            #     reactant.setConstant(False)
            #
            # for product_id in r.get_sbml_object().getProducts():
            #     product = r_new.createProduct()
            #     product.setSpecies(product_id)
            #     product.setConstant(False)
            #
            # for modifier_id in r.get_sbml_object().getModifiers():
            #     modifier = r_new.createModifier()
            #     modifier.setSpecies(modifier_id)

    def getColorsForSpecies(self, s:Species):

        species_color = {}

        sboTerm_color = {'SBO:0000278': '66FF66FF'}
        callback_color = 'CCFF66FF'

        sboTerm = s.getSboTerm()
        if sboTerm_color not in sboTerm_color:
            return callback_color
        else:
            return sboTerm_color[sboTerm]


    def addLayout(self, g_orig:nx.MultiGraph):

        g = copy.deepcopy(g_orig)

        species_glyph_width = settings.render.species_glyph_width
        species_glyph_height = settings.render.species_glyph_height
        padding = np.array([species_glyph_width, species_glyph_height])

        #
        # set the LayoutPkgNamespaces for Level 3 Version1 Layout Version 1
        #
        layout_ns = sbml.LayoutPkgNamespaces(3, 1, 1)
        renderns = sbml.RenderPkgNamespaces(3, 1, 1)

        #
        # Get a LayoutModelPlugin object plugged in the model object.
        #
        # The type of the returned value of SBase::getPlugin() function is SBasePlugin, and
        # thus the value needs to be casted for the corresponding derived class.
        #
        plugin = self.model.getPlugin("layout")
        rplugin = self.model.getPlugin("render")

        if plugin is None:
            raise Exception("Layout is not registered.")

        # if rplugin is None:
        #     raise Exception("Render is not registered.")
                    #
        # Creates a Layout object via LayoutModelPlugin object.
        #
        layout = plugin.createLayout()
        layout.setId("predicted_layout")

        lt.pan_to_origin(g, padding)
        # lt.pan_to_positive(g, [species_glyph_width, species_glyph_height])

        minCoords = [sys.maxsize, sys.maxsize]
        maxCoords = [-sys.maxsize, -sys.maxsize]

        cmp_dim = {}
        for cmp_id in [c['id'] for c in gu.get_list_of_compartments(g, with_id=True)]:
            cmp_dim[cmp_id] = [ np.array([sys.maxsize,sys.maxsize]), np.array([0,0]) ]


        species_glyphs = {}
        # species_coords = {}
        for n in gu.get_all_species_ids(g):
            node = g.nodes[n]
            s_id = gu.get_node_data(node).get_element_id()
            s_ref_id = n

            assert(self.getSpeciesById(s_id) != None)

            coords = lt.get_node_pos(node)

            species_glyph = layout.createSpeciesGlyph()

            glyph_id = "sg_{}".format(s_ref_id)

            species_glyphs[s_ref_id] = glyph_id
            # species_coords[s_ref_id] = coords

            species_glyph.setId(glyph_id)
            species_glyph.setSpeciesId(s_id)

            # TODO: A quick hack to address the issue of long names of species. This should be correctly handled via bounding boxes
            text = gu.get_node_data(node).get_name();
            if len(text) < 11:
                width = species_glyph_width
            else:
                width = species_glyph_width + (len(text) - 11) * 6
            bbX = coords[0] - width / 2
            bbY = coords[1] - species_glyph_height / 2
            species_glyph.setBoundingBox(sbml.BoundingBox(layout_ns,"bb_sg_{}".format(s_ref_id), bbX, bbY, width, species_glyph_height))

            textGlyph = layout.createTextGlyph()
            textGlyph.setId("tg_{}".format(s_ref_id))
            textGlyph.setBoundingBox(sbml.BoundingBox(layout_ns, "bb_tg_{}".format(s_ref_id), bbX, bbY, width, species_glyph_height))
            textGlyph.setOriginOfTextId(species_glyph.getId())
            textGlyph.setGraphicalObjectId(species_glyph.getId())

            minCoords[0] = min(bbX, minCoords[0])
            minCoords[1] = min(bbY, minCoords[1])
            maxCoords[0] = max(bbX + width, maxCoords[0])
            maxCoords[1] = max(bbY + species_glyph_height, maxCoords[1])

            cmp_id = gu.get_node_data(node).get_compartment_id()
            cmp_dim[cmp_id][0][0] = min(cmp_dim[cmp_id][0][0], coords[0])
            cmp_dim[cmp_id][0][1] = min(cmp_dim[cmp_id][0][1], coords[1])
            cmp_dim[cmp_id][1][0] = max(cmp_dim[cmp_id][1][0], coords[0])
            cmp_dim[cmp_id][1][1] = max(cmp_dim[cmp_id][1][1], coords[1])


        #add compartments layout information
        cmp_padding = np.array(settings.render.compartment_padding)
        for cmp_id in cmp_dim:

            #add padding
            pos = cmp_dim[cmp_id][0] - cmp_padding
            dim = cmp_dim[cmp_id][1] - cmp_dim[cmp_id][0] + 2*cmp_padding
            cmp_glyph = layout.createCompartmentGlyph()
            cmp_glyph.setId('cg_{}'.format(cmp_id))
            cmp_glyph.setCompartmentId(cmp_id)
            cmp_glyph.setBoundingBox(sbml.BoundingBox(layout_ns, "bb_cg_{}".format(cmp_id), float(pos[0]), float(pos[1]), float(dim[0]), float(dim[1])))

        srg_cnt_dict = {}
        for n_r in gu.get_all_reaction_ids(g):

            node = g.nodes[n_r]
            r_id = gu.get_node_data(node).get_element_id()

            reaction_glyph = layout.createReactionGlyph()
            reaction_glyph.setId("rg_{}".format(n_r))
            reaction_glyph.setReactionId(r_id)

            #TODO: see whether there is not an edge case where deepcopy is needed needed here
            l: be.ReactionLayout = copy.deepcopy(gu.get_node_data(node).get_layout(be.LAYOUT_TYPE.PREDICTED))
            # l: be.ReactionLayout = gu.get_node_data(node).get_layout(be.LAYOUT_TYPE.PREDICTED)

            l.treat_overlapping_ends()

            for line in l.get_reactants_center_curve_lines():
                curve = reaction_glyph.getCurve().createLineSegment()
                curve.setStart(create_sbml_point(layout_ns, line[0][0], line[0][1]))
                curve.setEnd(create_sbml_point(layout_ns, line[1][0], line[1][1]))

            center = l.get_center()
            # curve = reaction_glyph.getCurve().createLineSegment()
            # curve.setStart(create_sbml_point(layout_ns, center[0], center[1]))
            # curve.setEnd(create_sbml_point(layout_ns, center[0], center[1]))

            for line in reverse_lines(l.get_products_center_curve_lines()):
                curve = reaction_glyph.getCurve().createLineSegment()
                curve.setStart(create_sbml_point(layout_ns, line[0][0], line[0][1]))
                curve.setEnd(create_sbml_point(layout_ns, line[1][0], line[1][1]))


            mm = l.get_min_max()
            minCoords[0] = min(mm[0][0], minCoords[0])
            minCoords[1] = min(mm[0][1], minCoords[1])
            maxCoords[0] = max(mm[1][0], maxCoords[0])
            maxCoords[1] = max(mm[1][1], maxCoords[1])


            # curves connecting each of the species glyphs to the reaction
            for n_s in g[n_r]:
                # cnt_roles = len(g[n_r][n_s])
                # ix_mod = 0
                for role in g[n_r][n_s]: #role is be.SPECIES_ROLE
                    srg_id = 'srg_{}_{}'.format(n_s, role.name)
                    if srg_id not in srg_cnt_dict:
                        srg_cnt_dict[srg_id] = 0
                    srg_cnt_dict[srg_id] += 1
                    srg_id = '{}_{}'.format(srg_id, srg_cnt_dict[srg_id])

                    species_reference_glyph = reaction_glyph.createSpeciesReferenceGlyph()
                    species_reference_glyph.setId(srg_id)
                    species_reference_glyph.setSpeciesGlyphId(species_glyphs[n_s])
                    # species_reference_glyph.setSpeciesReferenceId(g_nodes[n_s]['elementId'])
                    # species_reference_glyph.setRole(sbml.SPECIES_ROLE_SUBSTRATE)
                    species_reference_glyph.setRole(role.name.lower())

                    species_reference_curve = species_reference_glyph.getCurve()
                    lines = l.get_species_curve_lines(n_s, role)
                    if not role.is_modifier():
                        # The SBML specs says that if the curve is role or product, the direction should be AWAY from the reaction cnter
                        lines = reverse_lines(lines)

                    for line in lines:
                        ls = species_reference_curve.createLineSegment()
                        ls.setStart(create_sbml_point(layout_ns, line[0][0], line[0][1]) )
                        ls.setEnd(create_sbml_point(layout_ns, line[1][0], line[1][1]))




        layout.setDimensions(sbml.Dimensions(layout_ns, float(maxCoords[0] - minCoords[0] + 2*species_glyph_width),
                                             float(maxCoords[1] - minCoords[1] + 2*species_glyph_height)))


def reverse_lines(lines):
    lines_r = copy.deepcopy(lines)
    lines_r.reverse()
    for line in lines_r:
        line.reverse()

    return lines_r

def treat_line(lines: List[gr.Line]) -> List[gr.Line]:
    """
    Minerva does not allow a line to end and start in the same point (zero length) so if that happens we add move
    the end point by one pixel
    :param line:
    :return:
    """

    ls = copy.deepcopy(lines)

    if len(lines) == 0:
        return lines
    # if positions of first point of the first line and last point of the last line are the same
    while gr.utils.dist(ls[0][0], ls[-1][1]) < 1:
        ls[0][0][0] += 1

    return ls


def create_sbml_point(layout_ns, x, y):
    return sbml.Point(layout_ns, float(x), float(y))