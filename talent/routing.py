import networkx as nx
import numpy as np
import logging
import math
import random
import copy
from rtree import index
from sortedcontainers import SortedList, SortedDict

from typing import List, Dict, Tuple, NamedTuple, Set

from . import graphics as gr
from . import settings
# from .grid import Grid

# import poly_point_isect


random.seed(42)

lines_cache:Dict[tuple, gr.Line] = {}

class OrthogonalLines:
    def __init__(self):
        self.__lines_vertical:Dict[float, List[List[float, float]]] = {} #for each x coordinate contains pairs of min and max y coordinate (i.e. list of lines)
        self.__lines_horizontal: Dict[float, List[List[float, float]]] = {}

    def add_points(self, points: List[np.ndarray]):
        for i in range(1, len(points)):
            self.add_line(gr.Line(points[i], points[i-1]))

    def add_line(self, l: gr.Line):
        if l[0][0] == l[1][0]: #vertical
            x = l[0][0]
            if x not in self.__lines_vertical:
                self.__lines_vertical[x] = []
            y_pair = [l[0][1], l[1][1]]
            if y_pair[0] > y_pair[1]:
                y_pair[0], y_pair[1] = y_pair[1], y_pair[0]
            if y_pair not in self.__lines_vertical[x]:
                self.__lines_vertical[x].append(y_pair)
        else: #horizontal
            y = l[0][1]
            if y not in self.__lines_horizontal:
                self.__lines_horizontal[y] = []
            x_pair = [l[0][0], l[1][0]]
            if x_pair[0] > x_pair[1]:
                x_pair[0], x_pair[1] = x_pair[1], x_pair[0]
            if x_pair not in self.__lines_horizontal[y]:
                self.__lines_horizontal[y].append(x_pair)

    def intersects(self, l: gr.Line or List[List[float]]) -> bool:
        if l[0][0] == l[1][0]: #vertical
            x = l[0][0]
            if x in self.__lines_vertical:
                y1, y2 = l[0][1], l[1][1]
                for lv in self.__lines_vertical[x]:
                    if lv[0] <= y1 <= lv[1] or lv[0] <= y2 <= lv[1]:
                        return True
        else: #horizontal
            y = l[0][1]
            if y in self.__lines_horizontal:
                x1, x2 = l[0][0], l[1][0]
                for lh in self.__lines_horizontal[y]:
                    if lh[0] <= x1 <= lh[1] or lh[0] <= x2 <= lh[1]:
                        return True
        return False


def get_line(u:tuple or np.ndarray, v:tuple or np.ndarray) -> gr.Line:
    # return gr.Line(u, v)
    u = tuple(u)
    v = tuple(v)
    if (u, v) in lines_cache:
        return lines_cache[(u,v)]
    else:
        l = gr.Line(u, v)
        lines_cache[(u, v)] = l
        return l

class RectangleIx:
    def __init__(self, r: gr.Rectangle, ix):
        self.l = r
        self.ix = ix


class RoutingGraph:

    def __init__(self, rects: List[gr.Rectangle], lines: List[gr.Line] = []):


        # p_tupples = [(p[0], p[1]) for p in points]

        self.__lines = copy.deepcopy(lines) # used in __overlap
        self.__rects = copy.deepcopy(rects)
        self.__g = nx.Graph()

        self.__rects_x1_sorted = SortedList(self.__rects, key=lambda o:o.get_x1())
        self.__rects_x2_sorted = SortedList(self.__rects, key=lambda o: o.get_x2())
        self.__rects_y1_sorted = SortedList(self.__rects, key=lambda o: o.get_y1())
        self.__rects_y2_sorted = SortedList(self.__rects, key=lambda o: o.get_y2())

        p = index.Property()
        # p.variant = index.RT_Linear

        self.__rtree = index.Index(properties=p)

        for i in range(len(rects)):
            r = rects[i]
            self.__rtree.insert(i, r.get_rtree_coords())

    def point_in_graph(self, p: Tuple[float]):
        return p in self.__g


    def add_nodes_and_connect(self, points: List[tuple or np.ndarray], s_ids: List[str or None] = None, interconnect:bool=True):
        """
        In the first step, the function takes all the input points and existing points, plus points
        which could be used by an orthogonal path connecting any pair of those nodes ([p1[0], p2[1]) and [p2[0], p1[1]]) and
        inserts them into routing graph. Then, all lines connecting those points are considered and line sweeping algorithm
        is used to find which of these lines do not intersect bouding boxes of objects in the graph. Finally, those
        pairs of nodes which correspond to lines which do not intersect any bounding box are connected by an edge
        in the routing graph.
        :param points: Points to be added to the routing graph
        :param s_ids: Species which the points are associated with
        :return:
        """

        def add_node_and_path(p1: Tuple, p2: Tuple):
            """
            Adds nodes corresponding to the input coordinates and to the paths list (living in the outer scope)
             adds all orthogonal lines connecting the two points.
            :param p1:
            :param p2:
            :return:
            """
            k = (p1[0], p2[1])
            l = (p2[0], p1[1])

            self.add_node(k)
            self.add_node(l)

            if p1 != k:
                paths.append([p1, k])
            if k != p2:
                paths.append([k, p2])
            if p1 != l:
                paths.append([p1, l])
            if 1 != p2:
                paths.append([l, p2])
            

        logging.info("add_nodes_and_connect")


        # Get the already existing points
        existing_points:List[tuple] = []
        for n in self.__g.nodes:
            if self.__g.nodes[n]['center'] == True or self.__g.nodes[n]['object'] is None:
                #the center node can be connected to anything since every curve would intersect with the object of which the node is center
                continue
            existing_points.append(n)

        # For each point and every existing point (including the new already added points) generate the points on
        # orthogonal positions and add the lines into the paths object
        paths: List[List[Tuple[float]]] = []
        for i1 in range(len(points)):
            p1 = tuple(points[i1])

            # If the node is already present, just connect it with the new nodes, but not with the existing
            # TODO: it can happen that one point is shared by mutiple species%
            if self.add_node(p1, center=False, s_id=s_ids[i1]):
                for ep in existing_points:
                    add_node_and_path(p1, ep)

            if interconnect:
                for i2 in range(0, i1):
                    add_node_and_path(p1, tuple(points[i2]))


        # run the sweep line algorithm which returns indexes of lines which intersect any of the rectangles
        ix_isect_paths = sweep_line(paths, self.__rects)


        # add adges corresponding to non-intersecting lines
        logging.info("Adding non intersecting edges")
        for ix in set(range(len(paths))) - ix_isect_paths:
            u = paths[ix][0]
            v = paths[ix][1]

            self.__g.add_edge(u, v, dist=gr.utils.dist(np.array(u), np.array(v)))
        logging.info("Finished adding non intersecting edges")


    # def add_node_and_connect(self, n: tuple or np.ndarray, center=False, s_id: str = None) -> bool:
    #     # tupples are more suitable since one can search in list of tupples or sort them ... both things can't be done with np arrays
    #
    #     nodes = list(self.__g.nodes)
    #     if not self.add_node(n, center, s_id) or center:
    #         # the center node can be connected to anything since every curve would intersect with the object of which the node is center
    #         return False
    #     nt = tuple(n)
    #
    #     # self.__g.add_node(nt)
    #     for m in nodes:
    #         if self.__g.nodes[m]['center'] == True:
    #             #the center node can be connected to anything since every curve would intersect with the object of which the node is center
    #             continue
    #         if nt[0] == m[0] or nt[1] == m[1]:
    #             self.__add_edge_if_non_intersecting(nt, m)
    #         else:
    #             k = (nt[0], m[1])
    #             l = (m[0], nt[1])
    #             self.add_node(k)
    #             self.add_node(l)
    #             self.__add_edge_if_non_intersecting(nt, k)
    #             self.__add_edge_if_non_intersecting(k, m)
    #             self.__add_edge_if_non_intersecting(nt, l)
    #             self.__add_edge_if_non_intersecting(l, m)
    #
    #     return True

    def lineIntersectsRects(self, l:gr.Line):

        candidates = [self.__rects[ix] for ix in self.__rtree.intersection(l.bb().get_rtree_coords())]
        for r in candidates:
            if r.intersects(l, consider_border=False):
                return True
        return False



    def __add_edge_if_non_intersecting(self, u: tuple, v: tuple):
        l = get_line(u,v)
        # self.__g.add_edge(u, v, dist=l.length())
        # return

        # # usin of rectangles in the bisect functions is because its sorted list of rectangeles and the key function needs to call get_
        # x = l.x_max
        # left = self.__rects_x1_sorted[:self.__rects_x1_sorted.bisect_left(gr.Rectangle(x, x, x, x))]
        # x = l.x_min
        # right = self.__rects_x2_sorted[self.__rects_x2_sorted.bisect_right(gr.Rectangle(x, x, x, x)):]
        # x = l.y_max
        # top = self.__rects_y1_sorted[:self.__rects_y1_sorted.bisect_left(gr.Rectangle(x, x, x, x))]
        # x = l.y_min
        # bottom = self.__rects_y2_sorted[self.__rects_y2_sorted.bisect_right(gr.Rectangle(x, x, x, x)):]
        #
        # candidates = set(left).intersection(right).intersection(top).intersection(bottom)
        if not self.lineIntersectsRects(l):
            self.__g.add_edge(u, v, dist=l.length())

    def add_edge(self, u, v):
        self.__g.add_edge(tuple(u), tuple(v), dist=get_line(u, v).length())

    def add_node(self, p: tuple or np.ndarray, center: bool = False, s_id: str = None) -> bool:
        pt = (p[0], p[1])
        if pt in self.__g.nodes():
            return False
        self.__g.add_node(pt, object=s_id, center=center)
        return True

    def remove_node(self, p: tuple or np.ndarray):
        self.__g.remove_node(tuple(p))

    # def __coords_convert(self, i: int, j: int = None) -> int or List[int]:
    #     if j is None:
    #         return [i % self.__grid_width, int(i / self.__grid_width)]
    #     else:
    #         return j * self.__grid_width + i
    #
    # def __add_rect(self, r: gr.Rectangle):
    #     self.__rects.append(r)
    #     to_remove = []
    #     for n_id in self.__g.nodes:
    #         if r.intersects(self.__g.nodes[n_id]['pos']):
    #             to_remove.append(n_id)
    #     self.__g.remove_nodes_from(to_remove)
    #
    #     to_remove = []
    #     # x_min, y_min = r.get_lt()
    #     # x_max, y_max = r.get_rb()
    #     for u, v in self.__g.edges:
    #
    #         u_pos = self.__g_node_pos[u]
    #         v_pos = self.__g_node_pos[v]
    #
    #         l = gr.Line(u_pos, v_pos, type_check=False)
    #         if l.intersects_rectangle(r):
    #             to_remove.append((u, v))
    #             self.__species_intersecting_edges.append((u, v))
    #             self.__species_intersecting_edges.append((v, u))
    #
    #         # if (u_pos[0] < x_min and v_pos[0] < x_min) or \
    #         #     (u_pos[0] > x_max and v_pos[0] > x_max) or \
    #         #     (u_pos[1] < y_min and v_pos[1] < y_min) or \
    #         #     (u_pos[1] > y_max and v_pos[1] > y_max):
    #         #     continue
    #
    #         # if r.intersects(gr.Line(u_pos, v_pos)):
    #         #     to_remove.append((u,v))
    #     self.__g.remove_edges_from(to_remove)
    #
    # def __add_line(self, l: gr.Line):
    #     self.__lines.append(l)
    #     to_remove = []
    #     for n1, n2 in self.__g.edges:
    #         if (n1 == 116 and n2 == 210) or (n2 == 116 and n1 == 210):
    #             lkjlkjlkj=1;
    #         lg = gr.Line(self.__g_node_pos[n1], self.__g_node_pos[n2], type_check=False)
    #         # lg = gr.Line(self.__g.nodes[n1]['pos'], self.__g.nodes[n2]['pos'], type_check=False)
    #         if lg.intersect_collinear(l):
    #         # if lg.collinear(l) and lg.intersects_line(l):
    #             to_remove.append([n1, n2])
    #     self.__g.remove_edges_from(to_remove)
    #
    # def __remove_line(self, l: gr.Line):
    #     """
    #     Removing a line means that two points on the grid which were previously not connecting because
    #     a line connecting them was intersecting "l" can now be connected and thus edge connecting the should
    #     be added to the routing graph.
    #     """
    #     for i in range(len(self.__lines)):
    #         if l == self.__lines[i]:
    #             del self.__lines[i]
    #             break
    #
    #     # We can't use the g.nodes directly, because not all indeces are actually present since some nodes
    #     # were removed due to intersects with species
    #     nodes = list(self.__g.nodes)
    #     for i in range(len(nodes)):
    #         for j in range(i+1, len(nodes)):
    #             if not self.__g.has_edge(nodes[i], nodes[j]):
    #                 pos_i:np.ndarray = self.__g_node_pos[nodes[i]]
    #                 pos_j:np.ndarray = self.__g_node_pos[nodes[j]]
    #
    #                 # i_ij_grid = self.__coords_convert(i)
    #                 # j_ij_grid = self.__coords_convert(j)
    #
    #                 # if settings.orthogonal_routing and not (i_ij_grid[0] == j_ij_grid[0] or i_ij_grid[1] == j_ij_grid[1]):
    #                 if settings.orthogonal_routing and not np.any(np.equal(pos_i, pos_j)):
    #                     continue
    #
    #                 l_ij:gr.Line = gr.Line(pos_i, pos_j)
    #                 if l_ij.collinear(l) and l_ij.intersects(l_ij) and (nodes[i], nodes[j]) not in self.__species_intersecting_edges:
    #                     # i and j can be connected given there is no other line that would interfere
    #                     other: bool = False
    #                     for l_aux in self.__lines:
    #                         if l_ij.collinear(l_aux) and l_ij.intersects(l_aux):
    #                             other = True
    #                             break
    #                     if not other:
    #                         self.__g.add_edge(nodes[i], nodes[j], dist=l_ij.length())
    #
    # def add_path(self, path: List[np.ndarray]):
    #     assert len(path) > 1
    #     for i in range(1, len(path)):
    #         self.__add_line(gr.Line(path[i-1], path[i]))
    #
    # def remove_path(self, path: List[np.ndarray]):
    #     assert len(path) > 1
    #     for i in range(1, len(path)):
    #         self.__remove_line(gr.Line(path[i-1], path[i]))
    #
    # def __get_closest_node(self, pos: np.ndarray):
    #     min_dist = sys.maxsize
    #     min_id = None
    #     for n_id in self.__g.nodes:
    #         dist = gr.utils.dist(pos, self.__g_node_pos[n_id])
    #         if dist < min_dist:
    #             min_dist = dist
    #             min_id = n_id
    #
    #     assert min_id is not None
    #     return min_id

    def add_path(self, path: List[np.ndarray]):
        assert len(path) > 1
        for i in range(1, len(path)):
            self.__lines.append(get_line(path[i - 1], path[i]))

    def remove_path(self, path: List[np.ndarray]):
        assert len(path) > 1
        for i in range(1, len(path)):
            l = get_line(path[i - 1], path[i])
            for i in range(len(self.__lines)-1,-1,-1):
                if l == self.__lines[i]:
                    del self.__lines[i]
                    break

    def __get_shift_vector_for_overlapping_line(self, line: gr.Line):

        dir = 1 if int(random.random()-0.5) > 0 else -1
        lane = math.ceil(random.random() * settings.render.routing_lanes / 2) # /2 because we consider both halves (the dir variable)
        shift = lane * (settings.render.routing_offset / settings.render.routing_lanes) * dir
        # shift = random.random()*settings.render.routing_offset * dir

        if line.horizontal():
            return np.array([0.0,shift])
        else:
            return np.array([shift, 0.0])

        # shift_dir = 1
        # shift_iter = 1
        # while True:
        #     for l in self.__lines:
        #         if line.intersect_collinear(l):
        #             #the shift alternately tries offset on both sides of the original line
        #             shift = shift_dir * math.ceil(shift_iter / 2)
        #             shift_dir *= -1
        #             shift_iter += 1
        #
        #             continue
        #     break

    def __overlap(self, line: gr.Line) -> bool:

        for l in self.__lines:
            if line.intersect_collinear(l):
                return True
        return False

    def __join_collinear(self, path: List[np.ndarray]) -> List[np.ndarray]:

        if len(path) < 2:
            return path

        path_new = [np.array(path[0]), np.array(path[1])]
        for i in range(2, len(path)):
            p1 = path[i-2]
            p2 = path[i-1]
            p3 = path[i]
            if p1[0] == p2[0] == p3[0] or p1[1] == p2[1] == p3[1]:
                path_new[-1] = np.array(p3)
            else:
                path_new.append(np.array(p3))


        return path_new

    def __handle_overlapping_lines(self, path: List[np.ndarray]) -> List[np.ndarray]:
        """
        If two path lines overlap one of them needs to be shifted a bit so that they are discernable
        :param path:
        :return:
        """

        path_new = copy.deepcopy(path)
        ix_to_remove = []
        for i in range(1, len(path_new)):
            # It could happen that the new position coinceedes with the next position forming a zero-length line
            if path_new[i][0] == path_new[i-1][0] and path_new[i][1] == path_new[i-1][1]:
                ix_to_remove.append(i)
                continue
            lp = get_line(path_new[i - 1], path_new[i])
            if self.__overlap(lp):
                shift = self.__get_shift_vector_for_overlapping_line(lp)
                path_new[i - 1] += shift
                path_new[i] += shift

        for i in ix_to_remove[::-1]:
            del path_new[i]

        return path_new

    def find_path(self, pos1: np.ndarray, pos2: np.ndarray, ol: OrthogonalLines) -> List[np.ndarray]:

        #TODO: Correctly handle situations when pos1 == pos2 currently this resolves into a path containing one node only

        # If ID is not None, all nodes sharing the id will be temporarilly connected and disconnected at the end
        # The nodes with the same ID will be connected to its center

        def get_object_nodes(p):
            """
            For the passed point, which corresponds to a node in the routing graph, identifies
            all other nodes which correspond to the same object (nodes can correspond to
            species ports and thus are associated with a species).
            :param p:
            :return:
            """
            nodes = []

            o_id = self.__g.nodes[p]["object"]
            if o_id is not None:
                for n in self.__g.nodes:
                    if self.__g.nodes[n]["object"] == o_id and n != p:
                        nodes.append(n)
            return nodes

        src = tuple(pos1)
        src_nodes = get_object_nodes(src)

        tgt = tuple(pos2)
        tgt_nodes = get_object_nodes(tgt)

        for n in src_nodes:
            self.__g.add_edge(src, n, dist=get_line(src, n).length())
        for n in tgt_nodes:
            self.__g.add_edge(tgt, n, dist=get_line(tgt, n).length())

        logging.info("Searching for path")
        try:
            # sp = nx.dijkstra_path(self.__g, source=source, target=target, weight='dist')
            def length(u, v):
                return gr.utils.dist(np.array(u), np.array(v))

            def dist(u, v, e):
                coef = 1
                if u != v:
                    if ol.intersects([u, v]):
                    # if self.__overlap(get_line(u,v)):
                         coef += 5 # TODO: use something better then just arbitrary number like this. Maybe something derived from the expected length of a path
                return e['dist']*coef

            # sp = nx.astar_path(self.__g, source=src, target=tgt, heuristic=length, weight='dist')
            sp = nx.dijkstra_path(self.__g, source=src, target=tgt, weight=dist)

        except nx.NetworkXNoPath:
            sp = []

        logging.info("Path searching finished")

        sp_pos = [np.array(n).astype(float) for n in sp]

        for n in src_nodes:
            self.__g.remove_edge(src,  n)
        for n in tgt_nodes:
            if (tgt,n) in self.__g.edges():
                #it can happen that, for example, taget and source are the same points and then the (tgt,n) edge wsa removed in the previous cycle
                self.__g.remove_edge(tgt, n)

        return self.__handle_overlapping_lines(self.__join_collinear(sp_pos))

class SweepLineObject(NamedTuple):
    is_line: bool #
    ix: int
    first: bool
    coord: float
    obj: gr.Line or gr.Rectangle

class Coord(NamedTuple):
    coord: float

def sweep_line(lines: List[List[Tuple[float]]], rects: List[gr.Rectangle]) -> Set[int]:
    """
    This is a specific line sweep algorithm since we know that the lines are either vertical or horizontal
    and we are checking intersection with rectangle.
    1. The rectangles and lines are sorted by their x coordinates (both lower and upper)
    2. The objects are processed in the order of increasing x coordinates.
        If lower x coordinate is encountered:
            - If it is a rectangle, it is added to the list of active rectangles (its y coordinates are stored and the list
             is sorted by them) and list of active lines is queried to obtain horizontal lines with y coordinate
             in the y-range of the current rectangle. These lines intersect current rectangle.
            - If the object is a line
                - If the line is vertical, we obtain from the rectangles active list rectangles in the y-range of the line -> intersections
                - If the line is horizontal, we add it to the list of active lines. Here only y coordinate is sufficient
                since the line is horizontal. This list will is queried when a rectangle is encountered.
        If upper x cooridnate is encountered, the respective objects in the active lists (lines and rectangels) are
        removed.


    :param lines:
    :param rects:
    :return: Indexes of lines which intersect any of the rectangles
    """

    logging.info("Beginning of sweep line")

    ix_isect: List[int] = []

    slos: List[SweepLineObject] = []
    for ix, l in enumerate(lines):

        if l[0][0] > l[1][0]:
            l[0], l[1] = l[1], l[0]

        slos.append(SweepLineObject(is_line=True, ix=ix, first=True, coord=l[0][0], obj=l))
        slos.append(SweepLineObject(is_line=True, ix=ix, first=False, coord=l[1][0], obj=l))
    for ix, r in enumerate(rects):
        slos.append(SweepLineObject(is_line=False, ix=ix, first=True, coord=r.get_x1(), obj=r))
        slos.append(SweepLineObject(is_line=False, ix=ix, first=False, coord=r.get_x2(), obj=r))

    slos = sorted(slos, key=lambda x: (x.coord, x.first))

    active_lines_dict = SortedDict()
    active_rects = SortedList(key=lambda x: x.coord)

    for slo in slos:
        if slo.first:
            if slo.is_line:
                # We do not store vertical lines since now we can easily check all the active rectangles which are between the y coordinates of the current vertical line
                y1 = slo.obj[0][1]
                y2 = slo.obj[1][1]
                if y1 != y2: #vertical line

                    if y1 > y2:
                        y1,y2 = y2, y1

                    if len(list(active_rects.irange(Coord(y1), Coord(y2)))) > 0:
                        ix_isect.append(slo.ix)

                    # encountered_rects = []
                    # for slo_r in active_rects.irange(Coord(y1), Coord(y2)):
                    #     ix_isect.append(slo.ix)
                    #     break
                    #     # if slo_r.ix not in encountered_rects:
                    #     #     ix_isect.append(slo.ix)
                    #     #     break # The line index can intersect multiple rectangles
                    #     # else:
                    #     #     encountered_rects.append(slo_r.ix)
                else:
                    #its horizontal line, so it does not matter whether we use y1 or y2
                    if y1 not in active_lines_dict:
                        active_lines_dict[y1] = SortedList()
                    active_lines_dict[y1].add(slo.ix)

            else:
                y1 = slo.obj.get_y1()
                y2 = slo.obj.get_y2()
                active_rects.add(SweepLineObject(is_line=slo.is_line, ix=slo.ix, first=True, coord=y1, obj=slo.obj))
                active_rects.add(SweepLineObject(is_line=slo.is_line, ix=slo.ix, first=False, coord=y2, obj=slo.obj))

                #We store only horizontal lines and so now we can check which active horizontal lines are in the y range of the rectangle and these surely intersect
                for key in active_lines_dict.irange(Coord(y1), Coord(y2)):
                    ix_isect += list(active_lines_dict[key])

        else:
            if slo.is_line and slo.obj[0][0] != slo.obj[1][0]:
                # We store only horizontal lines, so if this is the end of a vertical line we do not care about it
                y = slo.obj[0][1] #it is a horizontal line, so slo.obj[0][1] == slo.obj[1][1]
                active_lines_dict[y].remove(slo.ix)
                if len(active_lines_dict[y]) == 0:
                    del active_lines_dict[y]

            elif not slo.is_line:
                y1 = Coord(slo.obj.get_y1())
                y2 = Coord(slo.obj.get_y2())
                ars_slos = set(active_rects.irange(y1, y1)).union(active_rects.irange(y2, y2))
                for slo_r in ars_slos:
                    if slo_r.ix == slo.ix:
                        active_rects.remove(slo_r)

    logging.info("Finished sweep line")
    # A vertical line can intersect multiple rectangles so it can appear multiple times in ix_isect
    return set(ix_isect)








