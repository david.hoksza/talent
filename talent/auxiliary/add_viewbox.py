import os
import re
import sys
dir = 'out/pred/1/tmp/'

for f_name in os.listdir('{}'.format(dir)):
    if f_name.endswith('.svg'):

        data = ""
        with open('{}{}'.format(dir, f_name), 'r') as f:
            data = f.read()
            minx = sys.maxsize
            miny = sys.maxsize
            maxx = -sys.maxsize
            maxy = -sys.maxsize

            for g in re.finditer(r' x="([0-9]*)"', data):
                x = int(g.group(1))
                if x < minx:
                    minx = x
                if x > maxx:
                    maxx = x

            for g in re.finditer(r' y="([0-9]*)"', data):
                y = int(g.group(1))
                if y < miny:
                    miny = y
                if y > maxy:
                    maxy = y

            data = data.replace('<svg', '<svg viewBox="{} {} {} {}"'.format(minx, miny, maxx+300, maxy+300))
            f.close()

        with open('{}{}'.format(dir, f_name), 'w') as f:
            f.write(data)
            f.close()












