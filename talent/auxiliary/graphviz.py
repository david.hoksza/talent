import argparse
import common
import networkx as nx
import graph_utils as gu

import networkx.drawing.nx_pydot as pd

class Settings:
    reactionLabels = False

class GVProgram:
    dot = 0
    neato = 1

class GVOutputFileType:
    pdf = 0


def export_graph(g:nx.MultiGraph, f_name:str, f_type:GVOutputFileType):
    NotImplemented


def obtain_layout(g:nx.MultiGraph, prog:GVProgram):
    NotImplemented


def export_to_file(f_name_in, f_name_out):

    g = gu.load_graph(f_name_in, 'sbml')
    with common.open_file(f_name_out, 'w') as f:
        f.write(convert_to_dot(g))
        f.close()


def convert_to_dot(g:nx.MultiGraph):
    dot = ''

    dot += 'digraph g {\n graph [overlap=false]\n'
    for s_id in gu.get_all_species_ids(g):
        s = gu.get_node_data(g.nodes[s_id])
        sboTerm = s.get_type()
        if sboTerm == 247:
            shape = "shape=ellipse"
        else:
            shape = "shape=rectangle, style=rounded"

        dot += '\t{} [ label="{}", {} ];\n'.format(s_id, s.get_name(), shape)

    for r_id in gu.get_all_reaction_ids(g):
        r = gu.get_reaction(g, r_id)
        label = r.getName() if Settings.reactionLabels else ""
        dot += '\t{} [ label="{}", shape=square, fixedsize=true, width=0.2, height=0.2 ];\n'.format(r_id, label)

    for (n1, n2) in g.edges():
        for role in gu.get_roles(g, n1, n2):
            if role.is_reactant():
            # if 'substrate' in role:
                dot += '\t{} -> {}[arrowhead=none, arrowtail=normal,arrowsize=1.0];\n'.format(n1, n2)
            elif role.is_modifier():
            # elif 'modifier' in role:
                dot += '\t{} -> {}[arrowhead=none];\n'.format(n1, n2)
            else:
                dot += '\t{} -> {}[arrowhead=none, arrowtail=normal,arrowsize=1.0];\n'.format(n2, n1)

    dot += '}'

    return dot

if __name__ == '__main__':


    common.init_logging()

    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input",
                        required=True,
                        help="Input SBML file")
    parser.add_argument("-o", "--output",
                        required=True,
                        help="Output file")

    args = parser.parse_args()

    # export_to_file('data/biomodels/all/l3/BIOMD0000000001_url_sbo.xml', 'x')
    export_to_file(args.input, args.output)