import argparse
import common
import graph_utils as gu
import requests


def convert(ifn, ifmt, ofmt, minerva):

    response = ""

    with common.open_file(ifn) as f:

        data = f.read()

        # token = requests.get("{}/api/doLogin".format(minerva)).cookies['MINERVA_AUTH_TOKEN']
        response = requests.post("{}/api/convert/image/{}:{}".format(minerva, ifmt, ofmt),
                                 # cookies=dict(MINERVA_AUTH_TOKEN=token),
                                 data=data)
    return response.text


def output(data, ofn=None):
    if ofn:
        with common.open_file(ofn, 'w') as f:
            f.write(data)
    else:
        print(data)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", "--input",
                        required=True,
                        help="Input file in the sbml format")
    parser.add_argument("-o", "--output",
                        help="Output file")
    parser.add_argument("-m", "--minerva-instance",
                        required=False,
                        default="http://localhost:8080/minerva",
                        help="Address of MINERVA instance API (default=http://localhost:8080/minerva")
    parser.add_argument("-if", "--input-format",
                        required=False,
                        default="CellDesigner_SBML",
                        help="Format of the output file. Allowed values are [CellDesigner_SBML, SBML, SBGN-ML] (default=CellDesigner_SBML).")
    parser.add_argument("-of", "--output-format",
                        required=False,
                        default="svg",
                        help="Format of the output file. Allowed values are [svg, pdf] (default=svg).")


    args = parser.parse_args()

    output(convert(args.input, args.input_format, args.output_format, args.minerva_instance), args.output)
