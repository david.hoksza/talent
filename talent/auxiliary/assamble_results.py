import os


results_dir = './'
out_dir = 'out/pred/1/'
tmp_dir = './tmp/'

step = 20

def get_tgt_tmp(f_name):
    aux = f_name.split('-cc')
    tgt = aux[0]
    tmp = aux[1].split('from--')[1].split('-cc0')[0]

    return tgt, tmp

def assamble():

    cnt = 0

    for f_name in os.listdir('{}'.format(out_dir)):

        if f_name.endswith('.svg'):

            if cnt % step == 0:
                f_out = open('{}results_{}-{}.html'.format(out_dir, cnt, cnt + step - 1), 'w')

                f_out.write("<html>\n")
                f_out.write('<link rel="stylesheet" type="text/css" href="styles.css">\n')

                f_out.write("<body>\n")

            tgt, tmp = get_tgt_tmp(f_name)


            f_out.write('<div class="pair">\n')
            f_out.write('<div class="label">{}</div>\n'.format(f_name))
            f_out.write('<div class="first"><img class="first" src="{}{}"/></div>\n'.format(results_dir, f_name))
            f_out.write('<div class="second"><img class="second" src="{}{}.svg"/></div>\n'.format(tmp_dir, tmp))
            f_out.write('</div>')

            cnt += 1

            if cnt % step == 0:
                f_out.write("</body>\n")
                f_out.write("</html>\n")
                f_out.close()

assamble()




