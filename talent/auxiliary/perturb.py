import argparse
import graph_utils as gu
import random

cnt_species_remove_abs = 20
cnt_reactions_remove_abs = 2

def main(args):
    # cmprtmnt = 'Axonal remodeling'
    # cmprtmnt = 'Dopamine metabolism'
    cmprtmnt = 'Ubiquitin Proteasome System'

    g = gu.load_graph('out/pdmap-{}.json'.format(cmprtmnt), 'json')

    g_ccs = [g_cc for g_cc in gu.get_connected_components(g)]
    g_ccs = sorted(g_ccs,key=lambda g: len(g.nodes()), reverse=True)
    g_cc = g_ccs[0]

    g_cc_p = gu.perturb_species(g_cc, 'remove', absolute=cnt_species_remove_abs)

    fn = 'out/perturbed/{}_remove_species_abs_{}'.format(cmprtmnt, cnt_species_remove_abs)
    gu.draw([g_cc, g_cc_p], outputFile='{}.pdf'.format(fn))
    gu.save_graph(g_cc_p, '{}.json'.format(fn), 'json')
    gu.save_graph(gu.get_reaction_graph(g_cc_p), '{}-rg.json'.format(fn), 'json')

    g_cc_p = gu.perturb_reactions(g_cc, 'remove', absolute=cnt_reactions_remove_abs)

    fn = 'out/perturbed/{}_remove_reactions_abs_{}'.format(cmprtmnt, cnt_reactions_remove_abs)
    gu.draw([g_cc, g_cc_p], outputFile='{}.pdf'.format(fn))
    gu.save_graph(g_cc_p, '{}.json'.format(fn), 'json')
    gu.save_graph(gu.get_reaction_graph(g_cc_p), '{}-rg.json'.format(fn), 'json')

    gu.save_graph(g, 'out/perturbed/{}.json'.format(cmprtmnt), 'json')




if __name__ == '__main__':

    random.seed(42)

    # parser = argparse.ArgumentParser()

    # args = parser.parse_args()

    main(None)