import argparse
import libsbml as sbml

class SBOTerm:
    genericProtein = 'SBO:0000252'
    simpleMolecule = 'SBO:0000252'
    unknown = 'SBO:0000285'

latestLevel = sbml.SBMLDocument.getDefaultLevel()
latestVersion = sbml.SBMLDocument.getDefaultVersion()

def add_sbo(f_name):

    document = sbml.SBMLReader().readSBMLFromFile(f_name)

    errors = document.getNumErrors()

    # if errors > 0:
    #     document.printErrors()
    #     raise Exception('Invalid model')

    # olevel = document.getLevel()
    # oversion = document.getVersion()
    # success = False
    #
    # if olevel < latestLevel or oversion < latestVersion:
    #     print("Attempting to convert Level " + str(olevel) + " Version " + str(oversion)
    #           + " model to Level " + str(latestLevel)
    #           + " Version " + str(latestVersion) + "." + "\n")
    #     success = document.setLevelAndVersion(latestLevel, latestVersion)
    #
    #     if not success:
    #         print("Unable to perform conversion due to the following:" + "\n")
    #         document.printErrors()

    m = document.getModel()

    for i in range(0, m.getNumSpecies()):
        sp = m.getSpecies(i)

        if sp.getSBOTerm() == -1:

            annotation = sp.getAnnotation()

            if annotation:
                annotation = annotation.toXMLString().upper()
                if 'UNIPROT' in annotation or 'INTERPRO' in annotation:
                    sp.setSBOTerm(SBOTerm.genericProtein)
                elif 'CHEMBL' in annotation or 'KEGG.COMPOUND' in annotation:
                    sp.setSBOTerm(SBOTerm.simpleMolecule)
                else:
                    sp.setSBOTerm(SBOTerm.unknown)

    sbml.SBMLWriter().writeSBML(document, f_name.replace('.xml', '_sbo.xml'))

# add_sbo('data/biomodels/all/l2/BIOMD0000000013_url.xml')
# exit()

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--input",
                    required=True,
                    help="Input file")
args = parser.parse_args()

add_sbo(args.input)