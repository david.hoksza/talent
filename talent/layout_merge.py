import networkx as nx
import logging
import numpy as np
from itertools import combinations
import sys
import copy
from typing import List, Any, Dict, Set, Tuple, Collection, NamedTuple


from .layout import NodesMapping
from . import graph_utils as gu
from . import graphics as gr
from . import layout as lt
from . import settings
from . import bioentity as be

# import forcelayout as fl


class CompartmentCCNodeSpecies(NamedTuple):
    s_ids: List[str]    #species of the reaction r_id which are pertinent to current compartment
    # neighbors: List[str] #neighbors in other connected component
    pos: np.ndarray #position relative to the compartment


class CompartmentCCNode(NamedTuple):
    cmp_species: Dict[str, CompartmentCCNodeSpecies] # one for each connecting reaction
    bb: gr.Rectangle # bounding box

    def shift(self, v):
        self.bb.shift(v)
        for id in self.cmp_species:
            self.cmp_species[id].pos[0] += v[0]
            self.cmp_species[id].pos[1] += v[1]

CompartmentGraphsIxs = Dict[str, List[int]]

def merge_compartments(cmp_gs: Dict[str, List[nx.MultiGraph]], g_tgt: nx.MultiGraph) -> nx.MultiGraph:
    logging.info("Merging compartments...")
    # Create graph of connected components
    gs_cc: List[nx.MultiGraph] = []
    for gs in cmp_gs.values():
        gs_cc += gs


    # Take ccs in the order from mostly highly connected components (if equal, then by size) and lay
    # given CC based on the reactions connecting CC with its neighbors and reposition when overlapping

    nm:NodesMapping = create_node_mapping(gs_cc)
    g_cc = create_cc_graph(gs_cc, g_tgt, nm)
    layout_ccs(g_cc, gs_cc)
    g_res = merge_ccs(g_cc, gs_cc, g_tgt, nm)
    logging.info("Finished merging compartments")
    return g_res


def create_node_mapping(gs: List[nx.MultiGraph]) -> NodesMapping:
    nm: NodesMapping = NodesMapping()

    for g in gs:
        for id in g:
            orig_id = gu.get_node_data(id, g).get_original_id()
            nm.addMapping(orig_id, id)

    return nm


def locate_orig_id(id: str, gs_nodes: List[Set[str]], nm:NodesMapping) -> int:
    new_ids = set(nm.getSourceMapping(id))
    for i in range(len(gs_nodes)):
        if len(new_ids.intersection(gs_nodes[i])) > 0:
            return i
    assert False

def locate_g_ix(id: str, gs_nodes: List[Set[str]]) -> int:
    for i in range(len(gs_nodes)):
        if id in gs_nodes[i]:
            return i
    assert False

def create_cc_graph(gs: List[nx.MultiGraph], g_tgt_orig: nx.MultiGraph, nm: NodesMapping) -> nx.MultiGraph:

    cc_orig_ids = nm.getAllSourceIds()
    orig_r_ids = gu.get_all_reaction_ids(g_tgt_orig)
    # connecting_r_ids = set(orig_r_ids).difference(cc_orig_ids)

    gs_nodes = [set(g.nodes) for g in gs]

    # create a node for every connected component
    g_cc = nx.MultiGraph()
    i = 0
    for g in gs:
        # get bouding box of each of the species
        bbs = [gu.get_predicted_layout(g, s_id).get_bb().get() for s_id in gu.get_all_species_ids(g)]
        # get covering boudning box
        bb = gr.utils.get_bounding_box(bbs)
        # create a node data for a connected component
        node_data = CompartmentCCNode(cmp_species={}, bb=bb)
        # create a connected component node where i-th graph is assigned ID i
        g_cc.add_node(i, data=node_data)
        i += 1

    # add edges between the connected components (because the connected component can come from different compartments
    # and thus without splitting into compartments, the components would actually be connected)
    for r_id in orig_r_ids:
        s_ids_orig = list(g_tgt_orig[r_id])
        # TODO: The following command obtains for every s_id in orig target first corresponding id in result. However, in case of duplication there can be more of them and maybe the first one is not the best one
        try:
            s_ids = [nm.getSourceMapping(s_id)[0] for s_id in s_ids_orig]
        except:
            # One of the species must have been a connected component of size 1 and such species are not transferred/laid out
            continue
        # Get indexes of graphs which are connected by the reaction
        g_ixs = [locate_g_ix(id=s_id, gs_nodes=gs_nodes) for s_id in s_ids]
        g_ixs_set = set(g_ixs)
        if len(g_ixs_set) > 1:
            for g_ix in g_ixs_set:
                # retrieve reaction species from given compartment
                cc_s_ids = []
                for ix in range(len(s_ids)):
                    if g_ixs[ix] == g_ix:
                        cc_s_ids.append(s_ids[ix])
                # get their position
                avg_pos = gu.get_ids_avg_pos(gs[g_ix], cc_s_ids, layout_type=be.LAYOUT_TYPE.PREDICTED)
                cc_species_data = CompartmentCCNodeSpecies(s_ids=cc_s_ids, pos=avg_pos)
                g_cc.nodes[g_ix]['data'].cmp_species[r_id] = cc_species_data
            # a reaction spanning multiple compartments
            g_cc.add_edges_from(combinations(g_ixs, 2), data=r_id)

    return g_cc

def g_cc_multi_to_simple(g_cc: nx.MultiGraph) -> nx.Graph:
    """
    Converts multigraph where each node represent a connected component into a simple graph where
    nodes have weight corresponding to the number of reactions connecting the individual connected components (nodes).
    :param g_cc:
    :return:
    """
    g = nx.Graph()

    g_cc_nodes: List[int] = list(g_cc)
    g.add_nodes_from(g_cc_nodes)

    for i in range(len(g_cc_nodes)):
        n1 = g_cc_nodes[i]
        for j in range(i+1, len(g_cc_nodes)):
            n2 = g_cc_nodes[j]
            if n2 in g_cc[n1]:
                g.add_edge(n1, n2, weight=len(g_cc[n1][n2]))

    return g

def get_optimal_relative_pos_cc(g_cc, id1: int, id2: int, g1: nx.MultiGraph, g2: nx.MultiGraph):

    n1: CompartmentCCNode = g_cc.nodes[id1]['data']
    n2: CompartmentCCNode = g_cc.nodes[id2]['data']
    bb1 = n1.bb
    bb2 = n2.bb

    dir_min = {
        'cnt_intersections': sys.maxsize,
        'dist': sys.maxsize,
        'pos2_shift': None
    }

    for e in g_cc[id1][id2].values():
        r_id = e["data"]

        pos1: np.ndarray = n1.cmp_species[r_id].pos
        pos2: np.ndarray = n2.cmp_species[r_id].pos
        pos1_relative = pos1 - bb1.get_lt()  # position relative to the bounding box tope left corner
        pos2_relative = pos2 - bb2.get_lt()

        dirs = {}

        # Get the distance for each of the directions (imagining all possible mutual horizontal and vertical positions of the species)
        dirs[(1, 0)] = {'dist': (bb1.width() - pos1_relative[0]) + pos2_relative[0]}
        dirs[(-1, 0)] = {'dist': (bb2.width() - pos2_relative[0]) + pos1_relative[0]}
        dirs[(0, 1)] = {'dist': (bb1.height() - pos1_relative[1]) + pos2_relative[1]}
        dirs[(0, -1)] = {'dist': (bb2.height() - pos2_relative[1]) + pos1_relative[1]}

        # Get the number of intersections for each of the directions
        for dir in dirs:
            dist = max(dirs[dir]['dist'],
                       settings.render.optimal_species_reaction_dist * 2)
            pos2_new = pos1 + np.array(dir) * dist
            line = gr.Line(pos1, pos2_new, type_check=False)
            dirs[dir]['pos2_shift'] = pos2_new - pos2
            dirs[dir]['cnt_intersections'] = gu.count_intersections(g1, line=line) + gu.count_intersections(g2,
                                                                                                            line=line)

        dirs_min = sorted(dirs.items(), key=lambda kv: (kv[1]['cnt_intersections'], kv[1]['dist']))[0]
        if dirs_min[1]['cnt_intersections'] < dir_min['cnt_intersections'] or \
                (dirs_min[1]['cnt_intersections'] == dir_min['cnt_intersections'] and dirs_min[1]['dist'] < dir_min[
                    'dist']):
            dir_min = dirs_min[1]

    return dir_min

def shift_in_optimal_direction(g_cc, id1: int, id2: int, g1: nx.MultiGraph, g2: nx.MultiGraph):
    """
    Here we we find out the optimal position of compartment with id1 and id2.
    For each connecting reaction we imagine that the connecting species would lie on a vertical or horizonatal line and
    we check all four direction vectors. We then pick the one so that the distance of the species is minimal taking into
    account that the bounding boxes of the species can't intersect. Moreover, we prefer such positions which minimize
    the number of intersections with the tentative reaction line and existing species and reactions.


    :param g_cc:
    :param id1:
    :param id2:
    :return:
    """

    opt = get_optimal_relative_pos_cc(g_cc, id1, id2, g1, g2)
    g_cc.nodes[id2]['data'].shift(opt['pos2_shift'])


def shift_gs(g_cc: nx.MultiGraph, gs: List[nx.MultiGraph]):
    for i in range(len(gs)):
        g = gs[i]
        data: CompartmentCCNode = g_cc.nodes[i]['data']
        new_bb = data.bb
        old_bb = lt.min_max_coords(g)
        shift = new_bb.get_lt() - old_bb.min
        lt.pan(g, shift)


def initial_layout(g_cc: nx.MultiGraph, gs: List[nx.MultiGraph]):

    g_cc_simple: nx.Graph = g_cc_multi_to_simple(g_cc)

    st = nx.minimum_spanning_tree(g_cc_simple)
    centrality = nx.betweenness_centrality(st)
    centrality_sorted = sorted(centrality.items(), key=lambda kv: (kv[1], len(gs[kv[0]])), reverse=True)

    # Start in the "center" and find optimal positions of the remaining CCs using BFS. The rectangles can overlap
    active_queue: List[int] = [centrality_sorted[0][0]]
    processed: List[int] = []


    while len(active_queue) > 0:

        cc_id = active_queue[0]
        del active_queue[0]
        processed.append(cc_id)

        nb_cc_ids = st[cc_id]
        for nb_cc_id in nb_cc_ids:
            if nb_cc_id not in processed:
                shift_in_optimal_direction(g_cc, cc_id, nb_cc_id, gs[cc_id], gs[nb_cc_id])
                active_queue.append(nb_cc_id)

    shift_gs(g_cc, gs)

def score_compartments_position(g_cc: nx.MultiGraph, gs: List[nx.MultiGraph], cmp_ixs1: List[int], cmp_ixs2: List[int]):

    score = {'dist': 0, 'cnt_intersections': 0}

    # Retrive the positions of reactions (or rather their connecting points) in the compartments
    r_ids1 = {}
    r_ids2 = {}
    for cmp_ixs, r_ids in [(cmp_ixs1, r_ids1), (cmp_ixs2, r_ids2)]:
        for ix in cmp_ixs:
            cc_node: CompartmentCCNode = g_cc.nodes[ix]['data']
            for r_id in cc_node.cmp_species:
                r_ids[r_id] = {'pos': cc_node.cmp_species[r_id].pos, 'g_ix': ix}

    # Crosscheck the two dictionaries to find, reactions which connect the compartments and for each of them create a line
    # and test how many intersections it has and how long it is
    for r_id in set(r_ids1).intersection(r_ids2):
        line = gr.Line(r_ids1[r_id]['pos'], r_ids2[r_id]['pos'])
        score['dist'] += line.length()
        score['cnt_intersections'] += gu.count_intersections(gs[r_ids1[r_id]['g_ix']], line=line) + gu.count_intersections(gs[r_ids2[r_id]['g_ix']], line=line)

    return score


def resolve_compartments_overlap(g_cc: nx.MultiGraph, gs: List[nx.MultiGraph], cmp_ixs1: List[int], cmp_ixs2: List[int], bb1: gr.Rectangle, bb2: gr.Rectangle):
    min_score = {'dist': sys.maxsize, 'cnt_intersections': sys.maxsize}
    min_shift = None
    for dir in [[1, 0], [0, 1], [-1, 0],[0, -1]]:
        # the "distance"(one componennt is 0) by which the bb2 needs to be shifted to resolve the overlap
        if dir == [1,0]:
            shift = [bb1.get_x2() - bb2.get_x1(), 0] 
        elif dir == [-1,0]:
            shift = [bb1.get_x1() - bb2.get_x2(), 0]
        elif dir == [0, 1]:
            shift = [0, bb1.get_y2() - bb2.get_y1()]
        elif dir == [0, -1]:
            shift = [0, bb1.get_y1() - bb2.get_y2()]
        shift = np.array(shift) + np.array(dir)*settings.render.compartment_padding

        for ix in cmp_ixs2:
            get_cmp_cc_node(g_cc, ix).shift(shift)
            lt.pan(gs[ix], shift)

        score = score_compartments_position(g_cc, gs, cmp_ixs1, cmp_ixs2)
        if score['cnt_intersections'] < min_score['cnt_intersections'] or (score['cnt_intersections'] == min_score['cnt_intersections'] and score['dist'] < min_score['dist']):
            min_score = score
            min_shift = shift

        for ix in cmp_ixs2:
            get_cmp_cc_node(g_cc, ix).shift(-shift)
            lt.pan(gs[ix], -shift)

    # for ix in cmp_ixs2:
    #     get_cmp_cc_node(g_cc, ix).shift(min_shift)

    #shift also the rest of the components which were in the direction of the shift
    for ix in range(len(gs)):
        if ix not in cmp_ixs1:
            bb: gr.Rectangle = g_cc.nodes[ix]['data'].bb
            if ix in cmp_ixs2 or \
                    (min_shift[0] < 0 and bb.get_x1() < bb2.get_x2()) or (min_shift[0] > 0 and bb.get_x2() > bb2.get_x1()) or \
                    (min_shift[1] < 0 and bb.get_y1() < bb2.get_y2()) or (min_shift[1] > 0 and bb.get_y2() > bb2.get_y1()):
                get_cmp_cc_node(g_cc, ix).shift(min_shift)
                lt.pan(gs[ix], min_shift)


def get_compartment_dict(gs: List[nx.MultiGraph]) -> CompartmentGraphsIxs:
    cmp_ix: Dict[str, List[int]] = {}
    for i in range(len(gs)):
        cmp_name: str = gu.get_first_species(gs[i]).get_compartment_name()
        if cmp_name not in cmp_ix:
            cmp_ix[cmp_name] = []
        cmp_ix[cmp_name].append(i)

    return cmp_ix



def handle_overlaps(g_cc: nx.MultiGraph, gs: List[nx.MultiGraph], cmp_ix: CompartmentGraphsIxs):
    """
    There are two reasons for possible overlaps between the compartments:
        1. When laying out individual connected components, only the optimal mutual position of a pair of componenents
        is taken into account, and thus it can happen, that the optimal position will overlap with already laid out
        compartement.
        2. Splitting graph into compartments can created multiple connected components which are then laid out
        independently. It can thus happen that those components will be laid out around component belonging to different
        compartment. Later, when we join all the components into a single compartment, there will be substantial overlap
        between the borders of the two compartments.
    :param g_cc:
    :param gs:
    :param cmp_ix:
    :return:
    """

    cmp_names = list(cmp_ix)
    for i in range(len(cmp_ix)):
        cmp_name1 = cmp_names[i]
        for j in range(i+1, len(cmp_ix)):
            cmp_name2 = cmp_names[j]
            #we are obtaining the bounding boxes here and not before the cycle because they change when the compartments overlaps are resolved
            bb1: gr.Rectangle = gr.utils.get_bounding_box([get_cmp_cc_node(g_cc, ix).bb for ix in cmp_ix[cmp_name1]])
            bb2: gr.Rectangle = gr.utils.get_bounding_box([get_cmp_cc_node(g_cc, ix).bb for ix in cmp_ix[cmp_name2]])
            if bb1.intersects_rectangle(bb2):
                resolve_compartments_overlap(g_cc, gs, cmp_ix[cmp_name1], cmp_ix[cmp_name2], bb1, bb2)


def get_cmp_cc_node(g: nx.MultiGraph, i: int) -> CompartmentCCNode:
    return g.nodes[i]['data']


def get_cmp_bbs(g_cc: nx.MultiGraph, cmp_ix: CompartmentGraphsIxs) -> Dict[str, gr.Rectangle]:
    bbs = {}
    for cmp_name in cmp_ix:
        bbs[cmp_name] = gr.utils.get_bounding_box([get_cmp_cc_node(g_cc, ix).bb for ix in cmp_ix[cmp_name]])
    return bbs




def clear_spaces(g_cc: nx.MultiGraph, gs: List[nx.MultiGraph], cmp_ix: CompartmentGraphsIxs):

    if len(cmp_ix) <= 1:
        return

    cmp_bb = get_cmp_bbs(g_cc, cmp_ix)

    coords = []
    for cmp_name, bb in cmp_bb.items():
        coords.append({'coords': bb.get_lt(), 'begin': True, 'cmp_name': cmp_name})
        coords.append({'coords': bb.get_rb(), 'begin': False, 'cmp_name': cmp_name})

    sort_x = sorted(coords, key=lambda x: x['coords'][0])
    sort_y = sorted(coords, key=lambda x: x['coords'][1])

    for sort_coords, coord_ix in [(sort_x, 0), (sort_y, 1)]:
        coord_i = sort_coords[1]
        for i in range(2, len(sort_coords)):
            # The first two coordinates are either one compartment or the beginning of two different compartments
            coord_i_1 = coord_i
            coord_i = sort_coords[i]
            if not coord_i_1['begin'] and coord_i['begin']:
                diff = coord_i['coords'][coord_ix] - coord_i_1['coords'][coord_ix]
                shift = -(diff - settings.render.compartment_distance - settings.render.compartment_padding)
                if shift < 0:
                    shift_vect = np.array([0,0])
                    shift_vect[coord_ix] = shift
                    for j in range(i, len(sort_coords)):
                        if sort_coords[j]['begin']:
                            for ix in cmp_ix[sort_coords[j]['cmp_name']]:
                                get_cmp_cc_node(g_cc, ix).shift(shift_vect)
                                lt.pan(gs[ix], shift_vect)



def optimize_layout(g_cc, gs):
    NotImplemented


def layout_ccs(g_cc: nx.MultiGraph, gs: List[nx.MultiGraph]):

    initial_layout(g_cc, gs)
    cmp_ix = get_compartment_dict(gs)
    handle_overlaps(g_cc, gs, cmp_ix)
    # optimize_layout(g_cc, gs)

def merge_ccs(g_cc: nx.MultiGraph, gs: List[nx.MultiGraph], g_tgt: nx.MultiGraph, nm:NodesMapping) -> nx.MultiGraph:
    """
    Creates a new graph where it puts all the nodes and edges from the connectec components and also adds
    edges between them. These are available in the target, however we will need the nodes mapping as they
    have already been mapped and thus have different names in the result connected components as they were in
    the original target graph.
    :param g_cc: Compartment graph where each node is a compartment.
    :param gs:
    :param g_tgt:
    :param nm:
    :return:
    """
    g_res = nx.MultiGraph()

    for g in gs:
        for n_id in g:
            gu.copy_node(g, g_res, n_id)
        for n1, n2 in g.edges():
            for role in gu.get_roles(g, n1, n2):
                g_res.add_edge(n1, n2, role)

    # Now we need to add the reactions which connect the components
    connecting_r_ids = []
    for ix, data in g_cc.nodes(data=True):
        cc_node: CompartmentCCNode = data['data'] # each node represents a compartment
        for r_id in cc_node.cmp_species:
            if r_id not in connecting_r_ids:
                connecting_r_ids.append(r_id)
                # The following solution should also handle situations where the reaction is already there
                gu.copy_node(g_tgt, g_res, r_id)
                # for cc_node_species in cc_node.cmp_species[r_id]:
                for s_id in g_tgt[r_id]:
                    s_id_res = nm.getSourceMapping(s_id)[0] #TODO: the s_id might have been copied and then maybe a different node should be chosen
                    roles = g_tgt[r_id][s_id]
                    for role in roles:
                        g_res.add_edge(r_id, s_id_res, role)

    # we need to update the references
    # for n_id in g_res:
    #     gu.get_node_data(n_id, g_res).set_graph(g_res)

    for r_id in sorted(connecting_r_ids):
        gu.get_predicted_layout(g_res, r_id).recompute_layout()

    return g_res
