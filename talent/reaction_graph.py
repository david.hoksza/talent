import copy
import networkx as nx
import logging

from . import graph_utils as gu


def extract_reaction_graph(g) -> nx.MultiGraph:

    # g_undirected = g.to_undirected()
    # rg = nx.MultiGraph(g_undirected.subgraph(gu.get_all_reaction_ids(g_undirected)))
    rg = nx.MultiGraph(g.subgraph(gu.get_all_reaction_ids(g)))

    nodes = list(rg.nodes())

    neighbors = {}
    for node in nodes:
        neighbors[node] = set(nx.all_neighbors(g, node))


    for ix1 in range(len(nodes)):
        node_id1 = nodes[ix1]
        for ix2 in range(ix1+1, len(nodes)):
            node_id2 = nodes[ix2]
            # if len(list(nx.common_neighbors(g_undirected, node_id1, node_id2))) > 0:
            if neighbors[node_id1].intersection(neighbors[node_id2]):
                rg.add_edge(node_id1, node_id2)

    return rg


class ReactionGraph():

    def __init__(self, g, name="", rg=None, rg_ddup=None, ddup=False):

        self.orig = g
        self.rg = extract_reaction_graph(g) if rg is None else rg

        self.ddup = ddup
        if ddup:
            self.rg_ddup = gu.get_ddup_reaction_graph(self.rg, self.orig) if rg_ddup is None else rg_ddup

        self.name = name

    def deepcopy(self) -> 'ReactionGraph':
        rg = copy.deepcopy(self)
        gu.fix_bioentity_graph_links(rg.orig)
        gu.fix_bioentity_graph_links(rg.rg)
        return rg

    def get_reaction_graph(self, ddup=None) -> nx.MultiGraph:
        if ddup is None:
            ddup = self.is_ddup()
        return self.rg_ddup if ddup else self.rg

    def is_ddup(self):
        return self.ddup

    def get_original(self) -> nx.MultiGraph:
        return self.orig

    def get_name(self):
        return self.name

    def copy(self):
        return ReactionGraph(self.orig.copy(),
                             name=self.name,
                             rg=self.rg.copy() if self.rg is not None else None,
                             rg_ddup=self.rg_ddup.copy() if self.rg_ddup is not None else None,
                             ddup=self.ddup)