from . import common
import logging
import json


class SettingsRender:
    species_glyph_width = 80
    species_glyph_height = 40

    compartment_padding = [80,40]
    compartment_distance = 10

    optimal_species_reaction_dist = 80
    optimal_neighboring_species_dist = [100, 60]

    modifiers_optimal_dist = 80

    routing_offset = 15 # offset from species ports through which the edges can pass. Plus it defines belt in which routing overlaps can be handled
    routing_lanes = 10 #number of lanes within the offset in which the overlapping edges can be positioned

    min_connection_point_offset = 20 # how the connection point in case of multiple reactants of products should be offsetted with respect to the reaction center

    overlapping_reactions_center_spacing = 20 # If two reactions overlap because they share main reactant and product, their centers need to be shifted


class SettingsBeautification:
    compact_threshold_dist = 160
    compact_optimal_dist = 300
    duplication_distance_relative = [0.5, 0.5]
    duplication_distance_absolute = [0 * 80, 0 * 40]
    duplication_node_degree = 4
    duplication_type_macromolecules = True
    duplication_type_simple_molecules = True
    duplication_optimal_modifier_distance = 100
    duplication_optimal_non_modifier_distance = 160
    deduplication_distance_relative = [0.6, 0.6] # maximal distance in which the species can be to be considered for duplicatoin
    deduplication_distance_absolute = [10 * 80, 10 * 80]
    deduplication_simple_molecules =  True
    deduplication_large_molecules = True
    deduplication_cnt =  1  # how many deduplication events per a duplication group should happen in one beutification run (the duplicates are merged in the order of their distance)
    deduplication_max_line_intersection_increase = 2
    inflate_reactant_product_distance = 0

ed = {
    'seed_pairs_count': 500 , 'max_open_set_category_sizes': [1000, 100, 50], 'max_children': 4
    , "max_consecutive_inserts": 1
    # , 'use_adjecency_matrix_scoring': True
    # , 'max_time_in_s': 600, 'must_find_solution': True
    , 'max_time_in_s': 20, 'must_find_solution': True
    , 'penalize_superfluous_edges': True
    , 'switch_if_first_bigger': True
    , 'log_to_console': False
}
ed_for_approximation = {
    'seed_pairs_count': 50 , 'max_open_set_category_sizes': [50, 3], 'max_children': 2
    , 'use_adjecency_matrix_scoring': True
    , "max_consecutive_inserts": 1
    # , 'max_time_in_s': 5, 'must_find_solution': True
    , 'max_time_in_s': 2, 'must_find_solution': True
    , 'penalize_superfluous_edges': True
    , 'switch_if_first_bigger': True
    , 'log_to_console': False
}

render = SettingsRender
beautification = SettingsBeautification
cache_dir = "cache"
cnt_reactions_to_do_duplication_in_beautify = 5
dist_ratio_to_duplicate_in_beautify = 1
cnt_approx_to_try = 1
min_species_padding_pct = 20 #percent of width and height which needs to be around each species glyph
minerva_instance = "http://localhost:8080/minerva" #to be used for exporting images from SBML to other formats
# minerva_instance = "https://minerva-dev.lcsb.uni.lu/minerva" #to be used for exporting images from SBML to other formats
orthogonal_routing = True
orthogonalize_inserted_reactions = True

arguments = None

def parse_settings_file(file_name):
    try:
        f = common.open_file(file_name)
        fs = json.load(f)

        render.species_glyph_width = fs["render"]["species_glyph_width"]
        render.species_glyph_height = fs["render"]["species_glyph_height"]
        render.compact_threshold_dist = fs["render"]["compact_threshold_dist"]
        render.compact_optimal_dist = fs["render"]["compact_optimal_dist"]
        render.modifiers_optimal_dist = fs["render"]["modifiers_optimal_dist"]
        render.routing_offset = fs["render"]["routing_offset"]
        render.routing_lanes = fs["render"]["routing_lanes"]
        render.min_connection_point_offset = fs["render"]["min_connection_point_offset"]
        render.overlapping_reactions_center_spacing = fs["render"]["overlapping_reactions_center_spacing"]

        beautification.deduplication_max_distance_relative = fs["beautification"]["deduplication_max_distance_relative"]
        beautification.deduplication_max_distance_absolute = fs["beautification"]["deduplication_max_distance_absolute"]
        beautification.deduplication_simple_molecules = fs["beautification"]["deduplication_simple_molecules"]
        beautification.deduplication_cnt = fs["beautification"]["deduplication_cnt"]
        beautification.deduplication_max_line_intersection_increase = fs["beautification"]["deduplication_max_line_intersection_increase"]

        # global cache_dir; cache_dir = fs["cache_dir"]
        global cnt_reactions_to_do_duplication_in_beautify; cnt_reactions_to_do_duplication_in_beautify = fs["cnt_reactions_to_do_duplication_in_beautify"]
        global dist_ratio_to_duplicate_in_beautify; dist_ratio_to_duplicate_in_beautify = fs["dist_ratio_to_duplicate_in_beautify"]
        global cnt_approx_to_try; cnt_approx_to_try = fs["cnt_approx_to_try"]
        global min_species_padding_pct; min_species_padding_pct = fs["min_species_padding_pct"]
        global minerva_instance; minerva_instance = fs["minerva_instance"]
        global orthogonal_routing; orthogonal_routing = fs["orthogonal_routing"]

    except IOError:
        logging.error("Cannot open {}. Exiting.".format(file_name))

