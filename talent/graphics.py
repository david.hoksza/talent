import numpy as np
import math
import sys

from typing import List, Tuple

# from sortedcontainers import SortedList

class Rectangle:

    def __init__(self, x1, y1, x2, y2):
        if x1 > x2 or y1 > y2:
            raise ValueError("Coordinates are invalid")
        self.x1, self.y1, self.x2, self.y2 = x1, y1, x2, y2

        self.__clean()

    def __hash__(self):
        return hash((self.x1, self.y1, self.x2, self.y2))

    @staticmethod
    def from_point(p: np.ndarray, width:float, height:float):
        return Rectangle(p[0] - width/2, p[1] - height/2, p[0] + width/2, p[1] + height/2)

    def get_rtree_coords(self) -> tuple:
        return (self.x1, self.y1, self.x2, self.y2)

    def __clean(self):
        self.__lt = None
        self.__rt = None
        self.__rb = None
        self.__lb = None

        self.__line_top = None
        self.__line_bottom = None
        self.__line_left = None
        self.__line_right = None

    def inflate(self, x: float, y: float):
        self.x1 -= x
        self.x2 += x
        self.y1 -= y
        self.y2 += y

        self.__clean()

    def shift(self, shift: np.ndarray or tuple or List):
        self.__clean()
        self.x1 += shift[0]
        self.x2 += shift[0]
        self.y1 += shift[1]
        self.y2 += shift[1]

    def intersects(self, obj, consider_border=True):
        if isinstance(obj, Rectangle):
            return self.intersects_rectangle(obj)
        elif isinstance(obj, np.ndarray):
            return self.intersects_point(obj)
        elif isinstance(obj, Line):
            return self.intersects_line(obj, consider_border)
        elif isinstance(obj, List):
            if isinstance(obj[0], Line):
                return self.intersects_lines(obj, consider_border)
            else:
                assert False
        else:
            assert False

    __and__ = intersects

    def get_center(self):
        return self.get_lt() + np.array([(self.get_rt()[0] - self.get_lt()[0]) / 2, (self.get_lb()[1] - self.get_lt()[1]) / 2])

    def distance_rect(self, r2: 'Rectangle', two_d:bool = False) -> float or np.ndarray:
        r1 = self
        if r1.get_x2() < r2.get_x1():
            x_diff = r2.get_x1() - r1.get_x2()
        elif r2.get_x2() < r1.get_x1():
            x_diff = r2.get_x2() - r1.get_x1()
        else:
            x_diff = 0
            
        if r1.get_y2() < r2.get_y1():
            y_diff = r2.get_y1() - r1.get_y2()
        elif r2.get_y2() < r1.get_y1():
            y_diff = r2.get_y2() - r1.get_y1()
        else:
            y_diff = 0

        return np.array([x_diff, y_diff]) if two_d else math.sqrt(x_diff*x_diff + y_diff*y_diff)

    def distance_point(self, p: np.ndarray, two_d:bool = False) -> float or np.ndarray:

        if self.get_x2() < p[0]:
            x_diff = p[0] - self.get_x2()
        elif p[0] < self.get_x1():
            x_diff = p[0] - self.get_x1()
        else:
            x_diff = 0

        if self.get_y2() < p[1]:
            y_diff = p[1] - self.get_y2()
        elif p[1] < self.get_y1():
            y_diff = p[1] - self.get_y1()
        else:
            y_diff = 0

        return np.array([x_diff, y_diff]) if two_d else math.sqrt(x_diff * x_diff + y_diff * y_diff)


    def inside(self, other: 'Rectangle') -> bool:
        return self.get_x1() > other.get_x1() and self.get_x2() < other.get_x2() and self.get_y1() > other.get_y1() and self.get_y2() < other.get_y2()


    # https://stackoverflow.com/questions/25068538/intersection-and-difference-of-two-rectangles
    def intersects_rectangle(self, other: 'Rectangle', padding: np.ndarray or List = [0,0]):
        padding_x = padding[0] / 2.0
        padding_y = padding[1] / 2.0
        a_x1 = self.x1 - padding_x
        a_x2 = self.x2 + padding_x
        a_y1 = self.y1 - padding_y
        a_y2 = self.y2 + padding_y
        b_x1 = other.x1 - padding_x
        b_x2 = other.x2 + padding_x
        b_y1 = other.y1 - padding_y
        b_y2 = other.y2 + padding_y
        x1 = max(min(a_x1, a_x2), min(b_x1, b_x2))
        y1 = max(min(a_y1, a_y2), min(b_y1, b_y2))
        x2 = min(max(a_x1, a_x2), max(b_x1, b_x2))
        y2 = min(max(a_y1, a_y2), max(b_y1, b_y2))
        if x1 < x2 and y1 < y2:
            return type(self)(x1, y1, x2, y2)

    def intersects_point(self, p: np.ndarray) -> bool:
        return (self.x1 <= p[0] <= self.x2) and (self.y1 <= p[1] <= self.y2)

    def intersects_line(self, l: 'Line', consider_border=True) -> bool:
        return l.intersects_rectangle(self, consider_border)

    def intersects_lines(self, lines: List['Line'], consider_border=True) -> bool:
        for l in lines:
            if l.intersects_rectangle(self, consider_border):
                return True
        return False

    def rect_distance(self, r: 'Rectangle'):
        left = r.get_x2() < self.get_x1()
        right = self.get_x2() < r.get_x1()
        bottom = r.get_y2() < self.get_y1()
        top = self.get_y2() < r.get_y1()
        if top and left:
            return utils.dist((self.get_x1(), self.get_y2()), (r.get_x2(), r.get_y1()))
        elif left and bottom:
            return utils.dist((self.get_x1(), self.get_y1()), (r.get_x2(), r.get_y2()))
        elif bottom and right:
            return utils.dist((self.get_x2(), self.get_y1()), (r.get_x1(), r.get_y2()))
        elif right and top:
            return utils.dist((self.get_x2(), self.get_y2()), (r.get_x1(), r.get_y1()))
        elif left:
            return self.get_x1() - r.get_x2()
        elif right:
            return r.get_x1() - self.get_x2()
        elif bottom:
            return self.get_y1() - r.get_y2()
        elif top:
            return r.get_y1() - self.get_y2()
        else:  # rectangles intersect
            return 0.


    def __iter__(self):
        yield self.x1
        yield self.y1
        yield self.x2
        yield self.y2


    def shift(self, vect: np.ndarray):
        self.__clean()
        self.x1 += vect[0]
        self.y1 += vect[1]
        self.x2 += vect[0]
        self.y2 += vect[1]

    def __eq__(self, other):
        return isinstance(other, Rectangle) and tuple(self)==tuple(other)
    def __ne__(self, other):
        return not (self==other)

    def __repr__(self):
        return type(self).__name__+repr(tuple(self))

    def height(self) -> float:
        return math.fabs(self.y1 - self.y2)

    def width(self) -> float:
        return math.fabs(self.x1 - self.x2)

    def get_x1(self) -> float:
        return self.x1
    
    def get_y1(self) -> float:
        return self.y1

    def get_x2(self) -> float:
        return self.x2

    def get_y2(self) -> float:
        return self.y2

    def get_lt(self) -> np.ndarray:
        if self.__lt is None:
            self.__lt = np.array([self.x1, self.y1])
        return self.__lt

    def get_rt(self) -> np.ndarray:
        if self.__rt is None:
            self.__rt = np.array([self.x2, self.y1])
        return self.__rt

    def get_rb(self) -> np.ndarray:
        if self.__rb is None:
            self.__rb = np.array([self.x2, self.y2])
        return self.__rb

    def get_lb(self) -> np.ndarray:
        if self.__lb is None:
            self.__lb = np.array([self.x1, self.y2])
        return self.__lb

    def get_line_top(self) -> 'Line':
        if self.__line_top is None:
            self.__line_top = Line(self.get_lt(), self.get_rt())
        return self.__line_top

    def get_line_right(self) -> 'Line':
        if self.__line_right is None:
            self.__line_right = Line(self.get_rt(), self.get_rb())
        return self.__line_right

    def get_line_bottom(self) -> 'Line':
        if self.__line_bottom is None:
            self.__line_bottom = Line(self.get_lb(), self.get_rb())
        return self.__line_bottom

    def get_line_left(self) -> 'Line':
        if self.__line_left is None:
            self.__line_left = Line(self.get_lt(), self.get_lb())
        return self.__line_left


class Line:

    def __init__(self, p1: np.ndarray or list or tuple, p2: np.ndarray or list or tuple, allow_zero_length = False, type_check = True):

        if type_check:
            if isinstance(p1, list) or isinstance(p1, tuple):
                p1 = np.array(p1)
            if isinstance(p2, list) or isinstance(p2, tuple):
                p2 = np.array(p2)
            if not isinstance(p1, np.ndarray) or not isinstance(p2, np.ndarray):
                raise TypeError("Arguments need to be of type List or np.array or tuple")

            if not allow_zero_length:
                assert p1[0] != p2[0] or p1[1] != p2[1]

        self._p = [p1, p2]

        self.__clear()

    def __clear(self):
        self.__slope = None
        self.__intercept = None
        self.__x_max = None
        self.__y_max = None
        self.__x_min = None
        self.__y_min = None
        self.__length = None

    def shift(self, vec: np.ndarray):

        self._p[0] = self._p[0] + vec
        self._p[1] = self._p[1] + vec
        self.__clear()


    def __eq__(self, other):
        return \
            (np.all(self[0] == other[0]) and np.all(self[1] == other[1])) or \
            (np.all(self[1] == other[0]) and np.all(self[0] == other[1]))

    def __repr__(self):
        return repr(self._p)

    def __getitem__(self, key) -> np.ndarray:
        return self._p[key]

    @property
    def x_max(self):
        if self.__x_max is None:            
            self.__x_max = max(self[0][0], self[1][0])
        return self.__x_max

    @property
    def y_max(self):
        if self.__y_max is None:
            self.__y_max = max(self[0][1], self[1][1])
        return self.__y_max

    @property
    def x_min(self):
        if self.__x_min is None:
            self.__x_min = min(self[0][0], self[1][0])
        return self.__x_min

    @property
    def y_min(self):
        if self.__y_min is None:
            self.__y_min = min(self[0][1], self[1][1])
        return self.__y_min

    def get_tuples(self) -> List[Tuple[float]]:
        return (tuple(self._p[0]), tuple(self._p[1]))

    def bb(self) -> Rectangle:
        return Rectangle(self.x_min, self.y_min, self.x_max, self.y_max)

    def reverse(self):
        self._p.reverse()

    def length(self) -> float:
        if self.__length is None:
            self.__length = utils.dist(self[0], self[1])
        return self.__length

    def intersects(self, obj: 'Line' or Rectangle) -> bool:
        if isinstance(obj, Line):
            return self.intersects_line(obj)
        elif isinstance(obj, Rectangle):
            return self.intersects_rectangle(obj)
        else:
            assert False

    def intersects_line(self, line) -> bool:
        # https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
        return intersect(self[0], self[1], line[0], line[1])

    def intersect_collinear(self, line) -> bool:

        if self.slope == sys.maxsize and line.slope == sys.maxsize:
            if self.x_min == line.x_min:
                if line.y_min <= self.y_min <= line.y_max or self.y_min <= line.y_min <= self.y_max:
                    return True
            return False
        elif self.slope == 0 and line.slope == 0:
            if self.y_min == line.y_min:
                if line.x_min <= self.x_min <= line.x_max or self.x_min <= line.x_min <= self.x_max:
                    return True
            return False

        if self.slope != line.slope or self.intercept != line.intercept:
            return False
        else:
            return True


        # if self.slope != line.slope or self.intercept != line.intercept:
        #     return False
        #
        # if self.slope == sys.maxsize:
        #     if line.y_min <= self.y_min <= line.y_max or self.y_min <= line.y_min <= self.y_max:
        #         return True
        #     else:
        #         return False
        # else:
        #     if line.x_min <= self.x_min <= line.x_max or self.x_min <= line.x_min <= self.x_max:
        #         return True
        #     else:
        #         return False




        # return np_seg_intersect(self, line) is not None

    def collinear(self, line) -> bool:
        return orientation(self[0], self[1], line[0]) == 0 and orientation(self[0], self[1], line[1])

    @property
    def slope(self) -> float:
        if self.__slope is None:
            x_diff = (self[1][0] - self[0][0])
            if x_diff == 0:
                self.__slope = sys.maxsize
            else:
                self.__slope = (self[1][1] - self[0][1]) / x_diff
        return self.__slope

    @property
    def intercept(self) -> float:
        if self.__intercept is None:
            self.__intercept = self[0][1] - self.slope * self[0][0]

        return self.__intercept

    def get_y_for_x(self, x: float) -> float:
        return self.slope * x + self.intercept

    def intersects_rectangle(self, r: Rectangle, consider_border=True) -> bool:

        x_min, y_min = r.get_lt()
        x_max, y_max = r.get_rb()


        if not consider_border:
            x_min += 1
            y_min += 1
            x_max -= 1
            y_max -= 1

        # return ((x_min < self._p[0][0] and x_max > self._p[0][0]) or (x_min < self._p[1][0] and x_max > self._p[1][0]) or (x_min > self._p[0][0] and x_max < self._p[1][0])) \
        #        and \
        #        ((y_min < self._p[0][1] and y_max > self._p[0][1]) or (y_min < self._p[1][1] and y_max > self._p[1][1]) or (y_min > self._p[0][1] and y_max < self._p[1][1]))

        if (self._p[0][0] < x_min and self._p[1][0] < x_min) or\
            (self._p[0][0] > x_max and self._p[1][0] > x_max) or \
            (self._p[0][1] < y_min and self._p[1][1] < y_min) or \
            (self._p[0][1] > y_max and self._p[1][1] > y_max):
            return False

        if self.slope == 0 or self.slope == sys.maxsize:
            #if the line is horizontal or vertical, the above checks are sufficient to confirm an intersection
            return True

        # Test if the y positions where the line would meet the rectangle are below or above the rectangle
        y1 = self.get_y_for_x(x_min)
        y2 = self.get_y_for_x(x_max)
        if (y1 < y_min and y2 < y_min) or (y1 > y_max and y2 > y_max):
            return False

        return True

        # return self.intersects_line(Line([rect.x1, rect.y1], [rect.x2, rect.y1])) or \
        #        self.intersects_line(Line([rect.x2, rect.y1], [rect.x2, rect.y2])) or \
        #        self.intersects_line(Line([rect.x2, rect.y2], [rect.x1, rect.y2])) or \
        #        self.intersects_line(Line([rect.x1, rect.y2], [rect.x1, rect.y1]))

    def vertical(self):
        return self[0][0] == self[1][0]

    def horizontal(self):
        return self[0][1] == self[1][1]

    def intersection(self, obj: 'Line' or 'Rectangle') -> np.ndarray:
        if isinstance(obj, Line):
            return self.intersection_line(obj)
        elif isinstance(obj, Rectangle):
            return self.intersection_rectangle(obj)

        assert False

    def intersection_rectangle(self, r: 'Rectangle') -> np.ndarray or None:

        isect = self.intersection_line(r.get_line_top())
        if isect is None:
            self.intersection_line(r.get_line_right())
        if isect is None:
            self.intersection_line(r.get_line_bottom())
        if isect is None:
            self.intersection_line(r.get_line_left())

        return isect


    def intersection_line(self, l: 'Line') -> np.ndarray or None:
        line1 = self
        line2 = l

        xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
        ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

        def det(a, b):
            return a[0] * b[1] - a[1] * b[0]

        div = det(xdiff, ydiff)
        if div == 0:
            return None
            # raise Exception('lines do not intersect')

        d = (det(*line1), det(*line2))
        x = det(d, xdiff) / div
        y = det(d, ydiff) / div

        return np.array([x, y])



def shift_fishey(x: np.ndarray, y: np.ndarray, size:float) -> np.ndarray:
    """
    Shifts point x in the direction of y by size.
    :param x:
    :param y:
    :param size:
    :return:
    """

    v = y - x
    angle = math.atan2(v[1], v[0])
    v_new = np.array([size*math.cos(angle), size*math.sin(angle)])

    return x + v_new


def shift_cartesian(x: np.ndarray, shift: np.ndarray) -> np.ndarray:
    return x + shift


# Given three colinear points p, q, r, the function checks if
# point q lies on line segment 'pr'
def on_segment(p: np.ndarray, q: np.ndarray, r: np.ndarray) -> bool:
    if q[0] <= max(p[0], r[0]) and q[0] >= min(p[0], r[0]) and q[1] <= max(p[1], r[1]) and q[1] >= min(p[1], r[1]):
        return True

    return False


# To find orientation of ordered triplet (p, q, r).
# The function returns following values
# 0 --> p, q and r are colinear
# 1 --> Clockwise
# 2 --> Counterclockwise
def orientation(p: np.ndarray, q: np.ndarray, r: np.ndarray)->int:
    # See https://www.geeksforgeeks.org/orientation-3-ordered-points/
    # for details of below formula.
    val = (q[1] - p[1]) * (r[0] - q[0]) - (q[0] - p[0]) * (r[1] - q[1])

    if val == 0:
        return 0 # colinear

    return 1 if val > 0 else 2 # clock or counterclockwise


def intersect(p1: np.ndarray, q1: np.ndarray, p2: np.ndarray, q2: np.ndarray)->bool:
    # Find the four orientations needed for general and special cases
    o1 = orientation(p1, q1, p2)
    o2 = orientation(p1, q1, q2)
    o3 = orientation(p2, q2, p1)
    o4 = orientation(p2, q2, q1)

    # General case
    if o1 != o2 and o3 != o4:
        return True

    # Special Cases
    # p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if o1 == 0 and on_segment(p1, p2, q1):
        return True

    # p1, q1 and q2 are colinear and q2 lies on segment p1q1
    if o2 == 0 and on_segment(p1, q2, q1):
        return True

    # p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if o3 == 0 and on_segment(p2, p1, q2):
        return True

    # p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if o4 == 0 and on_segment(p2, q1, q2):
        return True

    return False  # Doesn't fall in any of the above cases


def np_perp( a ) :
    b = np.empty_like(a)
    b[0] = a[1]
    b[1] = -a[0]
    return b

def np_cross_product(a, b):
    return np.dot(a, np_perp(b))


def np_seg_intersect(a, b, considerCollinearOverlapAsIntersect = True) -> np.ndarray or bool:
    # https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect/565282#565282
    # http://www.codeproject.com/Tips/862988/Find-the-intersection-point-of-two-line-segments
    r = a[1] - a[0]
    s = b[1] - b[0]
    v = b[0] - a[0]
    num = np_cross_product(v, r)
    denom = np_cross_product(r, s)
    # If r x s = 0 and (q - p) x r = 0, then the two lines are collinear.
    if np.isclose(denom, 0) and np.isclose(num, 0):
        # 1. If either  0 <= (q - p) * r <= r * r or 0 <= (p - q) * s <= * s
        # then the two lines are overlapping,
        if(considerCollinearOverlapAsIntersect):
            vDotR = np.dot(v, r)
            aDotS = np.dot(-v, s)
            if (0 <= vDotR  and vDotR <= np.dot(r,r)) or (0 <= aDotS  and aDotS <= np.dot(s,s)):
                return True
        # 2. If neither 0 <= (q - p) * r = r * r nor 0 <= (p - q) * s <= s * s
        # then the two lines are collinear but disjoint.
        # No need to implement this expression, as it follows from the expression above.
        return None
    if np.isclose(denom, 0) and not np.isclose(num, 0):
        # Parallel and non intersecting
        return None
    u = num / denom
    t = np_cross_product(v, s) / denom
    if u >= 0 and u <= 1 and t >= 0 and t <= 1:
        res = b[0] + (s*u)
        return res
    # Otherwise, the two line segments are not parallel but do not intersect.
    return None

# def intersection_point(l1: Line, l2:Line) -> np.ndarray:
#     A = l1[0]
#     B = l1[1]
#     C = l2[0]
#     D = l2[1]
#
#     a1 = B[1] - A[1]
#     b1 = A[0] - B[0]
#     c1 = a1 * A[0] + b1 * A[1]
#
#     a2 = D[1] - C[1]
#     b2 = C[0] - D[0]
#     c2 = a2 * C[0] + b2 * C[1]
#
#     determinant = a1 * b2 - a2 * b1
#
#     if determinant == 0:
#         return None
#
#     x = (b2 * c1 - b1 * c2) / determinant
#     y = (a1 * c2 - a2 * c1) / determinant
#     return np.array([x,y])

# def line_sweep(lines: List[Line], rects: List[Rectangle]) -> List[bool]:
#     """
#     :param lines1:
#     :param lines2:
#     :return: Pairs of indexes of intersecting lines in lines1 and lines2
#     """
#
#     res = [False for l in lines]
#
#     class LSLine:
#         def __init__(self, l:Line, ix):
#             self.l = l
#             self.ix = ix
#
#     ls_lines = [LSLine(l) for l in lines]
#
#     min_x_sorted = SortedList(ls_lines, key=lambda o:o.l[0][0])
#     max_x_sorted = SortedList(ls_lines, key=lambda o:o.l[1][0])
#
#     for r in rects:
#         left = min_x_sorted.bisect_left(r.get_x2())
#         right = max_x_sorted.bisect_right(r.get_x1())
#         left_ix = set([l.ix for l in left])
#         right_ix = set([l.ix for l in right])
#         candidates = left_ix.intersection(right_ix)
#         for c_ix in candidates:
#             if lines[c_ix].intersects_rectangle(r):
#                 res[c_ix] = True
#
#     return res
#
#     class LSPoint:
#         def __init__(self, p:np.ndarray, shape:Line or Rectangle, ix):
#             self.p = p
#             self.ix = ix
#             self.shape = shape
#
#         def _cmp_key(self):
#             return (self.p[0])
#
#         def __eq__(self, other:'LSPoint'):
#             return self._cmp_key() == other._cmp_key()
#
#         def __lt__(self, other):
#             return self._cmp_key() < other._cmp_key()
#
#     aux_points: List[LSPoint] = []
#     for j in [0, 1]:
#         lines = lines1 if j ==0 else lines2
#         for i in range(len(lines1)):
#             aux_points.append(LSPoint(lines[i][0], j, i, True))
#             aux_points.append(LSPoint(lines[i][1], j, i, False))
#     points: SortedList[LSPoint] = SortedList(aux_points)
#
#     active = SortedList()
#     for i in range(len(points)):
#         p = points[i]
#
#         if p.is_first:
#             active.append(p)
#         else:
#             active.remove(LSPoint(p.p, p.ix_list, p.ix_line, True))


class utils:

    @staticmethod
    def size(p: np.ndarray or tuple) -> float:
        return math.sqrt(p[0]*p[0] + p[1]*p[1])

    @staticmethod
    def get_non_zero_component(p: np.ndarray) -> float:
        assert p[0] != 0 or p[1] != 0
        return p[0] if p[0] != 0 else p[1]



    @staticmethod
    def normalize(p:np.ndarray) -> np.ndarray:
        if p[0] == 0 and p[1] == 0:
            return np.array(p)
        else:
            return p / utils.size(p)

    @staticmethod
    def dot(p1: np.ndarray, p2: np.ndarray):
        return np.dot(p1, p2)

    @staticmethod
    def dist(p1: np.ndarray or tuple, p2: np.ndarray or tuple) -> float:
        return utils.size(p2 - p1)

    @staticmethod
    def orthogonal(p: np.ndarray):
        return np.array([p[1], -p[0]])

    @staticmethod
    def closest(p: np.ndarray, ps: List[np.ndarray]) -> np.ndarray:
        assert len(ps) > 0
        dist_min = utils.dist(p, ps[0])
        i_min = 0
        for i in range(1, len(ps)):
            dist = utils.dist(p, ps[i])
            if dist < dist_min:
                dist_min = dist
                i_min = i

        return ps[i_min]

    @staticmethod
    def equal(p1: np.ndarray or List, p2: np.ndarray or List):
        return p1[0] == p2[0] and p1[1] == p2[1]

    @staticmethod
    def get_cosine(u: np.ndarray, v: np.ndarray):
        return np.dot(u, v) / utils.size(u) / utils.size(v)

    @staticmethod
    def get_angle(u: np.ndarray, v: np.ndarray) -> float:
        """
        :param u:
        :param v:
        :return: Angle in radians between u and v
        """
        cosine = utils.get_cosine(u, v)
        return np.arccos(np.clip(cosine, -1, 1))

    @staticmethod
    def rotate(origin: np.ndarray, point: np.ndarray, angle: float) -> np.ndarray:
        """
        Rotate a point counterclockwise by a given angle around a given origin.

        The angle should be given in radians.

        source: https://stackoverflow.com/questions/34372480/rotate-point-about-another-point-in-degrees-python
        """
        ox, oy = origin[0], origin[1]
        px, py = point[0], point[1]

        qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
        qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
        return np.array([qx, qy])

    @staticmethod
    def is_more_horizontal(v: np.ndarray) -> bool:
        v_norm = utils.normalize(v)
        return abs(v_norm[0]) > abs(v_norm[1])

    @staticmethod
    def get_bounding_box(objs: np.ndarray or 'Rectangle') -> 'Rectangle':
        min_x: float = sys.maxsize
        min_y: float = sys.maxsize
        max_x: float = -sys.maxsize
        max_y: float = -sys.maxsize

        if isinstance(objs[0], np.ndarray):
            for pos in objs:
                assert isinstance(pos, np.ndarray)
                if pos[0] < min_x:
                    min_x = pos[0]
                if pos[1] < min_y:
                    min_y = pos[1]
                if pos[0] > max_x:
                    max_x = pos[0]
                if pos[1] > max_y:
                    max_y = pos[1]

        if isinstance(objs[0], Rectangle):
            for rect in objs:
                assert isinstance(rect, Rectangle)
                lt = rect.get_lt()
                rb = rect.get_rb()
                if lt[0] < min_x:
                    min_x = lt[0]
                if lt[1] < min_y:
                    min_y = lt[1]
                if rb[0] > max_x:
                    max_x = rb[0]
                if rb[1] > max_y:
                    max_y = rb[1]



        return Rectangle(min_x, min_y, max_x, max_y)










