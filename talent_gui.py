import sys
import pickle
import copy
import networkx as nx
from functools import partial
import os
from enum import Enum
import re


from PySide2.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QAction, QFileDialog, \
    QGridLayout, QLabel, QLineEdit, QVBoxLayout, QHBoxLayout, QScrollArea, QGroupBox, QComboBox, \
    QFormLayout, QTabWidget
from PySide2.QtCore import Qt, QPoint, QSize, QSettings
import PySide2.QtGui as QtGui
import PySide2.QtSvg as QtSvg

import talent.utils.convert as convert
import talent.utils.export as export
import talent.utils.transfer as transfer
import talent.beautification as bt
import talent.settings as settings
import talent.common as common
import talent.graph_utils as gu

class FORMAT(Enum):
    CellDesigner = 0
    SBML = 1
    SBGN = 2



class App(QMainWindow):

    def __init__(self):

        super().__init__()
        self.title = 'TALENT'
        self.setMinimumWidth(600)

        self.settings = QSettings("gui_config.ini", QSettings.IniFormat)

        self.iw: InputWidget = None
        self.ow: OperationWidget = None
        self.tw: QTabWidget = None

        self.initUI()



    def initUI(self):

        self.setWindowTitle(self.title)
        # self.setGeometry(self.left, self.top, self.width)

        self.iw = InputWidget(self)
        self.ow = OperationWidget()

        self.tb = QTabWidget()

        self.tb.addTab(self.iw, "Transfer")
        self.setCentralWidget(self.tb)


        self.tb.addTab(self.iw, "Transfer")
        self.replace_operation_widget()

        self.setCentralWidget(self.tb)

        self.statusBar()

        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('&File')

        openFileButton = QAction('Open beautification chain', self)
        openFileButton.setShortcut('Ctrl+O')
        openFileButton.setStatusTip('Open beautification chain file')
        openFileButton.triggered.connect(self.open_file)

        self.saveFileButton = QAction('Save beautification chain file', self)
        self.saveFileButton.setShortcut('Ctrl+S')
        self.saveFileButton.setStatusTip('Save full beutification chain as')
        self.saveFileButton.triggered.connect(self.save_file)
        self.saveFileButton.setEnabled(False)

        self.saveAsFileButton = QAction('Save beautification chain file as', self)
        self.saveAsFileButton.setStatusTip('Save the full beutification chain as')
        self.saveAsFileButton.triggered.connect(self.save_file_as)
        self.saveAsFileButton.setEnabled(False)

        exitButton = QAction(QtGui.QIcon('exit24.png'), 'Exit', self)
        exitButton.setShortcut('Ctrl+Q')
        exitButton.setStatusTip('Exit application')
        exitButton.triggered.connect(self.close)

        fileMenu.addAction(openFileButton)
        fileMenu.addAction(self.saveFileButton)
        fileMenu.addAction(self.saveAsFileButton)
        fileMenu.addAction(exitButton)

        self.show()

    def open_file(self):
        opened = self.load_bc_from_file(open_file(self, settings_key="input/opnBcFile"))
        if opened:
            self.saveFileButton.setEnabled(True)
            self.saveAsFileButton.setEnabled(True)

    def save_file(self, name=None):
        if name is None:
            name = self.ow.f_name
        self.ow.bc.save(fname=name)

    def save_file_as(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self, "Open file", self.ow.f_name,
                                                  "Python Files (*.pkl);;All Files (*)", options=options)
        self.save_file(fileName)

    def replace_operation_widget(self):

        count = self.tb.count()

        if count > 2:
            raise Exception('Unexpected number of tabs when replacing operation widget')

        if count == 2:
            self.tb.removeTab(1)

        self.tb.addTab(self.ow, "Beautify")

        if count == 2:
            self.tb.setCurrentIndex(1)

    def load_bc_from_file(self, f_name):

        if not f_name:
            return False

        ex.setStatusTip("Opening the file...")
        bc = pickle.load(open(f_name, "rb"))
        self.ow = OperationWidget(bc, f_name)
        self.replace_operation_widget()


        # ex.setCentralWidget(self.ow)
        # self.parent().setCentralWidget(ow)
        # self.parent().setWindowState(self.windowState() | Qt.WindowMaximized)
        ex.setStatusTip("")
        return True


class InputWidget(QGroupBox):
    def __init__(self, main_app:App):
        super().__init__('Transfer and layout')

        self.setStyleSheet("QGroupBox{border: 1px solid gray; border-radius: 3px; margin-top: 1.5em; margin-right: 1.5em; margin-left: 1.5em;}"
                           "QGroupBox::title {subcontrol-origin: margin; margin-top: 1em; left: 2em; padding: 0 3px 0 3px;}")

        layout = QGridLayout()
        # layout.setContentsMargins(50,30,50,30)
        layout.setHorizontalSpacing(10)

        lblTarget = QLabel("Target file:")
        self.edtTargetFileName = QLineEdit("")
        self.cbTgtType = QComboBox()
        self.cbTgtType.addItems([e.name for e in FORMAT])
        self.cbTgtType.setObjectName('cbTgtType')
        self.cbTgtType.setCurrentIndex(int(main_app.settings.value("input/"+self.cbTgtType.objectName(), FORMAT.CellDesigner.value)))
        opentTgtFileButton = QPushButton("...")
        opentTgtFileButton.setFixedWidth(25)
        self.edtTargetFileName.setText(main_app.settings.value("input/openTgtFileDir", ""))
        self.edtTargetFileName.editingFinished.connect(lambda: main_app.settings.setValue("input/openTgtFileDir", self.edtTargetFileName.text()))
        opentTgtFileButton.clicked.connect(partial(open_file, self, self.edtTargetFileName,
                                                   settings_key="input/openTgtFileDir",
                                                   extensions="SBML Files (*.xml);;SBML Files (*.sbml);;All Files (*)",
                                                   cbToUpdate=self.cbTgtType) )


        lblTemplate = QLabel("Template file:")
        self.edtTemplateFileName = QLineEdit("")
        self.cbTmpType = QComboBox()
        self.cbTmpType.addItems([e.name for e in FORMAT])
        self.cbTmpType.setObjectName('cbTmpType')
        self.cbTmpType.setCurrentIndex(int(main_app.settings.value("input/" + self.cbTmpType.objectName(), FORMAT.CellDesigner.value)))
        openTmpFileButton = QPushButton("...")
        openTmpFileButton.setFixedWidth(25)
        self.edtTemplateFileName.setText(main_app.settings.value("input/openTmpFileDir", ""))
        self.edtTemplateFileName.editingFinished.connect(lambda: main_app.settings.setValue("input/openTmpFileDir", self.edtTemplateFileName.text()))
        openTmpFileButton.clicked.connect(partial(open_file, self, self.edtTemplateFileName,
                                                  settings_key="input/openTmpFileDir",
                                                  extensions="SBML Files (*.xml);;SBML Files (*.sbml);;All Files (*)",
                                                  cbToUpdate=self.cbTmpType))



        lblOutputPath = QLabel("Output path:")
        self.edtOutputPath = QLineEdit("")
        self.edtOutputPath.setText(main_app.settings.value("input/openOutputPath", ""))
        openOutputPathButton = QPushButton("...")
        openOutputPathButton.setFixedWidth(25)
        openOutputPathButton.clicked.connect(partial(open_file, self, self.edtOutputPath,
                                                     settings_key="input/openOutputPath",
                                                     is_dir=True))

        # lblLayout = QLabel("Layout file:")
        # self.edtLayoutFileName = QLineEdit("")
        # openLayoutFileButton = QPushButton("...")
        # openLayoutFileButton.setFixedWidth(25)
        # openLayoutFileButton.clicked.connect(lambda: self.open_file(self.edtLayoutFileName))

        # hLine = QFrame()
        # hLine.setFrameShape(QFrame.HLine)
        # hLine.setFrameShadow(QFrame.Sunken)

        runButton = QPushButton("Transfer and layout")
        runButton.setStyleSheet("background-color: green; opacity: 70;")
        runButton.clicked.connect(self.transfer)

        layout.addWidget(lblTarget, 1, 1)
        layout.addWidget(self.edtTargetFileName, 1, 2)
        layout.addWidget(opentTgtFileButton, 1, 3)
        layout.addWidget(self.cbTgtType, 1, 4)
        layout.addWidget(lblTemplate, 2, 1)
        layout.addWidget(self.edtTemplateFileName, 2, 2)
        layout.addWidget(openTmpFileButton, 2, 3)
        layout.addWidget(self.cbTmpType, 2, 4)
        layout.addWidget(lblOutputPath, 3, 1)
        layout.addWidget(self.edtOutputPath, 3, 2)
        layout.addWidget(openOutputPathButton, 3, 3)

        # layout.addWidget(hLine, 3, 1, 1, 3)
        # layout.addWidget(QLabel("OR:"), 3, 2, alignment=Qt.AlignCenter)
        #
        # layout.addWidget(lblLayout, 4, 1)
        # layout.addWidget(self.edtLayoutFileName, 4, 2)
        # layout.addWidget(openLayoutFileButton, 4, 3)

        layout.addWidget(runButton, 5, 1, 1, 4)

        self.setLayout(layout)


    def transfer(self):

        tgt_path = self.edtTargetFileName.text()
        tmp_path = self.edtTemplateFileName.text()

        tgt_dir, tgt_file = os.path.split(os.path.abspath(tgt_path))
        tmp_dir, tmp_file = os.path.split(os.path.abspath(tmp_path))

        tgt_name, _ = os.path.splitext(tgt_file)
        tmp_name, _ = os.path.splitext(tmp_file)

        tgt_type = self.cbTgtType.currentText()
        tmp_type = self.cbTmpType.currentText()

        output_path = self.edtOutputPath.text() + "/"

        ex.setStatusTip("Transferring and laying out...")
        transfer.transfer(tgt_name="{}".format(tgt_name), tgt_fname=tgt_path, tgt_fmt=tgt_type,
                              tmps_path=tmp_path, tmp_fmt=tmp_type,
                              output_path=output_path,
                                separate_cmprtmnt=True,
                              split_by_cmprtmnt=False,
                              ddup_tgt=False, ddup_tmp=False,
                                cache_path=None, tmp_cc_before_ddup=False)
        ex.setStatusTip("")


def open_file(parent: QWidget, editBox:'QLineEdit'=None,
              settings_key=None,
              is_dir=False,
              extensions="Python Files (*.pkl);;All Files (*)",
              cbToUpdate:QComboBox=None):

    start_dir = ex.settings.value(settings_key, "") if settings_key else ""
    if os.path.isfile(start_dir):
        start_dir = os.path.split(os.path.abspath(start_dir))[0]
    options = QFileDialog.Options()
    options |= QFileDialog.DontUseNativeDialog
    if is_dir:
        options |= QFileDialog.ShowDirsOnly
        name = QFileDialog.getExistingDirectory(parent, "Open file", start_dir, options=options)
    else:
        name, _ = QFileDialog.getOpenFileName(parent, "Open file", start_dir, extensions, options=options)

    if name:

        if cbToUpdate is not None:
            with common.open_file(name) as f:
                text = f.read()
                key = ("input/{}".format(cbToUpdate.objectName()))
                if '<celldesigner:extension>' in text:
                    cbToUpdate.setCurrentIndex(FORMAT.CellDesigner.value)
                    ex.settings.setValue(key, FORMAT.CellDesigner.value)
                elif '<sbgn' in text:
                    cbToUpdate.setCurrentIndex(FORMAT.SBGN.value)
                    ex.settings.setValue(key, FORMAT.SBGN.value)
                else:
                    cbToUpdate.setCurrentIndex(FORMAT.SBML.value)
                    ex.settings.setValue(key, FORMAT.SBML.value)

        if editBox is not None:
            editBox.setText(name)

    if name and settings_key is not None:
        ex.settings.setValue(settings_key, name)
        # if is_dir:
        #     ex.settings.setValue(settings_key, name)
        # else:
        #     path, _ = os.path.split(os.path.abspath(name))
        #     ex.settings.setValue(settings_key, path)

    return name




def get_svg_from_op(op: bt.BeautificationOp) -> str:
    return get_svg_from_g(op.g)


def get_svg_from_g(g: nx.MultiGraph) -> str:
    sbml = gu.graph_to_sbml(g)
    return convert.sbml_convert(sbml, format='svg', minerva_instance=settings.minerva_instance)


def get_svg_from_chain(bc: bt.BeautificationChain, ix) -> str:
    return get_svg_from_op(bc[ix])



class OperationWidget(QWidget):

    def __init__(self, bc: bt.BeautificationChain=None, f_name=""):
        super().__init__()

        if bc is None:
            return

        self.f_name = f_name
        self.bc: bt.BeautificationChain = bc
        self.ix_op = len(bc) - 1
        self.main_image_view: MainImageView = None

        self.layout = QGridLayout()

        self.bc_view: BcView = BcView(self)
        self.main_image_view = MainImageView(self.bc[self.ix_op].g) if self.ix_op >=0 else MainImageView(self.bc.g)
        self.new_op_view = self.create_new_op_view()

        column = QWidget()
        self.left_layout = QVBoxLayout()
        self.left_layout.addWidget(self.main_image_view)
        self.left_layout.addWidget(self.get_export_view())
        column.setLayout(self.left_layout)
        self.layout.addWidget(column, 1, 1)
        column = QWidget()
        self.right_layout = QVBoxLayout()
        self.right_layout.addWidget(self.bc_view)
        self.right_layout.addWidget(self.new_op_view)
        column.setLayout(self.right_layout)
        self.layout.addWidget(column, 1, 2)

        # self.layout.addWidget(self.main_image_view, 1, 1)
        # self.layout.addWidget(self.bc_view, 1, 2)
        # self.layout.addWidget(self.get_export_view(), 2, 1)
        # self.layout.addWidget(NewOpView("Add operation: ", self), 2, 2)

        self.layout.setColumnStretch(1, 3)
        self.layout.setColumnStretch(2, 1)

        # self.right_layout.setStretch(1, 8)
        # self.right_layout.setStretch(2, 1)

        # self.reinitalize_bc()
        self.setLayout(self.layout)

    def create_new_op_view(self, operation_index: int=0) -> 'NewOpView':
        return NewOpView("Add operation: ", self, operation_index)

    def reinitalize_bc(self):

        # self.layout.removeWidget(self.bc_view)
        self.bc_view = BcView(self)
        self.new_op_view = self.create_new_op_view(self.new_op_view.get_current_operation_index())
        clearLayout(self.right_layout)
        self.right_layout.addWidget(self.bc_view)
        self.right_layout.addWidget(self.new_op_view)
        self.bc_view.verticalScrollBar().setValue(10000000)
        self.op_ix_change(self.ix_op)
        # self.right_layout.setStretch(1, 4)
        # self.right_layout.setStretch(2, 1)

    def op_ix_change(self, ix: int):

        if ix < -1 or ix >= len(self.bc):
            return

        self.bc_view.reset_op(self.ix_op)
        self.ix_op = ix
        self.bc_view.highlight_op(self.ix_op)

        svg_content = self.bc_view.get_svg(self.ix_op)
        self.main_image_view.update_svg(svg_content)


    # def get_main_view(self):
    #     sbml = export.graph_to_sbml(self.bc[self.ix_op].g)
    #     svg_content = export.sbml_convert(sbml, format='svg', minerva_instance=settings.minerva_instance)
    #
    #     svg_widget = QtSvg.QSvgWidget()
    #     svg_widget.load(str.encode(svg_content))
    #     sa = QScrollArea()
    #     sa.setWidget(svg_widget)
    #     return sa


    def get_export_view(self):
        gb = QGroupBox("Export")
        layout = QHBoxLayout()
        for fmt in ["SBML", "CellDesigner", "SBGN", "SVG", "PDF", "PNG"]:
            button = QPushButton(fmt)
            fmt_aux = copy.copy(fmt)
            button.clicked.connect(partial(self.export_to_file, fmt_aux))
            layout.addWidget(button)
        gb.setLayout(layout)
        return gb


    def export_to_file(self, fmt: str):
        options = QFileDialog.Options()
        # options |= QFileDialog.DontUseNativeDialog
        options |= QFileDialog.DontConfirmOverwrite
        file_typ_spec = ""
        if fmt == "SBML" or fmt == "CellDesigner" or fmt == "SBGN":
            file_typ_spec = "SBML, CellDesigner SBML, SBGN (*.xml)"
        elif fmt == "SVG":
            file_typ_spec = "SVG (*.svg)"
        elif fmt == "PDF":
            file_typ_spec = "PDF (*.pdf)"
        elif fmt == "PNG":
            file_typ_spec = "PNG (*.png)"

        f_name, file_type = QFileDialog.getSaveFileName(self, "Save file", "",
                                                  "{};;All Files (*)".format(file_typ_spec), options=options)
        if f_name:
            # ext = file_type.split('.')[1].replace(')', '')
            # convert.export_step(bc = self.bc, output_path="{}.{}".format(f_name, ext), format=fmt,
            #                        minerva_instance=settings.minerva_instance, step=self.ix_op)
            original_graph = self.ix_op == -1
            export.export_step(bc=self.bc, output_path=f_name, format=fmt,
                                minerva_instance=settings.minerva_instance, step=self.ix_op, original_graph=original_graph)

    def beautify(self, op: bt.BeautificationOp):
        if self.ix_op < len(self.bc) - 1:
            # we are adding afte the operation which is currently displayed
            self.bc.add(op, self.ix_op + 1)
        else:
            self.bc.add(op)
        self.bc.run(len(self.bc) - 1)
        self.ix_op += 1
        self.reinitalize_bc()




class MainImageView(QScrollArea):
    def __init__(self, g: nx.MultiGraph):

        super().__init__()

        self.mousePressPos = QPoint()
        self.scrollBarValuesOnMousePress = QPoint()

        sbml = gu.graph_to_sbml(g)
        svg_content = convert.sbml_convert(sbml, format='svg', minerva_instance=settings.minerva_instance)

        self.svg_widget = QtSvg.QSvgWidget()
        self.svg_widget.load(str.encode(svg_content))

        self.setWidget(self.svg_widget)

    def update_svg(self, svg_content):
        self.svg_widget.load(str.encode(svg_content))

    def mousePressEvent(self, event):
        self.mousePressPos = QPoint(event.pos())
        self.scrollBarValuesOnMousePress.setX(self.horizontalScrollBar().value())
        self.scrollBarValuesOnMousePress.setY(self.verticalScrollBar().value())
        event.accept()

    def mouseMoveEvent(self, event):
        if self.mousePressPos.isNull():
            event.ignore()
            return

        self.horizontalScrollBar().setValue(
            self.scrollBarValuesOnMousePress.x() - event.pos().x() + self.mousePressPos.x())
        self.verticalScrollBar().setValue(
            self.scrollBarValuesOnMousePress.y() - event.pos().y() + self.mousePressPos.y())
        self.horizontalScrollBar().update()
        self.verticalScrollBar().update()
        event.accept()

    def mouseReleaseEvent(self, event):
        self.mousePressPos = QPoint()
        event.accept()

    def wheelEvent(self, event):
        width = self.svg_widget.width()
        height = self.svg_widget.height()

        new_width = width + event.delta()
        new_height =  height + event.delta() * height / width

        if new_height > 0 and new_height > 0:
            self.svg_widget.resize(QSize(new_width, new_height))



class BcView(QScrollArea):

    def __init__(self, ow: OperationWidget):

        super().__init__()

        # self.setFixedHeight(500)

        self.ow = ow
        self.layout = QVBoxLayout(self)

        self.layout.addWidget(OpView(self.ow, -1))
        for i in range(len(self.ow.bc)):
            self.layout.addWidget(OpView(self.ow, i))

        widget = QWidget()
        widget.setLayout(self.layout)


        self.setWidget(widget)
        self.setWidgetResizable(True)

        self.keyPressEvent = self.key_pressed

    def reset_op(self, ix_op):
        self.layout.itemAt(ix_op + 1).widget().reset_style()

    def highlight_op(self, ix_op):
        self.layout.itemAt(ix_op + 1).widget().highlight()

    def get_svg(self, ix_op):
        return self.layout.itemAt(ix_op + 1).widget().get_svg_content()

    def key_pressed(self, event):
        if event.key() == Qt.Key_Up:
            self.ow.op_ix_change(self.ow.ix_op - 1)
        if event.key() == Qt.Key_Down:
            self.ow.op_ix_change(self.ow.ix_op + 1)


class OpView(QGroupBox):
    def __init__(self, ow:OperationWidget, ix_op: int):

        self.ow = ow
        self.ix_op = ix_op

        if self.ix_op >= 0:
            op = self.ow.bc[self.ix_op]
            super().__init__("Operation {}: {}".format(self.ix_op + 1, op.type.name))
        else:
            super().__init__("Initial layout")

        self.reset_style()

        layout = QVBoxLayout()
        svg_widget = QtSvg.QSvgWidget()

        if ix_op >= 0:
            self.svg_content = get_svg_from_op(op)
        else:
            self.svg_content = get_svg_from_g(self.ow.bc.g)

        svg_widget.load(str.encode(self.svg_content))
        svg_widget.setMinimumHeight(300)
        layout.addWidget(svg_widget)

        if ix_op >= 0:

            params_widget = QGroupBox("Parameters:")
            params_layout = QFormLayout()
            fill_op_params(op.params, params_layout)
            params_widget.setLayout(params_layout)
            layout.addWidget(params_widget)

        self.setLayout(layout)

        self.mouseReleaseEvent = self.op_clicked



    def op_clicked(self, event):
        self.ow.op_ix_change(self.ix_op)



    def reset_style(self):
        self.setStyleSheet("QGroupBox{border: 1px solid gray; border-radius: 3px; margin-top: 0.5em;}"
                           "QGroupBox::title {subcontrol-origin: margin; left: 10px; padding: 0 3px 0 3px;}")

    def highlight(self):
        self.setStyleSheet("QGroupBox{border: 3px solid red; border-radius: 3px; margin-top: 0.5em;}"
                           "QGroupBox::title {subcontrol-origin: margin; left: 10px; padding: 0 3px 0 3px;}")

    def get_svg_content(self):
        return self.svg_content



class NewOpView(QGroupBox):

    def __init__(self, label: str, ow: OperationWidget, operation_index = 0):

        super().__init__(label)

        # self.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)

        self.ow = ow

        # self.setStyleSheet("background-color: rgba(255, 0, 0, 50)")

        layout = QVBoxLayout()

        self.cb = QComboBox()
        self.cb.addItems([e.name for e in bt.BEAUTY_OPERATION_TYPE])
        self.cb.currentIndexChanged.connect(self.selection_change)

        self.params_widget = QGroupBox("Parameters:")
        self.params_widget.setLayout(QFormLayout())

        btn_beautify = QPushButton("Beautify")
        btn_beautify.setStyleSheet("background-color: green; opacity: 50")
        btn_beautify.clicked.connect(self.beautify)

        layout.addWidget(self.cb)
        layout.addWidget(self.params_widget)
        layout.addWidget(btn_beautify)

        self.setLayout(layout)
        self.cb.setCurrentIndex(operation_index)
        self.selection_change(operation_index)

    def get_current_operation_index(self) -> int:
        return self.cb.currentIndex()

    def selection_change(self, i):

        layout = self.params_widget.layout()
        clearLayout(layout)
        params = bt.BEAUTY_OPERATION_TYPE(i).get_params()
        fill_op_params(params, layout)

    def collect_params(self):
        params_dict = {}
        layout = self.params_widget.layout()
        i = 0
        while i < layout.count():
            label = layout.itemAt(i).widget().text().strip(" :")
            i += 1
            field = layout.itemAt(i).widget()
            i += 1

            if isinstance(field, QLineEdit):
                params_dict[label] = field.text().strip()
            else:
                param_layout = field.layout()
                param_list = []
                for j in range(param_layout.count()):
                    w = param_layout.itemAt(j).widget()
                    assert isinstance(w, QLineEdit)
                    param_list.append(w.text().strip())
                params_dict[label] = param_list

        return params_dict

    def convert_params(self, source: dict, target: bt.BeautificationOp):

        raise_message = "Problem with converting source param list to the target list - types do not match"
        for key in target.__dict__:
            assert key in source
            if isinstance(target.__dict__[key], str):
                target.__dict__[key] = str(source[key])
            elif isinstance(target.__dict__[key], bool):
                # THIS test needs to be before int because isinstance(True, int) == True !!!!
                target.__dict__[key] = bool(source[key])
            elif isinstance(target.__dict__[key], int):
                target.__dict__[key] = int(source[key])
            elif isinstance(target.__dict__[key], float):
                target.__dict__[key] = float(source[key])
            else:
                if isinstance(target.__dict__[key], list):
                    assert isinstance(source[key], list)
                    assert len(source[key]) == len(target.__dict__[key])
                    for i in range(len(target.__dict__[key])):
                        if isinstance(target.__dict__[key][i], str):
                            target.__dict__[key][i] = str(source[key][i])
                        elif isinstance(target.__dict__[key][i], int):
                            target.__dict__[key][i] = int(source[key][i])
                        elif isinstance(target.__dict__[key][i], float):
                            target.__dict__[key][i] = float(source[key][i])
                        else:
                            raise Exception(raise_message)
                else:
                    raise Exception(raise_message)


    def beautify(self):
        op_type = bt.BEAUTY_OPERATION_TYPE(self.cb.currentIndex())
        op_params = op_type.get_params()
        self.convert_params(self.collect_params(), op_params)
        op: bt.BeautificationOp = bt.BeautificationOp(op_type, op_params)
        ex.setStatusTip("Running beautification...")
        self.ow.beautify(op)
        ex.setStatusTip("")


def fill_op_params(params, layout: QFormLayout):
    if params is None:
        return
    for key in params.__dict__:
        val = params.__dict__[key]
        if isinstance(val, int) or isinstance(val, float) or isinstance(val, str):
            val_widget = QLineEdit(str(val))
        elif isinstance(val, list):
            param_layout = QVBoxLayout()
            for v in val:
                param_layout.addWidget(QLineEdit(str(v)))
            val_widget = QWidget()
            val_widget.setLayout(param_layout)
        else:
            raise Exception("Unknown parameter type ({})".format(val))

        layout.addRow(QLabel("{}: ".format(key)), val_widget)


def clearLayout(layout):
    if layout is not None:
        while layout.count():
            item = layout.takeAt(0)
            widget = item.widget()
            if widget is not None:
                widget.deleteLater()
            else:
                clearLayout(item.layout())

if __name__ == '__main__':

    common.init_logging()

    sys_argv = sys.argv
    sys_argv += ['--style', 'fusion']
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())