# Parkinson's disease map 
 
- ``pdmap.xml`` is the full Parkinson's Disease Map in the SBML v3 format as
 exported from the pdmap.uni.lu (see the comments in the SBML for the date)
- The full map was processed with the ``utils/converter.py`` script (split into
  compartments, create internal graph representation used in TALENT including
  the conversion into the reaction graph) and results stored in the processed
  directory.

From the project's root directory:

  ```
    python .\talent\utils\converter.py -i .\data\pdmap\pdmap.xml -od .\data\pdmap\processed\ 
  ```
