import argparse
import sys
# from ged4py.algorithm.graph_edit_dist import GraphEditDistance as ged_local
import networkx as nx
import os.path
import pickle
import random
import hashlib
import requests
import logging

from sged import sged_graph as ed

import talent.common as common
import talent.graph_utils as gu
import talent.layout as lt
import talent.bioentity as be
import talent.scoring as scoring
from talent.reaction_graph import ReactionGraph
import talent.settings as settings
import talent.sbml as sbml



def distinguish_mapped_template_nodes(g, mapping):
    g_new = gu.deepcopy_graph(g)
    for [i1, i2] in mapping:
        if i1 and i2:
            gu.get_node_data(g_new.nodes[i2]).get_layout(be.LAYOUT_TYPE.PREDICTED).get_visual().set_size(850)

    return g_new


def get_colors(n):

    random.seed(42)

    ret = []
    r = int(random.random() * 256)
    g = int(random.random() * 256)
    b = int(random.random() * 256)
    step = 256 / max(n,1)
    for i in range(max(n,1)):
        r += step
        g += step
        b += step
        r = (int(r) % 256) / 256
        g = (int(g) % 256) / 256
        b = (int(b) % 256) / 256
        ret.append((r,g,b))

    return ret


def distinguish_compartment_target_nodes(g):
    """
    Colors nodes based on which compartment they belong to
    :param g:
    :return:
    """
    g_new = gu.deepcopy_graph(g)
    nodes = g_new.nodes()
    compartments = sorted(
        list(
            set(
                [gu.get_node_data(nodes[n]).get_compartment_id() for n in nodes if gu.get_node_data(nodes[n]).is_species()]
            )
        )
    )
    colors = get_colors(len(compartments))
    cc = {}
    for i in range(len(compartments)):
        cc[compartments[i]] = colors[i]
    for n in g:
        entity = gu.get_node_data(nodes[n])
        if entity.is_species():
            entity.get_layout(be.LAYOUT_TYPE.PREDICTED).set_visual(
                be.Visual(size=850, color=cc[entity.get_compartment_id()])
            )
    return g_new


def serialize_mapping(mapping):
    res = ""
    for [i1, i2] in mapping:
        res += "{}:{}->".format(i1, i2)
    return res


def merge_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z


def get_mapping_file_name(name, params):
    keys = sorted(params.keys())
    fn = '{}_{}.transfer'.format(name, '_'.join('{}-{}'.format(k, params[k]) for k in keys))
    # return fn
    return hashlib.sha512(str.encode(fn)).hexdigest()


def get_params(ged_params):
    return {**ged_params, **settings.arguments}


def save_mapping(ged_res, name, ged_params, cache_path):
    pickle.dump(ged_res, open(cache_path + '//' + get_mapping_file_name(name, get_params(ged_params)), "wb"))


def load_mapping(name, ged_params, cache_path):
    file_name = cache_path + '//' + get_mapping_file_name(name, get_params(ged_params))

    return pickle.load(open(file_name, "rb")) if os.path.isfile(file_name) else None


def get_tgt_tmp_name(tgt, tmp):
    return "{}--from--{}".format(tgt.get_name(), tmp.get_name())


def obtain_ed(tgt, tmp, costs, cache_path, ed_settings={}, load_map=True, store_map=True, min_mapped_ratio=0):

    if (len(tmp.get_reaction_graph())/len(tgt.get_reaction_graph()) < min_mapped_ratio):
        return []

    mapp_name = get_tgt_tmp_name(tgt, tmp)

    logging.info("Obtaining mapping for target {} (size {}) and template {} (size: {})".format(
        tgt.get_name(), len(tgt.get_reaction_graph()), tmp.get_name(), len(tmp.get_reaction_graph())
    ))

    map_params = merge_dicts(ed_settings, {'tgt_ddup': tgt.is_ddup(), 'tmp_ddup': tgt.is_ddup()})

    res = None
    if cache_path and load_map:
        res = load_mapping(mapp_name, map_params)

    if res is None:
        res = ed(tgt.get_reaction_graph(), tmp.get_reaction_graph(),
                 node_costs=costs['node'], edge_costs=costs['edge'], user_settings=ed_settings)
        logging.info("Computed mapping for {} -> distance: {} -> mapping: {}".format(mapp_name, res[0]['distance']
                if len(res) > 0 else 'None', res[0]['mapping'] if len(res) > 0 else 'None'))
    else:
        logging.info("Using stored mapping for {} -> distance: {} -> mapping: {}".format(mapp_name, res[0]['distance']
                if len( res) > 0 else 'None', res[0]['mapping'] if len(res) > 0 else 'None'))

    if cache_path and store_map:
        save_mapping(res, mapp_name, map_params, cache_path)

    cnt_mapped = 0
    if len(res) > 0:
        for m in res[0]['mapping']:
            if m[0] is not None and m[1] is not None:
                cnt_mapped += 1
    mapped_ratio = cnt_mapped / len(tgt.get_reaction_graph())
    logging.info("Mapped ratio: {}".format(mapped_ratio))

    return res if mapped_ratio >= min_mapped_ratio else []
    # return res


def approximate_distances(tgt, tmps, node_edge_costs, use_cache):

    logging.info("Approximating distances")

    approx_res = []

    for i in range(len(tmps)):
    # for tmp in tmps:

        ed_settings = settings.ed_for_approximation

        res = obtain_ed(tgt, tmps[i], node_edge_costs[i], use_cache, ed_settings)
        # res = ed(tgt_rg, tmp_rg, node_costs=node_edge_costs['node'], edge_costs=['edge'], user_settings=ed_settings)
        approx_res.append(res)

    return approx_res


def get_node_edge_costs(tgt, tmps):
    logging.info("Computing node and edge costs between target and all {} templates".format(len(tmps)))

    return [{'node': scoring.LayoutNodeCosts(tgt, tmp),
             'edge': scoring.LayoutEdgeCosts(tgt, tmp)} for tmp in tmps]



def export_to_sbml(tgt_pred_layout, tgt_sbml_source, file_name):

    sbml = gu.create_sbml()
    sbml.addGraph(tgt_pred_layout)

    # if tgt_sbml_source is None:
    #     sbml = gu.create_sbml()
    #     sbml.addGraph(tgt_pred_layout)
    # else:
    #     g, sbml = gu.load_sbml(tgt_sbml_source)

    sbml.removeExistingLayouts()
    sbml.addLayout(tgt_pred_layout)
    sbml.save(file_name)

    return sbml


def convert_sbml_to_cd(fn_source, fn_target):
    minerva_isntance = settings.minerva_instance

    with common.open_file(fn_source) as f_source:

        # token = requests.get("{}/api/doLogin".format(minerva_isntance)).cookies['MINERVA_AUTH_TOKEN']
        response = requests.post("{}/api/convert/SBML:CellDesigner_SBML".format(minerva_isntance),
                                 # cookies=dict(MINERVA_AUTH_TOKEN=token),
                                 data=f_source.read())

    with common.open_file(fn_target, "w") as f_target:
        f_target.write(response.text)


def convert_sbml_to_sbgn(fn_source, fn_target):
    minerva_isntance = settings.minerva_instance

    with common.open_file(fn_source) as f_source:

        # token = requests.get("{}/api/doLogin".format(minerva_isntance)).cookies['MINERVA_AUTH_TOKEN']
        response = requests.post("{}/api/convert/SBML:SBGN-ML".format(minerva_isntance),
                                 # cookies=dict(MINERVA_AUTH_TOKEN=token),
                                 data=f_source.read())

    with common.open_file(fn_target, "w") as f_target:
        f_target.write(response.text)


def layout_tgt(tgt_orig, tmps, use_cache, tgt_sbml_source=None, output_dir = ""):
    """
    Lays out target graph while using multiple templates. First, the most promissing templates
    are chosen using fast distance computation and then mapping between the target and each of the templates
    is obtained using more expensive mapping function. The results are then sorted and for each
    of the target-template pairs, the layout is obtained using the previously computed mapping.
    Finally, the layouts are exported.

    :param tgt:
    :param tmps:
    :param use_cache:
    :param cnt_approx_to_try:
    :param tgt_sbml_source:
    :return:
    """

    tgt = tgt_orig.deepcopy()

    assert(len(tmps) > 0)

    ed_settings = settings.ed

    # get costs (match, indel) for the edit distance for each tgt-tmp pair based on node types
    costs = get_node_edge_costs(tgt, tmps)

    if len(tmps) > 1:
        approx_res = approximate_distances(tgt, tmps, costs, use_cache)
        approx_res_ix_sort = sorted(range(len(approx_res)), key=lambda k: approx_res[k][0]['distance'] if len(approx_res[k]) > 0 else sys.float_info.max)
    else:
        logging.info("Skipping approximation")

    for i in range(min(settings.cnt_approx_to_try, len(tmps))):

        if len(tmps) > 1:
            i_closest = approx_res_ix_sort[i]
        else:
            i_closest = 0

        tmp = tmps[i_closest]
        logging.info("Picking {} as the template ({}. closest template)".format(tmp.get_name(), i))

        res = obtain_ed(tgt, tmp, costs[i_closest], use_cache, ed_settings)
        if len(res) == 0:
            res = approx_res[i_closest]
            logging.info("Used approximate mapping")
            if len(res) == 0:
                logging.info("No mapping available. Skipping...")
                continue
        logging.info("Used mapping: distance {} - {}".format(res[0]['distance'], res[0]['mapping']))

        logging.info("Transferring layout from {} to {}".format(tmp.get_name(), tgt.get_name()))
        tgt_pred_layout = lt.transfer_layout(tgt, tmp, res[0]["mapping"], do_beautify=True) #TODO result can contain multiple mappings with the same minimum distance


        ###########################
        ######## EXPORT ###########
        ###########################

        tmp_dist = distinguish_mapped_template_nodes(tmp.get_reaction_graph(), res[0]['mapping'])
        tgt_pred_layout_cmp = distinguish_compartment_target_nodes(tgt_pred_layout)
        graphs = [tgt_pred_layout, tgt_pred_layout_cmp, tgt.get_original(), tgt.get_reaction_graph(), tmp_dist]
        layout_keys = [be.LAYOUT_TYPE.PREDICTED, be.LAYOUT_TYPE.PREDICTED, be.LAYOUT_TYPE.ORIGINAL, be.LAYOUT_TYPE.ORIGINAL, be.LAYOUT_TYPE.ORIGINAL]
        txts = ['predicted target layout', 'predicted target layout with distiguished compartments', 'known target layout', 'known target reaction graph layout',
                'template: dist = {} | transfer = {}'.format(res[0]['distance'], serialize_mapping(res[0]['mapping']))]

        out_file_name = "{}/{}-tmp_order_{}-tgt-ddup_{}_tmp-ddup_{}".format(output_dir, get_tgt_tmp_name(tgt, tmp), i, tgt.is_ddup(), tmp.is_ddup())


        logging.info("Exporting layout to {}".format(out_file_name))

        gu.draw(graphs, layout_keys=layout_keys, txts=txts, outputFile="{}.pdf".format(out_file_name))

        sbml_file_name = "{}.xml".format(out_file_name)
        cd_file_name = sbml_file_name.replace(".xml", "-cd.xml")
        sbgn_file_name = sbml_file_name.replace(".xml", ".sbgn")

        sbml = export_to_sbml(tgt_pred_layout, tgt_sbml_source, sbml_file_name)
        sbml.save_as_image("{}.svg".format(out_file_name))
        convert_sbml_to_sbgn(sbml_file_name, sbgn_file_name)
        convert_sbml_to_cd(sbml_file_name, cd_file_name)

        # logging.info("Exporting DOT to {}".format(out_file_name))
        # gu.export_to_dot(tgt_pred_layout, 'test.dot'.format(out_file_name))

def get_rg_restricted_g(rg, g):

    # TODO: This is very slow for large networks, such as full pdmap, and needs to be optimied
    res = gu.deepcopy_graph(g)

    # remove reaction which do not appear in the rg
    for n in gu.get_all_reaction_ids(res):
        if n not in rg.nodes():
            res.remove_node(n)

    # some species might become disconnected, so let's remove them
    for n in gu.get_all_species_ids(res):
        if len(list(nx.all_neighbors(res, n))) == 0:
            res.remove_node(n)

    return res


def process_tgt(tgt_name, tgt_fname, tgt_fmt, split_by_cmprtmnt, ddup):

    logging.info("Processing target")

    g = gu.load_graph(tgt_fname, tgt_fmt)
    if ddup:
        g = gu.get_ddup_graph(g)

    tgt_ccs = []
    if split_by_cmprtmnt:
        for cmp_name in gu.get_list_of_compartments(g):
            gc = gu.extract_comparment(gu.removeOneComponents(g), cmp_name)
            gc_rg = gu.get_reaction_graph(gc)
            gc_rg_ddup = gu.get_ddup_reaction_graph(gc_rg, gc) if ddup else gc_rg
            i = 0
            for cc_rg in sorted(gu.get_connected_components(gc_rg_ddup), key=lambda x:(len(x.nodes()), gu.serialize_graph(x)), reverse=True):
                tgt_ccs.append(ReactionGraph(get_rg_restricted_g(cc_rg, gc), rg=cc_rg, rg_ddup=cc_rg, ddup=ddup, name='{}-cmp_{}-cc_{}'.format(tgt_name, cmp_name, i)))
                i += 1

    else:
        g_rg = gu.get_reaction_graph(g)
        g_rg_ddup = gu.get_ddup_reaction_graph(g_rg, g) if ddup else g_rg
        i = 0
        for cc_rg in sorted(gu.get_connected_components(g_rg_ddup), key=lambda x: (len(x.nodes()), gu.serialize_graph(x)), reverse=True):
            tgt_ccs.append(ReactionGraph(get_rg_restricted_g(cc_rg, g), rg=cc_rg, rg_ddup=cc_rg, ddup=ddup, name='{}-cc_{}'.format(tgt_name, i)))
            i += 1

    logging.info("Obtained {} target components".format(len(tgt_ccs)))
    return tgt_ccs


def process_db(tmps_path, fmt, split_by_cmprtmnt, ddup, cc_before_ddup, tmps_is_dir):
    """
    DB is a directory with json files of the compartments to be used as templates
    :param dir_tmps:
    :param fmt:
    :param split_by_cmprtmnt:
    :param ddup:
    :param cc_before_ddup: Whether to split network by compartments before doing deduplication
    if ddup is set to true. This can have impact on the resulting connected components if there
    is a specie which is duplicated across multiple connected components which would become disconnected
    after deduplication, but are not disconnected before.
    :return:
    """

    logging.info("Processing database of templates")

    def process(g, g_rg, ddup, cc_before_ddup, name):
        tmps = []
        if cc_before_ddup:
            i = 0
            for cc_rg in sorted(gu.get_connected_components(g_rg),
                                key=lambda x: (len(g.nodes()), gu.serialize_graph(x))):
                cc_rg_ddup = gu.get_ddup_reaction_graph(cc_rg, g) if ddup else g_rg
                tmps.append(ReactionGraph(get_rg_restricted_g(cc_rg, g), rg=cc_rg, rg_ddup=cc_rg_ddup, ddup=ddup,
                                          name='{}-cc{}'.format(name, i)))
                i += 1
        else:
            gc_rg_ddup = gu.get_ddup_reaction_graph(g_rg, g) if ddup else g_rg
            i = 0
            for cc_rg in sorted(gu.get_connected_components(gc_rg_ddup),
                                key=lambda x: (len(g.nodes()), gu.serialize_graph(x))):
                # rg_ddup=cc_rg because connected component of a deduplicated graph is again a deduplicated graph
                tmps.append(ReactionGraph(get_rg_restricted_g(cc_rg, g), rg=cc_rg, rg_ddup=cc_rg, ddup=ddup,
                                          name='{}-cc{}'.format(name, i)))
                i += 1

        return tmps

    tmps = []
    cnt_cmprtments = 0

    tmp_files = os.listdir(tmps_path) if tmps_is_dir else [tmps_path]

    for fn in tmp_files:
        # if fn.endswith(fmt):
        tmp_path = "{}/{}".format(tmps_path, fn) if tmps_is_dir else fn
        logging.info("Reading in {}".format(tmp_path))
        g = gu.load_graph(tmp_path, fmt)

        if split_by_cmprtmnt:
            for cmprtmentName in gu.get_list_of_compartments(g):
                cnt_cmprtments += 1
                gc = gu.extract_comparment(gu.removeOneComponents(g), cmprtmentName)
                gc_rg = gu.get_reaction_graph(gc)
                tmps += process(gc, gc_rg, ddup, cc_before_ddup, cmprtmentName)
        else:
            cnt_cmprtments += 1
            g_rg = gu.get_reaction_graph(gu.removeOneComponents(g))
            name = common.extract_target_from_fn(fn)
            tmps += process(g, g_rg, ddup, cc_before_ddup, name)

    logging.info("DB contains {} compartment()s and {} connected component(s)".format(cnt_cmprtments, len(tmps)))
    return tmps

def layout_network_using_db(tgt_name, tgt_fname, tgt_fmt, split_by_cmprtmnt, tmps_path, tmp_fmt,
                            tmps_is_dir=True,
                            output_dir="out/pred",
                            ddup_tgt=False,
                            ddup_tmp=False, use_cache=True, tgt_sbml_source=None, tmp_cc_before_ddup=True):
    """

    :param tgt_name:
    :param tgt_fname:
    :param tgt_fmt:
    :param split_by_cmprtmnt:
    :param dir_tmps:
    :param tmp_fmt:
    :param ddup_tgt:
    :param ddup_tmp:
    :param use_cache:
    :param tgt_sbml_source: The target does not need to be provided as SBML, but can be preprocessed and
    passed as json file. However, if export to SBML is needed, the original SBML file with the network structure
    is taken and layout added on top of that.
    :param tmp_cc_before_ddup:
    :return:
    """

    settings.arguments = {
        'ddup_tgt': ddup_tgt,
        'ddup_tmp': ddup_tmp
    }

    tgt_ccs = process_tgt(tgt_name, tgt_fname, tgt_fmt, split_by_cmprtmnt, ddup_tgt)
    tmps = process_db(tmps_path, tmp_fmt, split_by_cmprtmnt, ddup_tmp, tmp_cc_before_ddup, tmps_is_dir = tmps_is_dir)

    i = 0
    for tgt in tgt_ccs:
        logging.info("Mapping connected component {} (size {})".format(i, len(tgt.get_reaction_graph())))
        layout_tgt(tgt, tmps, use_cache, tgt_sbml_source=tgt_sbml_source, output_dir=output_dir)
        i += 1



def extract_sorted_ccs(g):
    g_ccs = [g_cc for g_cc in gu.get_connected_components(g)]
    return sorted(g_ccs, key=lambda g: len(g.nodes()), reverse=True)




def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == '__main__':

    common.init_logging()

    tgt_dir = 'data/assorted/'
    tgt_name = 'target-sbml'
    # tmp_dir = 'data/reactome/pathways/sbml/'
    tmp_path = 'data/assorted/pd_autophagy.xml'
    layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(tgt_dir, tgt_name),
                            tgt_fmt="sbml", split_by_cmprtmnt=False,
                            tmps_path=tmp_path, tmp_fmt="sbml", tmps_is_dir=False,
                            output_dir="out/",
                            ddup_tgt=True, ddup_tmp=True, use_cache=False, tmp_cc_before_ddup=False
                            )
    exit()

    parser = argparse.ArgumentParser()

    parser.add_argument("-tgt", "--target",
                        required=True,
                        help="Target file")
    parser.add_argument("-tgt-fmt", "--target-format",
                        required=False,
                        default="pkl",
                        help="Target file format (pkl or sbml)")
    parser.add_argument("-tgt-sbml-source", "--target-sbml-source-file",
                        required=False,
                        help="SBML file with the target network without layout to which layout section with the predicted layout will be added.")
    parser.add_argument("-tmp-db", "--template-database-dir",
                        required=True,
                        help="Directory with templates in pkl format")
    parser.add_argument("-tmp-fmt", "--template-format",
                        required=False,
                        default="pkl",
                        help="Templates file format (pkl or sbml)")
    parser.add_argument("-ddup-tgt",
                        required=False,
                        type=str2bool,
                        default=False)
    parser.add_argument("-ddup-tmp",
                        required=False,
                        type=str2bool,
                        default=True)
    parser.add_argument("-split-cmp", "--split-by-compartments",
                        required=False,
                        type=str2bool,
                        default=False)
    parser.add_argument("-tmp-split-by-cc-before-ddup", "--tmp-split-by-cc-before-ddup",
                        required=False,
                        type=str2bool,
                        default=False)
    parser.add_argument("-cache", "--use-cache",
                        required=False,
                        action='store_true',
                        default=True)
    parser.add_argument("-s", "--settings",
                        required=False,
                        help="File with edit distance settings")

    args = parser.parse_args()
    name = common.extract_target_from_fn(args.target)

    if args["settings"]:
        settings.parse_settings_file(args["settings"])


    layout_network_using_db(
        tgt_name=name, tgt_fname=args.target, tgt_fmt=args.target_format, ddup_tgt=args.ddup_tgt,
        tmps_path=args.template_database_dir, tmps_is_dir=True, tmp_fmt=args.template_format, ddup_tmp=args.ddup_tmp,
        split_by_cmprtmnt=args.split_by_compartments, tmp_cc_before_ddup=args.tmp_split_by_cc_before_ddup,
        use_cache=args.use_cache)
    exit(0)

    # layout_network_using_db("dup", "tests/data/duplication1-tgt.sbml.xml", "sbml", False, "tests/data/db","sbml", ddup_tgt=True, ddup_tmp=True, use_cache=False,
    #                         # tgt_sbml_source='tests/data/duplication1-tgt.sbml.xml',
    #                         tmp_cc_before_ddup=False)

    # layout_network_using_db("Ubiquitin Proteasome System-remove20species", "out/perturbed/Ubiquitin Proteasome System_remove_species_abs_20.json", "json", False,
    #                         "out/pdmap-Ubiquitin Proteasome System.json", "json", tmps_is_dir=False,
    #                         ddup_tgt=True, ddup_tmp=True, use_cache=True, tmp_cc_before_ddup=False)



    # layout_network_using_db("Ubiquitin Proteasome System",
    #                         "out/pdmap-Ubiquitin Proteasome System.json", "json", False,
    #                         "out/pdmap-Ubiquitin Proteasome System.json", "json", tmps_is_dir=False,
    #                         ddup_tgt=True, ddup_tmp=True, use_cache=True, tmp_cc_before_ddup=False)

    # layout_network_using_db("Dopamine metabolism", "out/perturbed/Dopamine metabolism_remove_species_abs_20.json",
    #                         "json", False,
    #                         "out/pdmap-Dopamine metabolism.json", "json", tmps_is_dir=False,
    #                         ddup_tgt=True, ddup_tmp=True, use_cache=True, tmp_cc_before_ddup=False
    #                         )

    # layout_network_using_db("Dopamine metabolism", "out/pdmap-Dopamine metabolism.json",
    #                         "json", False,
    #                         "out/pdmap-Dopamine metabolism.json", "json", tmps_is_dir=False,
    #                         ddup_tgt=True, ddup_tmp=False, use_cache=True, tmp_cc_before_ddup=False
    #                         )

    # layout_network_using_db("alzpath-Cholesterol intake", "out/alzpath-Cholesterol intake.json",
    #                         "json", False,
    #                         "out/pdmap-PI3K_AKT signaling.json", "json", tmps_is_dir=False,
    #                         ddup_tgt=True, ddup_tmp=False, use_cache=True, tmp_cc_before_ddup=False
    #                         )


    # layout_network_using_db("muscle_map_seed", "out/muscle_map_seed.json",
    #                         "json", False,
    #                         "out/pdmap.json", "json", tmps_is_dir=False,
    #                         ddup_tgt=True, ddup_tmp=False, use_cache=True, tmp_cc_before_ddup=False
    #                         )

    # layout_network_using_db("alzpath-neuron", "out/alzpath-neuron.pkl",
    #                         "pkl", False,
    #                         "out/pdmap-Dopamine Metabolism.pkl", "pkl", tmps_is_dir=False,
    #                         ddup_tgt=True, ddup_tmp=True, use_cache=True, tmp_cc_before_ddup=False
    #                         ); exit()

    # layout_network_using_db("alzpath", "out/alzpath.pkl",
    #                         "pkl", False,
    #                         "out/pdmap.pkl", "pkl", tmps_is_dir=False,
    #                         ddup_tgt=True, ddup_tmp=True, use_cache=True, tmp_cc_before_ddup=False
    #                         ); exit()

    # dir = 'tests/bloating/'
    # tgt_name = 'model'
    # tmp_name = 'model'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path="{}{}.xml".format(dir, tmp_name), tmp_fmt="sbml", tmps_is_dir=False,
    #                         ddup_tgt=True, ddup_tmp=True, use_cache=False, tmp_cc_before_ddup=False
    #                         # , tgt_sbml_source = "tests/data/sbml_layout/{}.xml".format(name)
    #                         )

    # dir = 'tests/dup/'
    # tgt_name = 'linear_4A_B'
    # tmp_name = 'linear_3A_B'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path="{}{}.xml".format(dir, tmp_name), tmp_fmt="sbml", tmps_is_dir=False,
    #                         ddup_tgt=True, ddup_tmp=True, use_cache=False, tmp_cc_before_ddup=False
    #                         # , tgt_sbml_source = "tests/data/sbml_layout/{}.xml".format(name)
    #                         )

    # tgt_dir = 'data/biomodels/l3/'
    # tgt_name = 'BIOMD0000000232_url'
    # tmp_dir = 'data/pd_manual/'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(tgt_dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path=tmp_dir, tmp_fmt="sbml", tmps_is_dir=True,
    #                         ddup_tgt=False, ddup_tmp=False, use_cache=True, tmp_cc_before_ddup=False
    #                         # , tgt_sbml_source = "tests/data/sbml_layout/{}.xml".format(name)
    #                         )

    # tgt_dir = 'data/biomodels/all/l3/'
    # tgt_name = 'BIOMD0000000212_url_sbo'
    # tmp_dir = 'data/pd_manual/glycolisis.xml'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(tgt_dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path=tmp_dir, tmp_fmt="sbml", tmps_is_dir=False,
    #                         ddup_tgt=False, ddup_tmp=True, use_cache=True, tmp_cc_before_ddup=False
    #                         # , tgt_sbml_source = "tests/data/sbml_layout/{}.xml".format(name)
    #                         )

    # tgt_dir = 'data/biomodels/all/l3/'
    # tgt_name = 'BIOMD0000000025_url_sbo'
    # tmp_dir = 'test/biomodel-reactome/mapping/out/BIOMD0000000025/sbml/'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(tgt_dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path=tmp_dir, tmp_fmt="sbml", tmps_is_dir=True,
    #                         ddup_tgt=True, ddup_tmp=True, use_cache=True, tmp_cc_before_ddup=False
    #                         # , tgt_sbml_source = "tests/data/sbml_layout/{}.xml".format(name)
    #                         )

    # tgt_dir = 'data/biomodels/all/l3/'
    # tgt_name = 'BIOMD0000000001_url_sbo'
    # # tmp_dir = 'data/reactome/pathways/sbml/'
    # tmp_dir = 'data/reactome/pathways/sbml/R-MMU-8982491-Glycogen_metabolism-Mus_musculus_fixed.xml'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(tgt_dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path=tmp_dir, tmp_fmt="sbml", tmps_is_dir=False,
    #                         ddup_tgt=False, ddup_tmp=False, use_cache=True, tmp_cc_before_ddup=False
    #                         )
    #
    # tgt_dir = 'tests/sbml_layout/'
    # # tgt_name = 'r_ove_-r'
    # # tmp_dir = 'tests/sbml_layout/r_ove_-r.xml'
    # tgt_name = 'r_ove_-r-vert'
    # tmp_dir = 'tests/sbml_layout/r_ove_-r-vert.xml'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(tgt_dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path=tmp_dir, tmp_fmt="sbml", tmps_is_dir=False,
    #                         ddup_tgt=False, ddup_tmp=False, use_cache=True, tmp_cc_before_ddup=False
    #                         )

    # tgt_dir = 'data/biomodels/all/l3/'
    # tgt_name = 'BIOMD0000000001_url_sbo'
    # # tmp_dir = 'data/reactome/pathways/sbml/'
    # tmp_dir = 'data/reactome/pathways/sbml/R-MMU-140877-Formation_of_Fibrin_Clot-Mus_musculus_fixed.xml'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(tgt_dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path=tmp_dir, tmp_fmt="sbml", tmps_is_dir=False,
    #                         ddup_tgt=False, ddup_tmp=False, use_cache=True, tmp_cc_before_ddup=False
    #                         )
    # # #
    # tgt_dir = 'data/biomodels/all/l3/'
    # tgt_name = 'BIOMD0000000002_url_sbo'
    # # tmp_dir = 'data/reactome/pathways/sbml/'
    # tmp_dir = 'data/reactome/pathways/sbml/R-MMU-8956320-Nucleobase_biosynthesis-Mus_musculus_fixed.xml'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(tgt_dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path=tmp_dir, tmp_fmt="sbml", tmps_is_dir=False,
    #                         ddup_tgt=False, ddup_tmp=False, use_cache=True, tmp_cc_before_ddup=False
    #                         )
    #
    # tgt_dir = 'data/biomodels/all/l3/'
    # tgt_name = 'BIOMD0000000007_url_sbo'
    # # tmp_dir = 'data/reactome/pathways/sbml/'
    # tmp_dir = 'data/reactome/pathways/sbml/R-MMU-70326-Glucose_metabolism-Mus_musculus_fixed.xml'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(tgt_dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path=tmp_dir, tmp_fmt="sbml", tmps_is_dir=False,
    #                         ddup_tgt=False, ddup_tmp=False, use_cache=True, tmp_cc_before_ddup=False
    #                         )
    #
    # #
    # tgt_dir = 'data/biomodels/all/l3/'
    # tgt_name = 'BIOMD0000000032_url_sbo'
    # # tmp_dir = 'data/reactome/pathways/sbml/'
    # tmp_dir = 'data/reactome/pathways/sbml/R-MMU-140877-Formation_of_Fibrin_Clot-Mus_musculus_fixed.xml'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(tgt_dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path=tmp_dir, tmp_fmt="sbml", tmps_is_dir=False,
    #                         ddup_tgt=False, ddup_tmp=False, use_cache=True, tmp_cc_before_ddup=False
    #                         )
    # #
    # tgt_dir = 'data/biomodels/all/l3/'
    # tgt_name = 'BIOMD0000000042_url_sbo'
    # # tmp_dir = 'data/reactome/pathways/sbml/'
    # tmp_dir = 'data/reactome/pathways/sbml/R-MMU-140877-Formation_of_Fibrin_Clot-Mus_musculus_fixed.xml'
    # layout_network_using_db(tgt_name="{}".format(tgt_name), tgt_fname="{}{}.xml".format(tgt_dir, tgt_name),
    #                         tgt_fmt="sbml", split_by_cmprtmnt=False,
    #                         tmps_path=tmp_dir, tmp_fmt="sbml", tmps_is_dir=False,
    #                         ddup_tgt=False, ddup_tmp=False, use_cache=True, tmp_cc_before_ddup=False
    #                         )

    # layout_network_using_db("dup", "tests/data/dup_lin_seq-v3.xml", "sbml", False, "tests/data/db/", "sbml", ddup_tgt=True, ddup_tmp=True, use_cache=False);
    # layout_network_using_db("dup", "tests/data/dup_2reactions_3X_2Z-tgt-v3.xml", "sbml", False, "tests/data/db/", "sbml",ddup_tgt=True, ddup_tmp=True, use_cache=False);
    # layout_network_using_db("dup", "tests/data/duplication-tgt-v3.xml", "sbml", False, "tests/data/db/", "sbml", ddup_tgt=True, ddup_tmp=True, use_cache=False);
    # layout_network_using_db("dup", "test/muscle_map_seed-cell.json", "json", False, "tmp/db/", "json", ddup_tgt=True, ddup_tmp=True, use_cache=True);
    # exit(0)
    #
    # layout_network_using_db('Alpha-synuclein_spreading', 'tmp/pdmap-Alpha-synuclein_spreading.json', 'json', False, 'tmp/', 'json', False);
    # layout_network_using_db('Calcium_signaling', 'tmp/pdmap-Calcium_signaling.json', 'json', False, 'tmp/db', 'json', ddup_tgt=True, ddup_tmp=False)
    # layout_network_using_db('Mitochondrial_unfolded_protein_response', 'tmp/pdmap-Mitochondrial_unfolded_protein_response.json', 'json', False, 'tmp/', 'json', True);
    # layout_network_using_db('Axonal_remodeling_remove_species_abs_5', 'out/perturbed/Axonal_remodeling_remove_species_abs_5.json', 'json', False, 'tmp/', 'json', False);
    # layout_network_using_db('pdmap-Dopamine Metabolism', 'tmp/pdmap-Dopamine Metabolism.json', 'json', False, 'tmp/db/', 'json', ddup_tgt=True, ddup_tmp=True);
    # layout_network_using_db('pdmap-Ubiquitin Proteasome System', 'tmp/pdmap-Ubiquitin Proteasome System.json', 'json', False, 'tmp/db/', 'json', ddup_tgt=True, ddup_tmp=True,
    #                         use_cache=True, tgt_sbml_source='data/pdmap.xml')

    # layout_network_using_db('mms-cell', 'out/muscle_map_seed-cell.json', 'json', False, 'tmp/db/', 'json', ddup_tgt=True, ddup_tmp=True);
    # layout_network_using_db('mms', 'out/muscle_map_seed-cell.json', 'json', False, 'tmp/db/', 'json', ddup_tgt=True, ddup_tmp=True, use_cache=False )

    # layout_network_using_db('ER', 'out/alzpath-Cytoskeleton homeostasis.json', 'json', False, 'tmp/db_pdbmap_representative/', 'json', ddup_tgt=True, ddup_tmp=True);















# def main(args):
#
#     g_tgt = gu.load_graph(args.target, args.target_format)
#     g_tmp = gu.load_graph(args.template, args.template_format)
#
#     print('rg1')
#     g_tmp_rg = gu.get_reaction_graph(g_tmp)
#     # gu.save_graph(g_tmp_rg, "tmp.el", "edgelist")
#     print('rg2')
#     g_tmp_rg_ccs = gu.get_connected_components(g_tmp_rg)
#     print('rg3')
#
#     # for cmprt_name in ['phagophore', 'dendritic shaft', 'Inflammation Signalling / Apoptosis', 'Neuroinflammation']:
#     # for cmprt_name in ['phagophore']:
#     # for cmprt_name in ['early endosome']:
#     for cmprt_name in ['Inflammation Signalling / Apoptosis']:
#     # for cmprt_name in ['Fatty Acid / Ketone Body Metabolism']:
#     # for cmprt_name in ['Glycolysis']:
#     # for cmprt_name in ['autolysosome']:
#     # for cmprt_name in ['Axonal remodeling']:
#     # for cmprt_name in gu.get_list_of_compartments(g_tgt):
#
#         print('--------------------------------------')
#         print('Laying out {}'.format(cmprt_name))
#         print('---------------------------------------')
#
#         g_tgt_c = gu.extract_comparment(g_tgt, cmprt_name)
#
#         # remove_reaction(g_tgt_c, 're1127')
#         # remove_reaction(g_tgt_c, 're1147')
#         # remove_reaction(g_tgt_c, 're1150')
#         # remove_reaction(g_tgt_c, 're2994')
#
#         g_tgt_c_rg = gu.get_reaction_graph(g_tgt_c)
#         ix_cc = 0
#         for cc_rg in gu.get_connected_components(g_tgt_c_rg):
#             if (len(cc_rg)>5):
#                 # gu.save_graph(cc_rg, "tgt.el", "edgelist")
#                 # layout(cc_rg, g_tgt_c, g_tmp_rg_ccs, g_tmp, "{}_{}".format(cmprt_name.replace('/', '_'), ix_cc))
#                 layout(cc_rg, g_tgt_c, g_tmp_rg_ccs, g_tmp, "{}_{}".format(cmprt_name.replace('/', '_'), ix_cc), ddup=False)
#                 ix_cc += 1
# def test_perturbation(tmp_name, tgt_name):
#     g_tmp = gu.load_graph(tmp_name, 'json')
#     g_tgt = gu.load_graph(tgt_name, 'json')
#
#     g_tgt_ccs_0 = extract_sorted_ccs(g_tgt)[0]
#
#     g_tmp_rg = gu.get_reaction_graph(g_tmp)
#     g_tgt_rg = gu.get_reaction_graph(g_tgt_ccs_0)
#
# #     layout(g_tgt_rg, g_tgt_ccs_0, [g_tmp_rg], g_tmp, tgt_name.replace('.json', '_pred.json'), ddup=False, load_map=False)
#
#
# def test_perturbations():
#     # cmp = 'Fatty_Acid___Ketone_Body_Metabolism'
#     cmp = 'Axonal_remodeling'
#     prt_dir = 'out/perturbed'
#
#     test_perturbation('{}/{}.json'.format(prt_dir,cmp), '{}/{}_remove_species_abs_5.json'.format(prt_dir, cmp))
#     # test_perturbation('{}/{}.json'.format(prt_dir, cmp), '{}/{}_remove_reactions_abs_2.json'.format(prt_dir, cmp))